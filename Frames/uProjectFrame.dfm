object ProjectFrame: TProjectFrame
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  HelpType = htKeyword
  Align = alClient
  TabOrder = 0
  OnClick = FrameClick
  object RzPanel2: TRzPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 304
    Align = alClient
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 0
    object RzToolbar5: TRzToolbar
      Left = 1
      Top = 0
      Width = 449
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer27
        RzBut1
        RzSpacer28
        RzBut2
        RzSpacer29
        RzBut3
        RzSpacer1
        RzToolButton1)
      object RzSpacer27: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzBut1: TRzToolButton
        Left = 12
        Top = 2
        Width = 70
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21333#20307#24037#31243
        OnClick = RzBut1Click
      end
      object RzSpacer28: TRzSpacer
        Left = 82
        Top = 2
      end
      object RzBut2: TRzToolButton
        Left = 90
        Top = 2
        Width = 50
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzBut1Click
      end
      object RzSpacer29: TRzSpacer
        Left = 140
        Top = 2
      end
      object RzBut3: TRzToolButton
        Left = 148
        Top = 2
        Width = 50
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzBut1Click
      end
      object RzSpacer1: TRzSpacer
        Left = 198
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 206
        Top = 2
        Width = 50
        ImageIndex = 11
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21047#26032
        OnClick = RzToolButton1Click
      end
    end
    object tv: TTreeView
      Left = 1
      Top = 29
      Width = 449
      Height = 196
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Images = DM.TreeImageList
      Indent = 19
      ParentFont = False
      RowSelect = True
      TabOrder = 1
      OnChange = tvChange
      Items.NodeData = {
        03010000002A0000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        00010000000106E55D0B7A7998EE76A17B06742E0000000000000000000000FF
        FFFFFFFFFFFFFF0000000000000000000000000108B970FB510B4EB9657998EE
        76E55D0B7A}
    end
    object ProjectGrid: TcxGrid
      Left = 1
      Top = 249
      Width = 449
      Height = 55
      Align = alClient
      TabOrder = 2
      object tvProjectView: TcxGridDBTableView
        OnDblClick = tvProjectViewDblClick
        Navigator.Buttons.CustomButtons = <>
        OnEditing = tvProjectViewEditing
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.DataRowHeight = 26
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 15
        object tvProjectViewColumn1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = tvProjectViewColumn1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Width = 40
        end
        object tvProjectViewColumn2: TcxGridDBColumn
          Caption = #24037#31243#21517#31216
          DataBinding.FieldName = 'SignName'
          PropertiesClassName = 'TcxTextEditProperties'
          HeaderAlignmentHorz = taCenter
          Width = 194
        end
      end
      object ProjectLv: TcxGridLevel
        GridView = tvProjectView
      end
    end
    object RzPanel3: TRzPanel
      Left = 1
      Top = 225
      Width = 449
      Height = 24
      Align = alTop
      BorderOuter = fsNone
      Caption = #39033#30446#24037#31243
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 79
    Top = 104
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 79
    Top = 160
  end
end
