inherited frmCompanyFormStorage: TfrmCompanyFormStorage
  Caption = #20844#21496#20179#24211' ['#20986#24211']'
  ClientHeight = 606
  ClientWidth = 1074
  ExplicitWidth = 1090
  ExplicitHeight = 644
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 300
    Height = 606
    ExplicitHeight = 606
  end
  inherited RzPanel1: TRzPanel
    Left = 310
    Width = 764
    Height = 606
    ExplicitLeft = 310
    ExplicitWidth = 764
    ExplicitHeight = 606
    inherited RzToolbar13: TRzToolbar
      Width = 762
      Height = 54
      ExplicitWidth = 771
      ExplicitHeight = 54
      ToolbarControls = (
        RzSpacer30
        RzToolButton23
        RzSpacer57
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer113
        RzToolButton86
        RzSpacer116
        RzToolButton87
        RzSpacer114
        cxLabel55
        RzSpacer115
        cxDateEdit1
        RzSpacer117
        cxLabel56
        cxDateEdit2
        RzSpacer119
        RzToolButton89
        RzSpacer118
        RzToolButton88)
      inherited RzToolButton88: TRzToolButton
        Left = 4
        Top = 27
        ExplicitLeft = 4
        ExplicitTop = 27
      end
    end
    inherited Grid: TcxGrid
      Top = 54
      Width = 762
      ExplicitWidth = 805
      ExplicitHeight = 577
      inherited tvView: TcxGridDBTableView
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #21512#35745
            Position = spFooter
            Column = tvViewColumn1
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn6
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn6
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Column = tvViewColumn9
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn9
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn14
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end
          item
            Format = #21512#35745#65306
            Kind = skCount
            Column = tvViewColumn1
            Sorted = True
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn6
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Column = tvViewColumn9
          end
          item
            Format = #165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end
          item
            Format = #22823#20889':'
            Kind = skCount
          end>
        Styles.OnGetContentStyle = nil
        inherited tvViewColumn4: TcxGridDBColumn
          Properties.OnCloseUp = nil
        end
      end
    end
  end
  inherited RzPanel2: TRzPanel
    Width = 300
    Height = 606
    ExplicitWidth = 300
    ExplicitHeight = 606
  end
  inherited ADOMaking: TADOQuery
    AfterOpen = nil
  end
end
