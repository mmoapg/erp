object frmCostSubject: TfrmCostSubject
  Left = 0
  Top = 0
  Caption = #36153#29992#31185#30446
  ClientHeight = 469
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 0
    Width = 300
    Height = 29
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2
      RzSpacer3
      RzToolButton3
      RzSpacer4
      RzToolButton5)
    object RzSpacer1: TRzSpacer
      Left = 4
      Top = 2
      Width = 21
    end
    object RzToolButton1: TRzToolButton
      Left = 25
      Top = 2
      ImageIndex = 37
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 50
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 58
      Top = 2
      ImageIndex = 3
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 83
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 91
      Top = 2
      ImageIndex = 2
      OnClick = RzToolButton3Click
    end
    object RzSpacer4: TRzSpacer
      Left = 116
      Top = 2
    end
    object RzToolButton5: TRzToolButton
      Left = 124
      Top = 2
      ImageIndex = 8
      OnClick = RzToolButton5Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 450
    Width = 300
    Height = 19
    Panels = <>
  end
  object Grid: TcxGrid
    Left = 0
    Top = 29
    Width = 300
    Height = 421
    Align = alClient
    TabOrder = 2
    object tvTable: TcxGridDBTableView
      OnKeyDown = tvTableKeyDown
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvTableEditing
      OnEditKeyDown = tvTableEditKeyDown
      DataController.DataSource = DataCost
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 23
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvTableColumn1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvTableColumn1GetDisplayText
        HeaderAlignmentHorz = taCenter
        Width = 48
      end
      object tvTableColumn2: TcxGridDBColumn
        Caption = #31867#22411
        DataBinding.FieldName = 'CostName'
        HeaderAlignmentHorz = taCenter
        Width = 209
      end
    end
    object Lv: TcxGridLevel
      GridView = tvTable
    end
  end
  object ADOCost: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = ADOCostAfterOpen
    Parameters = <>
    Left = 144
    Top = 240
  end
  object DataCost: TDataSource
    DataSet = ADOCost
    Left = 144
    Top = 296
  end
end
