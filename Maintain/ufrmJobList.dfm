object frmJobList: TfrmJobList
  Left = 0
  Top = 0
  Caption = #32844#21153#20449#24687
  ClientHeight = 483
  ClientWidth = 293
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 29
    Width = 293
    Height = 435
    Align = alClient
    TabOrder = 0
    object tvJob: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvJobEditing
      OnEditKeyDown = tvJobEditKeyDown
      DataController.DataSource = DataJob
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsView.DataRowHeight = 23
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvJobColumn1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvJobColumn1GetDisplayText
        HeaderAlignmentHorz = taCenter
        Width = 48
      end
      object tvJobColumn2: TcxGridDBColumn
        Caption = #31867#22411
        DataBinding.FieldName = 'JobName'
        HeaderAlignmentHorz = taCenter
        Width = 209
      end
    end
    object Lv: TcxGridLevel
      GridView = tvJob
    end
  end
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 0
    Width = 293
    Height = 29
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2
      RzSpacer3
      RzToolButton3
      RzSpacer4
      RzToolButton5)
    object RzSpacer1: TRzSpacer
      Left = 4
      Top = 2
      Width = 21
    end
    object RzToolButton1: TRzToolButton
      Left = 25
      Top = 2
      ImageIndex = 37
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 50
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 58
      Top = 2
      ImageIndex = 3
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 83
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 91
      Top = 2
      ImageIndex = 2
      OnClick = RzToolButton3Click
    end
    object RzSpacer4: TRzSpacer
      Left = 116
      Top = 2
    end
    object RzToolButton5: TRzToolButton
      Left = 124
      Top = 2
      ImageIndex = 8
      OnClick = RzToolButton5Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 464
    Width = 293
    Height = 19
    Panels = <>
  end
  object ADOJob: TADOQuery
    Connection = DM.ADOconn
    CursorType = ctStatic
    AfterOpen = ADOJobAfterOpen
    Parameters = <>
    Left = 144
    Top = 248
  end
  object DataJob: TDataSource
    DataSet = ADOJob
    Left = 144
    Top = 304
  end
end
