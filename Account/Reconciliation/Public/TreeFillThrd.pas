unit TreeFillThrd;

interface

uses
  Classes, ComCtrls, ADODB, SysUtils,SyncObjs,Windows;

type
  TNodeData = record
    Index     : integer;
    Caption   : string;
    Code      : string;
    sum_Money : Currency;
    photo     : string;
    remarks   : string;
    Price     : Currency;
    Num       : Integer;
    parent    : Integer;
    PatType   : Integer;
    pid       : Integer;
    Input_time: TDateTime;
    End_time  : TDateTime;
  end;

  PNodeData = ^TNodeData;

  TTreeFill = class(TThread)
    TV: TTreeView;
    TableName   : string;
    FieldIndex  : string;
    FieldPID    : string;
    FieldCaption: string;
    FieldCode   : string;
    FieldRemarks: string;
    Fieldmoney  : string;
    Fieldphoto  : string;
    FieldPrice  : string;
    FieldNum     : string;
    FieldInput_time: string;
    FieldEnd_time  : string;
    FieldParent  : Integer;
    FieldPatType : Integer;
    RootText: string;
    Conn: TADOConnection;
    Ptr: PNodeData;
    constructor Create(ATreeView: TTreeView; AConn: TADOConnection;ATable: string;
                       ARootText     : string = '节点管理';
                       AFieldIndex   : string = 'parent';
                       AFieldPID     : string = 'PID';
                       AFieldCaption : string = 'Caption';
                       AFieldCode    : string = 'Code';
                       AFieldmoney   : string = 'sum_money';
                       AFieldphoto   : string = 'photo';
                       AFieldRemarks : string = 'Remarks';
                       AFieldPrice   : string = 'Price';
                       AfieldNum     : string = 'Num';
                       AfieldInput_time: string = 'Input_time';
                       AfieldEnd_time: string = 'End_time';
                       AfieldParent  : Integer= 0;
                       AfieldPatType : Integer= 0
                       );
  private
    procedure AddTree(Node: TTreeNode; PN: PNodeData;IsPatType : Boolean = False);
    function AddParentTree(Node : TTreeNode; PN: PNodeData):TTreeNode;stdcall;
  protected
    procedure Execute; override;
  public
 //   Critical1 : TCriticalSection;
  end;

implementation

//------------------------------------------------------------------------------
constructor TTreeFill.Create(ATreeView: TTreeView; AConn: TADOConnection;
                             ATable        : string;
                             ARootText     : string = '节点管理';
                             AFieldIndex   : string = 'parent';
                             AFieldPID     : string = 'PID';
                             AFieldCaption : string = 'Caption';
                             AFieldCode    : string = 'Code';
                             AFieldmoney   : string = 'sum_money';
                             AFieldphoto   : string = 'photo';
                             AFieldRemarks : string = 'Remarks';
                             AFieldPrice   : string = 'Price';
                             AfieldNum     : string = 'Num';
                             AfieldInput_time: string = 'Input_time';
                             AfieldEnd_time: string = 'End_time';
                             AfieldParent  : Integer= 0 ;
                             AfieldPatType : Integer= 0
                             );
begin

  FieldIndex := AFieldIndex;
  FieldPID := AFieldPID;
  FieldCaption := AFieldCaption;
  FieldCode    := AFieldCode;
  FieldRemarks := AFieldRemarks;
  Fieldmoney   := AFieldmoney;
  Fieldphoto   := AFieldphoto;
  FieldPrice   := AFieldPrice;
  FieldNum     := AfieldNum;
  FieldInput_time:=AfieldInput_time;
  FieldEnd_time:= AfieldEnd_time;
  FieldParent  := AfieldParent;
  FieldPatType := AfieldPatType;
  TV := ATreeView;
  TableName := ATable;
  RootText  := ARootText;
  Conn      := AConn;
  New(Ptr);
  //DeleteCriticalSection(Critical1);
  inherited Create(False);

end;
//------------------------------------------------------------------------------
procedure TTreeFill.Execute;
var
  Node: TTreeNode;

begin
  FreeOnTerminate := True;
  TV.Items.Clear;
  Ptr^.Index    := FieldParent;
  Ptr^.Caption  := RootText;
  Node := TV.Items.AddObject(nil, RootText, Ptr);
  Node.ImageIndex := 0;
  Node.SelectedIndex := 0;
  if FieldParent = 0 then
  begin
    AddTree(Node,Ptr,False);
  end
  else
  begin
    AddParentTree(Node,Ptr);
  end;
  TV.Items[0].Expanded := True;
//  TV.FullExpand;  全部展开
end;
//------------------------------------------------------------------------------
function TTreeFill.AddParentTree(Node : TTreeNode; PN: PNodeData):TTreeNode;stdcall;
var
  Query: TADOQuery;
  nNode: TTreeNode;
  PNode: PNodeData;
begin

  Query := TADOQuery.Create(nil);
  try
      Query.Connection := Conn;
      Query.SQL.Text := 'Select * from ' + TableName + ' where ' + 'parent' + ' =' + IntToStr(PN^.Index);

      if Query.Active then
         Query.Close;

      Query.Open;
      if not Query.Eof then
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName(FieldCaption).AsString;
        PNode^.Index      := Query.FieldByName(FieldIndex).AsInteger;
        PNode^.Code       := Query.FieldByName(FieldCode).AsString;
        PNode^.remarks    := Query.FieldByName(FieldRemarks).AsString;
        PNode^.sum_Money  := Query.FieldByName(Fieldmoney).AsFloat;
        PNode^.photo      := Query.FieldByName(Fieldphoto).AsString;
        PNode^.Price      := Query.FieldByName(FieldPrice).AsCurrency;
        PNode^.Num        := Query.FieldByName(FieldNum).AsInteger;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.pid        := Query.FieldByName('Pid').AsInteger;
        PNode^.Input_time := Query.FieldByName(FieldInput_time).AsDateTime;
        pNode^.End_time   := Query.FieldByName(FieldEnd_time).AsDateTime;
        nNode := TV.Items.AddChildObject(Node, PNode^.Caption, PNode);
        if Assigned(nNode) then
        begin
          nNode.ImageIndex    := 1;
          nNode.SelectedIndex := 2;
        end;
        AddTree(nNode, PNode,True);
      end;

  finally
    Query.Free;
  end;

end;
//------------------------------------------------------------------------------
procedure TTreeFill.AddTree(Node: TTreeNode; PN: PNodeData;IsPatType : Boolean = False);
var
  Query: TADOQuery;
  nNode: TTreeNode;
  PNode: PNodeData;

begin
  Query := TADOQuery.Create(nil);
  try
      Query.Connection := Conn;
      Query.SQL.Text := 'Select * from ' + TableName + ' where ' + FieldPID + ' =' + IntToStr(PN^.Index);

      if Query.Active then
         Query.Close;

      Query.Open;
      while not Query.Eof do
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName(FieldCaption).AsString;
        PNode^.Index      := Query.FieldByName(FieldIndex).AsInteger;
        PNode^.Code       := Query.FieldByName(FieldCode).AsString;
        PNode^.remarks    := Query.FieldByName(FieldRemarks).AsString;
        PNode^.sum_Money  := Query.FieldByName(Fieldmoney).AsFloat;
        PNode^.photo      := Query.FieldByName(Fieldphoto).AsString;
        PNode^.Price      := Query.FieldByName(FieldPrice).AsCurrency;
        PNode^.Num        := Query.FieldByName(FieldNum).AsInteger;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.pid        := Query.FieldByName('Pid').AsInteger;
        PNode^.Input_time := Query.FieldByName(FieldInput_time).AsDateTime;
        pNode^.End_time   := Query.FieldByName(FieldEnd_time).AsDateTime;
        pNode^.PatType    := Query.FieldByName('PatType').AsInteger;

        if IsPatType then
        begin

          if pNode^.PatType  = FieldPatType then
          begin
            nNode := TV.Items.AddChildObject(Node, PNode^.Caption, PNode);
            if Assigned(nNode) then
            begin
              nNode.ImageIndex    := 1;
              nNode.SelectedIndex := 2;
            end;


          end;

        end
        else
        begin

          nNode := TV.Items.AddChildObject(Node, PNode^.Caption, PNode);
          if Assigned(nNode) then
          begin
            nNode.ImageIndex    := 1;
            nNode.SelectedIndex := 2;
          end;

        end;

        AddTree(nNode, PNode,IsPatType);
        Query.Next;
      end;

  finally
    Query.Free;
  end;

end;
//------------------------------------------------------------------------------
end.

