object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = #32467#31639#23545#24080#34920
  ClientHeight = 601
  ClientWidth = 1149
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1149
    Height = 170
    ApplicationButton.Menu = dxRibbonBackstageView1
    BarManager = dxBarManager1
    Style = rs2016
    ColorSchemeName = 'Office2016Colorful'
    QuickAccessToolbar.Toolbar = dxBarManager1Bar1
    TabAreaToolbar.Toolbar = dxBarManager1Bar2
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    ExplicitWidth = 1079
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #31649#29702
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
  end
  object dxRibbonBackstageView1: TdxRibbonBackstageView
    Left = 717
    Top = 397
    Width = 193
    Height = 96
    Buttons = <>
    Ribbon = dxRibbon1
    object dxRibbonBackstageViewTabSheet1: TdxRibbonBackstageViewTabSheet
      Left = 132
      Top = 0
      Active = True
      Caption = 'Recent'
      DesignSize = (
        61
        96)
      object dxRibbonBackstageViewGalleryControl1: TdxRibbonBackstageViewGalleryControl
        Left = 12
        Top = 44
        Width = 300
        Height = 38
        Anchors = [akLeft, akTop, akBottom]
        BorderStyle = cxcbsNone
        OptionsView.ColumnAutoWidth = True
        OptionsView.ColumnCount = 1
        OptionsView.ContentOffset.All = 0
        OptionsView.Item.Text.AlignHorz = taLeftJustify
        OptionsView.Item.Text.AlignVert = vaCenter
        OptionsView.Item.Text.Position = posRight
        OptionsView.Item.PinMode = bgipmTag
        Ribbon = dxRibbon1
        TabOrder = 0
        object dxRibbonBackstageViewGalleryControl1Group1: TdxRibbonBackstageViewGalleryGroup
          ShowCaption = False
          object dxRibbonBackstageViewGalleryControl1Group1Item1: TdxRibbonBackstageViewGalleryItem
            Caption = 'New Item'
            Description = 'New Item Description'
          end
        end
      end
      object cxLabel1: TcxLabel
        Left = 12
        Top = 12
        AutoSize = False
        Caption = 'Recent Documents'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -16
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        Properties.LineOptions.Alignment = cxllaBottom
        Properties.LineOptions.Visible = True
        Transparent = True
        Height = 26
        Width = 300
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 578
    Width = 1149
    Height = 23
    Panels = <>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 559
    ExplicitWidth = 977
  end
  object RzSplitter1: TRzSplitter
    Left = 0
    Top = 170
    Width = 1149
    Height = 408
    Position = 364
    Percent = 32
    RealTimeDrag = True
    UpperLeft.BorderOuter = fsFlat
    LowerRight.BorderOuter = fsFlat
    HotSpotVisible = True
    SplitterWidth = 7
    Align = alClient
    GradientColorStyle = gcsMSOffice
    GradientDirection = gdDiagonalDown
    TabOrder = 3
    VisualStyle = vsClassic
    ExplicitTop = 78
    ExplicitWidth = 977
    ExplicitHeight = 481
    BarSize = (
      364
      0
      371
      408)
    UpperLeftControls = (
      tv
      RzToolbar1)
    LowerRightControls = (
      RzPageControl1
      RzToolbar2)
    object tv: TTreeView
      Left = 1
      Top = 36
      Width = 362
      Height = 371
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      Images = cxImageList1
      Indent = 19
      RowSelect = True
      TabOrder = 0
      OnClick = tvClick
      OnDragDrop = tvDragDrop
      OnDragOver = tvDragOver
      OnEdited = tvEdited
      OnExpanded = tvExpanded
      OnKeyDown = tvKeyDown
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        000300000001047998EE760D54F0792E0000000000000000000000FFFFFFFFFF
        FFFFFF0000000000000000020000000108FA5EBE8B55534D4F28003275B96529
        00220000000100000000000000FFFFFFFFFFFFFFFF0000000000000000000000
        000102F694CE57220000000000000000000000FFFFFFFFFFFFFFFF0000000000
        000000000000000102034ECC912E0000000000000000000000FFFFFFFFFFFFFF
        FF000000000000000006000000010808545C4F55534D4F2800594EB965290022
        0000000100000000000000FFFFFFFFFFFFFFFF00000000000000000100000001
        024C80E55D220000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        000000000001023167F05C240000000000000000000000FFFFFFFFFFFFFFFF00
        00000000000000010000000103506799654655220000000000000000000000FF
        FFFFFFFFFFFFFF00000000000000000000000001021753B96526000000000000
        0000000000FFFFFFFFFFFFFFFF00000000000000000200000001040652055355
        534D4F260000000000000000000000FFFFFFFFFFFFFFFF000000000000000001
        00000001042867E55DE55DCD79240000000000000000000000FFFFFFFFFFFFFF
        FF00000000000000000000000001033075E77E735E2600000000000000000000
        00FFFFFFFFFFFFFFFF0000000000000000010000000104A2944B7BE55DCD7924
        0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000000000001
        03E6790E546567260000000000000000000000FFFFFFFFFFFFFFFF0000000000
        000000010000000104DF79418D55534D4F240000000000000000000000FFFFFF
        FFFFFFFFFF00000000000000000000000001031554D08F799826000000000000
        0000000000FFFFFFFFFFFFFFFF0000000000000000010000000104D08F938F55
        534D4F240000000000000000000000FFFFFFFFFFFFFFFF000000000000000001
        0000000103310033003000220000000000000000000000FFFFFFFFFFFFFFFF00
        000000000000000000000001020180ED90260000000000000000000000FFFFFF
        FFFFFFFFFF0000000000000000030000000104A18BF66555534D4F2400000000
        00000000000000FFFFFFFFFFFFFFFF0000000000000000010000000103166398
        633A67240000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
        00000001035D79504E504E220000000000000000000000FFFFFFFFFFFFFFFF00
        00000000000000010000000102F294668F220000000000000000000000FFFFFF
        FFFFFFFFFF0000000000000000000000000102504E504E260000000000000000
        000000FFFFFFFFFFFFFFFF00000000000000000100000001047D6C668F0A5466
        8F240000000000000000000000FFFFFFFFFFFFFFFF0000000000000000000000
        00010368678973AE4E260000000000000000000000FFFFFFFFFFFFFFFF000000
        0000000000010000000104034ECC91E8956297240000000000000000000000FF
        FFFFFFFFFFFFFF0000000000000000000000000103D191A5941953}
      ExplicitHeight = 444
    end
    object RzToolbar1: TRzToolbar
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 356
      Height = 29
      AutoStyle = False
      Images = il
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      TabOrder = 1
      VisualStyle = vsClassic
      ToolbarControls = (
        RzToolButton1
        RzSpacer2
        RzToolButton2
        RzSpacer3
        RzToolButton16
        RzSpacer17
        RzToolButton3
        RzSpacer4
        cxLabel2
        Edit1
        RzSpacer5
        RzToolButton4
        RzSpacer15
        RzToolButton14
        RzSpacer16
        RzToolButton15)
      object RzToolButton1: TRzToolButton
        Left = 4
        Top = 2
        Width = 28
        ImageIndex = 0
        ShowCaption = True
        UseToolbarShowCaption = False
        OnClick = RzToolButton1Click
      end
      object RzSpacer2: TRzSpacer
        Left = 32
        Top = 2
        Width = 6
      end
      object RzToolButton2: TRzToolButton
        Left = 38
        Top = 2
        ImageIndex = 5
      end
      object RzSpacer3: TRzSpacer
        Left = 63
        Top = 2
        Width = 6
      end
      object RzToolButton3: TRzToolButton
        Left = 100
        Top = 2
        ImageIndex = 3
      end
      object RzSpacer4: TRzSpacer
        Left = 125
        Top = 2
        Width = 6
      end
      object RzSpacer5: TRzSpacer
        Left = 258
        Top = 2
        Width = 6
      end
      object RzToolButton4: TRzToolButton
        Left = 264
        Top = 2
        ImageIndex = 7
      end
      object RzSpacer15: TRzSpacer
        Left = 289
        Top = 2
        Width = 6
      end
      object RzToolButton14: TRzToolButton
        Left = 295
        Top = 2
        ImageIndex = 20
      end
      object RzSpacer16: TRzSpacer
        Left = 320
        Top = 2
        Width = 6
      end
      object RzToolButton15: TRzToolButton
        Left = 326
        Top = 2
        ImageIndex = 22
      end
      object RzSpacer17: TRzSpacer
        Left = 94
        Top = 2
        Width = 6
      end
      object RzToolButton16: TRzToolButton
        Left = 69
        Top = 2
        Hint = #32534#36753
        ImageIndex = 24
        ParentShowHint = False
        ShowHint = True
        OnClick = RzToolButton16Click
      end
      object Edit1: TEdit
        Left = 163
        Top = 4
        Width = 95
        Height = 21
        TabOrder = 0
      end
      object cxLabel2: TcxLabel
        Left = 131
        Top = 6
        Caption = #26597#35810':'
      end
    end
    object RzPageControl1: TRzPageControl
      Left = 1
      Top = 55
      Width = 776
      Height = 352
      Hint = ''
      ActivePage = TabSheet3
      Align = alClient
      TabOverlap = -5
      TabHeight = 25
      TabIndex = 1
      TabOrder = 0
      TabStyle = tsSquareCorners
      TabWidth = 80
      Transparent = True
      OnTabClick = RzPageControl1TabClick
      ExplicitLeft = -2
      ExplicitTop = 57
      ExplicitWidth = 604
      ExplicitHeight = 399
      FixedDimension = 25
      object TabSheet1: TRzTabSheet
        TabVisible = False
        Caption = #24080#30446#24635#20215
        ExplicitWidth = 600
        ExplicitHeight = 367
        object cxGrid4: TcxGrid
          Left = 0
          Top = 0
          Width = 772
          Height = 320
          Align = alClient
          BorderStyle = cxcbsNone
          TabOrder = 0
          ExplicitWidth = 600
          ExplicitHeight = 367
          object cxGridDBBandedTableView4: TcxGridDBBandedTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.DataRowHeight = 30
            OptionsView.Footer = True
            OptionsView.HeaderHeight = 30
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            OptionsView.BandHeaderHeight = 30
            Bands = <
              item
              end
              item
                Caption = #36134#30446#33539#22260#9
              end
              item
                Width = 532
              end>
            object cxGridDBBandedColumn34: TcxGridDBBandedColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Width = 60
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn35: TcxGridDBBandedColumn
              Caption = #32534#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn36: TcxGridDBBandedColumn
              Caption = #24405#20837#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn37: TcxGridDBBandedColumn
              Caption = #20844#21496#21517#31216
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn38: TcxGridDBBandedColumn
              Caption = #24448#26469#21333#20301
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn39: TcxGridDBBandedColumn
              Caption = #36153#29992#31867#21035
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 8
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn40: TcxGridDBBandedColumn
              Caption = #24037#31243#21517#31216
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn41: TcxGridDBBandedColumn
              Caption = #24320#22987#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn42: TcxGridDBBandedColumn
              Caption = #32467#26463#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn43: TcxGridDBBandedColumn
              Caption = #37329#39069
              PropertiesClassName = 'TcxCalcEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 78
              Position.BandIndex = 2
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn44: TcxGridDBBandedColumn
              Caption = #24635#20215#37329#39069#22823#20889
              HeaderAlignmentHorz = taCenter
              Width = 73
              Position.BandIndex = 2
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn45: TcxGridDBBandedColumn
              Caption = #22791#27880
              HeaderAlignmentHorz = taCenter
              Width = 142
              Position.BandIndex = 2
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView4Column1: TcxGridDBBandedColumn
              Caption = #24080#30446#31867#21035
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView4Column2: TcxGridDBBandedColumn
              Caption = #21046#34920#31867#22411
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView4Column3: TcxGridDBBandedColumn
              Caption = #24080#30446#26126#32454
              Position.BandIndex = 0
              Position.ColIndex = 9
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView4Column5: TcxGridDBBandedColumn
              Caption = #20132#26131#29366#24577
              Position.BandIndex = 2
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView4Column6: TcxGridDBBandedColumn
              Caption = #23457#26680#29366#24577
              Position.BandIndex = 2
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
          end
          object cxGridLevel4: TcxGridLevel
            GridView = cxGridDBBandedTableView4
          end
        end
      end
      object TabSheet2: TRzTabSheet
        TabVisible = False
        Caption = #24080#30446#25910#25903
        ExplicitWidth = 600
        ExplicitHeight = 367
        object Grid1: TcxGrid
          Left = 0
          Top = 0
          Width = 772
          Height = 320
          Align = alClient
          BorderStyle = cxcbsNone
          TabOrder = 0
          ExplicitWidth = 600
          ExplicitHeight = 367
          object tvView1: TcxGridDBBandedTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.Footer = True
            OptionsView.HeaderHeight = 40
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            OptionsView.BandHeaderHeight = 30
            Bands = <
              item
              end>
            object tvView1Column1: TcxGridDBBandedColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxCheckBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 60
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object tvView1Column13: TcxGridDBBandedColumn
              Caption = #24037#31243#21517#31216
              Position.BandIndex = 0
              Position.ColIndex = 13
              Position.RowIndex = 0
            end
            object tvView1Column3: TcxGridDBBandedColumn
              Caption = #24405#20837#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object tvView1Column2: TcxGridDBBandedColumn
              Caption = #32534#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object tvView1Column4: TcxGridDBBandedColumn
              Caption = #20184#27454#21333#20301#13#10#20844#21496#21517#31216
              HeaderAlignmentHorz = taCenter
              Width = 55
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object tvView1Column5: TcxGridDBBandedColumn
              Caption = #25910#27454#21333#20301#13#10#24448#26469#21333#20301
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
            object tvView1Column12: TcxGridDBBandedColumn
              Caption = #20132#26131#26041#24335
              Position.BandIndex = 0
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
            object tvView1Column6: TcxGridDBBandedColumn
              Caption = #20132#26131#26126#32454
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 14
              Position.RowIndex = 0
            end
            object tvView1Column7: TcxGridDBBandedColumn
              Caption = #25910#27454#20154#13#10' '#24080#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 10
              Position.RowIndex = 0
            end
            object tvView1Column8: TcxGridDBBandedColumn
              Caption = #20184#27454#20154#13#10'  '#24080#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 11
              Position.RowIndex = 0
            end
            object tvView1Column9: TcxGridDBBandedColumn
              Caption = #32467#31639#37329#39069
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 8
              Position.RowIndex = 0
            end
            object tvView1Column10: TcxGridDBBandedColumn
              Caption = #22823#20889
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 9
              Position.RowIndex = 0
            end
            object tvView1Column11: TcxGridDBBandedColumn
              Caption = #22791#27880
              HeaderAlignmentHorz = taCenter
              Width = 320
              Position.BandIndex = 0
              Position.ColIndex = 12
              Position.RowIndex = 0
            end
            object tvView1Column14: TcxGridDBBandedColumn
              Caption = #21046#34920#31867#22411
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object tvView1Column15: TcxGridDBBandedColumn
              Caption = #24080#30446#31867#21035
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object tvView1Column16: TcxGridDBBandedColumn
              Caption = #23454#25910#37329#39069
              Position.BandIndex = 0
              Position.ColIndex = 15
              Position.RowIndex = 0
            end
            object tvView1Column18: TcxGridDBBandedColumn
              Caption = #25187#32602#37329#39069
              Position.BandIndex = 0
              Position.ColIndex = 16
              Position.RowIndex = 0
            end
            object tvView1Column17: TcxGridDBBandedColumn
              Caption = #23457#21512#29366#24577
              Position.BandIndex = 0
              Position.ColIndex = 17
              Position.RowIndex = 0
            end
            object tvView1Column19: TcxGridDBBandedColumn
              Caption = #20132#26131#29366#24577
              Position.BandIndex = 0
              Position.ColIndex = 18
              Position.RowIndex = 0
            end
          end
          object Lv1: TcxGridLevel
            GridView = tvView1
          end
        end
      end
      object TabSheet6: TRzTabSheet
        TabVisible = False
        Caption = #20538#21153#27424#25454
        ExplicitWidth = 600
        ExplicitHeight = 367
        object cxGrid5: TcxGrid
          Left = 0
          Top = 0
          Width = 772
          Height = 320
          Align = alClient
          BorderStyle = cxcbsNone
          TabOrder = 0
          ExplicitWidth = 600
          ExplicitHeight = 367
          object cxGridDBBandedTableView5: TcxGridDBBandedTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.DataRowHeight = 30
            OptionsView.Footer = True
            OptionsView.HeaderHeight = 30
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            OptionsView.BandHeaderHeight = 30
            OptionsView.BandHeaders = False
            Bands = <
              item
              end>
            object cxGridDBBandedColumn46: TcxGridDBBandedColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Width = 60
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn47: TcxGridDBBandedColumn
              Caption = #32534#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn48: TcxGridDBBandedColumn
              Caption = #24405#20837#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn49: TcxGridDBBandedColumn
              Caption = #20844#21496#21517#31216
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn50: TcxGridDBBandedColumn
              Caption = #24448#26469#21333#20301
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn51: TcxGridDBBandedColumn
              Caption = #36153#29992#31867#21035
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn52: TcxGridDBBandedColumn
              Caption = #24037#31243#21517#31216
              Position.BandIndex = 0
              Position.ColIndex = 8
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn55: TcxGridDBBandedColumn
              Caption = #37329#39069
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 11
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn56: TcxGridDBBandedColumn
              Caption = #22823#20889
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 12
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn57: TcxGridDBBandedColumn
              Caption = #22791#27880
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 15
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column1: TcxGridDBBandedColumn
              Caption = #27424#26465#26085#26399
              Position.BandIndex = 0
              Position.ColIndex = 9
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column2: TcxGridDBBandedColumn
              Caption = #27424#25454#31867#22411
              Position.BandIndex = 0
              Position.ColIndex = 10
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column3: TcxGridDBBandedColumn
              Caption = #20132#26131#31867#22411
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column4: TcxGridDBBandedColumn
              Caption = #21046#34920#31867#21035
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column5: TcxGridDBBandedColumn
              Caption = #20132#26131#29366#24577
              Position.BandIndex = 0
              Position.ColIndex = 13
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView5Column6: TcxGridDBBandedColumn
              Caption = #23457#21512#29366#24577
              Position.BandIndex = 0
              Position.ColIndex = 14
              Position.RowIndex = 0
            end
          end
          object cxGridLevel5: TcxGridLevel
            GridView = cxGridDBBandedTableView5
          end
        end
      end
      object TabSheet5: TRzTabSheet
        Caption = #23545#24080#34920'('#27719#24635')'
        ExplicitWidth = 600
        ExplicitHeight = 367
        object cxGrid3: TcxGrid
          Left = 0
          Top = 0
          Width = 772
          Height = 320
          Align = alClient
          BorderStyle = cxcbsNone
          TabOrder = 0
          ExplicitWidth = 600
          ExplicitHeight = 367
          object cxGridDBBandedTableView3: TcxGridDBBandedTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.Footer = True
            OptionsView.HeaderHeight = 30
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            OptionsView.BandHeaderHeight = 30
            Bands = <
              item
              end
              item
                Caption = #36134#30446#33539#22260#9
              end
              item
                Width = 1000
              end>
            object cxGridDBBandedTableView3Column1: TcxGridDBBandedColumn
              Caption = #21333#21495
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn23: TcxGridDBBandedColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.VertSizing = False
              Width = 60
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn24: TcxGridDBBandedColumn
              Caption = #32534#21495
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn25: TcxGridDBBandedColumn
              Caption = #23545#24080#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn26: TcxGridDBBandedColumn
              Caption = #24448#26469#21333#20301
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn27: TcxGridDBBandedColumn
              Caption = #21333#20301#20998#31867
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn28: TcxGridDBBandedColumn
              Caption = #36153#29992#31867#21035
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn29: TcxGridDBBandedColumn
              Caption = #24320#22987#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn30: TcxGridDBBandedColumn
              Caption = #32467#26463#26085#26399
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn31: TcxGridDBBandedColumn
              Caption = #24080#30446#24635#20215
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 2
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn32: TcxGridDBBandedColumn
              Caption = #36134#26399#25903#20184
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 2
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn33: TcxGridDBBandedColumn
              Caption = #36134#26399#22686#22870
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 2
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column2: TcxGridDBBandedColumn
              Caption = #36134#26399#25187#32602
              Position.BandIndex = 2
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column3: TcxGridDBBandedColumn
              Caption = #23545#36134#26085#25903#20184
              Width = 80
              Position.BandIndex = 2
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column4: TcxGridDBBandedColumn
              Caption = #36134#26399#19979#27424#20313#27454
              Width = 100
              Position.BandIndex = 2
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column5: TcxGridDBBandedColumn
              Caption = #20132#26131#29366#24577
              Position.BandIndex = 2
              Position.ColIndex = 8
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column6: TcxGridDBBandedColumn
              Caption = #32467#31639#29366#24577
              Position.BandIndex = 2
              Position.ColIndex = 9
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column7: TcxGridDBBandedColumn
              Caption = #22791#27880
              Position.BandIndex = 2
              Position.ColIndex = 10
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column9: TcxGridDBBandedColumn
              Caption = #27424#25454#26085#26399
              Position.BandIndex = 2
              Position.ColIndex = 6
              Position.RowIndex = 0
            end
            object cxGridDBBandedTableView3Column10: TcxGridDBBandedColumn
              Caption = #21407#26377#27424#25454
              Position.BandIndex = 2
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
          end
          object cxGridLevel3: TcxGridLevel
            GridView = cxGridDBBandedTableView3
          end
        end
      end
      object TabSheet3: TRzTabSheet
        Caption = #24080#30446#24635#20215
        ExplicitWidth = 600
        ExplicitHeight = 393
        object cxTreeList1: TcxTreeList
          Left = 0
          Top = 0
          Width = 772
          Height = 320
          Align = alClient
          Bands = <
            item
              FixedKind = tlbfLeft
            end>
          DefaultRowHeight = 28
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'Office2010Blue'
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.CellHints = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.GoToNextCellOnTab = True
          OptionsBehavior.AutoDragCopy = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsBehavior.HeaderHints = True
          OptionsBehavior.IncSearch = True
          OptionsBehavior.RecordScrollMode = rsmByPixel
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.RowSizing = True
          OptionsData.AnsiSort = True
          OptionsData.CaseInsensitive = True
          OptionsData.Deleting = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellTextMaxLineCount = 10
          OptionsView.ShowEditButtons = ecsbAlways
          OptionsView.BandLineHeight = 26
          OptionsView.Bands = True
          OptionsView.CheckGroups = True
          OptionsView.Footer = True
          OptionsView.GridLines = tlglBoth
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 20
          Preview.AutoHeight = False
          Preview.Visible = True
          StateImages = cxImageList1
          TabOrder = 0
          ExplicitLeft = -4
          ExplicitTop = -2
          ExplicitWidth = 600
          ExplicitHeight = 367
          Data = {
            00000500B70300000F00000044617461436F6E74726F6C6C6572310F00000012
            000000546378537472696E6756616C7565547970651200000054637853747269
            6E6756616C75655479706512000000546378537472696E6756616C7565547970
            6512000000546378537472696E6756616C756554797065120000005463785374
            72696E6756616C75655479706512000000546378537472696E6756616C756554
            79706512000000546378537472696E6756616C75655479706512000000546378
            537472696E6756616C75655479706512000000546378537472696E6756616C75
            655479706512000000546378537472696E6756616C7565547970651200000054
            6378537472696E6756616C75655479706512000000546378537472696E675661
            6C75655479706512000000546378537472696E6756616C756554797065120000
            00546378537472696E6756616C75655479706512000000546378537472696E67
            56616C75655479706508000000445855464D540000020000003167F05C010101
            0101010101010101010101445855464D54000004000000094ECC91E55D0B7A01
            01010101010101010101010101445855464D54000002000000E55DE565010101
            0101010101010101010101445855464D54000001000000295900090000003200
            3000310037002D0034002D003200310001000300000080622F67585400020000
            0031003000000200000033003000010005000000350030002B00350030000001
            0000002959010101010101445855464D54000002000000A052ED730101010101
            010101010101010101445855464D54000004000000034ECC91E55D0B7A000900
            000032003000310037002D0034002D00320031000002000000094ECC91000200
            0000555F65510101010101010101010101445855464D54000002000000E55DE5
            650101010101010101010101010101445855464D54000002000000A052ED7301
            0101010101010101010101010101000000000000001208020000000000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF0100000012080200000000000000000000
            00FFFFFFFFFFFFFFFFFFFFFFFF020000000208010000000000000000000000FF
            FFFFFFFFFFFFFFFFFFFFFF03000000080A0000000000000000FFFFFFFFFFFFFF
            FFFFFFFFFF0400000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF05
            0000001208020000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF060000
            0008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF07000000080A000000
            0000000000FFFFFFFFFFFFFFFFFFFFFFFF0A0801000000}
          object cxTreeList1Column1: TcxTreeListColumn
            Caption.Text = #39033#30446#21517#31216
            DataBinding.ValueType = 'String'
            Width = 152
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column2: TcxTreeListColumn
            Caption.Text = #24405#20837#26085#26399
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column3: TcxTreeListColumn
            Caption.Text = #21517#31216
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column4: TcxTreeListColumn
            Caption.Text = #23703#20301#21517#31216
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 5
            Position.RowIndex = 0
            Position.BandIndex = 0
            SortOrder = soAscending
            SortIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column5: TcxTreeListColumn
            Caption.Text = #21333#20215
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 12
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column6: TcxTreeListColumn
            Caption.Text = #24635#20215
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 13
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column7: TcxTreeListColumn
            Caption.MultiLine = True
            Caption.Text = #22791#27880
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 14
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column8: TcxTreeListColumn
            Caption.Text = #35745#31639#20844#24335
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 6
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column9: TcxTreeListColumn
            Caption.Text = #21333#20301
            DataBinding.ValueType = 'String'
            Width = 89
            Position.ColIndex = 11
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column10: TcxTreeListColumn
            Caption.Text = #35268#26684
            DataBinding.ValueType = 'String'
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column11: TcxTreeListColumn
            Caption.Text = #21697#29260
            DataBinding.ValueType = 'String'
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column12: TcxTreeListColumn
            Caption.Text = #25968#37327
            DataBinding.ValueType = 'String'
            Position.ColIndex = 8
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column13: TcxTreeListColumn
            Caption.MultiLine = True
            Caption.Text = #21464#37327
            DataBinding.ValueType = 'String'
            Position.ColIndex = 7
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column14: TcxTreeListColumn
            Caption.Text = #24320#22987#26085#26399
            DataBinding.ValueType = 'String'
            Position.ColIndex = 9
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column15: TcxTreeListColumn
            Caption.Text = #32467#26463#26085#26399
            DataBinding.ValueType = 'String'
            Position.ColIndex = 10
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
      end
    end
    object RzToolbar2: TRzToolbar
      Left = 1
      Top = 1
      Width = 776
      Height = 54
      AutoStyle = False
      Images = il
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      TabOrder = 1
      VisualStyle = vsClassic
      ExplicitWidth = 604
      ToolbarControls = (
        RzToolButton13
        RzSpacer1
        RzToolButton5
        RzSpacer6
        RzToolButton6
        RzSpacer7
        RzToolButton7
        RzSpacer8
        RzToolButton8
        RzSpacer9
        RzToolButton9
        RzSpacer10
        cxLabel3
        Edit2
        RzSpacer11
        RzToolButton10
        RzSpacer12
        RzToolButton11
        RzSpacer13
        RzToolButton12
        RzSpacer14)
      object RzSpacer1: TRzSpacer
        Left = 43
        Top = 2
      end
      object RzToolButton5: TRzToolButton
        Left = 51
        Top = 2
        Width = 52
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#22686
        OnClick = RzToolButton5Click
      end
      object RzSpacer6: TRzSpacer
        Left = 103
        Top = 2
      end
      object RzToolButton6: TRzToolButton
        Left = 111
        Top = 2
        Width = 52
        ImageIndex = 5
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
      end
      object RzSpacer7: TRzSpacer
        Left = 163
        Top = 2
      end
      object RzToolButton7: TRzToolButton
        Left = 171
        Top = 2
        Width = 52
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
      end
      object RzSpacer8: TRzSpacer
        Left = 223
        Top = 2
      end
      object RzToolButton8: TRzToolButton
        Left = 231
        Top = 2
        Width = 77
        ImageIndex = 6
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #23548#20986'Excel'
      end
      object RzSpacer9: TRzSpacer
        Left = 308
        Top = 2
      end
      object RzToolButton9: TRzToolButton
        Left = 316
        Top = 2
        Width = 66
        DropDownMenu = Print
        ImageIndex = 8
        ShowCaption = True
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #25171#21360
      end
      object RzSpacer10: TRzSpacer
        Left = 382
        Top = 2
      end
      object RzSpacer11: TRzSpacer
        Left = 622
        Top = 2
      end
      object RzToolButton10: TRzToolButton
        Left = 630
        Top = 2
        Width = 52
        ImageIndex = 7
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #26597#35810
      end
      object RzSpacer12: TRzSpacer
        Left = 682
        Top = 2
      end
      object RzToolButton11: TRzToolButton
        Left = 4
        Top = 27
        Width = 90
        DropDownMenu = pm
        ImageIndex = 12
        ShowCaption = True
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #23457#21512#32467#31639
      end
      object RzSpacer13: TRzSpacer
        Left = 94
        Top = 27
      end
      object RzToolButton12: TRzToolButton
        Left = 102
        Top = 27
        Width = 76
        ImageIndex = 10
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #34920#26684#35774#32622
      end
      object RzSpacer14: TRzSpacer
        Left = 178
        Top = 27
      end
      object RzToolButton13: TRzToolButton
        Left = 4
        Top = 2
        Width = 39
        DropDownMenu = pm2
        ImageIndex = 18
        ToolStyle = tsDropDown
      end
      object cxLabel3: TcxLabel
        Left = 390
        Top = 6
        Caption = #26597#35810':'
      end
      object Edit2: TEdit
        Left = 422
        Top = 4
        Width = 200
        Height = 21
        TabOrder = 1
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    Categories.Strings = (
      #40664#35748)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 344
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Quick Access Toolbar'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = 'Tab Area Toolbar'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #39033#30446#32500#25252
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1113
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Text Edit Item'
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarButton1: TdxBarButton
      Caption = #26032#24314
      Category = 0
      Hint = #26032#24314
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000010000
        00060000000A0000000B0000000B0000000B0000000B0000000B0000000C0000
        000C0000000C0000000B00000007000000020000000000000000000000068057
        4CBDB37B69FFB37A68FFB37A68FFB27968FFB27968FFB27A68FFB27968FFB379
        68FFF0E5E2FF659B84FF297758FF1E563EB9000000000000000000000008B57D
        6BFFFBF7F3FFFBF6F3FFFBF6F3FFFBF5F2FFFBF5F2FFFBF5F2FFFBF8F5FFFEFD
        FCFFFFFEFEFF2C7D5EFF2AC592FF2C7C5DFF000000000000000000000008B67F
        6DFFFCF8F5FFD49F70FFD49E6FFFD19D6FFFD29D6EFFD19C6CFFF6ECE3FF6AA4
        8CFF2F8363FF2F8262FF3BCB9DFF2E8061FF2E7F61FF205A45B700000008B882
        6FFFFCF9F6FFDAAC7DFFDBAB7DFFDAAB7CFFDAAB7CFFD9AB7CFFFBF7F2FF338B
        6AFF9BEAD7FF50D2ABFF4DD2A9FF4CD1A8FF48D0A5FF318466FF00000007BA84
        73FFFDF9F8FFDDAF7EFFDDAF7EFFDDAD7EFFDDAD7EFFDBAD7EFFF8F0E7FF69AA
        91FF358E6EFF368E6DFF63D9B6FF348C6CFF348B6BFF26664FBD00000007BB87
        76FFFDFBF9FFF8F1EBFFF8F1ECFFF8F0ECFFF8F1EBFFF8F0EBFFFAF3EFFFFDFA
        F9FFFEFEFDFF389473FF9BEAD7FF379271FF000000000000000000000006BD8A
        78FFFDFBFAFFCDA493FFCCA393FFCCA292FFCCA291FFCBA090FFCBA08FFFCA9F
        8FFFF4EBE8FF6EB39BFF3B9877FF2B6F58BF000000000000000000000006BF8D
        7BFFFEFCFBFFFAF3EEFFFAF2EFFFF9F2EFFFFAF2EEFFF9F2EEFFFAF2EEFFFAF2
        EEFFFAF4F2FFFEFEFDFFF9F3F2FF00000008000000000000000000000005C190
        7EFFFEFDFCFFCEA898FFCEA796FFCEA695FFCDA594FFCDA493FFCCA393FFCCA2
        92FFC39685FFFDFBFAFFBF8D7BFF00000007000000000000000000000005C393
        81FFFEFDFDFFFBF4F2FFFAF5F2FFFAF5F1FFFAF5F1FFFBF5F1FFFBF4F1FFFAF4
        F1FFFAF4F0FFFEFCFBFFC18F7EFF00000007000000000000000000000004C596
        84FFFEFEFEFFD1AE9DFFD0AC9CFFD0AB9BFFCFAA99FFCEA999FFCEA897FFCEA7
        96FFC59B89FFFEFDFCFFC39381FF00000006000000000000000000000004C698
        87FFFFFEFEFFFCF7F5FFFBF6F5FFFBF7F5FFFCF6F4FFFBF7F4FFFBF7F4FFFBF6
        F4FFFBF6F4FFFEFDFDFFC59684FF00000006000000000000000000000003C99B
        8AFFFFFEFEFFD5B2A3FFD4B2A2FFD4B1A0FFD3AF9FFFD2AD9DFFD1AD9CFFD0AB
        9BFFC89F8FFFFFFEFEFFC79887FF00000005000000000000000000000003CA9E
        8DFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFE
        FEFFFFFEFEFFFFFEFEFFC99B8AFF000000040000000000000000000000029B7F
        74BFD0AB9CFFD0AB9CFFD0AA9CFFD0AA9BFFCFA99BFFCFA99AFFCFA999FFCFA8
        99FFCEA899FFCFA898FF997B71C0000000030000000000000000}
    end
    object dxBarButton2: TdxBarButton
      Caption = #26032#24314#25353#38062
      Category = 0
      Hint = #26032#24314#25353#38062
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000040000
        00130000001E0000002000000020000000200000002100000021000000210000
        002200000022000000220000002300000021000000160000000500000012281E
        16CB37291EFF463A31FFBD8150FFBC7E4DFFB97949FFB67646FFB37141FFB06D
        3DFFAD6839FFAB6535FF42362EFF3D3026FF241A13CE00000015000000193B2C
        21FF685C54FF483C34FFE8C28BFFE7C088FFE6BD85FFE5BB81FFE4B87CFFE3B5
        79FFE2B276FFE2B273FF443931FF51433AFF34261DFF0000001E000000183E2F
        24FF6C6057FF4A3F37FFD9B27DFFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A6
        6DFFD4A56AFFD4A268FF473B33FF5B4F47FF37291EFF0000001D000000164031
        26FF6F645CFF4C4038FFFFFFFFFFF7F1EBFFF7F0EBFFF7F0EBFFF7EFEBFFF6EF
        EAFFF6EFEAFFF6EFE9FF463B34FF5D5249FF3A2C21FF0000001B000000144434
        29FF73675FFF4F443CFFFFFFFFFFF8F2EDFFF8F1EDFFF7F1EDFFF7F0EDFFF8F1
        EBFFF7F0EBFFF7F0ECFF4A4037FF5F534BFF3D2E23FF00000019000000124637
        2CFF776B63FF50453DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF4E433BFF61544CFF403126FF0000001700000010493A
        2FFF796E66FF50453DFF61564EFF60564EFF60554DFF5F544CFF5E544CFF5E53
        4BFF5D524AFF5C5249FF5B5149FF61554DFF433429FF000000150000000E4C3D
        32FF7C706AFF674E44FF654B42FF634A41FF61473FFF5F473EFF5C443CFF5B43
        3AFF594139FF584038FF573F37FF63574FFF46362DFF000000130000000D4E3F
        35FF80746DFF6B5248FFF4ECE6FFE9DACEFFE9D8CDFFE9D8CCFFE9D8CBFFE8D7
        CAFFF3EAE2FFF3E9E2FF5A4139FF645850FF483A2FFF000000110000000B5142
        36FF827770FF70564DFFF9F5F2FFF4EAE4FFF1E6DEFFEBDCD2FFE9D9CCFF4E41
        3DFF60534CFFF3EAE3FF5D453CFF655951FF4C3D32FF0000000F000000095344
        39FF857A73FF755A50FFFAF6F3FFF5EDE7FFF4EDE6FFF4ECE6FFEFE2DAFF493D
        38FF5A4D46FFF4EBE4FF60483FFF655A52FF4F3F34FF0000000D000000075545
        3AFF887D76FF795E54FFFAF6F4FFF5EEE9FFF5EDE7FFF4EDE7FFF4ECE6FF473A
        36FF483D36FFE9D9CDFF644C43FF675A52FF514137FF0000000B000000065748
        3DFF898079FF7C6157FFFAF7F4FFFAF6F4FFFAF6F4FFFAF6F3FFFAF6F3FFFAF5
        F2FFF5EEE9FFF4ECE6FF695046FF82776FFF534439FF00000009000000034235
        2EC058493DFF7F645AFF998178FF967F75FF937A72FF8E786DFF8B7269FF866E
        64FF82695FFF7D645BFF6E544AFF56453BFF3F332BC200000005000000000000
        0002000000030000000400000004000000040000000400000005000000050000
        0005000000050000000500000006000000060000000400000001}
    end
    object dxBarButton3: TdxBarButton
      Caption = #26032#24314#25353#38062
      Category = 0
      Hint = #26032#24314#25353#38062
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000020000000A00000010000000090000000200000000000000000000
        00020000000A000000120000000C000000030000000000000000000000000000
        00020000000F0F0742921D0F7EEF0603347A0000000E00000002000000020000
        000F0804347C1D0F7EF00F084194000000120000000200000000000000000000
        0008120B47923233AFFF3648CCFF1D1EA5FF0603357A0000000F0000000F0703
        357C1F20A5FF3747CCFF2D2FAEFF120B46950000000B00000000000000000000
        000C281C8DF1596CD8FF3B51D3FF3A4FD2FF1E22A6FF0602347D0502357E2022
        A6FF3A50D3FF3A50D3FF4C5FD4FF291D8CF10000001000000000000000000000
        0006130F3C734D4FBAFF667EE0FF415AD6FF415AD7FF1F24A7FF2529A8FF415A
        D7FF415AD7FF5B72DEFF484AB8FF130F3C790000000900000000000000000000
        00010000000A16123F73585CC1FF758DE6FF4A64DBFF4A65DBFF4A65DBFF4A64
        DBFF6983E3FF5356C0FF16123F780000000C0000000200000000000000000000
        0000000000010000000A191643755D63C7FF6783E5FF5774E2FF5774E2FF5774
        E2FF565CC6FF1916437A0000000D000000020000000000000000000000000000
        00000000000100000009100E3D734A50BEFF7492EBFF6383E7FF6483E7FF6383
        E7FF3840B6FF0B0839780000000C000000020000000000000000000000000000
        0001000000071413416E555CC5FF85A1EFFF7897EDFF9CB6F4FF9DB7F5FF7997
        EEFF7796EDFF414ABCFF0E0C3C730000000A0000000100000000000000000000
        00041818456B636CCFFF93AFF3FF83A1F1FFA6BFF7FF676DCAFF7E87DDFFAFC7
        F8FF83A3F2FF83A1F1FF5058C4FF121040710000000600000000000000000000
        00065759C3EFAFC6F6FF8EADF4FFABC4F8FF6F76D0FF1817456F24244F70868E
        E1FFB5CCF9FF8DACF4FFA1B8F4FF5758C3EF0000000900000000000000000000
        000331326B8695A0EAFFC0D3F9FF7880D7FF1C1C496B00000006000000072527
        526C8B93E6FFC1D3F9FF949EE9FF303168870000000500000000000000000000
        00010000000431336B825E62CBEC1F204D680000000500000001000000010000
        00052728536B5E62CBEC31326883000000070000000100000000000000000000
        0000000000000000000200000004000000020000000100000000000000000000
        0001000000030000000500000004000000010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
    end
    object dxBarButton4: TdxBarButton
      Caption = #26032#24314#25353#38062
      Category = 0
      Hint = #26032#24314#25353#38062
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000002000000070000000C0000000F0000000F0000
        000C000000070000000200000000000000000000000000000000000000000000
        000000000001000000060403021A37211D8354322BB86F4339E56E4339E55633
        2CBB37211D830503021B00000006000000010000000000000000000000000000
        0001000000081C110F4B70453CE0B4958EFFDFD1CEFFF6F2F0FFF6F3F1FFDFD2
        CFFFB4958EFF70453CE01D12104F000000090000000100000000000000010000
        0006291A165F91655AFDDED0CCFFE0C2B1FFC38766FFB87550FFB46B45FFC589
        69FFE2C3B2FFDED0CCFF91645AFD281A165F0000000600000001000000020A07
        062285594EF0E5D9D7FFCB9676FFB56A3EFFC48A69FFFFEAE0FFF6D9CFFFB164
        3AFFB4683DFFCA9274FFE5D9D7FF85584EF00A0706230000000200000006442E
        2885BFA29BFFE4C7B5FFBC7044FFB86E43FFC68D6DFFFFEAE3FFF8DFD6FFB669
        3EFFB86C40FFB96B3FFFE3C4B2FFBFA29BFF442E298600000006000000097A54
        4BD1E7DCD9FFCE9674FFBF7548FFBD7346FFB76C41FFB8764FFFB36C43FFB86D
        42FFBC7144FFBB7043FFCC916EFFE7DCD9FF7A544BD10000000A0000000A9469
        5EEDF7F3F2FFC7875DFFC68657FFC9895BFFC18253FFE7C9B8FFDCB6A0FFBA77
        4BFFC27F51FFC07B4EFFC37F56FFF7F3F2FF94695EED0000000B000000099A70
        63EDF8F4F3FFD5A176FFCF9769FFCA8F61FFBE8054FFE3C5B2FFFFF0EDFFDEBC
        A9FFC1875EFFC78A5AFFCD9268FFF8F4F3FF987064ED0000000A000000078664
        5ACCECE1DEFFE0B895FFCD966AFFDFBEA6FFE8CDBFFFBE8864FFFEF2EDFFFFF4
        EFFFE2C4B1FFC88A5CFFDAAC88FFECE2DEFF86645ACD0000000800000004523E
        387FD1B6AEFFF4E5D7FFD29D6FFFE5C9B7FFFFF7F2FFCFA891FFFAECE7FFFFF4
        F1FFE6C9B9FFCB9162FFF1E0D3FFD1B6AEFF523E388000000004000000010D0A
        091AAD8679EFEFE6E3FFECD4BCFFCE9C73FFEBD7C9FFFFF8F4FFFFF8F4FFE7CE
        BFFFCA956BFFE9CEB7FFEFE6E3FFAE8679EF0D0A091B00000001000000000000
        0003362A2651C29B8DFCEBDDD8FFF5ECE5FFE3C6B0FFD3AA85FFD3A985FFE3C6
        AFFFF6EDE6FFEBDCD8FFC29A8DFC362A26520000000300000000000000000000
        0000000000032A211E3EA78477DBD9BFB5FFEBDDD8FFF6F1EFFFF6F1EFFFEBDD
        D8FFD9BFB5FFA78377DB2A211E3F000000030000000100000000000000000000
        000000000000000000020706050F58464075856B60ADAF8E7FE0AF8E7FE0876C
        62B057473F760706050F00000002000000000000000000000000000000000000
        0000000000000000000000000001000000020000000300000004000000040000
        0003000000020000000100000000000000000000000000000000}
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = ' '#28155#21152
      Category = 0
      Hint = ' '#28155#21152
      Visible = ivAlways
      LargeGlyph.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000000000000000
        0000000000000000000000000001000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000010000000300000004000000040000000500000005000000050000
        0005000000050000000500000005000000050000000500000005000000050000
        0005000000050000000500000005000000060000000600000005000000040000
        0004000000010000000000000000000000000000000000000000000000000000
        0001000000050000000A0000000F000000120000001200000012000000120000
        0012000000120000001300000013000000130000001300000013000000130000
        0014000000140000001400000014000000140000001400000014000000120000
        000C000000060000000100000000000000000000000000000000000000000000
        00030000000B7E5D52BEAC7E6EFFAB7E6FFFAC7E6EFFAB7E6EFFAB7D6DFFAB7D
        6DFFAA7D6DFFAA7D6DFFAB7D6DFFAB7C6CFFAA7C6BFFA97B6CFFA97B6BFFAA7B
        6BFFA97B6BFFA97B6BFFA87A6BFFA97B6AFFA9796AFFA97969FFA87A6AFF7957
        4CBF0000000D0000000300000000000000000000000000000000000000010000
        00030000000EB08474FFFFFFFFFFFAF5F2FFFAF5F1FFFAF5F1FFF9F5F2FFFAF5
        F1FFFAF4F1FFF9F5F1FFFAF5F1FFF9F5F1FFFAF5F1FFF9F4F1FFFAF4F1FFF9F5
        F1FFF9F5F1FFF9F5F1FFF9F4F1FFF9F4F1FFF9F4F1FFF9F4F1FFF9F4F1FFA87A
        6AFF000000120000000400000001000000000000000000000000000000010000
        00040000000FB28677FFFFFFFFFFF5ECE5FFF5ECE5FFF5ECE5FFF5ECE5FFF5EC
        E5FFF5EBE4FFF5EBE5FFF5EBE4FFF5EBE5FFF5EBE4FFF4EBE4FFF4EBE5FFF4EB
        E4FFF4EBE4FFF4EBE4FFF5EBE4FFF5EAE4FFF5EBE3FFF4EAE3FFFAF5F1FFAA7C
        6BFF000000130000000500000001000000000000000000000000000000000000
        00040000000FB38979FFFFFFFFFFF5ECE6FFF5ECE6FFF5ECE6FFF5ECE6FFF5EC
        E5FFF5ECE6FFF5ECE6FFF5ECE5FFF5EBE5FFF5ECE6FFF5ECE5FFF5ECE5FFF5EB
        E4FFF5EBE5FFF5EBE4FFF5EBE4FFF5EBE4FFF4EBE4FFF5EBE5FFF9F5F1FFAB7C
        6CFF000000130000000500000001000000000000000000000000000000000000
        00040000000EB58C7BFFFFFFFFFFF5EDE7FFF5EDE7FFF6ECE6FFF5ECE7FFF5ED
        E6FFF6EDE7FFF5EDE6FFF5ECE6FFF5ECE6FFF5ECE6FFF5ECE6FFF5ECE6FFF5EC
        E6FFF5ECE6FFF5ECE5FFF5ECE5FFF5ECE5FFF5ECE5FFF5ECE5FFFAF5F1FFAB7D
        6EFF000000120000000500000001000000000000000000000000000000000000
        00040000000EB78E7FFFFFFFFFFFF5EEE8FFF6EDE7FFF5EDE8FFC19888FFC098
        87FFC09888FFC09887FFC09786FFC09786FFC09786FFBF9786FFBF9785FFBF97
        85FFBF9685FFBF9685FFBE9685FFF5ECE5FFF5ECE6FFF5ECE6FFFAF5F2FFAC7F
        6EFF000000110000000500000001000000000000000000000000000000000000
        00030000000DB99082FFFFFFFFFFF7EEE9FFF6EFE9FFF6EEE8FFEFE2DBFFEEE2
        DAFFEEE2DAFFEFE2DAFFEEE1DAFFEEE2D9FFEEE1DAFFEDE1D9FFEEE1D9FFEDE1
        D9FFEDE1D9FFEDE1D8FFEEE1D9FFF5ECE7FFF5ECE6FFF6EDE6FFFAF6F2FFAD80
        70FF000000110000000500000001000000000000000000000000000000000000
        00030000000DBB9384FFFFFFFFFFF7EFEAFFF6EEE9FFF6EFEAFFC29B8BFFC29B
        8BFFC29B8BFFC29B8BFFC19A8AFFC29A8AFFC19A89FFC19A89FFC19989FFC19A
        88FFC09989FFC19888FFC09888FFF5EEE8FFF5EEE7FFF6EDE7FFFAF6F3FFAD81
        71FF000000100000000400000001000000000000000000000000000000000000
        00030000000CBD9586FFFFFFFFFFF7F0EBFFF7EFEBFFF7F0EAFFEFE3DDFFF0E3
        DDFFEFE3DCFFF0E3DCFFEFE3DCFFEFE2DBFFEFE2DBFFEFE2DBFFEFE3DBFFEFE2
        DBFFEFE3DBFFEEE2DBFFEEE2DBFFF3EDE9FFE8E6E5FFDCDCDAFFDCDBDAFFCDC3
        C0FF000000250000000900000001000000000000000000000000000000000000
        00030000000BBE9989FFFFFFFFFFF8F1ECFFF8F0EBFFF7F0ECFFC59F90FFC59F
        8FFFC59F8FFFC59F8EFFC49E8EFFC49E8EFFC49D8DFFC49D8DFFC39D8CFFC39D
        8CFFC29C8CFFC39C8BFFC29C8CFFEAE8E6FF4A7B67FF216549FF216448FF2164
        48FF184734D10000001900000001000000000000000000000000000000000000
        00030000000BC09A8BFFFFFFFFFFF8F1ECFFF8F2EDFFF7F1ECFFF1E5DFFFF0E4
        DFFFF0E5DEFFF0E5DEFFF1E5DEFFF1E4DEFFF0E4DDFFF0E4DDFFF0E4DDFFEFE4
        DDFFF0E4DDFFEFE3DCFFEFE4DCFFE3E1E1FF267859FF41C699FF23B57DFF23B2
        7CFF21664AFF0000002200000000000000000000000000000000000000000000
        00030000000AC19D8EFFFFFFFFFFF7F2EEFFF8F2EDFFF8F2EDFFC8A494FFC7A4
        93FFC8A393FFC7A293FFC7A292FFC6A292FFC6A292FFC6A191FFC5A190FFC6A0
        91FFC6A090FFC5A08FFFC59F8FFFE4E2E1FF287D5EFF49CAA0FF22B57EFF27B8
        83FF22694CFF0000002200000000000000000000000000000000000000000000
        00030000000AC39F90FFFFFFFFFFF8F3EFFFF9F3EFFFF9F2EFFFF8F2EFFFF8F2
        EEFFF8F2EEFFF8F2EEFFF8F2EEFFF8F2EEFFF8F2EDFFF8F2EDFFF8F1EDFFF6F1
        EEFFEFEDECFFE9E7E6FFE7E5E4FFD4D3D2FF2B8263FF4FCDA3FF27B884FF2EBC
        89FF246C4FFF000000340000001C0000001C0000001200000004000000000000
        000200000009C5A192FFFFFFFFFFF9F3F0FFF9F4EFFFF9F3EFFFCAA798FFCAA7
        98FFCAA798FFCAA697FFC9A697FFC9A697FFC9A696FFC9A695FFC8A595FFE3D7
        D3FF558C76FF287759FF277658FF277457FF2D8867FF37C293FF2CBD8AFF34C1
        91FF257052FF256E51FF256E50FF246D50FF1A4E39CA00000011000000000000
        000200000009C6A394FFFFFFFFFFFAF4F1FFF9F4F0FFFAF4F0FFF9F4F0FFF9F4
        F0FFF9F4F0FFF9F4F0FFF9F4F0FFF8F4F0FFF9F4EFFFF8F3EFFFF8F3EFFFEEED
        ECFF319170FF63D8B4FF41C99EFF40C99DFF3FC89BFF3DC89AFF2FC290FF3BC7
        98FF39C598FF38C596FF37C494FF36C493FF2C8665FF00000018000000000000
        000200000008C6A596FFFFFFFFFFFAF5F2FFF9F5F2FFF9F5F1FF00A664FF00A2
        61FF009F5EFF009D5BFF00985BFF009658FF009256FF009054FF008D51FFE5DC
        D9FF339575FF69DBBBFF3BCB9DFF47D0A6FF51D3ABFF57D4AFFF5AD6B2FF55D3
        ADFF50D0A8FF45CBA1FF36C595FF3EC79AFF2E8B6BFF00000018000000000000
        000200000008C8A697FFFFFFFFFFFAF6F3FFFAF5F3FFFAF6F2FFF3EBE5FFF3EB
        E5FFF2EAE5FFF3EBE5FFF3EAE5FFF2E9E4FFF2E9E5FFF2E9E4FFF3E9E5FFF1EE
        EDFF369C79FF9EECD7FFAAEFDDFFAAEFDEFFAAEFDDFF91E9D2FF64DBBAFF73DF
        C2FF8EE7D0FFA6EDDBFFA5ECDBFF96E8D2FF30906FFF00000014000000000000
        000200000007C9A799FFFFFFFFFFFAF7F4FFFAF7F3FFFAF6F3FFCFB0A2FFCEB0
        A1FFCFAFA1FFCEAEA1FFCEAEA0FFCEAEA0FFCDADA0FFCDAD9FFFCDAD9EFFE9E0
        DBFF62AE92FF37A07EFF379F7CFF379E7BFF379E7AFF9BECD8FF6EE1C3FF7FE4
        CAFF359A78FF349977FF349776FF349775FF266F56C90000000C000000000000
        000200000006CAA99AFFFFFFFFFFFBF7F5FFFAF7F5FFFAF7F4FFF4ECE7FFF4EC
        E7FFF3ECE8FFF3ECE7FFF3ECE7FFF3ECE7FFF3ECE7FFF3ECE6FFF3EBE7FFF4EE
        EAFFF5F2F0FFF3F1EFFFF3F1F0FFECEAEAFF39A280FFA4EFDEFF77E5CAFF89E9
        D2FF2F8969FF0000001C0000000F0000000F0000000A00000003000000000000
        000200000006CBAA9BFFFFFFFFFFFBF8F6FFFBF8F5FFFBF8F5FFD2B4A6FFD2B4
        A6FFD1B3A6FFD1B3A5FFD1B2A4FFD0B2A4FFD0B2A4FFD0B1A4FFD0B1A3FFD0B0
        A2FFCFB0A2FFCFB0A2FFCFAFA2FFF6F5F4FF3AA784FFABF2E2FF7FE8D0FF91EC
        D8FF328F6FFF0000000F00000000000000000000000000000000000000000000
        000100000006CCAB9CFFFFFFFFFFFBF9F7FFFBF9F7FFFBF9F6FFF4EDE9FFF4ED
        E9FFF4EDE9FFF4EDE9FFF4EEE9FFF4EDE9FFF4EDE8FFF4ECE8FFF4EDE8FFF3ED
        E8FFF3ECE8FFF3EDE8FFF3ECE7FFF8F7F6FF3BAA86FFDEFBF6FFDEFBF6FFDEFB
        F6FF359574FF0000000C00000000000000000000000000000000000000000000
        000100000005CCAC9DFFFFFFFFFFFCFAF8FFFCF9F8FFFCF9F7FFD3B8A9FFD3B7
        A8FFD4B7A8FFD3B7A8FFD3B6A8FFD3B6A7FFD3B6A7FFD3B6A7FFD2B5A6FFD2B4
        A6FFD2B4A6FFD2B4A6FFD1B3A6FFFBFAF9FF6BBFA4FF3DAD89FF3CAC89FF3CAC
        88FF28735AC40000000700000000000000000000000000000000000000000000
        000100000005CDAC9EFFFFFFFFFFFCFAF8FFFCFAF8FFFCFAF8FFFCF9F8FFFCFA
        F8FFFCF9F8FFFCF9F7FFFCF9F8FFFBF9F8FFFCF9F7FFFCF9F7FFFCF9F7FFFBF9
        F6FFFBF9F6FFFBF8F7FFFBF9F6FFFCF8F7FFFAF9F8FFFAF9F9FFF9F9F9FFEAE1
        DEFF0000000C0000000300000000000000000000000000000000000000000000
        000100000004CDAD9EFFFFFFFFFFFDFAF9FFFCFAF9FFFCFAF9FFFCFAF9FFFCFA
        F9FFFCFAF8FFFCFAF8FFFCFAF8FFFCFAF8FFFCFAF8FFFBF9F8FFFCFAF8FFFCFA
        F8FFFCF9F7FFFCFAF8FFFBF9F7FFFCFAF7FFFCF9F7FFFBF9F7FFFDFCFBFFC09B
        8BFF000000070000000200000000000000000000000000000000000000000000
        000100000004CDAD9FFFFFFFFFFFFDFBFAFFFCFBF9FFFCFBFAFFFDFAF9FFFCFB
        FAFFFCFBF9FFFCFAF9FFFDFAF9FFFCFBF9FFFDFAF9FFFDFAF8FFFCFBF9FFFCFA
        F8FFFCFAF8FFFBFAF9FFFCFAF8FFFBFAF8FFFCF9F8FFFBFAF8FFFDFCFBFFC19D
        8EFF000000070000000200000000000000000000000000000000000000000000
        000100000003CEAD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC39E
        8FFF000000060000000100000000000000000000000000000000000000000000
        000000000002998176BECEAEA0FFCEAEA0FFCEAEA0FFCEAEA0FFCEAEA0FFCEAE
        9FFFCEAEA0FFCEAD9FFFCEAD9FFFCEAD9FFFCEAD9FFFCEAD9FFFCEAD9FFFCDAD
        9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFF9880
        76BF000000040000000100000000000000000000000000000000000000000000
        0000000000010000000200000002000000030000000300000003000000030000
        0003000000030000000300000004000000040000000400000004000000040000
        0004000000040000000400000004000000050000000500000005000000050000
        0003000000010000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0001000000000000000000000000000000000000000000000000}
    end
  end
  object dxSkinController1: TdxSkinController
    NativeStyle = False
    SkinName = 'Office2013LightGray'
    Left = 312
  end
  object il: TImageList
    Left = 240
    Top = 264
    Bitmap = {
      494C01011A004000380010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000007000000001002000000000000070
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003333330033333300999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006666660066666600999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CCCCCC0066666600333333003333
      3300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009999990080808000666666006666
      6600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000066666600666666006666
      6600333333009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      8000666666009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CCCCCC00666666006666
      6600666666003333330000999900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099999900808080008080
      8000808080006666660099999900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000666666006666
      660099FFFF0099CCCC0099CCCC00009999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080008080
      8000E5E5E500CCCCCC00CCCCCC00999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC008080
      8000CCFFFF0099FFFF0099CCCC0099CCCC000099990099CCCC00000000000000
      0000000000000000000000000000000000000000000000000000999999008080
      8000E5E5E500E5E5E500CCCCCC00CCCCCC0099999900CCCCCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      CC00FFFFFF00CCFFFF0099FFFF0099FFFF0099CCCC0066CCCC00009999000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00FFFFFF00E5E5E500E5E5E500E5E5E500CCCCCC00CCCCCC00999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      CC00FFFFFF00CCFFFF00CCFFFF0099FFFF0099FFFF0099CCCC0099CCCC000099
      990066CCCC00000000000000000000000000000000000000000000000000CCCC
      CC00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500CCCCCC00CCCCCC009999
      9900CCCCCC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099CCCC00FFFFFF00CCFFFF00CCFFFF0099FFFF0099FFFF0099CCCC00FFCC
      CC00993300009933000000000000000000000000000000000000000000000000
      0000CCCCCC00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500CCCCCC00CCCC
      CC00666666006666660000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000066CCCC00FFFFFF00CCFFFF00CCFFFF00FFCCCC00CC660000CC6600009933
      0000993300009933000099330000000000000000000000000000000000000000
      0000CCCCCC00FFFFFF00E5E5E500E5E5E500CCCCCC0080808000808080006666
      6600666666006666660066666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099CCCC00FFFFFF00CCFFFF00CC660000CC660000CC660000CC66
      0000993300009933000099330000993300000000000000000000000000000000
      000000000000CCCCCC00FFFFFF00E5E5E5008080800080808000808080008080
      8000666666006666660066666600666666000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000066CCCC00FFFFFF00FFCCCC00CC660000CC660000CC660000CC66
      0000CC6600009933000099330000993300000000000000000000000000000000
      000000000000CCCCCC00FFFFFF00CCCCCC008080800080808000808080008080
      8000808080006666660066666600666666000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF990000FF990000CC660000CC660000CC660000CC66
      0000CC660000CC66000099330000993300000000000000000000000000000000
      0000000000000000000099999900999999008080800080808000808080008080
      8000808080008080800066666600666666000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CC660000FF990000FF990000CC660000CC660000CC66
      0000CC660000CC660000CC660000993300000000000000000000000000000000
      0000000000000000000066666600999999009999990080808000808080008080
      8000808080008080800080808000666666000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CC660000FF990000FF990000CC660000CC66
      0000CC660000CC660000CC660000CC6600000000000000000000000000000000
      0000000000000000000000000000666666009999990099999900808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009933000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009999990099999900999999009999990099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000993300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC6600009933000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900CCCCCC009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099330000CC660000CC660000CC660000CC660000CC660000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC6600009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00999999000000000000000000000000000000000000000000993300009933
      00009933000099330000CC660000CC660000CC66000099330000993300009933
      0000993300000000000000000000000000000000000000000000999999009999
      99009999990099999900CCCCCC00CCCCCC00CCCCCC0099999900999999009999
      990099999900000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC0099999900000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00999999000000000000000000000000000000000000000000993300009933
      00009933000099330000CC660000CC660000CC66000099330000993300009933
      0000993300000000000000000000000000000000000000000000999999009999
      99009999990099999900CCCCCC00CCCCCC00CCCCCC0099999900999999009999
      990099999900000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00999999000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC6600009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099330000CC660000CC660000CC660000CC660000CC660000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC6600009933000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900CCCCCC009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000993300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009933000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009999990099999900999999009999990099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC99660000000000CC99
      660000000000CC99660000000000CC99660000000000CC99660000000000CC99
      660000000000CC996600000000000000000000000000CCCCCC0000000000CCCC
      CC0000000000CCCCCC0000000000CCCCCC0000000000CCCCCC0000000000CCCC
      CC0000000000CCCCCC0000000000000000000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900999999009999990099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CC99660099330000CC660000CC660000CC660000CC66000099330000CC99
      6600000000000000000000000000000000000000000000000000000000000000
      0000CCCCCC0099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC0099999900CCCC
      CC000000000000000000000000000000000000000000CC996600000000009933
      0000993300009933000099330000993300009933000099330000993300009933
      000000000000CC996600000000000000000000000000CCCCCC00000000009999
      9900999999009999990099999900999999009999990099999900999999009999
      990000000000CCCCCC0000000000000000000000000000000000000000000000
      000099330000CC660000CC660000CC660000CC660000CC660000CC6600009933
      0000000000000000000000000000000000000000000000000000000000000000
      000099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000993300000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC009999990000000000000000000000000000000000CC996600000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      000000000000CC996600000000000000000000000000CCCCCC00000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      990000000000CCCCCC0000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000993300000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00999999000000000000000000000000000000000000000000000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000993300000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC009999990000000000000000000000000000000000CC996600000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      000000000000CC996600000000000000000000000000CCCCCC00000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      990000000000CCCCCC0000000000000000000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000993300000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00999999000000000000000000000000000000000000000000000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000000000
      000099330000CC660000CC660000CC660000CC660000CC660000CC6600009933
      0000000000000000000000000000000000000000000000000000000000000000
      000099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      99000000000000000000000000000000000000000000CC996600000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      000000000000CC996600000000000000000000000000CCCCCC00000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      990000000000CCCCCC0000000000000000000000000000000000000000000000
      0000CC99660099330000CC660000CC660000CC660000CC66000099330000CC99
      6600000000000000000000000000000000000000000000000000000000000000
      0000CCCCCC0099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC0099999900CCCC
      CC00000000000000000000000000000000000000000000000000000000009933
      0000CC996600CC996600CC996600CC996600CC996600CC996600CC9966009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900999999009999990099999900000000000000
      00000000000000000000000000000000000000000000CC996600000000009933
      0000993300009933000099330000993300009933000099330000993300009933
      000000000000CC996600000000000000000000000000CCCCCC00000000009999
      9900999999009999990099999900999999009999990099999900999999009999
      990000000000CCCCCC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC99660000000000CC99
      660000000000CC99660000000000CC99660000000000CC99660000000000CC99
      660000000000CC996600000000000000000000000000CCCCCC0000000000CCCC
      CC0000000000CCCCCC0000000000CCCCCC0000000000CCCCCC0000000000CCCC
      CC0000000000CCCCCC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CC996600CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600CC99
      6600CC996600CC996600CC996600CC9966009999990099999900999999009999
      9900999999009999990099999900999999009999990099999900999999009999
      9900999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000993300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      000000000000000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      0000000000000000000099330000CC6600009933000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900CCCCCC009999990000000000000000000000
      000000000000000000000000000000000000CC996600FFFFFF00CC996600CC99
      6600CC996600CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999009999
      9900999999009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00CC996600FFFF
      FF00FFFFFF00CC996600FFFFFF00993300009933000099330000993300009933
      00009933000099330000FFFFFF00CC9966009999990000000000999999000000
      0000000000009999990000000000999999009999990099999900999999009999
      9900999999009999990000000000999999000000000000000000000000000000
      000099330000CC660000CC660000CC660000CC660000CC660000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00999999000000
      000000000000000000000000000000000000CC996600FFFFFF00CC996600FFFF
      FF00FFFFFF00CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999000000
      0000000000009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000009933
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC6600009933
      0000000000000000000000000000000000000000000000000000000000009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      990000000000000000000000000000000000CC996600FFFFFF00CC996600CC99
      6600CC996600CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999009999
      9900999999009999990000000000000000000000000000000000000000000000
      000000000000000000000000000099999900000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC0099999900000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099999900000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      000099330000000000000000000000000000000000000000000099999900CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC0099999900000000000000000000000000CC996600FFFFFF00CC996600CC99
      6600CC996600CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999009999
      9900999999009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000993300009933
      00009933000099330000CC660000CC660000CC66000099330000993300009933
      0000993300000000000000000000000000000000000000000000999999009999
      99009999990099999900CCCCCC00CCCCCC00CCCCCC0099999900999999009999
      990099999900000000000000000000000000CC996600FFFFFF00CC996600FFFF
      FF00FFFFFF00CC996600FFFFFF00993300009933000099330000993300009933
      00009933000099330000FFFFFF00CC9966009999990000000000999999000000
      0000000000009999990000000000999999009999990099999900999999009999
      9900999999009999990000000000999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00CC996600FFFF
      FF00FFFFFF00CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999000000
      0000000000009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00CC996600CC99
      6600CC996600CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000999999009999
      9900999999009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC9966009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CC996600CC996600CC996600CC996600CC996600CC99
      6600CC996600CC996600CC996600CC9966009999990000000000000000000000
      0000000000000000000099999900999999009999990099999900999999009999
      9900999999009999990099999900999999000000000000000000000000000000
      00000000000099330000CC660000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099999900CCCCCC00CCCCCC00CCCCCC0099999900000000000000
      000000000000000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC996600CC996600CCCCCC00CCCCCC00CCCCCC00CCCCCC00CC99
      6600000000000000000000000000000000009999990000000000000000000000
      0000000000009999990099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000009933000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009999990099999900999999009999990099999900000000000000
      00000000000000000000000000000000000000000000CC996600CC996600CC99
      6600CC9966000000000000000000CC996600CC996600CC996600CC9966000000
      0000000000000000000000000000000000000000000099999900999999009999
      9900999999000000000000000000999999009999990099999900999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300000000000000000000000000000000000000000000999999009999
      9900999999009999990099999900999999009999990099999900999999009999
      990099999900000000000000000000000000CC666600CC666600CC666600CC66
      6600CC666600CC666600CC666600CC666600CC666600CC666600CC666600CC66
      6600CC666600CC666600CC666600CC6666009999990099999900999999009999
      9900999999009999990099999900999999009999990099999900999999009999
      99009999990099999900999999009999990000000000CC996600FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900CC99
      6600CC99660099330000000000000000000000000000B2B2B200CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00B2B2
      B200B2B2B200999999000000000000000000CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC00000000000000000099999900CC996600CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600CC99
      660099330000CC9966009933000000000000B2B2B200B2B2B200B2B2B200B2B2
      B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2
      B20099999900B2B2B2009999990000000000CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC00000000000000000099999900CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC
      9900CC996600993300009933000000000000B2B2B20000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00B2B2B200999999009999990000000000CC666600FFCCCC00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFCCCC00CC66660099999900CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC0099999900CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC990000CC000000990000FFCC99000000FF000000CC00FFCC
      9900CC996600CC9966009933000000000000B2B2B20000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00B2B2B20099999900CCCCCC00B2B2B20099999900CCCC
      CC00B2B2B200B2B2B2009999990000000000CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC00000000000000000099999900CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CC996600CC996600CC99660099330000B2B2B20000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B2B2B200B2B2B200B2B2B20099999900CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC00000000000000000099999900CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC
      9900CC996600CC996600CC99660099330000B2B2B20000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00B2B2B200B2B2B200B2B2B20099999900CC666600FFCCCC00FFCCCC000000
      9900000099000000990000009900FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFCCCC00CC66660099999900CCCCCC00CCCCCC009999
      9900999999009999990099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC009999990000000000CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600FFCC
      9900FFCC9900CC996600CC9966009933000000000000B2B2B200B2B2B200B2B2
      B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200CCCC
      CC00CCCCCC00B2B2B200B2B2B20099999900CC666600FFFFFF00FFFFFF000000
      9900FFFFFF00FFFFFF0000009900FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC6666009999990000000000000000009999
      99000000000000000000999999000000000000000000CCCCCC00000000000000
      0000CCCCCC000000000000000000999999000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC99
      6600FFCC9900FFCC9900CC996600993300000000000000000000B2B2B2000000
      000000000000000000000000000000000000000000000000000000000000B2B2
      B200CCCCCC00CCCCCC00B2B2B20099999900CC666600FFFFFF00FFFFFF000000
      9900FFFFFF00FFFFFF0000009900FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC6666009999990000000000000000009999
      99000000000000000000999999000000000000000000CCCCCC00000000000000
      0000CCCCCC00000000000000000099999900000000000000000000000000CC99
      6600FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500FFFFFF00CC99
      6600CC996600CC9966009933000000000000000000000000000000000000B2B2
      B20000000000CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC0000000000B2B2
      B200B2B2B200B2B2B2009999990000000000CC666600FFCCCC00FFCCCC000000
      9900000099000000990000009900FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFCCCC00CC66660099999900CCCCCC00CCCCCC009999
      9900999999009999990099999900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC0099999900000000000000000000000000CC99
      6600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CC996600000000000000000000000000000000000000000000000000B2B2
      B200000000000000000000000000000000000000000000000000000000000000
      0000B2B2B200000000000000000000000000CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC000000000000000000999999000000000000000000000000000000
      0000CC996600FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500FFFF
      FF00CC9966000000000000000000000000000000000000000000000000000000
      0000B2B2B20000000000CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC000000
      0000B2B2B200000000000000000000000000CC666600FFFFFF00FFFFFF00FFCC
      CC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFFFF00FFCCCC00FFFFFF00FFFF
      FF00FFCCCC00FFFFFF00FFFFFF00CC666600999999000000000000000000CCCC
      CC000000000000000000CCCCCC000000000000000000CCCCCC00000000000000
      0000CCCCCC000000000000000000999999000000000000000000000000000000
      0000CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC99660000000000000000000000000000000000000000000000
      0000B2B2B2000000000000000000000000000000000000000000000000000000
      000000000000B2B2B2000000000000000000CC666600FFCCCC00CC666600CC66
      6600FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCCCC00FFCC
      CC00CC666600CC666600FFCCCC00CC66660099999900CCCCCC00999999009999
      9900CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC009999990099999900CCCCCC00999999000000000000000000000000000000
      000000000000CC996600CC996600CC996600CC996600CC996600CC996600CC99
      6600CC996600CC99660000000000000000000000000000000000000000000000
      000000000000B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2
      B200B2B2B200B2B2B2000000000000000000CC666600FFCCCC00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCCCC00FFCC
      CC00FFCCCC00FFCCCC00FFCCCC00CC66660099999900CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC0000000000000000000000000000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CC666600CC666600CC666600CC66
      6600CC666600CC666600CC666600CC666600CC666600CC666600CC666600CC66
      6600CC666600CC666600CC666600CC6666009999990099999900999999009999
      9900999999009999990099999900999999009999990099999900999999009999
      99009999990099999900999999009999990000000000CCCCCC00C0C0C000E5E5
      E500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CC99990099330000CC999900CCCCCC0066999900666699009999
      9900E5E5E5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300009933000099330000000000000000000000000000000000000000
      0000000000009933000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600CC99
      6600CC9999009933000099330000993300000000000066CCFF003399CC006666
      990099999900E5E5E50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000CC660000CC66
      000099330000E5E5E500CC66000099330000E5E5E500E5E5E500E5E5E5009933
      0000CC660000CC66000099330000000000000000000000000000000000000000
      0000993300009933000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000993300009933000099330000CC99990000000000CCCCFF0066CCFF003399
      CC006666990099999900E5E5E500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000CC660000CC66
      000099330000E5E5E500CC66000099330000E5E5E500E5E5E500E5E5E5009933
      0000CC660000CC66000099330000000000000000000000000000000000009933
      0000993300009933000099330000993300009933000099330000993300009933
      00009933000000000000000000000000000000000000CC996600FFFFFF00E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E5008080800080808000808080009933
      00009933000099330000CC999900000000000000000000000000CCCCFF0066CC
      FF003399CC006666990099999900E5E5E5000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000CC660000CC66
      000099330000E5E5E500CC66000099330000E5E5E500E5E5E500E5E5E5009933
      0000CC660000CC66000099330000000000000000000000000000000000000000
      0000993300009933000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E5E5E50099996600FFFFCC00FFFFCC00FFFFFF00CCCC
      990099330000CC9999000000000000000000000000000000000000000000CCCC
      FF0066CCFF003399CC0066669900CCCCCC00FFCCCC00CC999900CC999900CC99
      9900CCCC9900E5E5E50000000000000000000000000099330000CC660000CC66
      000099330000E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E5009933
      0000CC660000CC66000099330000000000000000000000000000000000000000
      0000000000009933000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600FFFFFF00E5E5
      E500E5E5E500E5E5E50099999900F2EABF00FFFFCC00FFFFCC00FFFFCC00FFFF
      FF00666666000000000000000000000000000000000000000000000000000000
      0000CCCCFF0066CCFF00B2B2B200CC999900CCCC9900F2EABF00FFFFCC00F2EA
      BF00F2EABF00CC999900ECC6D900000000000000000099330000CC660000CC66
      0000CC660000993300009933000099330000993300009933000099330000CC66
      0000CC660000CC66000099330000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0099999900F2EABF00FFFFFF00F2EABF00FFFFCC00FFFF
      CC00666666000000000000000000000000000000000000000000000000000000
      000000000000E5E5E500CC999900FFCC9900FFFFCC00FFFFCC00FFFFCC00FFFF
      FF00FFFFFF00FFFFFF00CC999900E5E5E5000000000099330000CC660000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC660000CC66000099330000000000003399CC0000669900006699000066
      99000066990000669900006699000000000000000000CC996600CC996600CC99
      6600CC996600CC996600CC996600CC99660000000000CC996600FFFFFF00E5E5
      E500E5E5E500E5E5E50099999900F2EABF00FFFFFF00FFFFFF00F2EABF00FFFF
      CC00666666000000000000000000000000000000000000000000000000000000
      000000000000FFCCCC00CCCC9900FFFFCC00F2EABF00FFFFCC00FFFFCC00FFFF
      FF00FFFFFF00FFFFFF00F2EABF00CCCC99000000000099330000CC660000CC66
      0000993300009933000099330000993300009933000099330000993300009933
      0000CC660000CC66000099330000000000003399CC0099FFFF0099FFFF0099FF
      FF0099FFFF0099FFFF00006699000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC99660000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0099999900F2EABF00F2EABF00F2EABF009999
      6600808080000000000000000000000000000000000000000000000000000000
      000000000000CCCC9900FFCC9900F2EABF00F2EABF00FFFFCC00FFFFCC00FFFF
      CC00FFFFFF00FFFFFF00F2EABF00CC9999000000000099330000CC6600009933
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0099330000CC66000099330000000000003399CC0099FFFF0099FFFF0099FF
      FF0099FFFF0099FFFF00006699000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC99660000000000CC996600FFFFFF00E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500999999009999990099999900E5E5
      E500CC9966000000000000000000000000000000000000000000000000000000
      000000000000CC999900F2EABF00F2EABF00F2EABF00F2EABF00FFFFCC00FFFF
      CC00FFFFCC00FFFFCC00FFFFCC00CC9999000000000099330000CC6600009933
      0000FFFFFF00993300009933000099330000993300009933000099330000FFFF
      FF0099330000CC66000099330000000000003399CC0099FFFF0099FFFF0099FF
      FF0099FFFF0099FFFF00006699000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC99660000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CC9966000000000000000000000000000000000000000000000000000000
      000000000000CCCC9900F2EABF00FFFFCC00F2EABF00F2EABF00F2EABF00FFFF
      CC00FFFFCC00FFFFCC00F2EABF00CC9999000000000099330000CC6600009933
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0099330000CC66000099330000000000003399CC0099FFFF0099FFFF0099FF
      FF0099FFFF0099FFFF00006699000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CC99660000000000CC996600FFFFFF00E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500FFFFFF00CC996600CC996600CC99
      6600CC9966000000000000000000000000000000000000000000000000000000
      000000000000FFCCCC00CCCC9900FFFFFF00FFFFFF00F2EABF00F2EABF00F2EA
      BF00F2EABF00FFFFCC00CCCC9900CCCC99000000000099330000E5E5E5009933
      0000FFFFFF00993300009933000099330000993300009933000099330000FFFF
      FF00993300009933000099330000000000003399CC0099FFFF0099FFFF003399
      CC003399CC003399CC003399CC000000000000000000CC996600FFFFFF00FFFF
      FF00CC996600CC996600CC996600CC99660000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC996600E5E5E500CC99
      6600000000000000000000000000000000000000000000000000000000000000
      000000000000E5E5E500CC999900ECC6D900FFFFFF00FFFFCC00F2EABF00F2EA
      BF00F2EABF00FFCC9900CC999900E5E5E5000000000099330000CC6600009933
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0099330000CC66000099330000000000003399CC0099FFFF0099FFFF003399
      CC00CCFFFF0000669900000000000000000000000000CC996600FFFFFF00FFFF
      FF00CC996600E5E5E500CC9966000000000000000000CC996600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC996600CC9966000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFCCCC00CC999900FFCCCC00F2EABF00F2EABF00F2EA
      BF00CCCC9900CC999900FFCCCC00000000000000000099330000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300009933000099330000000000003399CC0099FFFF0099FFFF003399
      CC000066990000000000000000000000000000000000CC996600FFFFFF00FFFF
      FF00CC996600CC996600000000000000000000000000CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E5E5E500CCCC9900CC999900CC999900CC99
      9900CC999900E5E5E50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003399CC003399CC003399CC003399
      CC000000000000000000000000000000000000000000CC996600CC996600CC99
      6600CC9966000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF990000CC660000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC6600000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600CC99
      6600CC996600CC9966000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC66
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF0099330000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC66
      000000000000000000000000000000000000000000000000FF00000099000000
      99000000990000000000000000000000000000000000000000000000FF000000
      9900000099000000990000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00993300009933000099330000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000E5E5E500E5E5
      E500E5E5E500E5E5E500CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CC66
      000000000000000000000000000000000000000000000000FF000000CC000000
      CC000000CC00000099000000000000000000000000000000FF000000CC000000
      CC000000CC000000990000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF009933000099330000993300009933000099330000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      00000000000000000000000000000000000000000000000000000000FF000000
      CC000000CC000000CC0000009900000000000000FF000000CC000000CC000000
      CC00000099000000000000000000000000000000000000000000993300009933
      0000993300009933000099330000CC660000CC66000099330000993300009933
      0000993300009933000000000000000000000000000000000000CC996600FFFF
      FF009933000099330000FFFFFF00993300009933000099330000FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000E5E5E500E5E5
      E500E5E5E500CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CC66
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000CC000000CC000000CC00000099000000CC000000CC000000CC000000
      990000000000000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC6600009933000000000000000000000000000000000000CC996600FFFF
      FF0099330000FFFFFF00FFFFFF00FFFFFF00993300009933000099330000FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000CC000000CC000000CC000000CC000000CC00000099000000
      000000000000000000000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC6600009933000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009933000099330000FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000E5E5E500E5E5
      E500CCCCCC00CCCCCC00CC660000CC660000CC660000CC660000CC660000CC66
      0000CC660000CC660000CC660000CC6600000000000000000000000000000000
      0000000000000000FF000000CC000000CC000000CC0000009900000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300009933000099330000CC660000CC66000099330000993300009933
      0000993300009933000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0099330000FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00E5E5E500CC660000FF990000CC660000CC660000CC660000CC660000CC66
      0000CC660000CC660000CC660000CC6600000000000000000000000000000000
      00000000FF000000CC000000CC000000CC000000CC000000CC00000099000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC9966000000000000000000FF990000CC660000E5E5E500CCCC
      CC00FF990000FF990000FF990000FF990000FF990000CC660000CC660000CC66
      0000CC660000CC660000CC660000000000000000000000000000000000000000
      FF000000CC000000CC000000CC00000099000000CC000000CC000000CC000000
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC996600CC99
      6600CC996600CC9966000000000000000000FF990000CC660000FFFFFF00FF99
      0000FF996600FF996600FF990000FF990000FF990000FF990000FF990000CC66
      0000CC660000CC660000000000000000000000000000000000000000FF000000
      CC000000CC000000CC0000009900000000000000FF000000CC000000CC000000
      CC00000099000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC996600E5E5
      E500CC996600000000000000000000000000FF990000CC660000FF990000FF99
      6600FF996600FF996600FF996600FF996600FF990000FF990000FF990000FF99
      0000CC660000000000000000000000000000000000000000FF000000CC000000
      CC000000CC00000099000000000000000000000000000000FF000000CC000000
      CC000000CC000000990000000000000000000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC996600CC99
      660000000000000000000000000000000000FF990000FF990000FF996600FF99
      6600FF996600FF996600FF996600FF996600FF996600FF996600FF990000CC66
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC9966000000
      00000000000000000000000000000000000000000000FF99000099999900FFFF
      FF0099999900FFFFFF0099999900FFFFFF0099999900FFFFFF0099999900FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000099999900CCCC
      CC0099999900CCCCCC0099999900CCCCCC0099999900CCCCCC00999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000700000000100010000000000800300000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000001FFF1FFF00000000
      0FFF0FFF0000000083FF83FF0000000081FF81FF00000000C0FFC0FF00000000
      C03FC03F00000000E01FE01F00000000E007E00700000000F003F00300000000
      F001F00100000000F800F80000000000F800F80000000000FC00FC0000000000
      FC00FC0000000000FE00FE0000000000FFFFFFFFFFFFFFFFF83FF83FFEFFFEFF
      F83FF83FFC7FFC7FF83FF83FF83FF83FF83FF83FF01FF01FF83FF83FE00FE00F
      F83FF83FC007C007C007C007C007C007C007C007C007C007C007C007F83FF83F
      E00FE00FF83FF83FF01FF01FF83FF83FF83FF83FF83FF83FFC7FFC7FF83FF83F
      FEFFFEFFF83FF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFAAABAAABFC3FFC3FFFFFFFFFF00FF00FA00BA00BF00FF00FE00FE00F
      E007E007A00BA00BE007E007E00FE00FE007E007A00BA00BE007E007E00FE00F
      F00FF00FA00BA00BF00FF00FE00FE00FFC3FFC3FA00BA00BFFFFFFFFFFFFFFFF
      FFFFFFFFAAABAAABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FEFFFEFF
      00007FFEFC7FFC7F000043FEF83FF83F00005A02F01FF01F00005BFEE00FE00F
      000043FEC007C00700007FFEC007C007000043FEC007C00700005A02F83FF83F
      00005BFEF83FF83F000043FEF83FF83F00007FFEF83FF83F00007C00F83FF83F
      000F780FF83FF83F861F861FFFFFFFFFFFFFFFFFFFFFFFFFC007C00700000000
      8003800300006DB60001000100006DB600014001000000000001400100006DB6
      00007FF000006DB600004000000000008000800000006DB6C000DFE000006DB6
      E001E82100000000E007EFF700006DB6F007F41700006DB6F003F7FB00000000
      F803F803000003C0FFFFFFFF000000008FFFFFFFFFFFFFF807FFC001FBFF8000
      83FF8001F3FF800081FF8001E0078001C0FF8001F3FF8003E0038001FBFF8007
      F0018001FFFF8007F800800101808007F800800101808007F800800101808007
      F800800101808007F800800101808007F80080010180800FF80080010381801F
      FC0180010783803FFE03FFFF0F87FFFFFFFFFFFF801FFFFFFFFFC003000FFFFF
      FC3FC003000FFFFFFC3FC003000F87C3FC3FC003000F8383FC3FC003000FC107
      C003C003000FE00FC003C003000FF01FC003C0030000F83FC003C0030000F01F
      FC3FC0030001E00FFC3FC0030003C107FC3FC00700078383FC3FC00F000F87C3
      FFFFC01F800FFFFFFFFFFFFFC01FFFFF00000000000000000000000000000000
      000000000000}
  end
  object il2: TcxImageList
    FormatVersion = 1
    DesignInfo = 17563755
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0006000000090000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000600000002000000067D53
          45BDAE7360FFAD735FFFAE735FFFAD725EFFAD725EFFAC715DFFAC715DFFAC70
          5DFFAB705CFFAC705CFFAC705BFFAC6F5CFF7B5042BE0000000600000008AF76
          63FFF6EEE9FFF6EFE9FFF6EFE8FFF6EEE8FFF6EEE7FFF6EEE8FFF5EEE8FFF5ED
          E8FFF6EDE8FFF6ECE7FFF5EDE7FFF5EDE7FFAC705DFF0000000900000008B278
          65FFF9F0EBFFF4EAE3FFF4EAE3FFF4EAE3FFF4EAE3FFF4E9E2FFF4E9E3FFF6EF
          E9FFF9F3F0FFF4EAE3FFF4EAE2FFF7EFE9FFAD725EFF0000000900000007B37A
          67FFF9F3EFFFF4EBE5FFF4EBE5FFF4EBE5FFF4EAE5FFF7EDE7FFF3EDE8FFAE98
          8EFF6B4436FFF4EAE3FFF4EAE5FFF9F1ECFFAE7460FF0000000800000007B57D
          6AFFFAF5F1FFF6EBE7FFF6EBE6FFF7EDE8FFF9F3EFFFC9BAB1FF755143FF9172
          66FFE1D4CBFFF6EBE5FFF6EBE5FFFAF3EFFFB07662FF0000000800000006B67F
          6DFFFBF7F5FFF7EDE9FFF7EEEAFFDFD2CEFF8A6A5DFF7C594BFFD0C0B6FFF7ED
          E7FFF6EDE7FFF6EDE7FFF6EDE7FFFAF6F2FFB17864FF0000000700000006B882
          70FFFCF9F7FFF7EEEAFFF0E5E0FF765040FF997C71FFF7F1EDFFF8F0EDFFF7EE
          EAFFF7EEE9FFF6EEE9FFF6EDE9FFFBF7F5FFB37A67FF0000000700000005BA85
          73FFFDFAF9FFF7EFEBFFF7EFEBFFE0D3CCFF8F6E60FF805E4FFFD1C3BCFFFAF5
          F2FFF8F0ECFFF7EFEBFFF7EEEBFFFCF9F7FFB47D6AFF0000000600000005BC88
          76FFFDFBFBFFF8F0EDFFF8F2EDFFF8F0EDFFF8F0EEFFCBB9B2FF7F5B4CFF9678
          6CFFE7DDD9FFF8EFEDFFF7EFEDFFFDFAFAFFB67F6DFF0000000600000004BE8A
          78FFFEFDFCFFF8F2EFFFFAF2EFFFF8F2EFFFF8F2EFFFFAF2EFFFF5EEEAFFB8A2
          98FF77503FFFF8F2EEFFF8F2EEFFFDFCFBFFB88270FF0000000500000004BF8D
          7BFFFEFEFDFFFAF3F0FFFAF2F0FFFAF2EFFFF8F3EFFFFAF2F0FFFAF3F0FFF8F2
          EFFFFAF3F0FFFAF3F0FFF8F2EFFFFEFDFCFFBA8473FF0000000500000003C08F
          7DFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFE
          FEFFFFFEFEFFFFFEFEFFFFFEFEFFFEFEFDFFBB8775FF0000000400000002906A
          5EBFC2907FFFC1907EFFC18F7EFFC18F7EFFC08E7DFFC08E7CFFBF8D7CFFBF8D
          7BFFBF8C7AFFBF8B7AFFBE8B79FFBD8A79FF8C6659BF00000002000000000000
          0002000000020000000300000003000000030000000300000003000000030000
          0003000000030000000300000003000000030000000200000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000040000000700000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000020000
          00063D29238F714C43FF311F1A8F000000070000000200000001000000000000
          000000000000000000000000000000000000000000010D090723201613552519
          1565715045F3C2B0A7FF5E3F35F3231713671D1310570B070625000000000000
          00000000000000000000000000000000000000000002523B32B7987F76FF8467
          5DFFBFAEA4FFE2D9D2FFC6B7B1FF7D5F56FF896D63FF472F28B90000000B0000
          001100000012000000120000001200000012000000133C2C2780A48A80FFECE7
          E3FFBDA69CFF9E7E70FFC3AFA6FFEAE2DEFF997C72FF38282479714F44C29E6D
          5DFF9E6C5DFF9D6C5CFF9D6B5BFF9C6C5CFFBE9E94FFE1D6D2FF8D6C5EFFEAE4
          E0FF8B6B5FFFF7EEEAFF8B6B60FFE7DFD9FF765549FC00000008A17162FFF9F2
          EFFFF7EEE9FFF7EEEAFFC29F93FFF7EEE9FFFAF6F4FFC8B7B0FFAC9388FFEFEA
          E6FFB19E98FF755349FFAC978FFFE9E1DCFFA1887EFF3A2A257AA47567FFF9F2
          F0FFF7EEEBFFF7EEEAFFC3A095FFF7EEEAFFFBF6F4FFB8A096FFB7A095FFA88A
          7FFFD9CDC7FFF6F3F1FFD4C5BEFFA4877AFFB1998EFF675045B63137BBFF2F36
          BAFF2D34B8FF2C33B7FF2B31B6FFC3A196FFE3D3CEFFE6DAD6FFD7C6C1FFD4C4
          BEFFA38375FFE2D8D2FF9B7A6DFF3127225D30252151120E0C213E49C5FF778C
          FBFF657BF9FF647AFAFF373FC1FFF8EFEBFFF8F1EDFFFAF3F0FFD2B9B0FFFCF9
          F8FFCDBAB2FFA28273FFC0A99FFF0000000E0000000000000000434ECAFF798E
          FBFF778CFAFF758AFBFF3E48C6FFF8F0EDFFF8F0ECFFF8F0ECFFC5A499FFFAF6
          F4FFFDFBFAFFFEFCFBFFE9DEDAFF0000000C00000000000000004450CBFF4E59
          CEFF4E59CEFF4D59CDFF305DC3FF2466BEFF2363BCFF2361BBFF215FBAFFC7A6
          9BFFC6A69AFFD4B9B0FFAD8071FF0000000A0000000000000000B78E81FFFAF5
          F2FFF8F0EEFFF7F1EEFF2973C4FFB7E6FAFF74CFF6FF73CFF6FF2468BFFFF7F0
          EEFFF8F0EDFFFAF4F2FFB08577FF000000090000000000000000BA9285FFFAF5
          F2FFFAF5F2FFFAF5F2FF2D7CCAFFBBE9FBFFBBE9FBFFBBE9FAFF2A72C4FFFAF4
          F2FFFAF5F2FFFAF5F2FFB48A7CFF0000000800000000000000008C7065C1BE97
          89FFBC9688FFBC9588FF3084CDFF3082CDFF2E7FCBFF2D7ECAFF2C7CCAFFBA91
          83FFB89083FFB88F82FF876A5FC1000000050000000000000000000000030000
          0005000000050000000500000005000000050000000500000005000000060000
          0006000000060000000500000004000000010000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          00090000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000B0000000A00000006000000027D5345BDAE73
          60FFAD735FFFAE735FFFAD725EFFAD725EFFAC715DFFAC715DFFAC705DFFAB70
          5CFFAC705CFFAC705BFFAC6F5CFFAB6F5BFF7B5042BE00000006AF7663FFF6EE
          E9FFF6EFE9FFF6EFE8FFF6EEE8FFF6EEE7FFF6EEE8FFF5EEE8FFF5EDE8FFF6ED
          E8FFF6ECE7FFF5EDE7FFF5EDE7FFF5EDE7FFAC705CFF00000009B27865FFF9F0
          EBFFF4EAE3FFF4EAE3FFF4EAE3FFF4EAE3FFF4E9E2FFF4E9E3FFF4EAE2FFF4EA
          E3FFF4EAE3FFF4EAE2FFF4E9E2FFF6EEEAFFAD715EFF00000009B37A67FFF9F3
          EFFFF4EBE5FFF4EBE5FFF4EBE5FFF4EAE5FFF6EAE3FFF4EBE3FFF4EAE3FFF4EA
          E5FFF4EAE3FFF4EAE5FFF6EAE3FFF8F1EDFFAD7360FF00000008B57D6AFFFAF5
          F1FFF6EBE7FFD4C0B8FF683923FF683923FF683723FFD4C0B7FF683723FF6736
          23FF673623FFD4BFB7FFF6EBE5FFF8F3EFFFAF7662FF00000008B67F6DFFFBF7
          F5FFF7EDE9FF6C3A25FFF7EEE9FFF6EEE9FF6A3925FFF6EEE7FF693825FFF6ED
          E7FFF6EDE7FF6A3923FFF6EDE7FFFAF6F2FFB17864FF00000007B88270FFFCF9
          F7FFF7EEEAFFD7C3BBFF6E3C27FF6C3927FF6C3C27FFF7EEEAFF6C3927FFF7EE
          E9FFF6EEE9FF6C3A25FFF7EEE9FFFBF7F5FFB27A67FF00000007BA8573FFFDFA
          F9FFF7EFEBFFF7EFEBFFF7F0EBFFF8F0EBFF6E3E29FFF7EFEAFF6D3D28FFF7EF
          EBFFF7EFEBFF6E3C27FFF7EFEAFFFCF9F7FFB47C69FF00000006BC8876FFFDFB
          FBFFF8F0EDFFD8C8BEFF72402BFF72402BFFD8C6BFFFF7F0EDFF713F2AFF713F
          2AFF703E29FFD6C4BEFFF8EFEDFFFDFBFAFFB67F6CFF00000006BE8A78FFFEFD
          FCFFF8F2EFFFFAF2EFFFF8F2EFFFF8F2EFFFFAF2EFFFF8F2EEFF74412BFFF8F2
          EEFFF8F2EEFFF8F2EEFFF8F0EEFFFDFCFBFFB7826FFF00000005BF8D7BFFFEFE
          FDFFFAF3F0FFFAF2F0FFFAF2EFFFF8F3EFFFFAF2F0FFFAF3F0FF76442BFFFAF3
          F0FFFAF3F0FFF8F2EFFFFAF3EFFFFEFDFCFFB98472FF00000005C08F7DFFFFFE
          FEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFE
          FEFFFFFEFEFFFFFEFEFFFEFEFDFFFEFEFEFFBB8775FF00000004906A5EBFC290
          7FFFC1907EFFC18F7EFFC18F7EFFC08E7DFFC08E7CFFBF8D7CFFBF8D7BFFBF8C
          7AFFBF8B7AFFBE8B79FFBD8A79FFBD8A78FF8C6658BF00000002000000020000
          0002000000030000000300000003000000030000000300000003000000030000
          0003000000030000000300000003000000030000000200000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E0000000F0000000F0000000F0000000F0000001000000010000000100000
          0010000000100000001000000010000000100000000F0000000A0E5437C11474
          4DFF14744DFF14744DFF14744DFF14744DFFAD725EFFAC715DFFAC715DFFAC70
          5DFFAB705CFFAC705CFFAC705BFFAC6F5CFFAB6F5BFF7B5041C215764FFF59CD
          ABFF59CDABFF76DABEFF57CDAAFF58CDAAFFF5ECE5FFF5EBE5FFF4ECE5FFF4EB
          E5FFF5EBE5FFF5E9E4FFF4EBE4FFF4EBE4FFF4EBE5FFAC705CFF167951FF5ED0
          AFFF4AC7A2FF074023FF4AC8A2FF4AC7A2FFF5EBE5FFF4E9E2FFF4E9E3FFF4EA
          E2FFF4EAE3FFF4EAE3FFF4EAE2FFF4E9E2FFF4ECE6FFAD715EFF187C53FF63D2
          B3FF4BC8A3FF084325FF4AC8A3FF4AC8A2FFF5ECE8FF967568FFA78C80FFF4EA
          E3FFF4EAE5FFD3C2B8FF6B4130FFD5C2B8FFF6EDE9FFAD7360FF197E56FF69D4
          B6FF4CC9A3FF094827FF4CC9A3FF4BC9A3FFF7EEEAFFF1E7E0FF8B6759FFBEA6
          9CFFF6EDE6FF6D4332FFF6EBE5FF6D4232FFF6EFEAFFAF7662FF1A825AFF6FD7
          B9FF4DC9A4FF0A4D2AFF4DC9A4FF4DC9A4FFF8F2EEFFF6EEE7FFE9DED6FF7F59
          4AFFCEBCB2FFD6C5BCFF704633FFD5C5BCFFF8F1EDFFB17864FF1C855CFF75D9
          BCFF4ECAA4FF0A522EFF4ECAA4FF4DCAA5FFF9F3F0FFF7EEE9FFF7EEEAFFE0D1
          CAFF835B4BFFDFD1C9FFF6EDE9FFF7EEE9FFF8F2EFFFB27A67FF1E885FFF7ADB
          C1FF67D5B4FF0B5731FF67D5B5FF4FCAA5FFFAF5F2FFD8C8C0FF754B38FFD8C8
          C0FFD3C3BBFF845D4BFFE9DDD8FFF7EFEAFFFAF4F0FFB47C69FF1F8C63FF80DE
          C5FF1E7751FF0D5B35FF1D754FFF4FCBA5FFFAF5F4FF774D3AFFF7F0EDFF774D
          3AFFF8EFEDFFC2ACA3FF8F6C5EFFF3E9E7FFFAF5F3FFB67F6CFF218F65FF85E0
          C7FF3CAB85FF0D6037FF3CAB85FF51CBA7FFFBF7F6FFDBCBC4FF7A4E3BFFDACB
          C4FFF8F2EEFFF8F2EEFFB49B8FFF9F8072FFFAF7F4FFB7826FFF239369FF8AE1
          CBFF51CBA6FF23865EFF51CBA6FF52CCA7FFFBF9F7FFFAF2F0FFFAF3F0FFF8F2
          EFFFFAF3F0FFFAF3F0FFF8F2EFFFFAF3EFFFFBF7F6FFB98472FF24956BFF8DE2
          CDFF8DE3CBFF8DE2CCFF8DE3CCFF8CE2CBFFFDFCFBFFFCF9F8FFFCF9F8FFFCF9
          F7FFFCF8F7FFFCF8F6FFFCF8F7FFFCF8F6FFFCF8F7FFBB8775FF1B7151C02498
          6CFF24976CFF24976CFF24976CFF24966CFFC08E7DFFC08E7CFFBF8D7CFFBF8D
          7BFFBF8C7AFFBF8B7AFFBE8B79FFBD8A79FFBD8A78FF8C6659C1000000020000
          0004000000040000000400000004000000040000000400000004000000040000
          0005000000050000000500000005000000050000000500000003}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E0000000F0000000F0000000F0000000F0000001000000010000000100000
          0010000000100000001000000010000000100000000F0000000A0E5437C11474
          4DFF14744DFF14744DFF14744DFF14744DFFAD725EFFAC715DFFAC715DFFAC70
          5DFFAB705CFFAC705CFFAC705BFFAC6F5CFFAB6F5BFF7B5041C215764FFF59CD
          ABFF59CDABFF76DABEFF57CDAAFF58CDAAFFF5ECE5FFF5EBE5FFF4ECE5FFF4EB
          E5FFF5EBE5FFF5E9E4FFF4EBE4FFF4EBE4FFF4EBE5FFAC705CFF167951FF5ED0
          AFFF4AC7A2FF074023FF4AC8A2FF4AC7A2FFF5EBE5FFF4E9E2FFF4E9E3FFF4EA
          E2FFF4EAE3FFF4EAE3FFF4EAE2FFF4E9E2FFF4ECE6FFAD715EFF187C53FF63D2
          B3FF4BC8A3FF084325FF4AC8A3FF4AC8A2FFF5ECE8FFF6EAE3FF6B4333FFF4EA
          E3FFD3C2BAFF6B4130FFD3C2BAFFF6EAE3FFF6EDE9FFAD7360FF197E56FF69D4
          B6FF4CC9A3FF094827FF4CC9A3FF4BC9A3FFF7EEEAFFF6EDE6FF6E4433FFF6EB
          E6FF6D4333FFF6EBE5FF6D4232FFF6EBE5FFF6EFEAFFAF7662FF1A825AFF6FD7
          B9FF4DC9A4FF0A4D2AFF4DC9A4FF4DC9A4FFF8F2EEFFF6EEE7FF6F4734FFF7ED
          E7FF6F4734FFF6EDE7FF704633FFF6EDE7FFF8F1EDFFB17864FF1C855CFF75D9
          BCFF4ECAA4FF0A522EFF4ECAA4FF4DCAA5FFF9F3F0FFF7EEE9FF734936FFF7EE
          EAFF734736FFF6EEE9FF724835FFF7EEE9FFF8F2EFFFB27A67FF1E885FFF7ADB
          C1FF67D5B4FF0B5731FF67D5B5FF4FCAA5FFFAF5F2FF744C39FF754B38FFF8EF
          EBFF734B38FFF7EFEBFF744A37FFF7EFEAFFFAF4F0FFB47C69FF1F8C63FF80DE
          C5FF1E7751FF0D5B35FF1D754FFF4FCBA5FFFAF5F4FFF8F0EEFF774D3AFFF8F0
          EDFFD9C8C2FF764C39FFD8C8C2FFF8EFEDFFFAF5F3FFB67F6CFF218F65FF85E0
          C7FF3CAB85FF0D6037FF3CAB85FF51CBA7FFFBF7F6FFFAF2EFFFF8F2EEFFF8F2
          EFFFF8F2EEFFF8F2EEFFF8F2EEFFF8F0EEFFFAF7F4FFB7826FFF239369FF8AE1
          CBFF51CBA6FF23865EFF51CBA6FF52CCA7FFFBF9F7FFFAF2F0FFFAF3F0FFF8F2
          EFFFFAF3F0FFFAF3F0FFF8F2EFFFFAF3EFFFFBF7F6FFB98472FF24956BFF8DE2
          CDFF8DE3CBFF8DE2CCFF8DE3CCFF8CE2CBFFFDFCFBFFFCF9F8FFFCF9F8FFFCF9
          F7FFFCF8F7FFFCF8F6FFFCF8F7FFFCF8F6FFFCF8F7FFBB8775FF1B7151C02498
          6CFF24976CFF24976CFF24976CFF24966CFFC08E7DFFC08E7CFFBF8D7CFFBF8D
          7BFFBF8C7AFFBF8B7AFFBE8B79FFBD8A79FFBD8A78FF8C6659C1000000020000
          0004000000040000000400000004000000040000000400000004000000040000
          0005000000050000000500000005000000050000000500000003}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E0000000F0000000F0000000F0000000F0000001000000010000000100000
          0010000000100000001000000010000000100000000F0000000A0E5337C11474
          4DFF14744DFF14744DFF14744DFF14744DFFB37B6AFFB37B6AFFB27B69FFB37B
          69FFA66840FFA66940FFA76840FFA7683FFFA7683FFF784A2EC115764FFF59CD
          ABFF59CDABFF76DABEFF57CDAAFF58CDAAFFCFA89CFFF8F2EEFFF7F2EEFFCFA8
          9CFFE5C798FFE9CEA1FFE9D1A6FFE5C696FFE5C696FFA76A42FF167951FF5ED0
          AFFF4AC7A2FF074023FF4AC8A2FF4AC7A2FFC69A8DFFF7F0ECFFF7F0EBFFC69A
          8CFFE6CA9BFFE7CCA0FFAF7954FFE4C494FFE3C18CFFAA6C44FF187C53FF63D2
          B3FF4BC8A3FF084325FF4AC8A3FF4AC8A2FFC79C8EFFF8F1EDFFF8F1EDFFC69C
          8EFFE8CCA0FFD6B48EFFA46541FFD5B38EFFE7CA9CFFAC7047FF197E56FF69D4
          B6FF4CC9A3FF094827FF4CC9A3FF4BC9A3FFC89D8FFFC89D8FFFC79D8FFFC89D
          8FFFE9CFA5FFAA6F46FF975630FFA86D45FFE3C290FFAE744BFF1A825AFF7DDC
          C1FF4DC9A4FF0A4D2AFF4DC9A4FF4DC9A4FFC99E91FFF8F3EFFFF9F2F0FFC89E
          90FFEBD1AAFFE4C390FF9B5935FFE3C390FFE4C290FFB0774EFF1C855CFF86DE
          C5FF4ECAA4FF0A522EFF4ECAA4FF4DCAA5FFC9A092FFFAF4F1FFF9F4F1FFC9A0
          92FFECD6AFFFE4C591FF9E5E38FFE4C592FFE4C391FFB37B52FF1E885FFF8BE0
          CAFF7ADBBFFF136A44FF77DABEFF4FCAA5FFCBA293FFFBF5F3FFFAF5F3FFCAA1
          93FFEED9B3FFE5C592FFA3643CFFE5C593FFE5C592FFB67F56FF1F8C63FF80DE
          C5FF1E7751FF0D5B35FF1D754FFF4FCBA5FFCCA395FFCBA395FFCBA294FFCBA2
          94FFEFDBB7FFE5C593FFA7693FFFE5C693FFE5C694FFB98359FF218F65FF85E0
          C7FF3CAB85FF0D6037FF3CAB85FF51CBA7FFCCA497FFFCF7F5FFFBF7F5FFCCA4
          95FFF0DDBBFFE5C795FFAA6E44FFE5C795FFE6C795FFBB865DFF239369FF8AE1
          CBFF51CBA6FF23865EFF51CBA6FF52CCA7FFCDA698FFFCF8F6FFFCF8F7FFCDA5
          97FFF1DFBFFFE6C795FFAE7349FFE6C897FFE6C797FFBD8A61FF24956BFF8DE2
          CDFF8DE3CBFF8DE2CCFF8DE3CCFF8CE2CBFFD5B4A6FFFDFAF9FFFDFAF9FFD5B3
          A6FFF1E0C1FFF1DFC1FFF1E0C1FFF1E0C1FFF1E0C1FFC08D63FF1A7150C02498
          6CFF24976CFF24976CFF24976CFF24966CFFC49F92FFC39E91FFC39E91FFC39E
          91FFC39167FFC29167FFC29067FFC29067FFC29067FF906B4BC0000000020000
          0003000000040000000400000004000000040000000400000004000000040000
          0004000000040000000400000004000000040000000400000003}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000001000000060000000C0000000E0000000B000000050000
          0002000000000000000000000000000000000000000000000000000000000000
          00000000000000000006010C1F5B031A45B4021E52D002163CA40108164E0000
          000B000000010000000000000000000000000000000000000000000000000000
          00000000000000000008082A5CAF3F83BEFF6CA9D6FF0F4491FF052F78FF010F
          27710000000700000000000000000000000000000000000000000000000C0000
          000F0000000D000000090104071F13458AE678C6E5FFA4DBF1FF093D96FF062A
          69EA0000001300000002000000000000000000000000000000007E431BFF7C41
          1AFF683213FF1C0C045800000008030E1D41225DA6F66DB5DBFF207EC6FF1248
          98FF47160DA401000011000000020000000000000000000000000D080427864E
          23F03D1F0CA00000000C00000001000000070412224816437ED71855A5FFF2E9
          E2FFBA8169FF4E1A0EAE0000011000000002000000000000000000000007502E
          149C572B11D50000000B00000000000000000000000A4C2D159C7C6866FFC3B5
          B5FFF8EEE5FF705F8CFF010132AC0000010F0000000200000000000000031F13
          0941753E18FF100803380000000D0000000D02010018885225E9925829FF705B
          5EFE8A8CC3FFA2A8F1FF4146AFFF020138AB0000010E00000002000000000000
          000978471FDD894F22FF884D20FF864C20FF9A602CFF9A612CFF925828FF4623
          0CBB273C72AB5867CDFFA8B0F2FF464CB4FF03023DA90000010C000000000000
          00044C31178675461ED8000000130000001159381B9F9D632EFF864D20FF2010
          065C0000000B233269A4656FDAFFADB6F2FF474DB3FF01013D9E000000000000
          0002130C0629834B1FFF150C053702010110935F2DED9D632FFF703E19E80101
          000E0000000200000006343A7FA36976DCFFAAB3F0FF1C1B9FEC000000000000
          0000000000066D411DC5432811893322105FA26A32FF975D2BFF3D220E8A0000
          0005000000000000000100000005353B79965760C5EE0C0B3755000000000000
          0000000000033D27126C7C4F23D6714B24B7A46D33FF8C5425FC0E0803270000
          0002000000000000000000000001000000020000000300000002000000000000
          0000000000010A0703179E6B32F7A56F35FCA26B33FF623919BA000000060000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000004714D26B1A97338FF9D662EFF2D1C0B5A000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000235251254AA7439FF8A5829E902010009000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000006C6C
          6C6FB8B8B8C2B5B5B5C3B8B8B8C16F6F6F729999999DB5B5B5C2B2B2B2C4B2B2
          B2C4B5B5B5C26868686C00000000000000000000000000000000000000008D8D
          8D93564740F139251DFF675B57E86C6C6C748D8B8BA7473832F5321F19FF311E
          18FF50443DF08787878F00000000000000000000000000000000000000008F8F
          8F91B3B3B3C364534CEF675B56E8949494A2B2B2B2C45C4840F44B3229FF5F4F
          49EEB2B2B2C48989898C00000000000000000000000000000000000000000202
          020286868688908F8EA84E3D36F27D7572D2837975DD4E352CFF4C362EFBA4A2
          A2CA808080830000000000000000000000000000000000000000000000000000
          00000B0B0B0B9B9B9B9E928F8EBA533E36F8584036FB50372DFF7F7673DDADAD
          ADB4121212130000000000000000000000000000000000000000000000000000
          0000000000002F2F2F30B8B8B8C2705D56ED553C31FF5E4C45F2B5B5B5C34C4C
          4C4D000000000000000000000000000000000000000000000000000000000000
          000004040404A2A2A2A59C9693D25A3F35FF573E34FE533E37F8918E8DB69B9B
          9B9E070707070000000000000000000000000000000000000000000000000000
          000061616162B8B7B6C36B5249F75D4137FF81736EE48E8683D457433CF19393
          92A484848486BDBDBDC0B7B7B7C2B4B4B4C3B6B6B6C291919191000000006C6C
          6C6DBCBCBCC08B7C76E161463AFF624A3FF9B3B2B2C5989898A0796C67E46A58
          53EDB7B7B7C2B8B8B8C11B1791FF18138DFF141089FFBFBFBFBF000000006C6C
          6C6D897972E465493EFF63473CFF5E463BFB9F9B9AC08585858975625AED4F36
          2DFF695750EFBABABAC1908FADD21D1B96FF8988A5D3BFBFBFBF000000006C6C
          6C6CBEBEBEC0BDBDBDC0BCBCBCC0BDBDBDC0B1B1B1B286868687BDBDBDC0BBBB
          BBC0BCBCBCC0B0B0B0B3B8B8B8C18E8DAAD2211F9AFFBFBFBFBF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000909090924F51AEF02B2DA8FF494AA8F1BFBFBFBF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000090909091BDBDBDC0BCBCBCC0BDBDBDC091919191}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000030000
          000A0000000E0000000E0000000E0000000F0000000C00000003000000030000
          000D000000110000001100000012000000120000000F00000004000000098E52
          24FF8D5224FF8B5023FF8B5023FF8A4F22FF8A4E22FF0000000C0000000C874C
          21FF864B20FF864A1FFF86491FFF86481FFF85471EFF0000000E0000000B9A60
          2CFF9A5F2CFF995D2BFF995D2AFF985C2AFF8B5023FF0000000E0000000E9559
          27FF955727FF945626FF935626FF935526FF85491FFF00000011000000089C63
          2EFF27180B50000000141B1108479A5E2BFF8E5324FF0000000B0000000C975A
          29FF955A28FF1A10074E0000001B25150A57874B20FF0000000D000000020000
          0007040201135432169B9C612DFF9B602DFF3E24107D00000006000000064228
          127E8E5325FF8B5023FF502E159F040201190000000B00000003000000010000
          0007623D1DAA9F6530FF99602BFA201309450000000600000001000000010000
          000722150A4C8E5425FA8D5124FF5A3317B10000000B00000001000000033623
          1061A16932FFA16731FF53331798000000070000000100000000000000000000
          0001000000085434189B965B29FF8E5224FF301B0C6700000005000000057C50
          27C5A36B33FF9D6530FF1E13093E000000020000000000000000000000000000
          0000000000031F1309419B612DFF935827FF6C3D1CC900000009000000069F6B
          31F7A56D35FF9C632EFF05030212000000010000000000000000000000000000
          000000000001060402159D632FFF9A5F2BFF895122F20000000B00000005A16C
          32F7A77036FF9C642DFF05030211000000010000000000000000000000000000
          000000000001060402159F6630FF9D642FFF8B5326F70000000A000000038057
          2AC4A97237FF9E662FFF1F14093B000000020000000000000000000000000000
          00000000000220150A3EA16831FFA06731FF70451EC700000008000000013A28
          135CAA7439FFA16A32FF58381B93000000040000000000000000000000000000
          000100000005593B1D96A26B33FF9F6630FF33200E6200000003000000000000
          00036D4B24A6A67035FF996730F921160A3D0000000300000001000000010000
          000424180B43A16C34FAA56C35FF663F1EAC0000000700000001000000000000
          00000403010960432091A46E33FF99642EF6462E1675150D0628140D0628462E
          1576A26C33F7A77037FF593B1C940402010D0000000100000000000000000000
          000000000000000000021D140A2E5F401F9385592AD1A26C33FFA56F35FF8B5D
          2FD15F4120951B12093200000004000000010000000000000000000000000000
          0000000000000000000000000001000000020000000300000004000000040000
          0003000000030000000100000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00310000003400000036000000380000003B0000003D00000040000000430000
          0044000000470000004A0000004C000000500000005200000000000000000000
          001400000016000000190000001B0000001D0000001E00000021000000230000
          0026000000280000002A0000002D0000002F0000003200000000000000000000
          00030000000400000005000000060000000700000008000000090000000B0000
          000C0000000D0000000F00000011000000130000001500000000000000000000
          0000000000000000000000000000000000001721AAFF0E1385FF0505338B0000
          00150000000C0000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000002D43D4FF445FF4FF503A31FF4934
          2CFF221714990000001200000003000000000000000000000000000000060000
          000A0000000B0000000B0000000C0000000C19256F8B5D463CFF78594DFF7151
          45FF45396CFF04062FA100000019000000040000000000000000775448BDA575
          64FFA47464FFA47564FFA37463FFBA968AFFC6B0A8FF654D41FFA39596FF6C5D
          99FF5E61E3FF242792FF4E4971F3000000170000000200000000A97969FFEFE3
          DEFFEEE2DBFFEDE1DAFFEDE0D9FFECE0D9FFF0E9E5FFA4948DFF7E7EA6FF9EA7
          F2FF686CE6FF696CE6FF282B98FF070A379D0000000F00000002AD7E6EFFF1E7
          E1FFD2C1B8FF724E3CFF724C3CFF714D3BFFF1E9E4FFB2A098FF605C93FF6E78
          C6FFA7B1F4FF7279E9FF7278E9FF2B309EFF0A0D3E990000000EB18473FFF4EB
          E6FF775141FFF1E9E3FFF1E8E2FF754E40FFF0E7E0FF977B71FFF1EBE8FF8284
          BFFF747FCEFFB0BAF6FF7D85ECFF7D83ECFF3238A4FF0A0E3D8CB68979FFF5EF
          EBFFD8C8C0FF7C5646FF7A5546FF7A5444FFF4ECE6FF795543FFF5EEEBFFF3EE
          EBFF6E6BA4FF7B86D5FFBAC5F8FF8990EFFF8D95EBFF181D85F0BA8E7EFFF7F3
          F0FFF7F2EEFFF7F2EDFFF7F1EDFF7F5949FFF6F0ECFF7F5948FFF5EFEBFFF7F1
          EEFFBAA7A0FF8F93D0FF7B86D8FFC8D5FAFFA7B3EBFF161E7CCCBF9383FFFAF8
          F4FFF9F6F3FF845F4DFF835E4CFFDDD0C9FFF9F3EFFF835D4CFF825D4BFF825D
          4BFFE4DBD5FFF7F4F2FF908BBDFF5F67C9F4333B99CD0406162BC29988FFFCFA
          F7FFFBF9F5FFFBF8F5FFFBF8F5FFFAF7F5FFFAF7F4FF866050FFF9F6F3FFF9F5
          F2FFF9F4F2FFFAF7F4FFDBC5BEFF0000000A0000000400000002C69D8DFFFCFC
          FAFFFDFCFAFFFDFCFAFFFCFBFAFFFCFBF9FFFCFBF9FFA5877AFFFCFAF7FFFCF9
          F7FFFCF9F6FFFCF8F5FFC39888FF0000000500000000000000009E8476BED4B2
          A1FFD4B1A0FFD3B09FFFD2AF9EFFD1AE9DFFD1AC9CFFD0AB9AFFCEA999FFCEA8
          97FFCDA696FFCBA595FF96796DBF000000030000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00030000000C0000001300000014000000140000001500000015000000150000
          001600000016000000150000000E000000030000000000000000000000000000
          000B19437DC32058ABFF1F56ABFF1F56ABFF1E55AAFF1C54ABFF1D54A9FF1C52
          A9FF1B52A8FF1B51A8FF133A78C60000000D0000000000000000000000000000
          000F2763B2FF74CAF6FF60C0F4FF74CAF6FF7CCCF6FF77CAF6FF72C7F5FF6FC5
          F4FF69C3F4FF65C0F3FF1D53A9FF000000120000000000000000000000000000
          000E2A69B3FB7FCBF2FF52A3DBFF306CB2FF2F6BB1FF2E6AB1FF2D69B1FF2D68
          B0FF2C67B0FF2A65B0FF1F4A82C90000000C0000000000000000000000000000
          00081738629A67A6D9FF89D6F9FF4C94CFFF0B2459BA0002052A000000140000
          001100000011000000110000000B000000030000000000000000000000000000
          00010000000B1A3C689F6CACDCFF8DD8F9FF58A8E0FF102E68C5010205180000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000020000000D1B3F68A167ABDCFF78D1F8FF59A9DFFF0D2556A80000
          0007000000000000000000000000000000000000000000000000000000000000
          0000000000010000000B091B4B9F4D8DC5FF7BD4F9FF6EB4E1FF1E4673AA0000
          0007000000000000000000000000000000000000000000000000000000000000
          0001000000090A1F4D9B5694CAFF84DBFAFF7ABEE6FF2B5A8CC2020407160000
          0002000000000000000000000000000000000000000000000000000000000000
          00050C224D94649BCBFF8FE2FCFF81C3E8FF27547FB30204071E0000000D0000
          000B0000000C0000000B00000007000000020000000000000000000000000000
          00074186C5FBA1E3F9FF67B9E4FF3777BCFF3372B7FF316EB5FF2E6AB2FF2C67
          B0FF2A63ADFF2961ACFF1B4278BF000000070000000000000000000000000000
          0007458FCBFFC7F4FEFFC4F2FEFFC3F2FEFFBFF0FEFFBCF0FEFFB9EEFDFFB5ED
          FDFFB2ECFCFFAEEBFCFF3677BBFF000000090000000000000000000000000000
          000431729AC14299D0FF4298D0FF4398CFFF4397CFFF4396CFFF4395CEFF4294
          CEFF4292CDFF4190CCFF2C6090C3000000060000000000000000000000000000
          0001000000030000000600000006000000060000000600000007000000070000
          0007000000080000000700000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000070000
          000B0000000B0000000B0000000C0000000C0000000C0000000C0000000C0000
          000C0000000C0000000C0000000C0000000C00000008000000027F5447BEB074
          62FFAF7461FFAF7460FFAE735FFFAE725FFFAD715EFFAC705CFFAC705CFFAC70
          5CFFAC6F5BFFAC6F5BFFAB6F5BFFAB6E5AFF7B4F40BF00000008B67E6CFFFBF6
          F3FFFAF5F2FFFAF5F2FFFAF5F2FFFAF5F1FFFAF4F1FFF9F4F0FFF9F4F0FFF9F4
          F0FFF9F3F0FFF8F3EFFFF9F3EFFFF8F2EEFFB37968FF0000000BB77F6FFFFBF7
          F4FFF7EFE9FFF6EEE9FFF6EEE9FFF6EEE9FFF6EEE9FFF6EEE8FFF6EEE8FFF6EE
          E8FFF6EEE8FFF6EDE7FFF5EDE7FFF9F4F0FFB47B6AFF0000000BB88171FFFBF8
          F5FFF7F0EBFFC6A092FFC6A093FFC69F91FFC59F92FFC59E90FFC59E90FFC59E
          90FFC59E91FFC49E8FFFF6EEE9FFFAF4F2FFB47C6BFF0000000BB98472FFFCF9
          F7FFF8F0ECFFF8F1ECFFF7F0ECFFF7F0EBFFF8F0EBFFF7EFEAFFF7F0EAFFF7EF
          EAFFF7EFE9FFF7EFE9FFF6EFEAFFFAF6F3FFB67F6DFF0000000ABB8674FFFDFA
          F8FFF8F1EDFFC9A697FFC9A497FFC9A497FFC9A396FFC8A395FFC8A396FFC8A2
          95FFC7A295FFC7A394FFF7F0EAFFFAF7F5FFB7806FFF00000009BB8674FFFDFA
          F8FFF8F1EDFFF9F3EFFFF9F2EFFFF8F2EEFFF8F2EDFFF8F1ECFFF8F1ECFFF8F1
          ECFFF8F0ECFFF7F0ECFFF7F0EAFFFAF7F5FFB7806FFF00000009BC8877FFFDFB
          F9FFF9F3EFFFCCA99CFFCCA89CFFCBA89BFFCBA89BFFCBA79AFFCAA79AFFCAA6
          9AFFCAA699FFCAA699FFF7F0ECFFFBF8F6FFB98271FF00000008C08D7DFFFEFD
          FCFFFAF5F2FFFAF6F3FFFAF6F2FFFAF5F2FFFAF5F1FFF9F4F1FFF9F4F0FFF9F3
          F0FFF8F3EFFFF8F3EFFFF9F3F0FFFDFBFAFFBC8877FF00000008C28F7EFFFEFD
          FDFFFBF6F3FFCFADA2FFCFAFA0FFCFAEA1FFCFADA1FFCEAC9FFFCFAC9FFFCEAC
          9EFFCDAB9FFFCEAA9EFFFAF4F0FFFDFCFAFFBE8A79FF00000007C39281FFFEFE
          FDFFFBF7F4FFFBF7F4FFFAF7F5FFFBF7F4FFFAF6F4FFFAF5F3FFFAF6F3FFFAF6
          F2FFF9F5F2FFF9F5F2FFF9F6F2FFFDFCFCFFBF8D7BFF00000007C49483FFFFFE
          FEFFFBF8F5FFD1B2A6FFD1B2A6FFD2B1A5FFD2B1A5FFD1B0A4FFD1B1A4FFD1B0
          A3FFD1AFA3FFD0B0A3FFFAF6F3FFFEFDFCFFC08F7EFF00000006C59584FFFFFE
          FEFFFCF8F7FFFBF8F6FFFCF8F6FFFBF8F5FFFBF8F6FFFCF8F5FFFCF8F5FFFBF8
          F5FFFBF7F4FFFBF7F4FFFBF7F4FFFEFDFDFFC2907FFF00000005C69786FFFFFF
          FEFFFFFFFEFFFFFEFEFFFFFFFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFFFEFEFFFFFEFDFFFEFEFDFFC39281FF000000059A796DBFCFA4
          94FFCFA393FFCEA393FFCEA393FFCEA292FFCEA292FFCDA191FFCDA191FFCDA0
          90FFCCA090FFCC9F8FFFCB9E8EFFCB9E8EFF967569C000000003}
      end>
  end
  object pm: TPopupMenu
    Images = il2
    Left = 138
    Top = 268
    object N4: TMenuItem
      Caption = #24050#23545#24080
      ImageIndex = 4
    end
    object N1: TMenuItem
      Caption = #24050#32467#31639
    end
    object N5: TMenuItem
      Caption = #24050#28165#24080'('#20923#32467')'
      ImageIndex = 2
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #24050#23457#21512
      ImageIndex = 9
    end
    object N3: TMenuItem
      Caption = #26410#23457#21512
    end
  end
  object pm2: TPopupMenu
    Left = 208
    Top = 264
    object N7: TMenuItem
      Caption = #20840#36873
    end
    object N8: TMenuItem
      Caption = #19981#36873
    end
  end
  object Print: TPopupMenu
    Images = il2
    Left = 178
    Top = 268
    object MenuItem5: TMenuItem
      Caption = #30452#25509#25171#21360
      ImageIndex = 11
    end
    object MenuItem6: TMenuItem
      Caption = #35774#35745#25253#34920
    end
  end
  object cxImageList1: TcxImageList
    FormatVersion = 1
    DesignInfo = 21233771
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E000000100000001000000010000000100000001000000011000000110000
          001100000011000000100000000B00000003000000000000000019427ACA245A
          A5FF255CA7FF255BA7FF245AA6FF2459A6FF2358A5FF2358A4FF2356A4FF2256
          A4FF2255A3FF2154A3FF2153A1FF1C468AE303080F2900000002255DA5FF316B
          AEFF6DA6D5FF86CAF0FF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
          E1FF389BE0FF369AE0FF3498DFFF2C77C1FF10284D8B000000082B68AEFF4984
          BEFF4B8BC5FFB2E3F8FF68BBECFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
          E5FF47A6E4FF44A4E4FF41A2E3FF3A92D6FF1C4885D50000000D2F6FB4FF6CA7
          D2FF3F87C4FFAED9F0FF9AD8F5FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
          EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2D69B1FF040B142F3276B9FF8FC7
          E6FF509FD4FF86BCE0FFC5EFFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
          EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF448BC9FF122D4D81357CBCFFAFE3
          F5FF75C8EDFF59A2D4FFDDF7FDFFDFF8FEFFDDF7FEFFDBF7FEFFD8F5FEFFD4F4
          FDFFD0F2FDFFCCEFFCFFC7EDFBFFC1EBFBFF9ACBE9FF215187CB3882C1FFC7F5
          FEFF97E5FCFF64BAE5FF4D9FD3FF4D9DD2FF4B9BD1FF4A99CFFF4998CFFF4896
          CEFF4694CCFF4592CBFF3073B7FF3072B6FF2F71B5FF2A65A4EA3A88C5FFCDF7
          FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFF89DFFBFF86DEFAFF81DAFAFF7ED8
          F9FF7BD7F9FF79D6F9FF2A6BB0FF000000140000000A000000073D8EC8FFD0F8
          FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFBBF2FDFFD4F9FEFFD5F9FEFFD3F8
          FEFFD1F8FEFFCEF7FDFF3680BFFF0000000800000000000000003F92CBFFD3F9
          FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF4895CBFF3B8CC6FF3B8AC6FF3A89
          C5FF3A88C5FF3A87C3FF2A6391C20000000500000000000000004197CEFFE2FC
          FEFFE2FCFEFFE1FCFEFFD4F3FAFF458FBFEC040A0E1B00000006000000060000
          000600000006000000060000000400000001000000000000000031739ABF429A
          D0FF4299D0FF4299D0FF4297CFFF153244590000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0003000000030000000400000003000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000070000
          000B0000000B0000000B0000000C0000000C0000000C0000000C0000000C0000
          000C0000000C0000000C0000000C0000000C00000008000000027F5447BEB074
          62FFAF7461FFAF7460FFAE735FFFAE725FFFAD715EFFAC705CFFAC705CFFAC70
          5CFFAC6F5BFFAC6F5BFFAB6F5BFFAB6E5AFF7B4F40BF00000008B67E6CFFFBF6
          F3FFFAF5F2FFFAF5F2FFFAF5F2FFFAF5F1FFFAF4F1FFF9F4F0FFF9F4F0FFF9F4
          F0FFF9F3F0FFF8F3EFFFF9F3EFFFF8F2EEFFB37968FF0000000BB77F6FFFFBF7
          F4FFF7EFE9FFF6EEE9FFF6EEE9FFF6EEE9FFF6EEE9FFF6EEE8FFF6EEE8FFF6EE
          E8FFF6EEE8FFF6EDE7FFF5EDE7FFF9F4F0FFB47B6AFF0000000BB88171FFFBF8
          F5FFF7F0EBFFC6A092FFC6A093FFC69F91FFC59F92FFC59E90FFC59E90FFC59E
          90FFC59E91FFC49E8FFFF6EEE9FFFAF4F2FFB47C6BFF0000000BB98472FFFCF9
          F7FFF8F0ECFFF8F1ECFFF7F0ECFFF7F0EBFFF8F0EBFFF7EFEAFFF7F0EAFFF7EF
          EAFFF7EFE9FFF7EFE9FFF6EFEAFFFAF6F3FFB67F6DFF0000000ABB8674FFFDFA
          F8FFF8F1EDFFC9A697FFC9A497FFC9A497FFC9A396FFC8A395FFC8A396FFC8A2
          95FFC7A295FFC7A394FFF7F0EAFFFAF7F5FFB7806FFF00000009BB8674FFFDFA
          F8FFF8F1EDFFF9F3EFFFF9F2EFFFF8F2EEFFF8F2EDFFF8F1ECFFF8F1ECFFF8F1
          ECFFF8F0ECFFF7F0ECFFF7F0EAFFFAF7F5FFB7806FFF00000009BC8877FFFDFB
          F9FFF9F3EFFFCCA99CFFCCA89CFFCBA89BFFCBA89BFFCBA79AFFCAA79AFFCAA6
          9AFFCAA699FFCAA699FFF7F0ECFFFBF8F6FFB98271FF00000008C08D7DFFFEFD
          FCFFFAF5F2FFFAF6F3FFFAF6F2FFFAF5F2FFFAF5F1FFF9F4F1FFF9F4F0FFF9F3
          F0FFF8F3EFFFF8F3EFFFF9F3F0FFFDFBFAFFBC8877FF00000008C28F7EFFFEFD
          FDFFFBF6F3FFCFADA2FFCFAFA0FFCFAEA1FFCFADA1FFCEAC9FFFCFAC9FFFCEAC
          9EFFCDAB9FFFCEAA9EFFFAF4F0FFFDFCFAFFBE8A79FF00000007C39281FFFEFE
          FDFFFBF7F4FFFBF7F4FFFAF7F5FFFBF7F4FFFAF6F4FFFAF5F3FFFAF6F3FFFAF6
          F2FFF9F5F2FFF9F5F2FFF9F6F2FFFDFCFCFFBF8D7BFF00000007C49483FFFFFE
          FEFFFBF8F5FFD1B2A6FFD1B2A6FFD2B1A5FFD2B1A5FFD1B0A4FFD1B1A4FFD1B0
          A3FFD1AFA3FFD0B0A3FFFAF6F3FFFEFDFCFFC08F7EFF00000006C59584FFFFFE
          FEFFFCF8F7FFFBF8F6FFFCF8F6FFFBF8F5FFFBF8F6FFFCF8F5FFFCF8F5FFFBF8
          F5FFFBF7F4FFFBF7F4FFFBF7F4FFFEFDFDFFC2907FFF00000005C69786FFFFFF
          FEFFFFFFFEFFFFFEFEFFFFFFFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFFFEFEFFFFFEFDFFFEFEFDFFC39281FF000000059A796DBFCFA4
          94FFCFA393FFCEA393FFCEA393FFCEA292FFCEA292FFCDA191FFCDA191FFCDA0
          90FFCCA090FFCC9F8FFFCB9E8EFFCB9E8EFF967569C000000003}
      end>
  end
  object BMDThread1: TBMDThread
    UpdateEnabled = False
    Left = 272
    Top = 264
  end
end
