unit ufrmSetTable;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxContainer, cxGroupBox;

type
  TfrmSetTable = class(TForm)
    cxGroupBox1: TcxGroupBox;
    Grid: TcxGrid;
    tvViewTable: TcxGridDBTableView;
    tvViewTableColumn1: TcxGridDBColumn;
    tvViewTableColumn3: TcxGridDBColumn;
    tvViewTableColumn4: TcxGridDBColumn;
    tvViewTableColumn2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    cxGroupBox2: TcxGroupBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSetTable: TfrmSetTable;

implementation

{$R *.dfm}

end.
