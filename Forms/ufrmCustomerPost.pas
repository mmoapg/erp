unit ufrmCustomerPost;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxLabel, cxClasses, Vcl.ExtCtrls;

type
  TfrmCustomerPost = class(TForm)
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    bvl1: TBevel;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomerPost: TfrmCustomerPost;

implementation

{$R *.dfm}

uses
   uDataModule;

procedure TfrmCustomerPost.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomerPost.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomerPost.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13: cxButton1.Click;
    27: cxButton2.Click;
  end;
end;

end.
