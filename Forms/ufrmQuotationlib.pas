﻿unit ufrmQuotationlib;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxLabel, RzButton,
  RzPanel, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Menus, cxMaskEdit, cxDropDownEdit,
  Datasnap.DBClient,ufrmBaseController, dxCore, dxCoreClasses, dxHashUtils,
  dxSpreadSheetCore, dxSpreadSheetCoreHistory,
  dxSpreadSheetConditionalFormatting, dxSpreadSheetConditionalFormattingRules,
  dxSpreadSheetClasses, dxSpreadSheetContainers, dxSpreadSheetFormulas,
  dxSpreadSheetHyperlinks, dxSpreadSheetFunctions, dxSpreadSheetGraphics,
  dxSpreadSheetPrinting, dxSpreadSheetTypes, dxSpreadSheetUtils,
  dxBarBuiltInMenu, dxSpreadSheet, Vcl.ExtDlgs, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids,UtilsTree,FillThrdTree,UnitADO, cxPC, RzTabs, cxMemo,
  cxCurrencyEdit, Vcl.StdCtrls, cxButtons, cxGridBandedTableView,
  cxGridDBBandedTableView;

type
  TfrmQuotationlib = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    Splitter2: TSplitter;
    RzPanel10: TRzPanel;
    tv: TTreeView;
    RzToolbar6: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzPanel9: TRzPanel;
    RzToolbar5: TRzToolbar;
    RzSpacer13: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzToolButton16: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzToolButton1: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    Grid: TcxGrid;
    Lv1: TcxGridLevel;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzSpacer10: TRzSpacer;
    StatusBar1: TStatusBar;
    dsMaster: TClientDataSet;
    dsMasterSource: TDataSource;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    tvView1: TcxGridDBBandedTableView;
    tvView1Col1: TcxGridDBBandedColumn;
    tvView1Col2: TcxGridDBBandedColumn;
    tvView1Col3: TcxGridDBBandedColumn;
    tvView1Col4: TcxGridDBBandedColumn;
    tvView1Col5: TcxGridDBBandedColumn;
    tvView1Col6: TcxGridDBBandedColumn;
    tvView1Col7: TcxGridDBBandedColumn;
    tvView1Col8: TcxGridDBBandedColumn;
    tvView1Col9: TcxGridDBBandedColumn;
    tvView1Col10: TcxGridDBBandedColumn;
    tvView1Col11: TcxGridDBBandedColumn;
    tvView1Col12: TcxGridDBBandedColumn;
    tvView1Col13: TcxGridDBBandedColumn;
    tvView1Col14: TcxGridDBBandedColumn;
    procedure RzToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N7Click(Sender: TObject);
    procedure RzToolButton18Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure RzToolButton23Click(Sender: TObject);
    procedure RzToolButton24Click(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure tvView1Col1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure dsMasterAfterInsert(DataSet: TDataSet);
    procedure tvView1Col4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvView1DblClick(Sender: TObject);
    procedure tvView2Column3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton5Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure tvView1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvView2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvView3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dsMasterAfterOpen(DataSet: TDataSet);
    procedure dsMasterAfterClose(DataSet: TDataSet);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvClick(Sender: TObject);
    procedure tvView4Col1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvView4DblClick(Sender: TObject);
    procedure tvView4Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvView4EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvView1Col9PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView1Col10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView1Col11PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView1GetCellHeight(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
    procedure N4Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
  private
    { Private declarations }
    dwSQLText1 : string;
    dwSQLText2 : string;
    dwSQLText3 : string;

    g_SelectNodeId : Integer;
    g_TreeView : TNewUtilsTree;
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    procedure ReadData();
    procedure CalculDate(lpIndex : Byte);
  public
    { Public declarations }
    //全局变量
    NextClipHwnd:HWND; //剪贴板观察链中下一个窗口句柄
    ExcelApp : Variant;
    FileDirectory:string;
    ListINFO : TStringList;
    dwADO : TADO;
    dwOfferType : Byte;
  end;

var
  frmQuotationlib: TfrmQuotationlib;

implementation

uses
    uDataModule,Clipbrd,global,ComObj;//加入clipbrd单元

{$R *.dfm}

procedure TfrmQuotationlib.CalculDate(lpIndex : Byte);
var
  lvArtificialPrice : Currency;
  lvMaterialPrice : Currency;
  lvCostUnitPrice : Currency;
  t : Currency;
  label A;

begin
  if tvView1Col9.EditValue <> null then
  begin
    lvArtificialPrice := StrToCurrDef(VarToStr( tvView1Col9.EditValue ) , 0);
  end;
  if tvView1Col10.EditValue <> null then
  begin
    lvMaterialPrice   := StrToCurrDef(VarToStr( tvView1Col10.EditValue ) , 0);
  end;
  if tvView1Col11.EditValue <> null then
  begin
    lvCostUnitPrice   := StrToCurrDef(VarToStr( tvView1Col11.EditValue ) , 0);
  end;
  case lpIndex of
    0: tvView1Col11.EditValue := lvArtificialPrice + lvMaterialPrice;
    1:
    begin

      if lvCostUnitPrice <> 0 then
      begin
        if ( lvArtificialPrice <> 0) and (lvMaterialPrice <> 0) then
        begin
          goto a;
        end else
        begin

          if lvArtificialPrice <> 0 then
          begin
            t := lvCostUnitPrice - lvArtificialPrice;  //等于材料
            tvView1Col10.EditValue := t;
          end else
          begin
            if lvMaterialPrice <> 0 then
            begin
              t := lvCostUnitPrice - lvMaterialPrice; //等于人工单价
              tvView1Col9.EditValue := t;
            end;
          end;

        end;
A:
      end;

    end;

  end;

end;

procedure TfrmQuotationlib.cxButton1Click(Sender: TObject);
begin
  inherited;
  ReadData;
end;

procedure TfrmQuotationlib.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    ReadData;
  end;
end;

procedure TfrmQuotationlib.dsMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  RzToolButton15.Enabled := False;
  RzToolButton16.Enabled := False;
  RzToolButton17.Enabled := False;

  RzToolButton2.Enabled := False;
  RzToolButton5.Enabled := False;
  RzToolButton6.Enabled := False;

  N1.Enabled := False;
  N2.Enabled := False;
  N3.Enabled := False;
  N4.Enabled := False;
  N6.Enabled := False;
  N7.Enabled := False;
  N9.Enabled := False;
  N10.Enabled:= False;

  Self.cxTextEdit1.Enabled := False;
  Self.cxButton1.Enabled := False;
end;

procedure TfrmQuotationlib.dsMasterAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('Sign_Id').Value := g_SelectNodeId;
    FieldByName('tage').Value    := dwOfferType ;// Self.RzTab.Tabs[ Self.RzTab.TabIndex ].Tag;;
    tvView1Col13.EditValue := Date;
  end;
end;

procedure TfrmQuotationlib.dsMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  RzToolButton15.Enabled := True;
  RzToolButton16.Enabled := True;
  RzToolButton17.Enabled := True;

  RzToolButton2.Enabled := True;
  RzToolButton5.Enabled := True;
  RzToolButton6.Enabled := True;

  N1.Enabled := True;
  N2.Enabled := True;
  N3.Enabled := True;
  N4.Enabled := True;
  N6.Enabled := True;
  N7.Enabled := True;
  N9.Enabled := True;
  N10.Enabled:= True;

  Self.cxTextEdit1.Enabled := True;
  Self.cxButton1.Enabled := True;
end;

procedure TfrmQuotationlib.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//从剪贴板观察链中删除本观察窗口
  ChangeClipboardChain(Handle,NextClipHwnd);
  //将WM_DRAWCLIPBOARD消息传递到下一个观察链中的窗口
  SendMessage(NextClipHwnd,WM_CHANGECBCHAIN,Handle,NextClipHwnd);
end;

procedure TfrmQuotationlib.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {
  if (ssCtrl in Shift) then
  begin
    case Key of
      67: Self.RzToolButton3.Click;
      86: Self.RzToolButton4.Click;
    end;
  end;
  }
  if Key = 27 then
    Self.RzToolButton1.Click;

end;

procedure TfrmQuotationlib.FormShow(Sender: TObject);
begin
  //获得剪贴板观察链中下一个窗口句柄，并将句柄注册到剪贴板观察链中
  NextClipHwnd := SetClipBoardViewer(Handle);

  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                     DM.ADOconn,
                                     g_Table_Maintain_QuotationTree,
                                     '报价分类');
  g_TreeView.GetTreeTable(0);

  tvView1Col6.Visible := False;  //综合单价
  tvView1Col7.Visible := False;  //人工单价
  tvView1Col8.Visible := False;  //主材报价
  tvView1Col3.Visible := False;  //品牌
  tvView1Col4.Visible := False;  //规格
  tvView1.Bands[1].Visible := False;
  {
  tvView1Col9.Visible := False;  //人工报价
  tvView1Col10.Visible := False;  //材料单价
  tvView1Col11.Visible := False;  //成本单价
  }
end;

procedure TfrmQuotationlib.N10Click(Sender: TObject);
begin
  inherited;
  RzToolButton6.Click;
end;

procedure TfrmQuotationlib.N11Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmQuotationlib.N2Click(Sender: TObject);
begin
  inherited;
  RzToolButton2.Click;
end;

procedure TfrmQuotationlib.N3Click(Sender: TObject);
begin
  inherited;
  RzToolButton16.Click;
end;

procedure TfrmQuotationlib.N4Click(Sender: TObject);
begin
  inherited;
  RzToolButton17.Click;
end;

procedure TfrmQuotationlib.N7Click(Sender: TObject);
var
  row,col:TStringList;
  r,c:Integer;
  Copystrings:Tstringlist;
  s : string;
  i,j,row1,row2:Integer;

begin
  s := SplitClipboardStr(Clipboard.AsText);
  row:=TStringList.Create;
  row:=SplitString(s,'@');

  for i := 0 to  row.Count - 2 do
  begin
    Application.ProcessMessages;
    if Length(  row[i] ) <> 0 then
    begin
      col:=splitstring(row[i],'|');

      if col.Count >= 6 then
      begin
        with Self.dsMaster do
        begin
          if State = dsInactive then
          begin
            ShowMessage('未选择报价分类');
            Exit;
          end;
          Insert;

          Self.tvView1Col13.EditValue  := StrToDateDef(col[0] , Date); //日期
          Self.tvView1Col14.EditValue  := col[1]; //编制人
          Self.tvView1Col2.EditValue   := col[2]; //项目名称

          if dwOfferType = 1 then
          begin
            Self.tvView1Col3.EditValue   := col[3]; //品牌
            Self.tvView1Col4.EditValue   := col[4]; //规格
            Self.tvView1Col5.EditValue   := col[5]; //单位
          end else
          begin
            Self.tvView1Col5.EditValue   := col[3]; //单位
          end;

          case dwOfferType of
           1:
           begin
              if col.Count = 9 then
              begin
                tvView1Col7.EditValue  := DeleteMoney( col[6] );  //主材单价
                tvView1Col11.EditValue := DeleteMoney( col[7] ); //成本单价
                Self.tvView1Col12.EditValue  := StringReplace(col[8],'"','',[rfReplaceAll]);//施工工艺
              end;
              {
              //  dwOfferType := 1;  //主材报价
              Self.tvView1Col7.EditValue   := DeleteMoney( col[5] ); //主材报价
              Self.tvView1Col9.EditValue   := DeleteMoney( col[7] ); //人工单价
              Self.tvView1Col10.EditValue  := DeleteMoney( col[8] ); //材料成本
              Self.tvView1Col11.EditValue  := DeleteMoney( col[9] ); //成本单价
              }
              {
              tvView1Col7.Visible := True;  //主材单价
              tvView1Col11.Visible := True; //成本单价
              }
           end;
           2:
           begin
              if col.Count = 6 then
              begin
                tvView1Col6.EditValue  := DeleteMoney( col[4] );     //综合单价
                tvView1Col12.EditValue  := StringReplace(col[5],'"','',[rfReplaceAll]);//施工工艺
              end;
              {
              //  dwOfferType := 2;  //费用报价
              Self.tvView1Col6.EditValue   := DeleteMoney( col[4] ); //综合单价
              Self.tvView1Col7.EditValue   := DeleteMoney( col[5] ); //主材报价
              Self.tvView1Col8.EditValue   := DeleteMoney( col[6] ); //人工报价
              Self.tvView1Col9.EditValue   := DeleteMoney( col[7] ); //人工单价
              Self.tvView1Col10.EditValue  := DeleteMoney( col[8] ); //材料成本
              Self.tvView1Col11.EditValue  := DeleteMoney( col[9] ); //成本单价
              }
              {
              tvView1Col6.Visible  := True;     //综合单价
              }
           end;
           3:
           begin
              if col.Count = 9 then
              begin
                tvView1Col6.EditValue     := DeleteMoney( col[4] );  //综合单价
                tvView1Col10.EditValue    := DeleteMoney( col[5] );  //材料单价
                tvView1Col9.EditValue     := DeleteMoney( col[6] );  //人工报价
                tvView1Col11.EditValue    := DeleteMoney( col[7] );  //成本单价
                tvView1Col12.EditValue  := StringReplace( col[8],'"','',[rfReplaceAll]);//施工工艺
              end;
              {
              //  dwOfferType := 3;  //综合报价
              Self.tvView1Col6.EditValue   := DeleteMoney( col[4] ); //综合单价
              Self.tvView1Col7.EditValue   := DeleteMoney( col[5] ); //主材报价
              Self.tvView1Col8.EditValue   := DeleteMoney( col[6] ); //人工报价
              Self.tvView1Col9.EditValue   := DeleteMoney( col[7] ); //人工单价
              Self.tvView1Col10.EditValue  := DeleteMoney( col[8] ); //材料成本
              Self.tvView1Col11.EditValue  := DeleteMoney( col[9] ); //成本单价
              Self.tvView1Col12.EditValue  := StringReplace(col[10],'"','',[rfReplaceAll]);//施工工艺

              }
              {
              tvView1Col6.Visible      := True;   //综合单价
              tvView1.Bands[1].Visible := True;
              tvView1Col10.Visible     := True;  //材料单价
              tvView1Col9.Visible      := True;  //人工报价
              tvView1Col11.Visible     := True;  //成本单价
              }
           end;
           4:
           begin
             if col.Count = 7 then
             begin
               tvView1Col8.EditValue :=  DeleteMoney( col[4] );  //人工报价
               tvView1Col11.EditValue:=  DeleteMoney( col[5] );  //成本单价
               Self.tvView1Col12.EditValue  := StringReplace(col[6],'"','',[rfReplaceAll]);//施工工艺
             end;
             {
             Self.tvView1Col6.EditValue   := DeleteMoney( col[4] ); //综合单价
             Self.tvView1Col7.EditValue   := DeleteMoney( col[5] ); //主材报价
             Self.tvView1Col8.EditValue   := DeleteMoney( col[6] ); //人工报价
             Self.tvView1Col9.EditValue   := DeleteMoney( col[7] ); //人工单价
             Self.tvView1Col10.EditValue  := DeleteMoney( col[8] ); //材料成本
             Self.tvView1Col11.EditValue  := DeleteMoney( col[9] ); //成本单价
             Self.tvView1Col12.EditValue  := StringReplace(col[10],'"','',[rfReplaceAll]);//施工工艺
              }
             {
              tvView1Col8.Visible       := True;  //人工报价
              tvView1.Bands[1].Visible  := True;
              tvView1Col11.Visible      := True;  //成本单价
             }
           end;
          end;

        end;

      end;

    end;

  end;

end;

procedure TfrmQuotationlib.N9Click(Sender: TObject);
begin
  inherited;
  RzToolButton5.Click;
end;

procedure TfrmQuotationlib.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  s : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
    //  s := GetNewBHStr(g_Table_CompanyTree,'ID','SIGN',10);
      g_TreeView.AddChildNode(s,'',Date,m_OutText);
    end;
  end;

end;

procedure TfrmQuotationlib.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  S := 'UPDATE ' + g_Table_Maintain_QuotationTree +
  ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;

function TfrmQuotationlib.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmQuotationlib.ReadData();
var
  szSQLText : string;
  szSearch  : string;
  szSearchText: string;

begin
  szSearch := Self.cxTextEdit1.Text;
  if szSearch <> '' then
    szSearchText := ' AND ProjectName like "%' + szSearch + '%"';

  szSQLText := 'Select * from '+ g_Table_Maintain_QuotationList +
               ' WHERE Sign_Id=' + IntToStr(g_SelectNodeId) +
               ' AND tage=' + IntToStr(dwOfferType) + szSearchText;

  dwSQLText1 := szSQLText;
  dwADO.OpenSQL(Self.dsMaster,dwSQLText1); //六项
end;

procedure TfrmQuotationlib.tvClick(Sender: TObject);
var
  PData: PNodeData;
  Node : TTreeNode;
begin
  inherited;
  Node := tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level >= 1 then
    begin
      PData := PNodeData(Node.Data);
      if Assigned(PData) then
      begin
        g_SelectNodeId := PData^.Index;
        Self.cxTextEdit1.Text := '';
        if Node.Text = '主材报价' then
        begin
          dwOfferType := 1;

          tvView1Col6.Visible := False; //综合单价
          tvView1Col7.Visible := True;  //主材单价
          tvView1Col8.Visible := False; //人工报价
          tvView1Col3.Visible := True; //品牌
          tvView1Col4.Visible := True; //规格

          tvView1.Bands[1].Visible := True;
          tvView1Col10.Visible := False; //材料单价
          tvView1Col9.Visible  := False;//人工报价
          tvView1Col11.Visible := True; //成本单价

        end else
        if Node.Text = '费用报价' then
        begin
          dwOfferType := 2;

          tvView1Col6.Visible  := True;     //综合单价
          tvView1Col3.Visible  := False; //品牌
          tvView1Col4.Visible  := False; //规格


          tvView1Col7.Visible  := False;    //主材报价
          tvView1Col8.Visible  := False;    //人工报价
          tvView1.Bands[1].Visible := False;
          {
          tvView1Col10.Visible := False;  //材料单价
          tvView1Col9.Visible  := False;  //人工报价
          tvView1Col11.Visible := False;  //成本单价
          }
        end else
        if Node.Text = '综合报价' then
        begin
          dwOfferType := 3;

          tvView1Col6.Visible := True;   //综合单价
          tvView1Col7.Visible := False;  //主材报价
          tvView1Col8.Visible := False;  //人工报价
          tvView1Col3.Visible := False; //品牌
          tvView1Col4.Visible := False; //规格


          tvView1.Bands[1].Visible := True;
          tvView1Col10.Visible := True;  //材料单价
          tvView1Col9.Visible  := True;  //人工报价
          tvView1Col11.Visible := True;  //成本单价

        end else
        if Node.Text = '人工报价' then
        begin
          dwOfferType := 4;
          tvView1Col3.Visible := False; //品牌
          tvView1Col4.Visible := False; //规格

          tvView1Col6.Visible := False;  //综合单价
          tvView1Col7.Visible := False;   //主材单价
          tvView1Col8.Visible := True;  //人工报价

          tvView1.Bands[1].Visible := True;
          tvView1Col10.Visible := false;  //材料单价
          tvView1Col9.Visible  := False;  //人工报价
          tvView1Col11.Visible := True;  //成本单价

        end;
        ReadData;//读数据

      end;
    end;
  end;

end;

procedure TfrmQuotationlib.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  inherited;
  if Node.Level>1 then
  begin
    IstreeEdit(s,Node);
  end;
end;

procedure TfrmQuotationlib.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.tv.Selected.Parent.Selected := True;
    Self.RzToolButton18.Click;
  end;
end;

procedure TfrmQuotationlib.tvView1Col10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  Self.tvView1Col10.EditValue := StrToCurrDef(VarToStr(DisplayValue),0);
  CalculDate(0);
end;

procedure TfrmQuotationlib.tvView1Col11PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  Self.tvView1Col11.EditValue := StrToCurrDef(VarToStr(DisplayValue),0);
  if dwOfferType = 3 then CalculDate(1);
end;

procedure TfrmQuotationlib.tvView1Col1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmQuotationlib.tvView1Col4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  DD1 , DD2 , DD3 : Variant;
  szMoney : Currency;
  s : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    szMoney := StrToCurrDef(s,0);
    {
    Self.tvView1Col4.EditValue := szMoney;
    Self.tvView1Col5.EditValue := szMoney * 40 / 100; //人工
    Self.tvView1Col6.EditValue := szMoney * 10 / 100; //辅料
    Self.tvView1Col7.EditValue := szMoney * 30 / 100; //主料
    Self.tvView1Col8.EditValue := szMoney * 5  / 100; //损耗
    Self.tvView1Col9.EditValue := szMoney * 10 / 100; //机械加工
    Self.tvView1Col10.EditValue:= szMoney * 5  / 100; //运输存储
    }
  end;
end;

procedure TfrmQuotationlib.tvView1Col9PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  Self.tvView1Col9.EditValue := StrToCurrDef(VarToStr(DisplayValue),0);
  CalculDate(0);
end;

procedure TfrmQuotationlib.tvView1DblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure IsEditing( DataSet : TDataSet ; var  AAllow : Boolean);
begin
  with DataSet do
  begin
    if (State <> dsInactive) and (State <> dsEdit) and (State <> dsInsert) then
    begin
      AAllow := False;
    end else
    begin
      AAllow := True;
    end;
  end;

end;

procedure TfrmQuotationlib.tvView1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szDataSet : TDataSet;

begin
  inherited;
  szDataSet := (Sender as TcxGridDBTableView).DataController.DataSet;
  IsEditing( szDataSet ,AAllow);
end;

procedure TfrmQuotationlib.tvView1EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {
  if (AItem.Index = tvView1Col10.Index) and (Key = 13) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOInsert(Self.dsMaster1) ;
      Self.Grid.SetFocus;
      Self.tvView1Col1.Editing := True;
      Self.tvView1Col1.FocusWithSelection;
    end;
  end;
  }
end;

procedure TfrmQuotationlib.tvView1GetCellHeight(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
begin
  inherited;
  if AHeight < 23 then AHeight := 23;
end;

procedure TfrmQuotationlib.tvView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
    Self.RzToolButton17.Click;
end;

procedure TfrmQuotationlib.tvView2Column3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szMoney : Currency;
begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    szMoney := StrToCurrDef(s,0);
    {
    Self.tvView2Column3.EditValue := DisplayValue;
    Self.tvView2Column4.EditValue := szMoney * 45 / 100; //人工
    Self.tvView2Column5.EditValue := szMoney * 10 / 100; //辅料
    Self.tvView2Column6.EditValue := szMoney * 40 / 100; //主料
    Self.tvView2Column7.EditValue := szMoney * 5  / 100; //损耗
    }
  end;
end;

procedure TfrmQuotationlib.tvView2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {
  if (AItem.Index = tvView2Column7.Index) and (Key = 13) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOInsert(Self.dsMaster2) ;
      Self.Grid.SetFocus;
      Self.tvView2Column1.Editing := True;
      Self.tvView2Column1.FocusWithSelection;
    end;
  end;
  }
end;

procedure TfrmQuotationlib.tvView3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {
  if (AItem.Index = tvView3Column3.Index) and (Key = 13) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOInsert(Self.dsMaster3) ;
      Self.Grid.SetFocus;
      Self.tvView3Column1.Editing := True;
      Self.tvView3Column1.FocusWithSelection;
    end;
  end;
  }
end;

procedure TfrmQuotationlib.tvView4Col1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmQuotationlib.tvView4DblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmQuotationlib.tvView4Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var szDataSet : TDataSet;
begin
  inherited;
  szDataSet := (Sender as TcxGridDBBandedTableView).DataController.DataSet;
  IsEditing( szDataSet ,AAllow);
end;

procedure TfrmQuotationlib.tvView4EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (AItem.Index = tvView1Col10.Index) and (Key = 13) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOInsert(Self.dsMaster) ;
      Self.Grid.SetFocus;
      Self.tvView1Col1.Editing := True;
      Self.tvView1Col1.FocusWithSelection;
    end;
  end;
end;

procedure TfrmQuotationlib.RzToolButton15Click(Sender: TObject);
  function IsDelete(View : TcxGridDBBandedTableView):Boolean;
  begin
    Result := False;
    with View do
    begin
      if DataController.RowCount <> 0 then
      begin
        if Application.MessageBox('确认删除当前记录?','确认删除',MB_YesNo+MB_IconQuestion) = IDYES then
        begin
          Controller.DeleteSelection;
          Result := True;
        end;
      end;
    end;
  end;

  procedure GridCopy(View : TcxGridDBBandedTableView);
  var
    m , n : Integer;
  begin

    if View.OptionsSelection.MultiSelect then //多选复制
    begin
      View.CopyToClipboard(False);
    end else //单选只复制单元格
    with View,View.Controller,View.DataController do
    begin
      m := FocusedItemIndex;
      n := FocusedRowIndex;
      Clipboard.AsText := VarToStr(GetValue(n,m));
    end;

    if Clipboard.AsText <> '' then
    begin
      Application.MessageBox( '复制成功！' ,'提示:', MB_OK + MB_ICONINFORMATION);
    end;
  end;

var
  i : Integer;
  tag:Integer;
  s : string;

begin
  inherited;

  if (Sender = Self.RzToolButton15) or (Sender = N1) then
  begin
    //新增
    dwADO.ADOInsert(Self.dsMaster) ;
    Self.Grid.SetFocus;
    Self.tvView1Col2.Editing := True;
  end else
  if (Sender = Self.RzToolButton2) or (Sender = N2) then
  begin
    //编辑
    dwADO.ADOIsEdit(Self.dsMaster);
  end else
  if (Sender = Self.RzToolButton16) or (Sender = N3) then
  begin
    //保存
    if dwADO.UpdateTableData(dwSQLText1,'Id',1,Self.dsMaster) then
    begin
      Application.MessageBox( '数据保存成功！' ,'六项', MB_OK + MB_ICONINFORMATION);
    end else
    begin
      Application.MessageBox('数据保存失败！','六项', MB_OK + MB_ICONWARNING);
    end;
    ReadData;

  end else
  if (Sender = Self.RzToolButton17) or (Sender = N4) then
  begin
    //删除
    if IsDelete(Self.tvView1) then
    begin
      dwADO.UpdateTableData(dwSQLText1,'Id',1,Self.dsMaster);
      ReadData;
    end;
  end else
  if (Sender = N6) then
  begin
    //复制
    GridCopy(Self.tvView1);
  end else
  if Sender = Self.RzToolButton5 then
  begin
    //导入
  end else
  if (Sender = Self.RzToolButton6) or (Sender = N10) then
  begin
    //导出
    CxGridToExcel(Self.Grid,'明细表');
  end;

end;

procedure TfrmQuotationlib.RzToolButton18Click(Sender: TObject);
  function InDeleteData(ATable,code : string):Integer;stdcall;
  var
    s : string;
  begin

    if Length(ATable) <>  0 then
    begin
      s := 'delete * from '+ ATable +' where Sign_Id in('+ Code  +')';
      with  DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Result   := ExecSQL;
      end;

    end;

  end;
var
  Node : TTreeNode;
  szCodeList : string;

begin
  Node := Self.tv.Selected;
  if Sender = Self.RzToolButton18 then
  begin
    if Node.Level >= 1 then
      TreeAddName(Self.tv)
    else
      ShowMessage('顶级节点禁止新增！')
  end else
  if Sender = Self.RzToolButton19 then
  begin
    Self.tv.Selected.EditText;
  end else
  if Sender = Self.RzToolButton20 then
  begin
    if Node.Level>1 then
    begin
      if MessageBox(handle, '是否删除报价分类节点？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
      begin

        Node := Self.Tv.Selected;
        if Assigned(Node) then
        begin
          GetIndexList(Node,szCodeList);
          InDeleteData( g_Table_Maintain_QuotationList,szCodeList);
          g_TreeView.DeleteTree(Node);
          ReadData;

        end;

      end;

    end;

  end;
end;

procedure TfrmQuotationlib.RzToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmQuotationlib.RzToolButton23Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
begin
  inherited;
  PrevNode   := Tv.Selected.getPrevSibling;
  NextNode   := Tv.Selected.getNextSibling;
  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;

  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;

procedure TfrmQuotationlib.RzToolButton24Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  inherited;
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin
  //  ShowMessage('Index:' + IntToStr(OldNode.Index));
    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmQuotationlib.RzToolButton5Click(Sender: TObject);
{
'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + '%s' +
                ';Extended Properties="Excel 8.0;HDR=YES;IMEX=0";Persist Security Info=False';
                }
  procedure ExcelToCxGrid(cxGrid :  TcxGridDBBandedTableView ;   aSQLText:string='Select * FROM ');
  var
    szConnect : string;
    I: Integer;
    szADOQuery : TADOQuery;
    szOpenDlg: TOpenDialog;
    j : Integer;
    s : string;
    FieldName : string;                          
    UserName : string;
    ExcelApp : Variant;
    szFildList : TStringList;
    szConn : TADOConnection;
    szSheetName : string;
  begin
    {
    IMEX 有三种模式，各自引起的读写行为也不同，容後再述：
         0 is Export mode
         1 is Import mode
         2 is Linked mode (full update capabilities
    }
    szOpenDlg := TOpenDialog.Create(nil);
    try

      if szOpenDlg.Execute then
      begin
        szConnect  := Format(Connect,[szOpenDlg.FileName]) ;
        szConn := TADOConnection.Create(nil);
        try
          szConn.LoginPrompt := False;
          szconn.ConnectionString := szConnect;
          szconn.Connected := True;
          szFildList := TStringList.Create;
          try
            szConn.GetTableNames(szFildList,False);
            szSheetName := szFildList.Strings[0];
          //  ShowMessage(szFildList.Text);
          finally
            szFildList.Free;
          end;


            szADOQuery := TADOQuery.Create(nil);
            try

              with szADOQuery do
              begin
                Connection := szConn;
                Close;
                SQL.Text := aSQLText+' [' +  szSheetName + ']'; //$
                Parameters.Clear;
                ParamCheck := false;
                Open;
                if RecordCount <> 0 then
                begin
        
                  while not Eof do
                  begin
                    Application.ProcessMessages;
                    //if not DataController.DataSet.Active then  DataController.DataSet.Active:=True;
                    cxGrid.DataController.DataSet.Append;

                    for I := 0 to Fields.Count-1 do
                    begin
                      FieldName := Fields[i].FieldName;
                      s := FieldByName(  FieldName ).AsString;
                      with cxGrid do
                      begin
                        BeginUpdate;
                        for j := 0 to ColumnCount-1 do
                        begin
                          if Columns[j].Caption = FieldName then
                          begin
                             UserName := Columns[j].DataBinding.FieldName;
                             if UserName <> '' then
                                DataController.DataSet.FieldByName(UserName).AsString := s;
                          end;
                        end;
                        EndUpdate;
                      end;

                    end;
                    Next;
                  end;

                end;

        
              end;
            finally
              szADOQuery.Free;
            end;
          szConn.Connected := False;
        finally
          szConn.Free;
        end;

      end;

    finally
      szOpenDlg.Free;
    end;
      
  end;

begin
  inherited;
  ExcelToCxGrid(Self.tvView1);
end;


end.
