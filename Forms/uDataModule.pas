unit uDataModule;

interface

uses
  SysUtils, Classes, ADODB, DB, Controls, Vcl.ImgList, System.ImageList,
  cxGraphics, cxEdit, cxDBEditRepository, cxStyles, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter, dxPSCore, dxPScxCommon, Datasnap.DBClient,
  Datasnap.Provider, cxEditRepositoryItems, cxGridTableView, cxClasses;

type
  TDM = class(TDataModule)
    ADOconn: TADOConnection;
    Qry: TADOQuery;
    il1: TImageList;
    cxImageList1: TcxImageList;
    strepPredefined: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    cxStyle40: TcxStyle;
    cxStyle41: TcxStyle;
    cxStyle42: TcxStyle;
    cxStyle43: TcxStyle;
    cxStyle44: TcxStyle;
    cxStyle45: TcxStyle;
    cxStyle46: TcxStyle;
    cxStyle47: TcxStyle;
    cxStyle48: TcxStyle;
    cxStyle49: TcxStyle;
    cxStyle50: TcxStyle;
    cxStyle51: TcxStyle;
    cxStyle52: TcxStyle;
    cxStyle53: TcxStyle;
    cxStyle54: TcxStyle;
    cxStyle55: TcxStyle;
    cxStyle56: TcxStyle;
    cxStyle57: TcxStyle;
    cxStyle58: TcxStyle;
    cxStyle59: TcxStyle;
    cxStyle60: TcxStyle;
    cxStyle61: TcxStyle;
    cxStyle62: TcxStyle;
    cxStyle63: TcxStyle;
    cxStyle64: TcxStyle;
    cxStyle65: TcxStyle;
    cxStyle66: TcxStyle;
    cxStyle67: TcxStyle;
    cxStyle68: TcxStyle;
    cxStyle69: TcxStyle;
    cxStyle70: TcxStyle;
    cxStyle71: TcxStyle;
    cxStyle72: TcxStyle;
    cxStyle73: TcxStyle;
    cxStyle74: TcxStyle;
    cxStyle75: TcxStyle;
    cxStyle76: TcxStyle;
    cxStyle77: TcxStyle;
    cxStyle78: TcxStyle;
    cxStyle79: TcxStyle;
    cxStyle80: TcxStyle;
    cxStyle81: TcxStyle;
    cxStyle82: TcxStyle;
    cxStyle83: TcxStyle;
    cxStyle84: TcxStyle;
    cxStyle85: TcxStyle;
    cxStyle86: TcxStyle;
    cxStyle87: TcxStyle;
    cxStyle88: TcxStyle;
    cxStyle89: TcxStyle;
    cxStyle90: TcxStyle;
    cxStyle91: TcxStyle;
    cxStyle92: TcxStyle;
    cxStyle93: TcxStyle;
    cxStyle94: TcxStyle;
    cxStyle95: TcxStyle;
    cxStyle96: TcxStyle;
    cxStyle97: TcxStyle;
    cxStyle98: TcxStyle;
    cxStyle99: TcxStyle;
    cxStyle100: TcxStyle;
    cxStyle101: TcxStyle;
    cxStyle102: TcxStyle;
    cxStyle103: TcxStyle;
    cxStyle104: TcxStyle;
    cxStyle105: TcxStyle;
    cxStyle106: TcxStyle;
    cxStyle107: TcxStyle;
    cxStyle108: TcxStyle;
    cxStyle109: TcxStyle;
    cxStyle110: TcxStyle;
    cxStyle111: TcxStyle;
    cxStyle112: TcxStyle;
    cxStyle113: TcxStyle;
    cxStyle114: TcxStyle;
    cxStyle115: TcxStyle;
    cxStyle116: TcxStyle;
    cxStyle117: TcxStyle;
    cxStyle118: TcxStyle;
    cxStyle119: TcxStyle;
    cxStyle120: TcxStyle;
    cxStyle121: TcxStyle;
    cxStyle122: TcxStyle;
    cxStyle123: TcxStyle;
    cxStyle124: TcxStyle;
    cxStyle125: TcxStyle;
    cxStyle126: TcxStyle;
    cxStyle127: TcxStyle;
    cxStyle128: TcxStyle;
    cxStyle129: TcxStyle;
    cxStyle130: TcxStyle;
    cxStyle131: TcxStyle;
    cxStyle132: TcxStyle;
    cxStyle133: TcxStyle;
    cxStyle134: TcxStyle;
    cxStyle135: TcxStyle;
    cxStyle136: TcxStyle;
    cxStyle137: TcxStyle;
    cxStyle138: TcxStyle;
    cxStyle139: TcxStyle;
    cxStyle140: TcxStyle;
    cxStyle141: TcxStyle;
    cxStyle142: TcxStyle;
    cxStyle143: TcxStyle;
    cxStyle144: TcxStyle;
    cxStyle145: TcxStyle;
    cxStyle146: TcxStyle;
    cxStyle147: TcxStyle;
    cxStyle148: TcxStyle;
    cxStyle149: TcxStyle;
    cxStyle150: TcxStyle;
    cxStyle151: TcxStyle;
    cxStyle152: TcxStyle;
    cxStyle153: TcxStyle;
    cxStyle154: TcxStyle;
    cxStyle155: TcxStyle;
    cxStyle156: TcxStyle;
    cxStyle157: TcxStyle;
    cxStyle158: TcxStyle;
    cxStyle159: TcxStyle;
    cxStyle160: TcxStyle;
    cxStyle161: TcxStyle;
    cxStyle162: TcxStyle;
    cxStyle163: TcxStyle;
    cxStyle164: TcxStyle;
    cxStyle165: TcxStyle;
    cxStyle166: TcxStyle;
    cxStyle167: TcxStyle;
    cxStyle168: TcxStyle;
    cxStyle169: TcxStyle;
    cxStyle170: TcxStyle;
    cxStyle171: TcxStyle;
    cxStyle172: TcxStyle;
    cxStyle173: TcxStyle;
    cxStyle174: TcxStyle;
    cxStyle175: TcxStyle;
    cxStyle176: TcxStyle;
    cxStyle177: TcxStyle;
    cxStyle178: TcxStyle;
    cxStyle179: TcxStyle;
    cxStyle180: TcxStyle;
    cxStyle181: TcxStyle;
    cxStyle182: TcxStyle;
    cxStyle183: TcxStyle;
    cxStyle184: TcxStyle;
    cxStyle185: TcxStyle;
    cxStyle186: TcxStyle;
    cxStyle187: TcxStyle;
    cxStyle188: TcxStyle;
    cxStyle189: TcxStyle;
    cxStyle190: TcxStyle;
    cxStyle191: TcxStyle;
    cxStyle192: TcxStyle;
    cxStyle193: TcxStyle;
    cxStyle194: TcxStyle;
    cxStyle195: TcxStyle;
    cxStyle196: TcxStyle;
    cxStyle197: TcxStyle;
    cxStyle198: TcxStyle;
    cxStyle199: TcxStyle;
    cxStyle200: TcxStyle;
    cxStyle201: TcxStyle;
    cxStyle202: TcxStyle;
    cxStyle203: TcxStyle;
    cxStyle204: TcxStyle;
    cxStyle205: TcxStyle;
    cxStyle206: TcxStyle;
    cxStyle207: TcxStyle;
    cxStyle208: TcxStyle;
    cxStyle209: TcxStyle;
    cxStyle210: TcxStyle;
    cxStyle211: TcxStyle;
    cxStyle212: TcxStyle;
    cxStyle213: TcxStyle;
    cxStyle214: TcxStyle;
    cxStyle215: TcxStyle;
    cxStyle216: TcxStyle;
    cxStyle217: TcxStyle;
    cxStyle218: TcxStyle;
    cxStyle219: TcxStyle;
    cxStyle220: TcxStyle;
    cxStyle221: TcxStyle;
    cxStyle222: TcxStyle;
    cxStyle223: TcxStyle;
    cxStyle224: TcxStyle;
    cxStyle225: TcxStyle;
    cxStyle226: TcxStyle;
    cxStyle227: TcxStyle;
    cxStyle228: TcxStyle;
    cxStyle229: TcxStyle;
    cxStyle230: TcxStyle;
    cxStyle231: TcxStyle;
    cxStyle232: TcxStyle;
    cxStyle233: TcxStyle;
    cxStyle234: TcxStyle;
    cxStyle235: TcxStyle;
    cxStyle236: TcxStyle;
    cxStyle237: TcxStyle;
    cxStyle238: TcxStyle;
    cxStyle239: TcxStyle;
    cxStyle240: TcxStyle;
    cxStyle241: TcxStyle;
    cxStyle242: TcxStyle;
    cxStyle243: TcxStyle;
    cxStyle244: TcxStyle;
    cxStyle245: TcxStyle;
    cxStyle246: TcxStyle;
    cxStyle247: TcxStyle;
    cxStyle248: TcxStyle;
    cxStyle249: TcxStyle;
    cxStyle250: TcxStyle;
    cxStyle251: TcxStyle;
    cxStyle252: TcxStyle;
    cxStyle253: TcxStyle;
    cxStyle254: TcxStyle;
    cxStyle255: TcxStyle;
    cxStyle256: TcxStyle;
    cxStyle257: TcxStyle;
    cxStyle258: TcxStyle;
    cxStyle259: TcxStyle;
    cxStyle260: TcxStyle;
    cxStyle261: TcxStyle;
    cxStyle262: TcxStyle;
    cxStyle263: TcxStyle;
    cxStyle264: TcxStyle;
    cxStyle265: TcxStyle;
    cxStyle266: TcxStyle;
    cxStyle267: TcxStyle;
    cxStyle268: TcxStyle;
    cxStyle269: TcxStyle;
    cxStyle270: TcxStyle;
    cxStyle271: TcxStyle;
    cxStyle272: TcxStyle;
    cxStyle273: TcxStyle;
    cxStyle274: TcxStyle;
    cxStyle275: TcxStyle;
    cxStyle276: TcxStyle;
    cxStyle277: TcxStyle;
    cxStyle278: TcxStyle;
    cxStyle279: TcxStyle;
    cxStyle280: TcxStyle;
    cxStyle281: TcxStyle;
    cxStyle282: TcxStyle;
    cxStyle283: TcxStyle;
    cxStyle284: TcxStyle;
    cxStyle285: TcxStyle;
    cxStyle286: TcxStyle;
    cxStyle287: TcxStyle;
    cxStyle288: TcxStyle;
    cxStyle289: TcxStyle;
    cxStyle290: TcxStyle;
    cxStyle291: TcxStyle;
    cxStyle292: TcxStyle;
    cxStyle293: TcxStyle;
    cxStyle294: TcxStyle;
    cxStyle295: TcxStyle;
    cxStyle296: TcxStyle;
    cxStyle297: TcxStyle;
    cxStyle298: TcxStyle;
    cxStyle299: TcxStyle;
    cxStyle300: TcxStyle;
    cxStyle301: TcxStyle;
    cxStyle302: TcxStyle;
    cxStyle303: TcxStyle;
    cxStyle304: TcxStyle;
    cxStyle305: TcxStyle;
    cxStyle306: TcxStyle;
    cxStyle307: TcxStyle;
    cxStyle308: TcxStyle;
    cxStyle309: TcxStyle;
    cxStyle310: TcxStyle;
    cxStyle311: TcxStyle;
    cxStyle312: TcxStyle;
    cxStyle313: TcxStyle;
    cxStyle314: TcxStyle;
    cxStyle315: TcxStyle;
    cxStyle316: TcxStyle;
    cxStyle317: TcxStyle;
    cxStyle318: TcxStyle;
    cxStyle319: TcxStyle;
    cxStyle320: TcxStyle;
    cxStyle321: TcxStyle;
    cxStyle322: TcxStyle;
    cxStyle323: TcxStyle;
    cxStyle324: TcxStyle;
    cxStyle325: TcxStyle;
    cxStyle326: TcxStyle;
    cxStyle327: TcxStyle;
    cxStyle328: TcxStyle;
    cxStyle329: TcxStyle;
    cxStyle330: TcxStyle;
    cxStyle331: TcxStyle;
    cxStyle332: TcxStyle;
    cxStyle333: TcxStyle;
    cxStyle334: TcxStyle;
    cxStyle335: TcxStyle;
    cxStyle336: TcxStyle;
    cxStyle337: TcxStyle;
    cxStyle338: TcxStyle;
    cxStyle339: TcxStyle;
    cxStyle340: TcxStyle;
    cxStyle341: TcxStyle;
    cxStyle342: TcxStyle;
    cxStyle343: TcxStyle;
    cxStyle344: TcxStyle;
    cxStyle345: TcxStyle;
    cxStyle346: TcxStyle;
    cxStyle347: TcxStyle;
    cxStyle348: TcxStyle;
    cxStyle349: TcxStyle;
    cxStyle350: TcxStyle;
    cxStyle351: TcxStyle;
    cxStyle352: TcxStyle;
    cxStyle353: TcxStyle;
    cxStyle354: TcxStyle;
    cxStyle355: TcxStyle;
    cxStyle356: TcxStyle;
    cxStyle357: TcxStyle;
    cxStyle358: TcxStyle;
    cxStyle359: TcxStyle;
    cxStyle360: TcxStyle;
    cxStyle361: TcxStyle;
    cxStyle362: TcxStyle;
    cxStyle363: TcxStyle;
    cxStyle364: TcxStyle;
    cxStyle365: TcxStyle;
    cxStyle366: TcxStyle;
    cxStyle367: TcxStyle;
    cxStyle368: TcxStyle;
    cxStyle369: TcxStyle;
    cxStyle370: TcxStyle;
    cxStyle371: TcxStyle;
    cxStyle372: TcxStyle;
    cxStyle373: TcxStyle;
    cxStyle374: TcxStyle;
    cxStyle375: TcxStyle;
    cxStyle376: TcxStyle;
    SumMoney: TcxStyle;
    cxGridTableViewStyleSheet1: TcxGridTableViewStyleSheet;
    EditRepository: TcxEditRepository;
    edrepUserInfo: TcxEditRepositoryLookupComboBoxItem;
    TreeImageList: TcxImageList;
    DataSetProvider1: TDataSetProvider;
    CostComboBox: TcxEditRepositoryLookupComboBoxItem;
    DataCost: TDataSource;
    SignNameBox: TcxEditRepositoryLookupComboBoxItem;
    ClientSignName: TClientDataSet;
    DataSignName: TDataSource;
    ClientCost: TClientDataSet;
    SetTab: TClientDataSet;
    DataTab: TDataSource;
    TabBox: TcxEditRepositoryLookupComboBoxItem;
    SetProjectName: TClientDataSet;
    DataProjectName: TDataSource;
    ProjectNameBox: TcxEditRepositoryLookupComboBoxItem;
    MarketTypeBox: TcxEditRepositoryLookupComboBoxItem;
    SetMarketType: TClientDataSet;
    DataMarketType: TDataSource;
    BasePrinter: TdxComponentPrinter;
    BasePrinterLink1: TdxGridReportLink;
    CompanyList: TcxEditRepositoryLookupComboBoxItem;
    SetCompany: TClientDataSet;
    DataCompany: TDataSource;
    MaterialContractList: TcxEditRepositoryLookupComboBoxItem;
    SetMaterialContrac: TClientDataSet;
    DataMaterialContract: TDataSource;
    SetWrokType: TClientDataSet;
    DataWrokType: TDataSource;
    WorkType: TcxEditRepositoryLookupComboBoxItem;
    Year: TcxEditRepositoryComboBoxItem;
    Days: TcxEditRepositoryComboBoxItem;
    CheckWorkType: TcxEditRepositoryComboBoxItem;
    cxStyle377: TcxStyle;
    SetSection: TClientDataSet;
    DataSection: TDataSource;
    section: TcxEditRepositoryLookupComboBoxItem;
    JobBox: TcxEditRepositoryLookupComboBoxItem;
    SetJob: TClientDataSet;
    DataJob: TDataSource;
    UnitList: TcxEditRepositoryLookupComboBoxItem;
    SetUnitList: TClientDataSet;
    DataUnitList: TDataSource;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure DataSetProvider1AfterUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind);
  private
    { Private declarations }
    g_Code : string;
    g_Caption : string;
  public
    { Public declarations }
    function GetRowCount(ATabName : string):Integer;stdcall;
    function getPayContactCount(ATable,code : string):Integer;stdcall;
    function DeleteData(ATable,code : string):Integer;stdcall;
    function getTableNum(ATable: string):Integer;stdcall;
    function SelectCode(ATable,code : string):Integer;stdcall;
    function getDataMax(ATabName : string; AValue : Integer):string;stdcall;
    function getParentID(ATable : string; PID : Integer):string;stdcall;
    function InDeleteData(ATable,code : string):Integer;stdcall;
    function getExpendituresum(s1 : string):Currency;stdcall;
    function getLastId(ATabName : string ):Integer;stdcall;
    function getTreeCode(ATableName : string ; pn : Integer ;var value : string ) :string;stdcall;
    function AddPayInfo(ATableName,Code, parent , caption , content , Units , remarks:string;pay_time,End_time: TDateTime;Price : Currency;truck : Integer ; numbers : string ; num   :Single ; pacttype : Integer):Integer;stdcall;
    function UpdatePayInfo(ATableName,caption , content , Units ,  remarks:string;pay_time,End_time: TDateTime;Price : Currency;truck : Integer  ; numbers : string ; num   :Single ; Code : Integer):Integer;stdcall;
    function getDataMaxDate(ATabName : string):string;stdcall;
    function IsNumbers(ATableName : string; lpNumber : string):Boolean;
    function ADOQueryData(Qry : TADOQuery ; s : string):Integer;
    function ADOOOPEN(ADQ: TadoQuery;lpSql : string):Integer;
    function DataInitialization():Boolean;
  end;

var
  DM: TDM;

implementation

uses
   global,MidasLib,Vcl.Forms, Vcl.Dialogs,Winapi.Windows;


{$R *.dfm}

function TDM.ADOOOPEN(ADQ: TadoQuery;lpSql : string):Integer;
begin
  with ADQ do
  begin
    Close;
    SQL.Clear;
    SQL.Text := lpSql;
    Open;
    Result := RecordCount;

  end;
end;

function ADOADD(ADQ: TadoQuery):Boolean;
begin
  with ADQ do
  begin
    if State <> dsInactive then
    begin

    end else
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING);
    end;
  end;
end;

function TDM.IsNumbers(ATableName : string; lpNumber : string):Boolean;
var
  s : string;
  
begin
  s := 'Select * from '+ ATableName +' where numbers="'+ lpNumber+'"';
  with dm.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Result := 0 <> RecordCount;
  end;

end;  

function TDM.getExpendituresum(s1 : string):Currency;stdcall;
var
  s : string;
  
begin
  Result := 0;
  s := 'Select sum(e.Price * e.num ) as s from expenditure as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    if 0 <> RecordCount then
    begin
      Result := FieldByName('s').AsCurrency;
    end;
  end;
end;  

function TDM.getPayContactCount(ATable,code : string):Integer;stdcall;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select count (*) as t from ' + ATable + ' where code="' + code + '"';
    Open;
    if RecordCount <> 0 then
      Result :=  FieldByName('t').AsInteger;
  end;
end;

function TDM.getTableNum(ATable: string):Integer;stdcall;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from ' + ATable;
    Open;
    Result := RecordCount;
  end;
end;

function TDM.DeleteData(ATable,code : string):Integer;stdcall;
var
  s : string;
begin
  if Length(ATable) <> 0 then
  begin
    s := 'delete * from '+ ATable +' where code="' + Code + '"';
    with  DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result:= ExecSQL;
    end;
  end;  

end;

function TDM.InDeleteData(ATable,code : string):Integer;stdcall;
var
  s : string;
begin
  if Length(ATable) <>  0 then
  begin
    s := 'delete * from '+ ATable +' where Code in('''+ Code  +''')';
    OutputLog(s);
    with  DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result:= ExecSQL;
    end;

  end;

end;

function TDM.SelectCode(ATable,code : string):Integer;stdcall;
var
  s : string;
begin
  s := 'Select * from '+ ATable +' where code="' + Code + '"';
  with  DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Result := RecordCount;
  end;
//  OutputLog('SelectCode:' + IntToStr(Result));
  
end;

function TDM.getDataMax(ATabName : string; AValue : Integer):string;stdcall;
var
  i : Integer;
  s : string;
begin
  i := DM.getTableNum(ATabName);
  if i <> 0 then
  begin
    s := 'SELECT MAX(编号) FROM ' + ATabName;
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <> 0 then
      begin
         Result := IntToStr( FieldByName('expr1000').AsInteger + AValue + 1 );  //+ 21000 +1
      end;
      OutputLog(IntToStr(RecordCount));
    end;

  end
  else
  begin
     Result := IntToStr(AValue); //支款方编号
  end;

end;

function TDM.getDataMaxDate(ATabName : string):string;stdcall;
var
  i : Integer;
  s : string;
  Year : Word;
  Month: Word;
  Day  : Word;

begin
  DecodeDate(Now, Year, Month, Day); //开始时间
  i := DM.getTableNum( ATabName );
  if i <> 0 then
  begin
      s := 'SELECT MAX(编号) FROM ' + ATabName;

      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        if RecordCount <> 0 then
        begin
          i := FieldByName('expr1000').AsInteger;
          s := Format('%d%d%d%d',[Year,Month,day,i +1 ]);
          Result := s ;
        end;
      end;
  end
  else
  begin
    s := Format('%d%d%d%d',[Year,Month,day, 1]);
    Result := s;
  end;

end;

function TDM.getLastId(ATabName : string ):Integer;stdcall;
var
  s : string;
begin
    s := 'SELECT MAX(parent) FROM ' + ATabName;
    with DM.Qry do
    begin
      OutputLog(s);
      
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      OutputLog('Parent:' + IntToStr(RecordCount));
      if RecordCount <> 0 then
      begin
         Result := FieldByName('Expr1000').AsInteger;  //+ 21000 +1
         Inc(Result);
         
      end else
      begin
        Result := 1;
      end;

    end;
end;

function TDM.GetRowCount(ATabName : string):Integer;stdcall;
var
  i : Integer;
  s : string;
begin
  s := 'SELECT Count(*) FROM ' + ATabName;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    if RecordCount <> 0 then
       Result := Fields[0].AsInteger +1
    else
       Result := 1;
  end;
end;

function TDM.GetParentID(ATable : string; PID : Integer):string;stdcall;
var
  Query: TADOQuery;
  Caption : string;
  Code : string;
  parent : Integer;
  sum_Money:Currency;
  szPID : integer;
  szMoney : Currency;
  Money : Currency;

begin
  if Length(ATable) <> 0 then
  begin
      Query := TADOQuery.Create(nil);
      try
          Query.Connection := DM.ADOconn;
      //   OutputLog('-------------------------------------' + IntToStr( PID ));
          Query.SQL.Text := 'Select * from ' + ATable + ' where ' + 'parent' + ' =' + IntToStr( PID );
          if Query.Active then
             Query.Close;

          Query.Open;
          while not Query.Eof do
          begin

            Caption := Query.FieldByName('name').AsString;
            Code    := Query.FieldByName('code').AsString;
            parent  := Query.FieldByName('parent').AsInteger;
            szPID   := Query.FieldByName('PID').AsInteger;

            if szPID = 0 then
            begin
              OutputLog('Code:' + Code + ' parent:' + IntToStr(parent) + ' PID:' + IntToStr(szPID) + ' name:' + Caption);
              g_Code := Code;
              g_Caption := Caption;
              Break;
            end;

            GetParentID(ATable,szPID);
            Query.Next;
          end;

          Result := g_Code;
          OutputLog('Result:' + Result + ' Caption:' + g_Caption);
      finally
        Query.Free;
      end;

  end;

end;

function getSonTreeCode(ATableName : string ; pn : Integer ;var value : string ):string;stdcall;
var
  Query: TADOQuery;
  Code : string;
  parent : Integer;
  szPID : integer;

begin
  if Length(ATableName) <> 0 then
  begin
      Query := TADOQuery.Create(nil);
      try
          Query.Connection := DM.ADOconn;
          Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'PID' + ' =' + IntToStr( pn );
          if Query.Active then
             Query.Close;

          Query.Open;
          while not Query.Eof do
          begin
            Code    := Query.FieldByName('code').AsString;
            parent  := Query.FieldByName('parent').AsInteger;
            szPID   := Query.FieldByName('PID').AsInteger;

            if Length(value) = 0 then
              value := Code
            else
              value := value + ''',''' + Code;

            getSonTreeCode(ATableName,parent,value);
            Query.Next;
          end;
          Result := value;
      finally
        Query.Free;
      end;

  end;

end;


function TDM.getTreeCode(ATableName : string ; pn : Integer ;var value : string ) :string;stdcall;
var
  Query: TADOQuery;
  Caption : string;
  Code : string;
  parent : Integer;
  sum_Money:Currency;
  szPID : integer;
  szMoney : Currency;
  Money : Currency;
  szCodeList : string;
  
begin
  if Length(ATableName) <> 0 then
  begin
    Query := TADOQuery.Create(nil);
    try
      Query.Connection := DM.ADOconn;
      Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'parent' + ' =' + IntToStr( pn );

      if Query.Active then Query.Close;

      Query.Open;
      if not Query.Eof then
      begin
        Code    := Query.FieldByName('code').AsString;
        parent  := Query.FieldByName('parent').AsInteger;
        getSonTreeCode(ATableName,parent,szCodeList);
        Result := Code + ''',''' + szCodeList;
      end;
      value := Result;

      OutputLog('getTreeCode:' + value);
      
    finally
      Query.Free;
    end;

  end;

end;

function TDM.AddPayInfo(ATableName,Code, parent , caption , content , Units , remarks:string;
  pay_time,End_time: TDateTime;Price : Currency;truck : Integer ; numbers : string ; num   :Single ; pacttype : Integer):Integer;stdcall;
var
  s : string;

begin
  s := Format( 'Insert into '+ ATableName  +' (code,'+
                                              'parent,'    +
                                              'numbers,'   +
                                              'caption,'   +
                                              'content,'   +
                                              'pay_time,'  +
                                              'End_time,'  +
                                              'truck,'     +
                                              'Price,'     +
                                              'num,'       +
                                              'Units,'     +
                                              'pacttype,'  +
                                              'remarks'    +
                                              ') values("%s","%s","%s","%s","%s","%s","%s","%d",''%m'',''%f'',"%s","%d","%s")',[
                                                                      Code,
                                                                      parent,
                                                                      numbers,
                                                                      Caption,
                                                                      Content,
                                                                      FormatDateTime('yyyy-MM-dd',pay_time),
                                                                      FormatDateTime('yyyy-MM-dd',End_time),
                                                                      Truck,
                                                                      Price,
                                                                      Num,
                                                                      Units,
                                                                      pacttype,
                                                                      Remarks
                                                                      ] );


    OutputLog(s);
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result := ExecSQL;
    end;

end;

function TDM.UpdatePayInfo(ATableName,caption , content , Units , remarks:string;pay_time,End_time: TDateTime;Price : Currency;truck : Integer  ; numbers : string ; num   :Single ; Code : Integer):Integer;stdcall;
var
  s : string;
  
begin
  s := Format('Update '+ ATableName +' set caption="%s",'  +
                                       'content="%s",'  +
                                       'pay_time="%s",' +
                                       'End_time="%s",' +
                                       'Price=''%m'','  +
                                       'num=''%f'','    +
                                       'truck=''%d'','  +
                                       'Units="%s",'    +
                                       'remarks="%s",'  +

                                        ' where 编号=%d',[
                                                     Caption,
                                                     Content,
                                                     FormatDateTime('yyyy-MM-dd',pay_time),
                                                     FormatDateTime('yyyy-MM-dd',End_time),  
                                                     Price,
                                                     Num,
                                                     truck,
                                                     Units,
                                                     Remarks,

                                                     code
                                                     ]);

  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Result := ExecSQL;
  end;
                      
end;

function DBTableExists(aTableName: string;aADOConn:TADOConnection): Boolean;
var
  vTableNames:   TStringList;
begin
    Result:=False;
    vTableNames :=  TStringList.Create;
    try
        aADOConn.GetTableNames(vTableNames); //取得所有表名
        if  vTableNames.IndexOf(aTableName)>= 0 then //判断是否存在
        Result:=True;
    finally
        vTableNames.Free;
    end;
end;

procedure TDM.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

function TDM.ADOQueryData(Qry : TADOQuery ; s : string):Integer;
begin
  with Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Result := RecordCount;
  end;
end;

function TDM.DataInitialization():Boolean;
var
  t : Integer;
begin
//  CoInitialize(nil);  //必须调用（需Uses ActiveX）
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_CompanyManage;
    ADOQuery1.Open;
    ClientSignName.Data := DataSetProvider1.Data;
    ClientSignName.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_Cost;
    ADOQuery1.Open;
    ClientCost.Close;
    ClientCost.Data := DataSetProvider1.Data;
    ClientCost.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_Tab;
    ADOQuery1.Open;
    SetTab.Close;
    SetTab.Data := DataSetProvider1.Data;
    SetTab.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_Project;
    ADOQuery1.Open;
    SetProjectName.Close;
    SetProjectName.Data := DataSetProvider1.Data;
    SetProjectName.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from pay_type'; //+ g_Table_Maintain_Project;
    ADOQuery1.Open;
    SetMarketType.Data := DataSetProvider1.Data;
    SetMarketType.Open;

    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' WHere PID=0';
    SetCompany.Data := DataSetProvider1.Data;
    SetCompany.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_WorkType;
    ADOQuery1.Open;
    SetWrokType.Data := DataSetProvider1.Data;
    SetWrokType.Open;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_section;
    ADOQuery1.Open;
    SetSection.Data := DataSetProvider1.Data;
    SetSection.Open;

    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Maintain_Job;
    ADOQuery1.Open;
    SetJob.Data := DataSetProvider1.Data;
    SetJob.Open;

    ADOQuery1.Close;
    ADOQuery1.SQL.Text :=  'Select * from unit where chType=0';
    ADOQuery1.Open;
    SetUnitList.Data := DataSetProvider1.Data;
    SetUnitList.Open;

//  CoUninitialize;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
var
  szDataPath , s: string;
  I: Integer;
  Year : Integer;

begin

  //A4 210 x 297 mm
  Year := 2000;
  for I := 0 to 100 do
  begin
    Inc(Year);
    Self.Year.Properties.Items.Add( IntToStr( Year ) );
  end;

  try
    szDataPath := ExtractFilePath(ParamStr(0)) + 'Data.mdb';
    s := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + szDataPath +
           ';Persist Security Info=False';

    Self.ADOconn.ConnectionString :=  s;
    Self.ADOconn.Connected:=true;
    DataInitialization; //初始化数据
  except
    ADOconn.Connected:=false;
  end;
end;

procedure TDM.DataSetProvider1AfterUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind);
begin
  //DstId TADODataset
  //FId 为自增字段
  if UpdateKind=ukInsert then
  begin
    {
    DstId.CommandText:='select @@Identity as Id ';
    DstId.Open;
    DeltaDS.FieldByName('Id').ReadOnly:=False;
    DeltaDS.FieldByName('Id').NewValue:=DstId.FieldByName('Id').AsInteger ;
    DstId.Close;
    }
  end;
{
//新增时，从表的关联字段与主表的自增字段同步更新
procedure TProducts.DataSetProvider1BeforeUpdateRecord(Sender: TObject;
        SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
        UpdateKind: TUpdateKind; var Applied: Boolean);
begin
//DstProduct为从表的Name
//CategoryID是从表的对于主表的字增自段的关联字段
// qryIdentity是TADOQuery qryIdentity.SQL:='select @@identity'
if (UpdateKind = ukInsert) and
(SourceDS = DstProduct) and
(DeltaDS.FieldByName('CategoryID').Value = Unassigned) ) then
begin
if DeltaDS.BOF then
begin
qryIdentity.Close;
qryIdentity.Open;
end;
DeltaDS.FieldByName('CategoryID').NewValue := qryIdentity.Fields[0].Value;
end;
end;
}
end;

end.
