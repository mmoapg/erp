unit ufrmPayee;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DB, ADODB,TreeUtils, RzButton, ExtCtrls,StdCtrls, RzPanel;

type
  TfrmPayee = class(TForm)
    Qry: TADOQuery;
    TreeView1: TTreeView;
    RzPanel1: TRzPanel;
    RzBitBtn1: TRzBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure RzBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTree : TTreeUtils;
  end;

var
  frmPayee: TfrmPayee;

implementation

uses
   uDataModule,TreeFillThrd,global,ufrmMain,ufrmSort;

{$R *.dfm}

procedure TfrmPayee.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FTree.Free;
end;

procedure TfrmPayee.FormShow(Sender: TObject);
begin
  FTree := TTreeUtils.Create(Self.TreeView1,
                                      DM.ADOconn,
                                      'out_tree',
                                      'parent',
                                      'PID',
                                      'Name',
                                      m_Code,
                                      m_money ,
                                      m_dwPhoto,
                                      m_Remarks,
                                      'Price',
                                      'num',
                                      'Input_time',
                                      'End_time',
                                      'PatType',
                                      '֧�'
                                      );


  FTree.FillTree(0,g_pacttype);
end;

procedure TfrmPayee.RzBitBtn1Click(Sender: TObject);
var
  Child : TfrmSort;
begin
  FTree.Free;
  Child := TfrmSort.Create(Application);
  try
    Child.ShowModal;
  finally
  end;
  FormShow(Sender);
end;

procedure QueryCall(Qry : TADOQuery ; ATabName , ACode : string);
var
  s : string;
  Caption : string;

begin
   {
  s := 'select * from ' + ATabName +' where '+ m_Code +'="' + ACode + '"';
  with qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    frmPayMoney.cxComboBox1.Clear;
  //  frmPayMoney.cxComboBox2.Clear;
    while not Eof do
    begin
      Caption := FieldByName('Caption').AsString;
      frmPayMoney.cxComboBox1.Properties.Items.Add(Caption);
    //  frmPayMoney.cxComboBox2.Properties.Items.Add(Caption);
      Next;
    end;

  end;
   }
end;

procedure TfrmPayee.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  Nod: TNodeData;
  szNode : PNodeData;
  szCode : string;
  s : string;

begin
  if Assigned(Node) then
  begin
    szNode := PNodeData(Node.Data);
    szCode := szNode.Code;

    if (Node.Level > 0) and (Node.Count = 0) then
    begin
      try

        if Length(szCode) <> 0 then
        begin
          QueryCall(Self.Qry,m_out_contact,szCode);
        end;

      finally

      end;
    end;
  end;

end;

end.
