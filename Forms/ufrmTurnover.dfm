object frmTurnover: TfrmTurnover
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #21608#36716#27454#31649#29702
  ClientHeight = 257
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox1: TcxGroupBox
    Left = 8
    Top = 8
    Caption = #21608#36716#27454#26032#22686'/'#32534#36753
    TabOrder = 0
    Height = 241
    Width = 644
    object cxLabel1: TcxLabel
      Left = 30
      Top = 64
      Caption = #21333#12288#12288#20215' '
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 91
      Top = 62
      Properties.OnChange = cxCurrencyEdit1PropertiesChange
      TabOrder = 2
      Width = 93
    end
    object cxLabel2: TcxLabel
      Left = 190
      Top = 64
      Caption = #22823#12288#12288#20889
    end
    object cxComboBox1: TcxComboBox
      Left = 91
      Top = 96
      TabOrder = 3
      Width = 93
    end
    object cxLabel3: TcxLabel
      Left = 30
      Top = 97
      Caption = #21333#12288#12288#20301' '
    end
    object cxLabel4: TcxLabel
      Left = 190
      Top = 97
      Caption = #25968#12288#12288#20540
    end
    object cxLabel5: TcxLabel
      Left = 30
      Top = 29
      Caption = #24037#31243#21517#31216
    end
    object cxTextEdit1: TcxTextEdit
      Left = 91
      Top = 28
      TabOrder = 0
      Width = 173
    end
    object cxTextEdit2: TcxTextEdit
      Left = 340
      Top = 29
      TabOrder = 1
      Width = 254
    end
    object cxLabel6: TcxLabel
      Left = 270
      Top = 29
      Caption = #39033#30446#37096#36130#21153
    end
    object cxLabel7: TcxLabel
      Left = 308
      Top = 97
      Caption = #36215#22987#26085#26399
    end
    object cxDateEdit1: TcxDateEdit
      Left = 369
      Top = 97
      TabOrder = 5
      Width = 82
    end
    object cxDateEdit2: TcxDateEdit
      Left = 511
      Top = 97
      TabOrder = 6
      Width = 82
    end
    object cxLabel8: TcxLabel
      Left = 453
      Top = 97
      Caption = #32467#26463#26085#26399
    end
    object cxButton1: TcxButton
      Left = 519
      Top = 125
      Width = 75
      Height = 28
      Caption = #38468#20214
      TabOrder = 8
      OnClick = cxButton1Click
    end
    object cxTextEdit3: TcxTextEdit
      Left = 248
      Top = 63
      Enabled = False
      Properties.ReadOnly = True
      TabOrder = 19
      Width = 345
    end
    object cxLabel9: TcxLabel
      Left = 30
      Top = 134
      Caption = #22791#12288#12288#27880' '
    end
    object cxMemo1: TcxMemo
      Left = 91
      Top = 128
      TabOrder = 7
      Height = 86
      Width = 414
    end
    object cxButton2: TcxButton
      Left = 519
      Top = 157
      Width = 74
      Height = 28
      Caption = #30830#23450'(&O)'
      TabOrder = 9
      OnClick = cxButton2Click
    end
    object cxButton3: TcxButton
      Left = 519
      Top = 189
      Width = 74
      Height = 28
      Caption = #36864#20986'(&C)'
      TabOrder = 10
      OnClick = cxButton3Click
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 248
      Top = 96
      Properties.DisplayFormat = '0.##;0.##'
      Properties.OnChange = cxCurrencyEdit1PropertiesChange
      TabOrder = 4
      Width = 54
    end
  end
end
