﻿unit ufrmConstructManage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  dxGDIPlusClasses, cxImage, Vcl.OleCtrls, FillThrdTree,UtilsTree,
  ufrmBaseController, cxGraphics, cxControls, cxLookAndFeels,Dialogs,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.Menus, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxCurrencyEdit,
  cxMemo, Vcl.ComCtrls, dxCore, cxDateUtils, frxClass, frxDBSet,
  System.ImageList, Vcl.ImgList, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.StdCtrls, Vcl.FileCtrl, RzFilSys, RzButton,
  Vcl.Mask, RzEdit, RzLabel, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, RzSplit, RzPanel, cxMaskEdit, cxDBEdit, cxButtons, cxLabel,
  cxGroupBox, RzTabs,TreeUtils,TreeFillThrd, cxSplitter, Datasnap.DBClient,
  Winapi.ShlObj, cxShellCommon, cxListView, cxShellListView, cxTreeView,
  cxShellTreeView,UnitADO;

type
  TfrmConstructManage = class(TfrmBaseController)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    RzPageControl2: TRzPageControl;
    TabSheet5: TRzTabSheet;
    TabSheet16: TRzTabSheet;
    TabSheet11: TRzTabSheet;
    cxGroupBox6: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel10: TcxLabel;
    cxGroupBox7: TcxGroupBox;
    cxLabel12: TcxLabel;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    cxButton17: TcxButton;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    cxGroupBox8: TcxGroupBox;
    cxLabel20: TcxLabel;
    cxLabel21: TcxLabel;
    cxLabel22: TcxLabel;
    cxLabel23: TcxLabel;
    cxLabel24: TcxLabel;
    cxLabel25: TcxLabel;
    cxLabel26: TcxLabel;
    cxLabel27: TcxLabel;
    cxLabel28: TcxLabel;
    cxLabel29: TcxLabel;
    cxLabel30: TcxLabel;
    cxLabel31: TcxLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    cxDBTextEdit5: TcxDBTextEdit;
    cxDBTextEdit6: TcxDBTextEdit;
    cxDBTextEdit7: TcxDBTextEdit;
    cxDBTextEdit9: TcxDBTextEdit;
    cxDBTextEdit10: TcxDBTextEdit;
    cxDBTextEdit8: TcxDBTextEdit;
    cxDBTextEdit11: TcxDBTextEdit;
    cxDBTextEdit12: TcxDBTextEdit;
    cxDBTextEdit13: TcxDBTextEdit;
    cxDBTextEdit14: TcxDBTextEdit;
    cxDBTextEdit15: TcxDBTextEdit;
    cxDBTextEdit16: TcxDBTextEdit;
    cxDBTextEdit17: TcxDBTextEdit;
    cxDBTextEdit18: TcxDBTextEdit;
    cxDBTextEdit19: TcxDBTextEdit;
    cxDBTextEdit20: TcxDBTextEdit;
    cxDBTextEdit21: TcxDBTextEdit;
    cxDBTextEdit22: TcxDBTextEdit;
    cxDBTextEdit23: TcxDBTextEdit;
    cxDBTextEdit24: TcxDBTextEdit;
    cxDBTextEdit25: TcxDBTextEdit;
    cxLabel9: TcxLabel;
    cxButton22: TcxButton;
    cxButton20: TcxButton;
    cxButton25: TcxButton;
    Grid: TcxGrid;
    tvPactView: TcxGridDBTableView;
    LvPact: TcxGridLevel;
    tvPactViewCol1: TcxGridDBColumn;
    tvPactViewCol2: TcxGridDBColumn;
    tvPactViewCol11: TcxGridDBColumn;
    tvPactViewCol12: TcxGridDBColumn;
    tvPactViewCol3: TcxGridDBColumn;
    tvPactViewCol13: TcxGridDBColumn;
    tvPactViewCol4: TcxGridDBColumn;
    tvPactViewCol7: TcxGridDBColumn;
    tvPactViewCol8: TcxGridDBColumn;
    tvPactViewCol9: TcxGridDBColumn;
    tvPactViewCol10: TcxGridDBColumn;
    tvPactViewCol14: TcxGridDBColumn;
    tvPactViewCol15: TcxGridDBColumn;
    tvPactViewCol16: TcxGridDBColumn;
    tvPactViewCol5: TcxGridDBColumn;
    tvPactViewCol6: TcxGridDBColumn;
    il1: TImageList;
    TabSheet31: TRzTabSheet;
    AllocateBankrollGrid: TcxGrid;
    tvABView: TcxGridDBTableView;
    ABLv: TcxGridLevel;
    tvABViewColumn1: TcxGridDBColumn;
    tvABViewColumn2: TcxGridDBColumn;
    tvABViewColumn3: TcxGridDBColumn;
    tvABViewColumn4: TcxGridDBColumn;
    tvABViewColumn5: TcxGridDBColumn;
    tvABViewColumn6: TcxGridDBColumn;
    tvABViewColumn7: TcxGridDBColumn;
    tvABViewColumn8: TcxGridDBColumn;
    tvABViewColumn10: TcxGridDBColumn;
    tvABViewColumn11: TcxGridDBColumn;
    tvABViewColumn12: TcxGridDBColumn;
    tvABViewColumn13: TcxGridDBColumn;
    tvABViewColumn14: TcxGridDBColumn;
    tvABViewColumn15: TcxGridDBColumn;
    dsSurvey: TDataSource;
    QrySurvey: TADOQuery;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    Client: TRzPanel;
    dsPactInfo: TDataSource;
    QryPactInfo: TADOQuery;
    dsAllocateBankroll: TDataSource;
    QryAllocateBankroll: TADOQuery;
    pmPrint: TPopupMenu;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    Excel1: TMenuItem;
    RzToolbar6: TRzToolbar;
    RzSpacer24: TRzSpacer;
    RzToolButton13: TRzToolButton;
    RzSpacer34: TRzSpacer;
    RzToolButton26: TRzToolButton;
    RzSpacer35: TRzSpacer;
    RzToolButton27: TRzToolButton;
    RzSpacer36: TRzSpacer;
    RzToolButton28: TRzToolButton;
    RzToolButton29: TRzToolButton;
    RzToolButton30: TRzToolButton;
    RzSpacer37: TRzSpacer;
    RzSpacer38: TRzSpacer;
    RzToolButton31: TRzToolButton;
    RzSpacer39: TRzSpacer;
    cxLabel40: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel41: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzToolbar7: TRzToolbar;
    RzSpacer40: TRzSpacer;
    RzToolButton32: TRzToolButton;
    RzSpacer41: TRzSpacer;
    RzToolButton33: TRzToolButton;
    RzSpacer42: TRzSpacer;
    RzToolButton34: TRzToolButton;
    RzSpacer43: TRzSpacer;
    RzToolButton35: TRzToolButton;
    RzToolButton36: TRzToolButton;
    RzToolButton37: TRzToolButton;
    RzSpacer44: TRzSpacer;
    RzSpacer45: TRzSpacer;
    RzToolButton38: TRzToolButton;
    RzSpacer46: TRzSpacer;
    cxLabel42: TcxLabel;
    cxDateEdit3: TcxDateEdit;
    cxLabel43: TcxLabel;
    cxDateEdit4: TcxDateEdit;
    RzSpacer47: TRzSpacer;
    RzSpacer48: TRzSpacer;
    RzSpacer50: TRzSpacer;
    RzSpacer51: TRzSpacer;
    RzSpacer52: TRzSpacer;
    frxPactInfo: TfrxDBDataset;
    RzToolbar8: TRzToolbar;
    RzSpacer53: TRzSpacer;
    RzToolButton39: TRzToolButton;
    RzSpacer54: TRzSpacer;
    RzToolButton43: TRzToolButton;
    RzToolButton44: TRzToolButton;
    RzSpacer59: TRzSpacer;
    RzSpacer56: TRzSpacer;
    RzToolButton40: TRzToolButton;
    RzSpacer55: TRzSpacer;
    RzToolButton42: TRzToolButton;
    RzSpacer58: TRzSpacer;
    RzToolButton45: TRzToolButton;
    RzSpacer60: TRzSpacer;
    RzToolButton46: TRzToolButton;
    RzSpacer61: TRzSpacer;
    RzToolButton47: TRzToolButton;
    RzSpacer62: TRzSpacer;
    RzToolButton48: TRzToolButton;
    RzSpacer63: TRzSpacer;
    frxAllocateBankroll: TfrxDBDataset;
    tvABViewColumn9: TcxGridDBColumn;
    tvPactViewColumn1: TcxGridDBColumn;
    tvABViewColumn16: TcxGridDBColumn;
    frxSurvey: TfrxDBDataset;
    cxLabel52: TcxLabel;
    cxLabel53: TcxLabel;
    cxLabel54: TcxLabel;
    RzToolButton41: TRzToolButton;
    cxDBTextEdit26: TcxDBTextEdit;
    cxDBTextEdit27: TcxDBTextEdit;
    cxDBTextEdit28: TcxDBTextEdit;
    N4: TMenuItem;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    cxDBTextEdit29: TcxDBTextEdit;
    cxLabel17: TcxLabel;
    Panel2: TPanel;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    tv1: TTreeView;
    TabSheet2: TRzTabSheet;
    tv2: TTreeView;
    TabSheet3: TRzTabSheet;
    tv3: TTreeView;
    TabSheet4: TRzTabSheet;
    tv4: TTreeView;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    Splitter1: TSplitter;
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    ADOQuery3: TADOQuery;
    DataSource3: TDataSource;
    RzToolbar1: TRzToolbar;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzPanel1: TRzPanel;
    RzToolbar14: TRzToolbar;
    RzSpacer112: TRzSpacer;
    RzToolButton84: TRzToolButton;
    RzSpacer121: TRzSpacer;
    RzToolButton91: TRzToolButton;
    RzSpacer122: TRzSpacer;
    RzSpacer125: TRzSpacer;
    RzToolButton96: TRzToolButton;
    RzToolButton92: TRzToolButton;
    Splitter3: TSplitter;
    cxGrid1: TcxGrid;
    tvFiles: TcxGridDBTableView;
    tvFilesColumn1: TcxGridDBColumn;
    tvFilesColumn2: TcxGridDBColumn;
    tvFilesColumn4: TcxGridDBColumn;
    tvFilesColumn3: TcxGridDBColumn;
    tvFilesColumn5: TcxGridDBColumn;
    tvFilesColumn6: TcxGridDBColumn;
    Lv: TcxGridLevel;
    DataMaster: TDataSource;
    Master: TClientDataSet;
    RzEdit2: TRzEdit;
    cxLabel18: TcxLabel;
    Left: TRzPanel;
    RzSpacer7: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzSpacer10: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RightKeys: TPopupMenu;
    N5: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tv1Edited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure cxSplitter2CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure RzPageControl2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tvFlowingAccountColumn14GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvWorkGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton28Click(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure tvPactViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure Excel1Click(Sender: TObject);
    procedure RzToolButton31Click(Sender: TObject);
    procedure RzToolButton38Click(Sender: TObject);
    procedure cxDateEdit15PropertiesCloseUp(Sender: TObject);
    procedure tvABViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvPactViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton32Click(Sender: TObject);
    procedure tvABViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton39Click(Sender: TObject);
    procedure tvLeaseCol19CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvABViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvABViewColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvPactViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxDBCurrencyEdit1PropertiesEditValueChanged(Sender: TObject);
    procedure RzToolButton40Click(Sender: TObject);
    procedure RzToolButton42Click(Sender: TObject);
    procedure RzToolButton45Click(Sender: TObject);
    procedure RzToolButton46Click(Sender: TObject);
    procedure RzToolButton47Click(Sender: TObject);
    procedure RzToolButton48Click(Sender: TObject);
    procedure tvPactViewDblClick(Sender: TObject);
    procedure tvABViewDblClick(Sender: TObject);
    procedure RzToolButton26Click(Sender: TObject);
    procedure RzToolButton41Click(Sender: TObject);
    procedure RzToolButton84Click(Sender: TObject);
    procedure QryPactInfoAfterOpen(DataSet: TDataSet);
    procedure N4Click(Sender: TObject);
    procedure tvPactViewCol8GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure FormActivate(Sender: TObject);
    procedure tvPactViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvABViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvPactViewCol12PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvPactViewCol14PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure tv1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tv1Editing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure RzToolButton37Click(Sender: TObject);
    procedure RzToolButton30Click(Sender: TObject);
    procedure RzToolButton44Click(Sender: TObject);
    procedure tvFilesDblClick(Sender: TObject);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure MasterAfterClose(DataSet: TDataSet);
    procedure tv1DblClick(Sender: TObject);
    procedure tvFilesEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvFilesEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure RzEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton92Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
  private
    { Private declarations }
    dwADO : TADO;
    dwCurrentPath : WideString;
    dwSQlText  : string;

    g_Table_PactTree : string;

    g_DossierTree : TNewUtilsTree;
    g_Procedures  : TNewUtilsTree;
    g_SceneManage : TNewUtilsTree;
    g_Projectdata : TNewUtilsTree;

    g_treeType  : Integer;
    g_treeParent: Integer;

    g_treeDataA : TTreeUtils;
    g_treeDataB : TTreeUtils;
    g_treeDataC : TTreeUtils;
    g_treeDataD : TTreeUtils;

    g_PostCode  : string;
    g_PostArchivesCode : string;
    g_ProjectDir: string;
    g_BuildStorage : string;
    g_ProjectName  : string;
    g_DirWorksheet : string;
    g_DirQuantity  : string;
    g_DirBuildMechanics : string;
    g_DirBuildFlowingAccount  : string;

    procedure TreeOperate(Node : TTreeNode;Atype : Byte;s: string = '');
    procedure TreeAddName(Tv : TTreeView); //树增加名称

    function SelectData():Boolean;
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    function GetData(lpSQlText : string):Boolean;
  public
    { Public declarations }
    function GetDisparityMoney(ProjectOffer , MarkeOffer : Variant) : Boolean;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte;lpSuffix:string);
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmConstructManage: TfrmConstructManage;
  g_dir_ProjectPact : string = 'ProjectPact';

  g_Table_ArchivesManage : string = 'sk_ArchivesManage';
  g_Dir_ArchivesManage   : string = 'ArchivesManage';

implementation

uses
   uDataModule,
   global,
   ufunctions,
   ufrmPostPact,
   ShellAPI,
   uProjectFrame,
   cxGridExportLink,
   ufrmIsViewGrid,
   ufrmConstructPost,
   ufrmCalcExpression,
   DateUtils , Math ;

{$R *.dfm}

function TfrmConstructManage.GetData(lpSQlText : string):Boolean;
begin
  if Length(lpSQlText) <> 0 then
  begin
    with DM do
    begin
      ADOQuery1.Close;
      ADOQuery1.SQL.Text := lpSQlText;
      ADOQuery1.Open;
      Self.Master.Data := DataSetProvider1.Data;
      Self.Master.Open;
    end;
  end;
end;

procedure TfrmConstructManage.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..2] of TTableList;

begin

  case Message.Msg of
    WM_FrameClose:
      begin

        Self.QrySurvey.Close;
        Self.QryPactInfo.Close;
      end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;
      SelectData();

      with Self.ADOSignName do
      begin
        Close;
        SQL.Text := 'Select *from ' + g_Table_CompanyManage;
        Open;
      end;

      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Text := 'Select *from ' + g_Table_Maintain_WorkType;
        Open;
      end;

      with Self.ADOQuery2 do
      begin
        Close;
        SQL.Text := 'Select *from ' + g_Table_MakingsList;
        Open;
      end;


      with Self.ADOQuery3 do
      begin
        Close;
        SQL.Text := 'Select *from ' + g_Table_Maintain_CompanyInfoTree;
        Open;
      end;
      //****************************************************************************
      //工程档案

      g_DossierTree := TNewUtilsTree.Create(Self.tv1,
                                         DM.ADOconn,
                                         g_Table_Project_Dossier_tree,
                                         '工程档案');
      g_DossierTree.GetTreeTable(0);

      //****************************************************************************
      //施工手续
      g_Procedures := TNewUtilsTree.Create(Self.tv2,
                                         DM.ADOconn,
                                         g_Table_Project_Procedures_Tree,
                                         '工程施工');
      g_Procedures.GetTreeTable(0);

      //****************************************************************************
      g_SceneManage := TNewUtilsTree.Create(Self.tv3,
                                         DM.ADOconn,
                                         g_Table_Project_SceneManage_Tree,
                                         '现场管理');
      g_SceneManage.GetTreeTable(0);
      //****************************************************************************
      g_Projectdata := TNewUtilsTree.Create(Self.tv4,
                                         DM.ADOconn,
                                         g_Table_Project_System_Tree,
                                         '工程资料');
      g_Projectdata.GetTreeTable(0);
      //****************************************************************************
      g_treeType := Self.RzPageControl1.ActivePageIndex;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;

      szTableList[0].ATableName := g_Table_Project_Survey;
      szTableList[0].ADirectory := '';  //工程概况附件可以单位删除

      szTableList[1].ATableName := g_Table_Project_PactInfo;
      szTableList[1].ADirectory := g_dir_PactInfo;

      szTableList[2].ATableName := g_Table_Project_AllocateBankroll;
      szTableList[2].ADirectory := g_dir_AllocateBankroll;

      DeleteTableFile(szTableList,szCodeList);
    end;

  end;

  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;
{
function GetProjectSurvey(lpCode , lpFieldNmae : string ):string;
begin
  Result := '';
  with DM.Qry do
  begin
    Close;
    SQL.Text := 'Select *from ' + g_Table_Project_Survey + ' Where code = "' + lpCode + '"';
    Open;
    if RecordCount <> 0 then
    begin
      Result := FieldByName(lpFieldNmae).AsString;
    end;
  end;

end;
}
procedure TfrmConstructManage.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmConstructManage.cxButton1Click(Sender: TObject);
var
  s : string;
  szCode : string;
  DD1 : string;
  DD2 : string;
  DD3 : string;
  DD4 : string;

begin
  {
  DD1 := 'AN-';
  DD2 := 'SN-';
  DD3 := 'DN-';
  DD4 := 'GN-';
  
  s := Self.cxTextEdit1.Text;
  
  if Length(s) <> 0 then
  begin
     if Length( g_PostCode  ) <> 0 then
     begin

          g_treeType := Self.RzPageControl1.ActivePageIndex;
          case Self.RzPageControl1.ActivePageIndex of
            0:
            begin
              szCode := DM.getDataMaxDate(g_Table_Project_Dossier_tree);
              if g_treeDataA.AddChildNode(Concat(DD1, szCode),'',0.0,0,0,'',Now, Now ,g_treeType,s) then
                Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION)
              else
                Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONQUESTION);

            end;
            1:
            begin
              szCode := DM.getDataMaxDate(g_Table_Project_Procedures_Tree);
              if g_treeDataB.AddChildNode(Concat(DD2, szCode),'',0.0,0,0,'',Now, Now ,g_treeType,s) then
                Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION)
              else
                Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONQUESTION);

            end;
            2:
            begin
              szCode := DM.getDataMaxDate(g_Table_Project_SceneManage_Tree);
              if g_treeDataC.AddChildNode(Concat(DD3, szCode),'',0.0,0,0,'',Now, Now ,g_treeType,s) then
                Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION)
              else
                Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONQUESTION);

            end;
            3:
            begin
              szCode := DM.getDataMaxDate(g_Table_Project_System_Tree);
              if g_treeDataD.AddChildNode(Concat(DD4, szCode),'',0.0,0,0,'',Now, Now ,g_treeType,s) then
                Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION)
              else
                Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONQUESTION);

            end;

          end;

     end else
     begin
        //没有选择一个项目名
        Application.MessageBox('先选择一个项目名称在添加此项',m_title,MB_OK + MB_ICONQUESTION)
     end;

  end;  
  }
end;

procedure TfrmConstructManage.cxDateEdit15PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := (Sender as TcxDateEdit).Text;
  if s = '' then
  begin
    (Sender as TcxDateEdit).Date := Date;
  end;  
end;

procedure TfrmConstructManage.cxDBCurrencyEdit1PropertiesEditValueChanged(
  Sender: TObject);
var
  s  : Currency;
begin
  inherited;
  s := Self.cxDBCurrencyEdit1.Value;
  Self.cxDBTextEdit7.Text := MoneyConvert(s);
  with Self.QrySurvey do
  begin
    if (State = dsEdit) or
       (State = dsInsert) then
    begin
      Post;
    end;
  end;
end;

procedure TfrmConstructManage.cxSplitter2CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
//  FramerControl.Width:= Self.RzPanel5.Width;
//  FramerControl.Height:=Self.RzPanel5.Height;
end;

procedure TfrmConstructManage.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin

  end;
end;

procedure TfrmConstructManage.Excel1Click(Sender: TObject);
var
  szIsRport : Boolean;
begin
  inherited;
  
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    1:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    2:
    begin
    // CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    3:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    4:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    5:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet13.Caption);
    end;
    6:
    begin
      //增奖扣罚
    end;
    7:
    begin
      //租赁管理
    end;
    8:
    begin
      //快捷单据
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet15.Caption);
    end;
    9:
    begin
      //合同信息
      szIsRport := IsRport(Self.QryPactInfo);
      if szIsRport then
      begin
        CxGridToExcel(Self.Grid,Self.TabSheet16.Caption);
      end;
    end;
    10:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet17.Caption);
    end;
    11:
    begin
    //  CxGridToExcel(Self.SanctionGrid,Self.TabSheet18.Caption);
    end;
    12:
    begin
      //拨款申请单
      CxGridToExcel(Self.AllocateBankrollGrid,Self.TabSheet31.Caption);
    end;
  end;
end;

procedure TfrmConstructManage.FormActivate(Sender: TObject);
begin
  inherited;
  dwCurrentPath :=  Concat(g_Resources + '\', g_Dir_ArchivesManage ) ;
  if not DirectoryExists( dwCurrentPath ) then CreateOpenDir(Handle, dwCurrentPath ,False);
end;

procedure TfrmConstructManage.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  i : Integer;
begin
  g_pacttype := 0;
//  FramerControl.Close;
  Action := caFree;
end;

procedure IsADOClose(Qry : TADOQuery);
begin
  with Qry do
  begin
    if State <> dsInactive then
    begin

      if (State = dsEdit) or (State = dsInsert) then
      begin
        Refresh;
        Close;
      end;

    end;

  end;

end;

procedure TfrmConstructManage.FormCreate(Sender: TObject);
var
  i : Integer;
  List : TStringList;
  ChildFrame : TProjectFrame;
begin
  inherited;
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.IsSystem := True;
  ChildFrame.Parent := Self.Left;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit3.Date := Date;
  Self.cxDateEdit4.Date := Date;

end;

procedure TfrmConstructManage.FormShow(Sender: TObject);
begin
  g_ProjectDir   := Concat(g_Resources,'\ArchivesManage');
  g_BuildStorage := concat(g_Resources,'\BuildStorage');
  g_DirWorksheet := Concat(g_Resources,'\Worksheet');
end;

//删除表中数据时清除所有关联附件与表中数据
function Delete(AtableList : array of TTableList ; lpCodeList : string ; lpFieldName : string = 'Code'):Boolean;
var
  s : Integer;
  i : Integer;
  szTableName : string;
  szDirectory : string;
  szPath : string;
  sqlstr : string;

begin
   Result := True;
   for I := 0 to High(AtableList) do
   begin
     szTableName := AtableList[i].ATableName;
     szDirectory := AtableList[i].ADirectory;

     if (Length(szTableName ) <> 0) and (Length(szDirectory) <> 0) then
     begin
       //查询附件路径
       sqlstr := 'select * from ' + szTableName +' where ' + lpFieldName + ' in('''+ lpCodeList  +''')';
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := sqlstr;
          Open;

          if 0 <> RecordCount then
          begin

            while not Eof do
            begin
              szPath := Concat(g_Resources,'\' + szDirectory + '\' + FieldByName('numbers').AsString );
              if DirectoryExists(szPath) then
              begin
                DeleteDirectory(szPath);
              end;
              Next;
            end;

          end;
          Close;
          SQL.Text := 'delete * from '+ szTableName +' where '+ lpFieldName + ' in('''+ lpCodeList  +''')';
          ExecSQL;
        end;

     end;

   end;
end;

procedure TfrmConstructManage.TreeOperate(Node : TTreeNode;Atype : Byte;s: string = '');
var
  szNode : PNodeData;
  szTableList : array[0..1] of TTableList;
begin

  if Assigned(Node) then
  begin
    if Node.Level = 0 then
    begin
      case Atype of
        0:
        begin
          Application.MessageBox('顶级节点禁止编辑.',m_title,MB_OK + MB_ICONQUESTION) ;
        end;
        1:
        begin
          Application.MessageBox('顶级节点禁止删除.',m_title,MB_OK + MB_ICONQUESTION) ;
        end;     
      end;
    end else
    begin
      szNode := PNodeData(Node.Data);
      
      szTableList[0].ATableName := g_Table_ArchivesManage;
      szTableList[0].ADirectory := g_Dir_ArchivesManage;
      
      case Self.RzPageControl1.ActivePageIndex of
        0:
        begin
            case Atype of
              0://编辑
              begin
              //  g_treeDataA.ModifyNodeCaption(s,Node);
              end;
              1://删除
              begin
                Delete(szTableList,szNode.Code,'Parent');
                g_DossierTree.DeleteTree(Node);
              end;

            end;
        end;
        1:
        begin
            case Atype of
              0://编辑
              begin
              //  g_treeDataB.ModifyNodeCaption(s,Node);
              end;
              1://删除
              begin
                Delete(szTableList,szNode.Code,'Parent');
                g_Procedures.DeleteTree(Node);
              end;

            end;
        end;
        2:
        begin
            case Atype of
              0://编辑
              begin
              //  g_treeDataC.ModifyNodeCaption(s,Node);
              end;
              1://删除
              begin
                Delete(szTableList,szNode.Code,'Parent');
                g_SceneManage.DeleteTree(Node);
              end;

            end;

        end;
        3:
        begin
            case Atype of
              0://编辑
              begin
              //  g_treeDataD.ModifyNodeCaption(s,Node);
              end;
              1://删除
              begin
                Delete(szTableList,szNode.Code,'Parent');
                g_Projectdata.DeleteTree(Node);
              end;

            end;
        end;
      end;
    end;

  end else
  begin
    Application.MessageBox('先选择一个名称在操作此项.',m_title,MB_OK + MB_ICONQUESTION) ;
  end;
  
end;

function GetTreeNodeValue(Node : TTreeNode ;AType : Byte):string;stdcall;
var
  szNode : PNodeData;
begin
  Result := '';
  
  if Assigned(Node) then
  begin

    szNode := PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      case AType of
        0:
        begin
          Result := szNode.Caption;
        end;
        1:
        begin
          Result := szNode.Code;
        end;    
      end;

    end;
  //  Application.MessageBox('先选择一个名称在操作此项.',m_title,MB_OK + MB_ICONQUESTION) ;
  end;  

end;

procedure TfrmConstructManage.N4Click(Sender: TObject);
var
  szIsRport : Boolean;
begin
  inherited;

//  frxReport.Clear;
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
      {
      //工程概况
      szIsRport := IsRport(Self.QrySurvey);
      if szIsRport then
      begin

        frxReport.LoadFromFile(g_ReportPath + '\' + Self.frxSurvey.UserName + '.fr3');

        Self.frxSurvey.FieldAliases.Clear;
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit1.DataBinding.DataField  + '=' + Self.cxLabel1.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit2.DataBinding.DataField  + '=' + Self.cxLabel2.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit3.DataBinding.DataField  + '=' + Self.cxLabel7.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit4.DataBinding.DataField  + '=' + Self.cxLabel8.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit5.DataBinding.DataField  + '=' + Self.cxLabel3.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit6.DataBinding.DataField  + '=' + Self.cxLabel4.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit7.DataBinding.DataField  + '=' + Self.cxLabel9.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit8.DataBinding.DataField  + '=' + Self.cxLabel14.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit9.DataBinding.DataField  + '=' + Self.cxLabel11.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit10.DataBinding.DataField + '=' + Self.cxLabel13.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit11.DataBinding.DataField + '=' + Self.cxLabel12.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit12.DataBinding.DataField + '=' + Self.cxLabel15.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit13.DataBinding.DataField + '=' + Self.cxLabel16.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit14.DataBinding.DataField + '=' + Self.cxLabel20.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit15.DataBinding.DataField + '=' + Self.cxLabel21.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit16.DataBinding.DataField + '=' + Self.cxLabel22.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit17.DataBinding.DataField + '=' + Self.cxLabel29.Caption);

        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit18.DataBinding.DataField + '=' + Self.cxLabel25.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit19.DataBinding.DataField + '=' + Self.cxLabel24.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit20.DataBinding.DataField + '=' + Self.cxLabel23.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit21.DataBinding.DataField + '=' + Self.cxLabel30.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit22.DataBinding.DataField + '=' + Self.cxLabel28.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit23.DataBinding.DataField + '=' + Self.cxLabel27.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit24.DataBinding.DataField + '=' + Self.cxLabel26.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit25.DataBinding.DataField + '=' + Self.cxLabel31.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit26.DataBinding.DataField + '=' + Self.cxLabel54.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit27.DataBinding.DataField + '=' + Self.cxLabel52.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBTextEdit28.DataBinding.DataField + '=' + Self.cxLabel53.Caption);

        Self.frxSurvey.FieldAliases.Add(Self.cxDBCurrencyEdit1.DataBinding.DataField + '=' + Self.cxLabel10.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBDateEdit1.DataBinding.DataField + '=' + Self.cxLabel5.Caption);
        Self.frxSurvey.FieldAliases.Add(Self.cxDBDateEdit2.DataBinding.DataField + '=' + Self.cxLabel6.Caption);
        frxReport.ShowReport;  //报表预览
      end else
      begin
      //  ShowMessage('无法打印');
      end;

      if szIsRport then
      begin
        if Sender = Self.MenuItem7 then
        begin
          frxReport.DesignReport();
        end else
        if Sender = Self.MenuItem8 then
        begin
          frxReport.ShowReport;  //报表预览
        end else
        if Sender = Self.MenuItem9 then
        begin
          frxReport.Print;
        end;
      end;
      }
    end;
    1:
    begin

    end;
    2:
    begin
      DM.BasePrinterLink1.Component := Self.Grid;
      DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
      DM.BasePrinter.Preview(True, nil);
    end;
    3:
    begin
      DM.BasePrinterLink1.Component := Self.AllocateBankrollGrid;
      DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
      DM.BasePrinter.Preview(True, nil);
    end;

  end;

end;

procedure TfrmConstructManage.N5Click(Sender: TObject);
var
  dir: WideString;
  s : string;
begin
  inherited;
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin

    end;
    1:
    begin
      if Self.tvFiles.Controller.FocusedRowIndex >= 0 then
      begin
        s := VarToStr( Self.tvFilesColumn2.EditValue );
        dir := ConcatEnclosure(dwCurrentPath,s) ;
        CreateOpenDir(Handle,dir,True);
      end;
    end;
    2:
    begin

    end;
  end;

end;

procedure TfrmConstructManage.QryPactInfoAfterOpen(DataSet: TDataSet);
begin
  inherited;
//  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmConstructManage.RzToolButton13Click(Sender: TObject);
var
//  Child : TfrmProjectWorksheet;
  szName: string;
  szCount : Integer;
  szIndex : Integer;
  szRecordCount : Integer;
  i : Integer;
  str: string;

begin
  inherited;
  with Self.QryPactInfo do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin

      if Sender = Self.RzToolButton13 then
      begin
        Append;
        FieldByName('Code').Value := g_PostCode;

        szName := Self.tvPactViewCol2.DataBinding.FieldName;
        FieldByName(szName).Value := DM.getDataMaxDate(g_Table_Project_PactInfo);

        szName := Self.tvPactViewCol3.DataBinding.FieldName;
        FieldByName(szName).Value := Date;
        szName := Self.tvPactViewCol4.DataBinding.FieldName;
        FieldByName(szName).Value := g_ProjectName;

        szName := Self.tvPactViewCol6.DataBinding.FieldName;  //项目部
        FieldByName(szName).Value :=  GetProjectSurvey(g_PostCode,Self.cxDBTextEdit29.DataBinding.DataField);

        szName := Self.tvPactViewCol5.DataBinding.FieldName; //资质单位
        FieldByName(szName).Value := GetProjectSurvey(g_PostCode,Self.cxDBTextEdit26.DataBinding.DataField);
        SetGridFocus(Self.tvPactView);
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
      end else
      if Sender = Self.RzToolButton27 then
      begin
      //  cxGridDeleteData(Self.tvPactView,g_dir_PactInfo,g_Table_Project_PactInfo);
        DeleteSelection(Self.tvPactView,g_dir_PactInfo,g_Table_Project_PactInfo,
                        Self.tvPactViewCol2.DataBinding.FieldName);

      end;

    end;

  end;
end;

procedure TfrmConstructManage.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  szCode : string;
  Suffix : string;

begin
  Randomize; 
  Suffix := IntToStr(Random(999999)+1);
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
      case Self.RzPageControl1.ActivePageIndex of
        0:
         begin

           szCode := 'DO' + GetRowCode( g_Table_Project_Dossier_tree ,
                               'Code',
                              Suffix ,
                               1001);

           g_DossierTree.AddChildNode(szCode,'',Date,m_OutText);
         end;
        1:
         begin
           szCode := 'ZO' + GetRowCode( g_Table_Project_Procedures_Tree ,
                               'Code',
                              Suffix ,
                               1002);
           g_Procedures.AddChildNode(szCode,'',Date,m_OutText);
         end;
        2:
         begin
           szCode := 'TO' + GetRowCode( g_Table_Project_SceneManage_Tree,
                               'Code',
                              Suffix ,
                               1003);

           g_SceneManage.AddChildNode(szCode,'',Date,m_OutText);
         end;
        3:
         begin
           szCode := 'CO' + GetRowCode( g_Table_Project_System_Tree ,
                               'Code',
                              Suffix ,
                               1004);

           g_Projectdata.AddChildNode(szCode,'',Date,m_OutText);
         end;
      end;

    end;

  end;

end;


procedure TfrmConstructManage.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  TreeAddName(Self.tv1);
end;

function QueryRange(StartDate,EndDate : string; Qry : TADOQuery; sqltext : string  ):Boolean;stdcall;
var
  szColName : string;
begin
  if (StartDate = '')  and (EndDate  = '') then
  begin
    Application.MessageBox( '起始日期或结束日期不是有效的查询日期，查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)
  end else
  begin
    if CompareDateTime(StrToDate(EndDate),StrToDate(StartDate)) <> -1 then
    begin
      with Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqltext;
        Parameters.ParamByName('t1').Value := StartDate;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := EndDate;
        Open;
      end;
    
    end else
    begin
      Application.MessageBox( '起始日期不能大于结束日期，范围查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)

    end;    
  
  end;
  
end; 

procedure TfrmConstructManage.RzToolButton26Click(Sender: TObject);
begin
  inherited;
  IsDeleteEmptyData(Self.QryPactInfo,
                    g_Table_Project_PactInfo ,
                    Self.tvPactViewCol4.DataBinding.FieldName);
end;

procedure TfrmConstructManage.RzToolButton28Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_dir_PactInfo;
    Child.ShowModal;
    SetcxGrid(Self.tvPactView,0, g_dir_PactInfo);
  finally
    Child.Free;
  end;
end;

procedure TfrmConstructManage.RzToolButton2Click(Sender: TObject);
var
  Node : TTreeNode;
begin
  inherited;
  //删除
  if MessageBox(Handle,'是否要删除此节点','提示',MB_YESNO + MB_ICONQUESTION) = IDYes then
  begin
    case Self.RzPageControl1.ActivePageIndex of
      0:
      begin
        TreeOperate(Self.tv1.Selected,1);
      end;
      1:
      begin
        TreeOperate(Self.tv2.Selected,1);
      end;
      2:
      begin
        TreeOperate(Self.tv3.Selected,1);
      end;
      3:
      begin
        TreeOperate(Self.tv4.Selected,1);
      end;
    end;

    GetData(dwSQlText);
  end;

end;

procedure TfrmConstructManage.RzToolButton30Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmConstructManage.RzToolButton31Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.tvPactViewCol3.DataBinding.FieldName;
  sqltext := 'select * from ' + g_Table_Project_PactInfo + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  QueryRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,Self.QryPactInfo,sqltext);
end;

procedure TfrmConstructManage.RzToolButton32Click(Sender: TObject);
var
  szColName : string;
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  if Sender = Self.RzToolButton32 then
  begin
    //新增
    with Self.QryAllocateBankroll do
    begin
      if State = dsInactive then
      begin
         Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
      end else
      begin
        Append;
        FieldByName('Code').Value := g_PostCode;

        szColName := Self.tvABViewColumn2.DataBinding.FieldName;
        FieldByName(szColName).Value := DM.getDataMaxDate(g_Table_Project_AllocateBankroll);

        szColName := Self.tvABViewColumn3.DataBinding.FieldName;
        FieldByName(szColName).Value := Date;

        szColName := Self.tvABViewColumn4.DataBinding.FieldName;
        FieldByName(szColName).Value := g_ProjectName;

        szColName := Self.tvABViewColumn5.DataBinding.FieldName; //资质单位
        FieldByName(szColName).Value := GetProjectSurvey(g_PostCode,Self.cxDBTextEdit26.DataBinding.DataField);

        szColName := Self.tvABViewColumn6.DataBinding.FieldName;  //项目部
        FieldByName(szColName).Value :=  GetProjectSurvey(g_PostCode,Self.cxDBTextEdit29.DataBinding.DataField);

        SetGridFocus(Self.tvABView );
        Self.AllocateBankrollGrid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
      end;
    end;
  end else
  if Sender = Self.RzToolButton33 then
  begin
    //保存
    IsDeleteEmptyData(Self.QryAllocateBankroll,
                    g_Table_Project_AllocateBankroll ,
                    Self.tvABViewColumn4.DataBinding.FieldName);
  end else
  if Sender = Self.RzToolButton34 then
  begin
    //删除
  //  cxGridDeleteData(Self.tvABView,g_dir_AllocateBankroll,g_Table_Project_AllocateBankroll);

    DeleteSelection(Self.tvABView,g_dir_AllocateBankroll,g_Table_Project_AllocateBankroll,
                        Self.tvPactViewCol2.DataBinding.FieldName);

  end else
  if Sender = Self.RzToolButton35 then
  begin
    //表格设置
    Child := TfrmIsViewGrid.Create(Application);
    try
      Child.g_fromName := Self.Name + g_dir_AllocateBankroll;
      Child.ShowModal;
      SetcxGrid(Self.tvABView,0, g_dir_AllocateBankroll);
    finally
      Child.Free;
    end;
  end;
end;

procedure TfrmConstructManage.RzToolButton37Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmConstructManage.RzToolButton38Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.tvABViewColumn3.DataBinding.FieldName;
  sqltext := 'select * from ' + g_Table_Project_AllocateBankroll + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  QueryRange(Self.cxDateEdit3.Text,Self.cxDateEdit4.Text,Self.QryAllocateBankroll,sqltext);
end;

procedure TfrmConstructManage.RzToolButton39Click(Sender: TObject);
begin
  inherited;

  if Sender = Self.RzToolButton39 then
  begin
    with Self.QrySurvey do
    begin
      case State of
        dsInactive: begin;            //操作没打开
                    end;
        dsBrowse: ;
        dsEdit:
          begin
            Post;
            Refresh;
            ShowMessage('编辑完成');
          end;
        dsInsert: 
          begin          
            Post;
            Refresh;
            ShowMessage('新增完成');
          end;
        dsSetKey: ;
        dsCalcFields: ;
        dsFilter: ;
        dsNewValue: ;
        dsOldValue: ;
        dsCurValue: ;
        dsBlockRead: ;
        dsInternalCalc: ;
        dsOpening: ;
      end;
    end;
  end;

end;

procedure TfrmConstructManage.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  Close;
end;

function GetTableId(qry : TADOQuery):string;
begin
  Result := '';
  with qry do
  begin
    if State <> dsInactive then
    begin
      Result := FieldByName('numbers').AsString;
    end;
  end;
end;

procedure TfrmConstructManage.RzToolButton40Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
  Id := GetTableId(Self.QrySurvey);
  dirName := 'insuranceEnclosure';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton41Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := 'ColligateFile';
  CreateOpenDir(Handle,Concat(g_Resources,'\'+ g_dir_ProjectPact +'\' + s),True);
end;

procedure TfrmConstructManage.RzToolButton42Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
  Id := GetTableId(Self.QrySurvey);
  dirName := 'Drawing';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton44Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmConstructManage.RzToolButton45Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
  //AbroadCard
  Id := GetTableId(Self.QrySurvey);
  dirName := 'AbroadCard';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton46Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
//TenderFiled
  Id := GetTableId(Self.QrySurvey);
  dirName := 'TenderFiled';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton47Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
//Tender
  Id := GetTableId(Self.QrySurvey);
  dirName := 'Tender';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton48Click(Sender: TObject);
var
  dirName  : string;
  Id : string;
begin
  inherited;
//Budget book
  Id := GetTableId(Self.QrySurvey);
  dirName := 'BudgetBook';
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_dir_Survey +'\' + id + '\' + dirName
                              ),True);
end;

procedure TfrmConstructManage.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  //编辑
  case Self.RzPageControl1.ActivePageIndex of
    0: Self.tv1.Selected.EditText;
    1: Self.tv2.Selected.EditText;
    2: Self.tv3.Selected.EditText;
    3: Self.tv4.Selected.EditText;
  end;
end;

procedure TfrmConstructManage.RzToolButton84Click(Sender: TObject);
//var
//  Child : TfrmConstructPost;
begin
  inherited;
  if Sender = RzToolButton84 then //新增
  begin
    dwADO.ADOAppend(Self.Master);
    Self.cxGrid1.SetFocus;
    Self.tvFilesColumn1.FocusWithSelection;
  end else
  if Sender = RzToolButton96 then //编辑
  begin
    dwADO.ADOIsEdit(Self.Master);
  end else
  if Sender = RzToolButton6 then  //保存
  begin
    dwADO.ADOSaveData(Self.Master,dwSQlText);
  end else
  if Sender = RzToolButton91 then  //删除
  begin
    dwADO.ADODeleteSelection(Self.tvFiles,
                           dwCurrentPath,
                           g_Table_ArchivesManage,
                           Self.tvFilesColumn2.DataBinding.FieldName);
  end;


  {
  Child := TfrmConstructPost.Create(Application);
  try
      Child.g_TableName   := g_TableName;
      Child.g_PostCode    := g_PostCode;//项目id
      Child.g_PostParent  := g_PostArchivesCode;//档案Id
      Child.g_ProjectDir  := g_ProjectDir;//附件目录

      if Sender = RzToolButton96 then  //编辑
      begin
          if not Self.DBGridEh1.DataSource.DataSet.IsEmpty then
          begin
            Self.DBGridEh1.DataSource.DataSet.Edit;
            Child.g_PostCode       := Self.DBGridEh1.DataSource.DataSet.FieldByName('Code').AsString;//项目id
            Child.g_PostParent     := Self.DBGridEh1.DataSource.DataSet.FieldByName('Parent').AsString;//档案Id
            Child.g_PostNumbers    := Self.DBGridEh1.DataSource.DataSet.FieldByName('Numbers').AsString;
            Child.cxTextEdit1.Text := Self.DBGridEh1.DataSource.DataSet.FieldByName('chCaption').AsString;
            Child.DateTimePicker1.Date :=  Self.DBGridEh1.DataSource.DataSet.FieldByName('chDate').AsDateTime;
            Child.cxMemo1.Text     := Self.DBGridEh1.DataSource.DataSet.FieldByName('chRemarks').AsString;
            Child.ShowModal;

          end else
          begin
            Application.MessageBox('先选中一个条目，然后选择施工类别，才能进行编辑！',m_title,MB_OK + MB_ICONQUESTION) ;
          end;

      end else   //新增
      begin
          if Length( g_PostCode ) <> 0  then
          begin

            if Length(g_PostArchivesCode) <> 0 then
            begin
              Child.g_PostNumbers := DM.getDataMaxDate(g_TableName);
              Child.ShowModal;
              Self.DBGridEh1.DataSource.DataSet.Edit;
            end else
            begin
              Application.MessageBox('请选择一个施工类别后在进行！',m_title,MB_OK + MB_ICONQUESTION) ;
            end;

          end else
          begin
            Application.MessageBox('先选中一个项目，然后选择施工类别，才能操作！',m_title,MB_OK + MB_ICONQUESTION) ;
          end;

      end;

      with Self.qry1 do
      begin
        if (State <> dsEdit) and ( State <> dsInsert ) then Edit;

        Requery();
      end;

  finally
    Child.Free;
  end;
  }
end;

procedure TfrmConstructManage.RzToolButton92Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  s := Self.RzEdit2.Text;
  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := dwSQlText + ' AND ' + tvFilesColumn5.DataBinding.FieldName + ' Like "%' + s + '%"';
    ShowMessage(ADOQuery1.SQL.Text);
    ADOQuery1.Open;
    Self.Master.Data := DataSetProvider1.Data;
    Self.Master.Open;
  end;
end;

function TfrmConstructManage.SelectData():Boolean;
var
  s : string;

begin
  s := ' Code ="' + g_PostCode + '"';
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
      //工程概况
      with Self.QrySurvey do
      begin
        Close;

        SQL.Text := 'Select *from '+ g_Table_Project_Survey +' where Code="' + g_PostCode + '"';

        Open;
        if RecordCount = 0 then
        begin
          Append;
          FieldByName('Code').Value := g_PostCode;
          FieldByName('numbers').Value := 'SY-' + DM.getDataMaxDate(g_Table_Project_Survey);
          s := Self.cxDBDateEdit1.DataBinding.DataField;
          FieldByName(s).Value := Date;
          s := Self.cxDBDateEdit2.DataBinding.DataField;
          FieldByName(s).Value := Date;
          s := Self.cxDBTextEdit4.DataBinding.DataField;
          FieldByName(s).Value := '建筑集体商业险';
          Post;
        end else
        begin
          Edit;
        end;

      end;
    end;
    1:
    begin
      //工程档案
    //  FramerControl.Left   := 8;
    //  FramerControl.Top    := 8;
    ////  FramerControl.Width  := Self.RzPanel5.Width - 16;
    //  FramerControl.Height := Self.RzPanel5.Height- 16;
    end;
    2:
    begin
      //合同信息
      with Self.QryPactInfo do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * from ' + g_Table_Project_PactInfo + ' where Code="' + g_PostCode + '"';
        Open;
      end;
      SetcxGrid(Self.tvPactView,0,g_dir_PactInfo);
    end;
    3:
    begin
      //拨款申请
      with Self.QryAllocateBankroll do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT * from ' + g_Table_Project_AllocateBankroll + ' where Code="' + g_PostCode + '"';
        Open;
      end;
      SetcxGrid(Self.tvABView,0,g_dir_AllocateBankroll);
    end;

  end;

end;

procedure TfrmConstructManage.RzEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin

  end;
end;

procedure TfrmConstructManage.RzPageControl2Click(Sender: TObject);
begin
  SelectData();
end;
{
procedure ShowData(Qry : TClientDataSet; ACode, BCode : string);
begin
end;
}
function UpDateIndex(lpTableName : string ; lpNode : TTreeNode) : Integer;
var
  s : string;
begin
  S := 'UPDATE ' + lpTableName + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;
    
function TfrmConstructManage.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;
  Tv : TTreeView;
  szTableName : string;
  
begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    case Self.RzPageControl1.ActivePageIndex of
      0: g_DossierTree.DeleteTree(Node); //档案管理
      1: g_Procedures.DeleteTree(Node);
      2: g_SceneManage.DeleteTree(Node);
      3: g_Projectdata.DeleteTree(Node);
    end;
  end else
  begin
    case Self.RzPageControl1.ActivePageIndex of
      0:
       begin
         Tv := Self.tv1;
         g_DossierTree.ModifyNodeSignName(s,Node);
         szTableName := g_Table_Project_SceneManage_Tree;
       end;  
      1:
       begin
         Tv := Self.tv2;
         g_Procedures.ModifyNodeSignName(s,Node);
         szTableName := g_Table_Project_Procedures_Tree;
       end;
      2:
       begin
         Tv := Self.tv3;
         g_SceneManage.ModifyNodeSignName(s,Node);
         szTableName := g_Table_Project_SceneManage_Tree
       end; 
      3:
       begin
         Tv := Self.tv4;
         g_Projectdata.ModifyNodeSignName(s,Node);
         szTableName := g_Table_Project_System_Tree
       end;  
      
    end;
    //更新位置
    UpDateIndex(szTableName,Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(szTableName,PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(szTableName,NextNode);

  end;
end;

procedure TfrmConstructManage.MasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton84.Enabled := False;
  Self.RzToolButton6.Enabled  := False;
  Self.RzToolButton91.Enabled := False;
  Self.RzToolButton96.Enabled := False;
  Self.RzEdit2.Enabled := False;
  Self.RzToolButton92.Enabled := False;
end;

procedure TfrmConstructManage.MasterAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  dwPrefix , dwSuffix  : string;

begin
  inherited;
  dwPrefix := 'DA';
  dwSuffix := '100009';
  with DataSet do
  begin
    szCode := dwPrefix + GetRowCode(g_Table_ArchivesManage,Self.tvFilesColumn2.DataBinding.FieldName,dwSuffix,200000);
    Self.tvFilesColumn4.EditValue := Date;
    Self.tvFilesColumn2.EditValue := szCode;
    FieldByName('Parent').Value:= g_PostArchivesCode;
    FieldByName('Code').Value  := g_PostCode;
  end;

end;

procedure TfrmConstructManage.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton84.Enabled := True;
  Self.RzToolButton6.Enabled  := True;
  Self.RzToolButton91.Enabled := True;
  Self.RzToolButton96.Enabled := True;
  Self.RzEdit2.Enabled := True;
  Self.RzToolButton92.Enabled := True;
end;

procedure TfrmConstructManage.tv1DblClick(Sender: TObject);
var
  s : string;
  PData: PNodeData;
  szRowCount : Integer;
  g_SelectNodeId : Integer;
  szCode : string;
  Node : TTreeNode;

begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0:  Node := Self.tv1.Selected;
    1:  Node := Self.tv2.Selected;
    2:  Node := Self.tv3.Selected;
    3:  Node := Self.tv4.Selected;
  end;

  if Node.Level >= 1 then
  begin
    PData := PNodeData(Node.Data);
    if Assigned(PData) then
    begin
      g_SelectNodeId := PData^.Index;
      g_PostArchivesCode := pData^.Code;
      //有项目也有档案名
      dwSQlText := 'Select * from '+ g_Table_ArchivesManage +
                   ' where Code="'+ g_PostCode + '" and Parent="' + g_PostArchivesCode +'"';
      GetData(dwSQlText);
    end;

  end;

end;

procedure TfrmConstructManage.tv1Edited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  //档案编辑
  IstreeEdit(s,Node);
end;

procedure TfrmConstructManage.tv1Editing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  inherited;
  if Node.Level = 0 then
  begin
    AllowEdit := False;
  end;
end;

procedure TfrmConstructManage.tv1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  //回车连续录入
  if Key = 13 then
  begin
    case Self.RzPageControl1.ActivePageIndex of
      0: Self.tv1.Selected.Parent.Selected := True;
      1: Self.tv2.Selected.Parent.Selected := True;
      2: Self.tv3.Selected.Parent.Selected := True;
      3: Self.tv4.Selected.Parent.Selected := True;
    end;
    Self.RzToolButton1.Click;
  end else
  if Key = 46 then
  begin
    Self.RzToolButton2.Click;
  end;
end;

procedure TfrmConstructManage.tvABViewColumn10PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  s : string;
  szColName : string;
begin
  inherited;
  try
    s := VarToStr(DisplayValue);
    with Self.QryAllocateBankroll do
    begin
      if State <> dsInactive then
      begin
        szColName := Self.tvABViewColumn9.DataBinding.FieldName;
        Edit;
        FieldByName(szColName).Value := MoneyConvert(StrToFloatDef(s,0));
        if IsNewRow then
        begin
        end;
      end;
    end;
  except
  end;
end;

procedure TfrmConstructManage.tvABViewColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvABView,1, g_dir_AllocateBankroll);
end;

procedure TfrmConstructManage.tvABViewDblClick(Sender: TObject);
var
  szDir : string;

begin
  inherited;
  szDir :=  Concat(g_Resources,'\' + g_dir_AllocateBankroll);
  CreateEnclosure(Self.tvABView.DataController.DataSource.DataSet,Handle, szDir);
end;

procedure TfrmConstructManage.tvABViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if (AItem.Index = Self.tvABViewColumn1.Index) or
     (AItem.Index = Self.tvABViewColumn2.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmConstructManage.tvABViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if AItem.Editing then
    begin
      if AItem.Index = Self.tvABViewColumn10.Index then
      begin
        if Self.tvABView.Controller.FocusedRow.IsLast then
        begin
          if IsNewRow then
          begin
          //  Self.tvABView.DataController.UpdateData;
            Self.RzToolButton32.Click;
          end;
        end;

      end;

    end;

  end;

end;

procedure TfrmConstructManage.tvABViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton34.Click;
  end;
end;

procedure TfrmConstructManage.tvFilesDblClick(Sender: TObject);
var
  dir: WideString;
  s : string;
begin
  inherited;
  s := VarToStr( Self.tvFilesColumn2.EditValue );
  dir := ConcatEnclosure(dwCurrentPath ,s) ;
  if not DirectoryExists(dir) then
  begin
    CreateOpenDir(Handle,dir,False);
  end;
end;

procedure TfrmConstructManage.tvFilesEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);

  if (AItem.Index = Self.tvFilesColumn1.Index) or (Aitem.Index = Self.tvFilesColumn2.Index) then AAllow := False;

end;

procedure TfrmConstructManage.tvFilesEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvFilesColumn5.Index) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOAppend(Self.Master);
      Self.tvFilesColumn3.FocusWithSelection;
    end;
  end;

end;

procedure TfrmConstructManage.tvFlowingAccountColumn14GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmConstructManage.tvLeaseCol19CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clYellow;
  ACanvas.Font.Color := clBlue;  //
end;

function TfrmConstructManage.GetDisparityMoney(ProjectOffer , MarkeOffer : Variant) : Boolean;
var
  szProjectOffer , szMarketOffer : Variant;
  A,B,szDisparityMoney : Currency;
  szColName : string;

begin
  with Self.QryPactInfo do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvPactViewCol12.DataBinding.FieldName;
      FieldByName(szColName).Value := ProjectOffer;

      szColName := Self.tvPactViewCol14.DataBinding.FieldName;
      FieldByName(szColName).Value := MarkeOffer;

      szColName := Self.tvPactViewCol15.DataBinding.FieldName;
      A := StrToCurrDef( VarToStr(ProjectOffer) ,0 );
      B := StrToCurrDef( VarToStr(MarkeOffer) ,0 );
      FieldByName(szColName).Value := A - B;
    end;
  end;

end;

procedure TfrmConstructManage.tvPactViewCol12PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  RecordIndex : Integer;
  szProjectOffer , szMarketOffer : Variant;
begin
  inherited;
  RecordIndex := Self.tvPactView.Controller.FocusedRowIndex;
  szMarketOffer := Self.tvPactView.DataController.Values[RecordIndex,Self.tvPactViewCol14.Index];
  GetDisparityMoney(DisplayValue,szMarketOffer);

  {
  with Self.tvPactView.DataController do
  begin
    szProjectOffer:= Values[RecordIndex,Self.tvPactViewCol12.Index];
    szMarketOffer := Values[RecordIndex,Self.tvPactViewCol14.Index];
  end;
  }
end;

procedure TfrmConstructManage.tvPactViewCol14PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  RecordIndex : Integer;
  szProjectOffer , szMarketOffer : Variant;
begin
  inherited;
  RecordIndex := Self.tvPactView.Controller.FocusedRowIndex;
  szProjectOffer := Self.tvPactView.DataController.Values[RecordIndex,Self.tvPactViewCol12.Index];
  GetDisparityMoney(szProjectOffer,DisplayValue);
end;

procedure TfrmConstructManage.tvPactViewCol8GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
//人工 材料 分包 机械 租赁 其他
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('人工');
    Items.Add('材料');
    Items.Add('分包');
    Items.Add('机械');
    Items.Add('租赁');
    Items.Add('其他');
  end;
end;

procedure TfrmConstructManage.tvPactViewColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvPactView,1, g_dir_PactInfo);
end;

procedure TfrmConstructManage.tvPactViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  //附件
  szDir :=  Concat(g_Resources,'\' + g_dir_PactInfo);
  CreateEnclosure(Self.tvPactView.DataController.DataSource.DataSet,Handle, szDir );
end;

procedure TfrmConstructManage.tvPactViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if (AItem.Index = Self.tvPactViewCol1.Index) or
     (AItem.Index = Self.tvPactViewCol2.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmConstructManage.tvPactViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if AItem.Editing then
    begin
      if AItem.Index = Self.tvPactViewCol15.Index then
      begin
        if Self.tvPactView.Controller.FocusedRow.IsLast then
        begin
          if IsNewRow then
          begin
          //  Self.tvPactView.DataController.UpdateData;
            Self.RzToolButton13.Click;
          end;
        end;

      end;

    end;

  end;

end;

procedure TfrmConstructManage.tvPactViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton27.Click;
  end;
end;

procedure TfrmConstructManage.tvWorkGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

end.
