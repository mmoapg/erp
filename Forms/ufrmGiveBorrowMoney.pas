unit ufrmGiveBorrowMoney;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RzPanel, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, Menus, cxStyles, cxEdit, cxScheduler,
  cxSchedulerStorage, cxSchedulerCustomControls, cxSchedulerCustomResourceView,
  cxSchedulerDayView, cxSchedulerDateNavigator, cxSchedulerHolidays,
  cxSchedulerTimeGridView, cxSchedulerUtils, cxSchedulerWeekView,
  cxSchedulerYearView, cxSchedulerGanttView, cxClasses, cxCustomData,
  cxCustomPivotGrid, cxPivotGrid, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, ComCtrls, Mask, RzEdit, RzButton, StdCtrls, RzTabs,
  ImgList, cxContainer, cxCurrencyEdit, cxDBEdit, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, ADODB, cxButtons, RzLabel, RzBckgnd,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmGiveBorrowMoney = class(TForm)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label19: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn7: TRzBitBtn;
    DateTimePicker1: TDateTimePicker;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label2: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxMemo1: TcxMemo;
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxCurrencyEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode : string;
    g_PostNumbers : string;
    g_ProjectDir  : string;
    g_IsModify : Boolean;
    
  end;

var
  frmGiveBorrowMoney: TfrmGiveBorrowMoney;
  g_TableName : string = 'sk_GiveBorrowMoney';
  
implementation

uses
   ufrmBorrowMoney,global,uDataModule,DateUtils;

{$R *.dfm}

procedure TfrmGiveBorrowMoney.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit1.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmGiveBorrowMoney.cxCurrencyEdit2PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit2.Text := MoneyConvert(Self.cxCurrencyEdit2.Value);
end;

procedure TfrmGiveBorrowMoney.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmGiveBorrowMoney.FormShow(Sender: TObject);
begin
  {
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin   //添加还款
      if not frmBorrowMoney.DBGridEh3.DataSource.DataSet.IsEmpty then
      begin
        g_PostCode    := frmBorrowMoney.DBGridEh3.DataSource.DataSet.FieldByName('Code').AsString;
        g_PostNumbers := frmBorrowMoney.DBGridEh3.DataSource.DataSet.FieldByName('numbers').AsString;
        szCaption     := frmBorrowMoney.DBGridEh3.DataSource.DataSet.FieldByName('chName').AsString;

      end
    end;
    1:
    begin  //修改还款
      if not frmBorrowMoney.DBGridEh1.DataSource.DataSet.IsEmpty then
      begin
        g_PostCode  := frmBorrowMoney.DBGridEh1.DataSource.DataSet.FieldByName('Code').AsString;
        g_PostNumbers := frmBorrowMoney.DBGridEh1.DataSource.DataSet.FieldByName('numbers').AsString;
        szCaption := frmBorrowMoney.DBGridEh1.DataSource.DataSet.FieldByName('chName').AsString;

        with Self.ADOQuery1 do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from '+ g_TableName +' where numbers="'+ g_PostNumbers+'"';
          Open;
        end;

      end;
    end;
  end;
  Self.RzPanel3.Caption := szCaption;
  }
end;

procedure TfrmGiveBorrowMoney.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmGiveBorrowMoney.RzBitBtn3Click(Sender: TObject);
var
  s : string;
  szCaption: string;
  szContent : string;
  szRemarks: string;
  pData : PPaytype;
  szPayDate : TDateTime;
  szEnclosure : string; //附件
  szGiveMoney : Currency; //还款金额
  szRate : Currency; //利率
  szInterest : Currency; //利息
begin
  if g_PostNumbers = '' then
  begin
    //选择还款条目
    Application.MessageBox('请先选择一条要还款的条目!',m_title,MB_OK + MB_ICONQUESTION);
    Close;
  end
  else
  begin
    szGiveMoney := Self.cxCurrencyEdit1.Value;; //还款金额
    szRemarks   := Self.cxMemo1.Text;  //备注
    szEnclosure := '';    //附件
    szInterest  := Self.cxCurrencyEdit2.Value ;  //利息
    szPayDate   := Self.DateTimePicker1.Date;
    if g_IsModify then
    begin
      s := Format('Update '+ g_TableName + ' set chGiveMonety=''%f'',chInterest=''%f'',chPayDate="%s",chRemarks="%s" where numbers="%s"',[
                                        szGiveMoney,
                                        szInterest,
                                        FormatDateTime('yyyy-MM-dd',szPayDate),
                                        szRemarks,
                                        g_PostNumbers
                                       ]);

      OutputLog(s);
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        m_IsModify := True;
        if ExecSQL <>  0 then
        begin
          //还款成功
          Application.MessageBox('还款编辑成功!',m_title,MB_OK + MB_ICONQUESTION);
        end
        else
        begin
          //还款失败
          Application.MessageBox('还款编辑失败!',m_title,MB_OK + MB_ICONQUESTION);
        end;

      end;

    end else
    begin
      s := Format( 'Insert into ' + g_TableName +' (code,'+
                                                   'numbers,'+
                                                   'Parent,' +
                                                   'chGiveMonety,' +
                                                   'chInterest,' +
                                                   'chPayDate,' +
                                                   'chEnclosure,'+
                                                   'chRemarks'+
                                                   ') values("%s","%s",%s,%f,%f,"%s","%s","%s")',[
                                                                g_PostCode,
                                                                DM.getDataMaxDate(g_TableName),
                                                                g_PostNumbers,
                                                                szGiveMoney,
                                                                szInterest,
                                                                FormatDateTime('yyyy-MM-dd',szPayDate),
                                                                szEnclosure,
                                                                szRemarks
                                                                ]);

      OutputLog(s);
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        m_IsModify := True;
        if ExecSQL <>  0 then
        begin
          //还款成功
          Application.MessageBox('还款添加成功!',m_title,MB_OK + MB_ICONQUESTION);
        end
        else
        begin
          //还款失败
          Application.MessageBox('还款添加失败!',m_title,MB_OK + MB_ICONQUESTION);
        end;

      end;

    end;

  end;
      
end;

procedure TfrmGiveBorrowMoney.RzBitBtn4Click(Sender: TObject);
{
var
  s : string;
  szCaption: string;
  szContent : string;
  szRemarks: string;
  szPayDate : TDateTime;
  szEnclosure : string; //附件
  szGiveMoney : Currency; //还款金额
  szInterest : Currency; //利息
  }
begin
  {
  szGiveMoney := Self.cxDBCurrencyEdit1.Value;
  if szGiveMoney = 0 then
  begin

  end
  else
  begin
     Self.ADOQuery1.UpdateBatch(arAll);
  end;
  }
end;

procedure TfrmGiveBorrowMoney.RzBitBtn7Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

end.
