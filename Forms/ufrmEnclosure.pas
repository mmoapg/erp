unit ufrmEnclosure;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Winapi.ShlObj,
  cxShellCommon, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxBreadcrumbEdit, dxShellBreadcrumbEdit, cxTreeView, cxShellTreeView,
  cxContainer, cxEdit, cxListView, cxShellListView, RzButton, RzPanel,
  Vcl.ExtCtrls;

type
  TfrmEnclosure = class(TForm)
    cxShellListView1: TcxShellListView;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzSpacer57: TRzSpacer;
    RzToolButton1: TRzToolButton;
    StatusBar1: TStatusBar;
    cxShellTreeView1: TcxShellTreeView;
    Splitter4: TSplitter;
    RzToolButton88: TRzToolButton;
    RzSpacer1: TRzSpacer;
    procedure RzToolButton1Click(Sender: TObject);
    procedure cxShellListView1DblClick(Sender: TObject);
    procedure RzToolButton85Click(Sender: TObject);
  private
    { Private declarations }
    procedure GetCurrentPath();
  public
    { Public declarations }
  end;

var
  frmEnclosure: TfrmEnclosure;

implementation

{$R *.dfm}

procedure TfrmEnclosure.cxShellListView1DblClick(Sender: TObject);
begin
  GetCurrentPath;
end;

procedure TfrmEnclosure.GetCurrentPath();
begin
  Self.StatusBar1.Panels[1].Text := Self.cxShellListView1.Root.CurrentPath;
end;

procedure TfrmEnclosure.RzToolButton1Click(Sender: TObject);
begin
  with Self.cxShellListView1 do
  begin
    if Root.CurrentPath <> Root.CustomPath then BrowseParent;
  end;
  GetCurrentPath;
end;

procedure TfrmEnclosure.RzToolButton85Click(Sender: TObject);
begin
  Self.cxShellListView1.Refresh;
end;

end.
