unit ufrmIsViewGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxTextEdit,
  cxTLdxBarBuiltInMenu, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxInplaceContainer, cxBlobEdit, dxSkinscxPCPainter,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Data.Win.ADODB, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TfrmIsViewGrid = class(TForm)
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid2DBTableView1Column2GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
  private
    { Private declarations }
  public
    { Public declarations }
    g_fromName : string;
  end;

var
  frmIsViewGrid: TfrmIsViewGrid;

implementation

uses
   uDataModule;

{$R *.dfm}

procedure TfrmIsViewGrid.cxGrid2DBTableView1Column2GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmIsViewGrid.FormActivate(Sender: TObject);
var
  szTableName : string;

begin
  szTableName := 'sk_Config';
  with Self.ADOQuery2 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + szTableName + ' where fromName="' + g_fromName + '"' ;
    Open;
  end;
end;

procedure TfrmIsViewGrid.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Self.ADOQuery2.State = dsEdit then
  begin
    Self.ADOQuery2.Post;
  end;
end;

end.
