object frmCustomerSummaryU: TfrmCustomerSummaryU
  Left = 0
  Top = 0
  Caption = #23458#25143#32479#35745
  ClientHeight = 407
  ClientWidth = 676
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 676
    Height = 407
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2013White'
    OptionsView.RowHeaderWidth = 116
    OptionsData.CancelOnExit = False
    OptionsData.Editing = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.DeletingConfirmation = False
    OptionsData.Inserting = False
    Navigator.Buttons.CustomButtons = <>
    ParentFont = False
    TabOrder = 0
    DataController.DataSource = ds
    ExplicitHeight = 338
    Version = 1
    object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = #27719#24635#21512#35745
      Properties.HeaderAlignmentHorz = taCenter
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.Caption = #23567#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.DisplayFormat = #165',0.00;'#165'-,0.00 ('#21547#20154#24037#32452')'
      Properties.DataBinding.FieldName = 'ASmall'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      Properties.Caption = #22823#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'ALarge'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #20248#24800#21069#21512#35745
      Properties.HeaderAlignmentHorz = taCenter
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
      Properties.Caption = #23567#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'BSmall'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
      Properties.Caption = #22823#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'BLarge'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = #20248#24800#21512#35745
      Properties.HeaderAlignmentHorz = taCenter
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
      Properties.Caption = #23567#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'CSmall'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
      Properties.Caption = #22823#20889
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'CLarge'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
      Properties.Caption = #26126#32454#21015#34920
      Properties.HeaderAlignmentHorz = taCenter
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow
      Properties.Caption = #32508#21512#32452#30452#25509#36153
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'Colligate'
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow
      Properties.Caption = #36153#29992#32452#21512#35745
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.DataBinding.FieldName = 'Extras'
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow
      Properties.Caption = #20154#24037#32452#30452#25509#36153
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'ManpowerSmall'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow
      Properties.Caption = #31246#21069#37329#39069#21512#35745
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'TaxGoldFront'
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow11: TcxDBEditorRow
      Properties.Caption = #30452#25509#36153#21512#35745
      Properties.HeaderAlignmentHorz = taRightJustify
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.DataBinding.FieldName = 'DirectTotal'
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
  end
  object dxGroupname: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F06000000080000000700070041536D616C6C00FF00
      000001000700414C6172676500080000000700070042536D616C6C00FF000000
      01000700424C6172676500080000000700070043536D616C6C00FF0000000100
      0700434C61726765000100000000000000000000000000000000000000000000
      00000000000000}
    SortOptions = []
    Left = 168
    Top = 184
    object dxGroupnameASmall: TCurrencyField
      FieldName = 'ASmall'
    end
    object dxGroupnameALarge: TStringField
      FieldName = 'ALarge'
      Size = 255
    end
    object dxGroupnameBSmall: TCurrencyField
      FieldName = 'BSmall'
    end
    object dxGroupnameBLarge: TStringField
      FieldName = 'BLarge'
      Size = 255
    end
    object dxGroupnameCSmall: TCurrencyField
      FieldName = 'CSmall'
    end
    object dxGroupnameCLarge: TStringField
      FieldName = 'CLarge'
      Size = 255
    end
    object dxGroupnameManpowerSmall: TCurrencyField
      FieldName = 'ManpowerSmall'
    end
    object dxGroupnameColligate: TStringField
      FieldName = 'Colligate'
      Size = 255
    end
    object dxGroupnameExtras: TStringField
      FieldName = 'Extras'
      Size = 255
    end
    object dxGroupnameTaxGoldFront: TCurrencyField
      FieldName = 'TaxGoldFront'
    end
    object dxGroupnameDirectTotal: TCurrencyField
      FieldName = 'DirectTotal'
    end
  end
  object ds: TDataSource
    DataSet = dxGroupname
    Left = 168
    Top = 240
  end
end
