unit ufrmBorrowEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxMemo, cxDBEdit, cxCurrencyEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxTextEdit, RzButton, StdCtrls, ComCtrls, RzLabel,
  RzBckgnd, Menus, cxButtons, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxCore,
  cxDateUtils;

type
  TfrmBorrowEdit = class(TForm)
    GroupBox1: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label32: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label26: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    RzBitBtn5: TRzBitBtn;
    RzBitBtn8: TRzBitBtn;
    RzBitBtn9: TRzBitBtn;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxMemo1: TcxMemo;
    cxMemo2: TcxMemo;
    cxTextEdit1: TcxTextEdit;
    GroupBox2: TGroupBox;
    Label21: TLabel;
    Label25: TLabel;
    Label23: TLabel;
    Label20: TLabel;
    Label27: TLabel;
    Label22: TLabel;
    Label30: TLabel;
    RzLabel1: TRzLabel;
    RzLabel2: TRzLabel;
    RzLabel3: TRzLabel;
    RzLabel4: TRzLabel;
    RzLabel5: TRzLabel;
    RzLabel6: TRzLabel;
    RzLabel7: TRzLabel;
    RzLabel8: TRzLabel;
    RzLabel9: TRzLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxCurrencyEdit9: TcxCurrencyEdit;
    cxCurrencyEdit10: TcxCurrencyEdit;
    cxCurrencyEdit11: TcxCurrencyEdit;
    cxCurrencyEdit12: TcxCurrencyEdit;
    cxCurrencyEdit13: TcxCurrencyEdit;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxCurrencyEdit7: TcxCurrencyEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxDateEdit2: TcxDateEdit;
    cxDateEdit3: TcxDateEdit;
    RzSeparator1: TRzSeparator;
    cxComboBox1: TcxComboBox;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure RzBitBtn9Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxCurrencyEdit3PropertiesChange(Sender: TObject);
    procedure cxDateEdit3PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit4PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit13PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesChange(Sender: TObject);
    procedure RzBitBtn8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PostNumbers: string;
    g_TableName  : string;
    
  end;

var
  frmBorrowEdit: TfrmBorrowEdit;

implementation

uses
   ufrmCalc,global,DateUtils,uDataModule,Math;

{$R *.dfm}

procedure TfrmBorrowEdit.cxButton1Click(Sender: TObject);
var
  DD1 , DD2  : TDateTime;
  szStartYear, szStartMonth , szStartDay  : Word;
  szStopYear , szStopMonth  , szStopDay   : Word;

  szTmpDay , szMonthDay : Integer;
  t :TDatetime;
  szSumDay:integer;
  szMonthSpan : Double;
  c : Real;

  szDateType : Integer;
  szPrincipal     : Currency; //本金
  szMoneyRate     : Currency; //利率
  szTotal         : Currency; //总额
  szMonthInterest : Currency; //月利息
  szDaysInterest  : Currency; //天利息
  szTotalDaysInterest : Currency; //总天息

Begin
  DD1 := Self.cxDateEdit2.Date;
  DD2 := Self.cxDateEdit3.Date;
  if DD2 <= DD1 then
  begin
    ShowMessage('结束日期不得小于开始日期');
  end else
  begin
    szDateType := Self.cxComboBox1.ItemIndex; //日期类型

    Self.cxCurrencyEdit6.Value  := 0;  //月数
    Self.cxCurrencyEdit12.Value := 0; //月息
    Self.cxCurrencyEdit9.Value  := 0; //月总息

    Self.cxCurrencyEdit7.Value  := 0;  //超出天数
    Self.cxCurrencyEdit11.Value := 0; //天息
    Self.cxCurrencyEdit10.Value := 0; //天总息

    szPrincipal := Self.cxCurrencyEdit4.Value;
    szMoneyRate := Self.cxCurrencyEdit5.Value / 100;  //利率
    szDaysInterest := szPrincipal * szMoneyRate / 30; //天息

    DecodeDate(DD1,szStartYear, szStartMonth, szStartDay); //分解
    szMonthDay := DaysInMonth(DD1) ; //反回总天数
    DecodeDate(DD2,szStopYear, szStopMonth, szStopDay); //分解
    szMonthSpan := MonthSpan(DD2,DD1);
    c := Frac(szMonthSpan);
    //**************************************************************************
    //算出月数
    if (szStopMonth = szStartMonth) and (szStopDay = szStartDay)  then
      t := Ceil(szMonthSpan)
    else
      t := Trunc( szMonthSpan );
    //**************************************************************************
    //算出天数
    szTmpDay := (szMonthDay - (szStartDay)) + szStopDay;
    if szStartDay > szStopDay then
      szSumDay := szTmpDay
    else
      szSumDay := szTmpDay - szMonthDay;

    case szDateType of
     0:
     begin
       //月份
     end;
     1:
     begin
       //总天数
       szSumDay := Ceil( DaysBetween(DD1,DD2) );
     end;
    end;
    //**************************************************************************
          //算利息
     szMonthInterest     := Round(t) * szMoneyRate * szPrincipal; //月利息
     szTotalDaysInterest := szSumDay * szDaysInterest;  //天息
    //**************************************************************************
    case szDateType of  // 0 = 月  1 = 天
      0:
      begin
        Self.cxCurrencyEdit13.Value := szMonthInterest ;
        Self.cxCurrencyEdit3.Value  := szMonthInterest  ; //+ szMonthInterest
      end;
      1:
      begin
        //取天数
        Self.cxCurrencyEdit13.Value := szTotalDaysInterest ;
        Self.cxCurrencyEdit3.Value  := szTotalDaysInterest ; //+ szMonthInterest
      end;
    end;
    //****************************************************************************
    Self.cxCurrencyEdit6.Value  := Round(t);  //月数
    Self.cxCurrencyEdit7.Value  := szSumDay ;  //超出天数

    Self.cxCurrencyEdit12.Value := szMoneyRate * szPrincipal; //月息
    Self.cxCurrencyEdit9.Value  := szMonthInterest ; //月总息

    Self.cxCurrencyEdit11.Value := szDaysInterest; //天息
    Self.cxCurrencyEdit10.Value := szTotalDaysInterest; //天总息

    case szDateType of  // 0 = 月  1 = 天
      0:
      begin
        Self.cxCurrencyEdit13.Value := szMonthInterest + szTotalDaysInterest;
        Self.cxCurrencyEdit3.Value  := szMonthInterest + szTotalDaysInterest;
      end;
      1:
      begin
        //取天数
        Self.cxCurrencyEdit13.Value := szTotalDaysInterest ;
        Self.cxCurrencyEdit3.Value  := szTotalDaysInterest ;//+ szMonthInterest;
      end;
    end;
  end;

End;

{
var
  szStartDate , szEndDate : TDateTime;
  szMonth : Word;
  Date : TDateTime;
  Year, Month, Day, Hour:Word;
  Year1, Month1, Day1, Hour1:Word;

  nDays   : integer;
  szPrincipal     : Currency; //本金
  szMoneyRate     : Currency; //利率
  szTotal         : Currency; //总额
  szMonthInterest : Currency; //月利息
  szDaysInterest  : Currency; //天利息
  szTotalDaysInterest : Currency; //总天息
  szResidue  : Currency; //剩余天数
  DD1 : Double;
  szDateType : Integer;
  szMonthSpan : Double;

begin

   if Self.cxDateEdit3.Text  <>  '' then
   begin
      szDateType := Self.cxComboBox1.ItemIndex; //日期类型

      Self.cxCurrencyEdit6.Value  := 0;  //月数
      Self.cxCurrencyEdit12.Value := 0; //月息
      Self.cxCurrencyEdit9.Value  := 0; //月总息

      Self.cxCurrencyEdit7.Value  := 0;  //超出天数
      Self.cxCurrencyEdit11.Value := 0; //天息
      Self.cxCurrencyEdit10.Value := 0; //天总息

      szPrincipal := Self.cxCurrencyEdit4.Value;
      szMoneyRate := Self.cxCurrencyEdit5.Value / 100;  //利率
      szDaysInterest := szPrincipal * szMoneyRate / 30; //天息

      szStartDate := Self.cxDateEdit2.Date;
      szEndDate   := Self.cxDateEdit3.Date;
      DecodeDate(szStartDate, Year, Month, Day); //开始时间
      DecodeDate(szEndDate, Year1, Month1, Day1);//结束时间

      if Day > Day1 then
      begin
        Day := (DaysInMonth(szStartDate) - Day) + Day1;
      end else
      begin
        Day := Day1 - Day;
      end;
      //这个就是一年或者一个月
      szMonthSpan := MonthSpan(szStartDate,szEndDate); //取带小数的月

   //   szMonth := Ceil( szMonthSpan );  //往大取月数
   //   szMonth := Trunc(szMonthSpan);

      Day     := Ceil( DaySpan( szStartDate,szEndDate ) ); //取出天数

      ShowMessage(FloatToStr( Ceil( DaySpan( szStartDate,szEndDate ) ) mod 30  ));

      if Day < 1 then Day := DaysBetween(szEndDate,szStartDate);


      //***********************************************************************
      //算利息
      szMonthInterest     := szMonth * szMoneyRate * szPrincipal; //月利息
      szTotalDaysInterest := Day * szDaysInterest;  //天息
      //***********************************************************************
      //显示
      Self.cxCurrencyEdit6.Value  := szMonth;  //月数
      Self.cxCurrencyEdit12.Value := szMoneyRate * szPrincipal; //月息
      Self.cxCurrencyEdit9.Value  := szMonthInterest ; //月总息

      Self.cxCurrencyEdit7.Value  := day ;  //超出天数
      Self.cxCurrencyEdit11.Value := szDaysInterest; //天息
      Self.cxCurrencyEdit10.Value := szTotalDaysInterest; //天总息
      //***********************************************************************
      case szDateType of  // 0 = 月  1 = 天
        0:
        begin
          Self.cxCurrencyEdit13.Value := szMonthInterest ;
          Self.cxCurrencyEdit3.Value  := szMonthInterest  ; //+ szMonthInterest
        end;
        1:
        begin
          //取天数
          Self.cxCurrencyEdit13.Value := szTotalDaysInterest ;
          Self.cxCurrencyEdit3.Value  := szTotalDaysInterest ; //+ szMonthInterest
        end;
      end;

   end;

end;
}
procedure TfrmBorrowEdit.cxButton2Click(Sender: TObject);
var
  Calc : TfrmCalc;

begin
  Calc := TfrmCalc.Create(Self);
  Calc.Show;
end;

procedure TfrmBorrowEdit.cxCurrencyEdit13PropertiesChange(Sender: TObject);
begin
  Self.RzLabel9.Caption := MoneyConvert(Self.cxCurrencyEdit13.Value);
end;

procedure TfrmBorrowEdit.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.Label40.Caption := MoneyConvert( Self.cxCurrencyEdit1.Value );
  Self.cxCurrencyEdit4.Value := Self.cxCurrencyEdit1.Value;
end;

procedure TfrmBorrowEdit.cxCurrencyEdit2PropertiesChange(Sender: TObject);
begin
  Self.cxCurrencyEdit5.Value := Self.cxCurrencyEdit2.Value;
end;

procedure TfrmBorrowEdit.cxCurrencyEdit3PropertiesChange(Sender: TObject);
begin
  Self.Label29.Caption := MoneyConvert( Self.cxCurrencyEdit3.Value );
end;

procedure TfrmBorrowEdit.cxCurrencyEdit4PropertiesChange(Sender: TObject);
begin
  Self.Label1.Caption := MoneyConvert(Self.cxCurrencyEdit4.Value);
end;

procedure TfrmBorrowEdit.cxDateEdit3PropertiesChange(Sender: TObject);
begin
  Self.cxDateEdit1.Date := Self.cxDateEdit3.Date;
end;

procedure TfrmBorrowEdit.FormCreate(Sender: TObject);
begin
  Self.cxComboBox1.Properties.Items.Add('月');
  Self.cxComboBox1.Properties.Items.Add('天');
  Self.cxComboBox1.ItemIndex := 1;
  Self.Label29.Caption := '';
  Self.Label40.Caption := '';
  Self.Label1.Caption := '';
  Self.RzLabel9.Caption := '';
  Self.cxDateEdit1.Date := Date;
end;

procedure TfrmBorrowEdit.RzBitBtn5Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

procedure TfrmBorrowEdit.RzBitBtn8Click(Sender: TObject);
var
  s : string;
  szPrincipal : Currency;
  szInterest  : Currency; //总息
  szRate      : Currency; //利率
  szEndDate   : TDate;    //结束日期
  szName      : string;   //借款人/单位
  szContent   : string;   //借款事由
  szRemarks   : string;   //支付内容
  
begin
  szPrincipal := Self.cxCurrencyEdit1.Value;
  if szPrincipal = 0 then
  begin
    Application.MessageBox('编辑后的本金不能为零!',m_title,MB_OK + MB_ICONQUESTION)
  end
  else
  begin
    szInterest  := Self.cxCurrencyEdit3.Value;
    szName      := Self.cxTextEdit1.Text;
    szContent   := Self.cxMemo1.Text;
    szRemarks   := Self.cxMemo2.Text;
    szEndDate   := Self.cxDateEdit1.Date;
    szRate      := Self.cxCurrencyEdit2.Value;

    s := Format( 'Update '+ g_TableName +' set chPrincipal=''%f'',' +
                                              'chInterest=''%f'','  +
                                              'chName=''%s'','      +
                                              'chContent=''%s'','   +
                                              'chRemarks="%s",'+
                                              'chEndDate="%s",'+
                                              'chRate=%f where Numbers="%s"',[
                                              szPrincipal,
                                              szInterest,
                                              szName,
                                              szContent,
                                              szRemarks,
                                              FormatDateTime('yyyy-MM-dd',szEndDate),
                                              szRate,
                                              g_PostNumbers
                                              ]);


    OutputLog(s);
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      if ExecSQL  <> 0 then
      begin
        m_IsModify := True;
        Application.MessageBox('编辑成功!',m_title,MB_OK + MB_ICONQUESTION)
      end
      else
      begin
        Application.MessageBox('编辑失败!',m_title,MB_OK + MB_ICONWARNING)
      end;

    end;

  end;

end;

procedure TfrmBorrowEdit.RzBitBtn9Click(Sender: TObject);
begin
  Close;
end;

end.
