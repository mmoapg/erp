﻿unit ufrmCalcExpression;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.Menus, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, GridsEh, DBAxisGridsEh, DBGridEh,
  RzButton, cxTextEdit, Vcl.StdCtrls, cxButtons, cxMaskEdit, cxDropDownEdit,
  cxMemo, Vcl.ExtCtrls, RzPanel,ufrmBaseController, cxLabel, Vcl.ComCtrls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Data.Win.ADODB, cxDBEdit, cxSpinEdit;

type
  TfrmCalcExpression = class(TfrmBaseController)
    StatusBar1: TStatusBar;
    tvCalcExpression: TcxGridDBTableView;
    Lv: TcxGridLevel;
    CalcGrid: TcxGrid;
    tvCalcExpressionColumn1: TcxGridDBColumn;
    tvCalcExpressionColumn3: TcxGridDBColumn;
    tvCalcExpressionColumn4: TcxGridDBColumn;
    tvCalcExpressionColumn5: TcxGridDBColumn;
    tvCalcExpressionColumn6: TcxGridDBColumn;
    ds: TDataSource;
    tvCalcExpressionColumn2: TcxGridDBColumn;
    qry: TADOQuery;
    tvCalcExpressionColumn7: TcxGridDBColumn;
    tvCalcExpressionColumn8: TcxGridDBColumn;
    tvCalcExpressionColumn9: TcxGridDBColumn;
    RzToolbar12: TRzToolbar;
    RzSpacer100: TRzSpacer;
    RzToolButton75: TRzToolButton;
    RzSpacer101: TRzSpacer;
    RzToolButton76: TRzToolButton;
    RzSpacer102: TRzSpacer;
    RzToolButton77: TRzToolButton;
    RzSpacer103: TRzSpacer;
    RzToolButton79: TRzToolButton;
    RzSpacer104: TRzSpacer;
    RzToolButton80: TRzToolButton;
    RzSpacer108: TRzSpacer;
    RzSpacer109: TRzSpacer;
    RzToolButton78: TRzToolButton;
    RzPanel1: TRzPanel;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzToolButton1: TRzToolButton;
    pm: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure tvCalcExpressionColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryAfterInsert(DataSet: TDataSet);
    procedure tvCalcExpressionTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvCalcExpressionColumn9GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvCalcExpressionEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure RzToolButton80Click(Sender: TObject);
    procedure RzToolButton75Click(Sender: TObject);
    procedure RzToolButton78Click(Sender: TObject);
    procedure RzToolButton77Click(Sender: TObject);
    procedure RzToolButton76Click(Sender: TObject);
    procedure tvCalcExpressionColumn6GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvCalcExpressionColumn4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvCalcExpressionEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvCalcExpressionColumn6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure tvCalcExpressionDblClick(Sender: TObject);
  private
    { Private declarations }

    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    g_TableName : string;
    g_ParentNumber : string;
    g_CalcCount  : string;
    g_Company    : string;
    g_PostCode   : string;
    g_Prefix     : string;
  end;

var
  frmCalcExpression: TfrmCalcExpression;
  g_CalcName : string = 'CalcExpression';

implementation

uses
   ufrmIsViewGrid,uDataModule,global;

{$R *.dfm}


procedure TfrmCalcExpression.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    SetGrid(lpGridView.Columns[i],index,Self.Name + lpSuffix);
  end;
end;

procedure TfrmCalcExpression.tvCalcExpressionColumn4PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  s : string;
  szValue : string;
  szSpec  : string;
  Msg : string;
  i : Integer;
  f : Double;
  szColName : string;

begin
  inherited;

  with self.qry do
  begin
    if (State = dsEdit) or (State = dsinsert) then
      begin
          szColName := Self.tvCalcExpressionColumn5.DataBinding.FieldName;
          szSpec := VarToStr( DisplayValue );
          if Length(szSpec) <> 0 then
          begin
            szValue := FunExpCalc(szSpec,2);
            if TryStrToInt(szValue,i) or TryStrToFloat(szValue, f) then
            begin
              FieldByName(szColName).Value := szValue;
              szColName := Self.tvCalcExpressionColumn4.DataBinding.FieldName;
              FieldByName(szColName).Value := DisplayValue;
            end;
            Msg := szValue;
          end else
          begin
            FieldByName(szColName).Value := 0;
            Msg := '表达式为空不能进行有效计算';
          end;

          Self.StatusBar1.Panels[2].Text := Msg;

      end;
  end;
end;

procedure TfrmCalcExpression.tvCalcExpressionColumn6GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do
          Items.Add(List.Strings[i]);

    finally
      List.Free;
    end;
  end;
end;

procedure TfrmCalcExpression.tvCalcExpressionColumn6PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  g_Company := VarToStr(DisplayValue);
end;

procedure TfrmCalcExpression.tvCalcExpressionColumn9GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmCalcExpression.tvCalcExpressionColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SetcxGrid(Self.tvCalcExpression,1,g_CalcName);
end;

procedure TfrmCalcExpression.tvCalcExpressionDblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmCalcExpression.tvCalcExpressionEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.qry);
  if AItem.Index = Self.tvCalcExpressionColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmCalcExpression.tvCalcExpressionEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvCalcExpressionColumn5.Index) then
  begin
    if Self.tvCalcExpression.Controller.FocusedRow.IsFirst and (IsNewRow) then
    begin
      with Self.qry do
      begin
        if State <> dsInactive then
        begin
          First;
          Insert;
          Self.tvCalcExpressionColumn1.FocusWithSelection;
        end;

      end;

    end;

  end;

end;

procedure TfrmCalcExpression.tvCalcExpressionTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  g_CalcCount := AText;
end;

procedure TfrmCalcExpression.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.CalcGrid,'工程量明细') ;
end;

procedure TfrmCalcExpression.FormActivate(Sender: TObject);
begin
  inherited;
  with Self.qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from ' + g_TableName + ' Where parent="' + g_ParentNumber + '"';
    Open;

    if 0 <> RecordCount then
    begin
      g_Company   := FieldByName('ItemUnit').AsString;
    end;

  end;

  SetcxGrid(Self.tvCalcExpression,0,g_CalcName);
end;

procedure TfrmCalcExpression.FormClose(Sender: TObject;
  var Action: TCloseAction);

begin
  inherited;
  with Self.qry do
  begin
    case State of
      dsEdit: 
       begin
         if Application.MessageBox('当前有正在编辑的数据，是否需要保存？','编辑',MB_YESNO + MB_ICONQUESTION) = IDYES then
         begin
           Post;
           Refresh;
         end;  
         
       end;
      dsInsert:
       begin
         if Application.MessageBox('有新增的数据还未保存，是否需要保存？','新增',MB_YESNO + MB_ICONQUESTION) = IDYES then
         begin
           Post;
           Refresh;
         end;  
       end;  
    end;
      
  end;

end;

procedure TfrmCalcExpression.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmCalcExpression.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.CalcGrid;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCalcExpression.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton76.Click;
end;

procedure TfrmCalcExpression.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton77.Click;
end;

procedure TfrmCalcExpression.qryAfterInsert(DataSet: TDataSet);
var
  DD1 : string;
  
  szColName : string;
  szId : string;
  i : Integer;
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvCalcExpressionColumn1.DataBinding.FieldName;
      szId := DM.getDataMaxDate(g_TableName);
      DataSet.FieldByName('Code').AsString    := g_PostCode;
      DataSet.FieldByName('Parent').AsString  := g_ParentNumber;
      DataSet.FieldByName(szColName).AsString := g_Prefix + szId;
      szColName := Self.tvCalcExpressionColumn6.DataBinding.FieldName;
      g_Company := 'm2';
      DataSet.FieldByName(szColName).AsString := g_Company;
    end;
  end;

end;

procedure TfrmCalcExpression.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmCalcExpression.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(True,Self.qry);
end;

procedure TfrmCalcExpression.RzToolButton75Click(Sender: TObject);
begin
  inherited;
  with Self.qry do
  begin

    if State <> dsInactive then
    begin
      First;
      Insert;
      Self.tvCalcExpressionColumn2.FocusWithSelection;
      Self.CalcGrid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);
    end;

  end;

end;

procedure TfrmCalcExpression.RzToolButton76Click(Sender: TObject);
begin
  inherited;
  SaveGrid(Self.qry);
end;

procedure TfrmCalcExpression.RzToolButton77Click(Sender: TObject);
begin
  inherited;
  //删除
  cxGridDeleteData(Self.tvCalcExpression,'',g_TableName);
  Self.qry.Requery();
end;

procedure TfrmCalcExpression.RzToolButton78Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  inherited;
  //表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_CalcName;
    Child.ShowModal;
    SetcxGrid(Self.tvCalcExpression,0,g_CalcName);
  finally
    Child.Free;
  end;

end;

procedure TfrmCalcExpression.RzToolButton80Click(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
