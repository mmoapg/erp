unit ufrmInAccounts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DB, ADODB, RzButton;

type
  TfrmInAccounts = class(TForm)
    ListView1: TListView;
    Panel1: TPanel;
    Qry: TADOQuery;
    RzBitBtn4: TRzBitBtn;
    Edit1: TEdit;
    Label1: TLabel;
    RzBitBtn5: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn1: TRzBitBtn;
    procedure FormShow(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1Data(Sender: TObject; Item: TListItem);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ShowListView(List : TListView ; AIndex : Byte ; AName : string );
  public
    { Public declarations }
  end;

var
  frmInAccounts: TfrmInAccounts;
  g_TableName : string = 'contract_tree';

implementation

uses
   TreeFillThrd,
   uDataModule,
   global,
   ufrmManage,
   ufunctions,
   ufrmPostAccounts;

{$R *.dfm}

procedure TfrmInAccounts.ShowListView(List : TListView ; AIndex : Byte ; AName : string );
var
  Query: TADOQuery;
  PNode: PNodeData;
  ListItem : TListItem;

begin
  Query := TADOQuery.Create(nil);
  try

      Query.Connection := DM.ADOconn;
      case AIndex of
        0:
        begin
          Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' = 0' ;
        end;
        1:
        begin
          Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' = 0 and name like "%'+ AName +'%"';
        end;
      end;

      if Query.Active then
         Query.Close;

      Query.Open;

      List.Clear;
      while not Query.Eof do
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName('name').AsString;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.Code       := Query.FieldByName('Code').AsString;
        PNode^.Input_time := Query.FieldByName('Input_time').AsDateTime;
        PNode^.End_time   := Query.FieldByName('End_time').AsDateTime;
        PNode^.remarks    := Query.FieldByName('remarks').AsString;
        ListItem := List.Items.Add ;
        ListItem.Data := PNode;
        ListItem.Caption := PNode.Code;
        ListItem.SubItems.Add(PNode^.Caption);
        ListItem.SubItems.Add(FormatDateTime('yyyy-MM-dd',PNode^.Input_time));
        ListItem.SubItems.Add(PNode^.remarks);
        Query.Next;
      end;

  finally
    Query.Free;
  end;
end;

procedure TfrmInAccounts.Edit1Change(Sender: TObject);
var
  s : string;

begin
  s := Self.Edit1.Text;
  if Length(s) = 0 then
  begin
    ShowListView(Self.ListView1,0,'');
  end;  
end;

procedure TfrmInAccounts.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then Self.RzBitBtn4.Click;
end;

procedure TfrmInAccounts.FormShow(Sender: TObject);
begin
  ShowListView(Self.ListView1,0,'');
end;

procedure TfrmInAccounts.ListView1Data(Sender: TObject; Item: TListItem);
begin
  if Assigned(Item.Data) then
  begin
    Dispose(Item.Data);
  end;  
end;

procedure TfrmInAccounts.ListView1DblClick(Sender: TObject);
var
  PNode: PNodeData;
  Data : Pointer;
begin
  if nil <> Self.ListView1.Selected then
  begin
    Data  := Self.ListView1.Selected.Data;

    if Assigned(Data) then
    begin
      PNode := PNodeData(Data);
      if Assigned(PNode) then
      begin
        g_Parent   := pNode.parent;
        g_pacttype := 9;
        g_ModuleIndex := 1; //收款项目下的
        TfrmManage.Create(Application);
        Self.Close;
      end;

    end;

  end;
end;

procedure TfrmInAccounts.RzBitBtn1Click(Sender: TObject);
var
  Child : TfrmPostAccounts;
  szName: string;
  szNode: PNodeData;

begin
  if m_user.dwType = 0 then
  begin
    Child:= TfrmPostAccounts.Create(Self);
    try
      Child.g_TableName := g_TableName;

      if Sender = RzBitBtn1 then
      begin //新增
        Child.g_PostCode := IntToStr( DM.getDataMaxDate(g_TableName) );
        Child.g_IsModify := False;
        Child.ShowModal ;
        ShowListView(Self.ListView1,0,'');
      end else
      if Sender = RzBitBtn2 then
      begin //编辑
        Child.g_IsModify := True;
        if Assigned( Self.ListView1.Selected ) then
        begin
          szNode := PNodeData(Self.ListView1.Selected.Data);
          if Assigned(szNode) then
          begin
            Child.g_PostCode   := szNode.Code;
            Child.RzEdit1.Text := szNode.Caption;
            Child.RzMemo1.Text := szNode.remarks;
            Child.dtp1.Date    := szNode.Input_time;
            Child.ShowModal;
            ShowListView(Self.ListView1,0,'');
          end;

        end;  

      end;

    finally
      Child.Free;
    end;

  end;

end;

procedure TfrmInAccounts.RzBitBtn3Click(Sender: TObject);
var
  szNode : PNodeData;
  szCode : string;
  Code : string;
  szCodeList    : string;
  szTableList : array[0..2] of TTableList;

begin
  szTableList[0].ATableName := 'expenditure';  //收款明细
  szTableList[0].ADirectory := 'DetailedManage';

  szTableList[1].ATableName := 'income'; //拨款明细进度
  szTableList[1].ADirectory := 'PayManage';

  szTableList[2].ATableName := 'pactmoney'; //结算表
  szTableList[2].ADirectory := '';

  if m_user.dwType = 0 then
  begin
      szNode := PNodeData(Self.ListView1.Selected.Data);
      if Assigned(szNode) then
      begin

         DM.getTreeCode(g_TableName, szNode^.parent,szCodeList); //获取要删除的id表
         szCode := szNode^.Code;

         if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
               '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
          begin
            DeleteTableFile(szTableList,szCodeList);
            DM.InDeleteData(g_TableName,szCodeList); //清除主目录表内关联
            m_IsModify := True;
            Self.FormShow(Sender);
          end;
      end;
  end;

end;

procedure TfrmInAccounts.RzBitBtn4Click(Sender: TObject);
var
  s : string;
begin
  s := Self.Edit1.Text;
  
  if Length(s) <> 0 then
  begin
    ShowListView(Self.ListView1,1,s);
  end
  else
  begin
    Application.MessageBox('请输入要搜索的名称!!',m_title,MB_OK + MB_ICONQUESTION )
  end;   

end;

procedure TfrmInAccounts.RzBitBtn5Click(Sender: TObject);
begin
  g_Parent   := 0;
  g_pacttype := 9;
  g_ModuleIndex := 1;
  TfrmManage.Create(Self);
  Close;
end;

end.
