unit ufrmSumDetailed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxGridChartView, cxGridDBChartView, cxTextEdit, dxmdaset,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series, Vcl.ExtCtrls,
  VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart, RzTabs, cxCurrencyEdit,
  RzButton, RzPanel, Vcl.Menus,ufrmBaseController;

type
  TfrmSumDetailed = class(TfrmBaseController)
    SumSource: TDataSource;
    SumMem: TdxMemData;
    PaidSource: TDataSource;
    PaidMem: TdxMemData;
    RPSource: TDataSource;
    RPMem: TdxMemData;
    ChartSource: TDataSource;
    RzPageControl2: TRzPageControl;
    Tab1: TRzTabSheet;
    Tab2: TRzTabSheet;
    DBChart1: TDBChart;
    Series2: TPieSeries;
    Grid: TcxGrid;
    TableView1: TcxGridDBTableView;
    TableView1Column1: TcxGridDBColumn;
    TableView1Column2: TcxGridDBColumn;
    TableView1Column3: TcxGridDBColumn;
    TableView2: TcxGridDBTableView;
    TableView2Column1: TcxGridDBColumn;
    TableView2Column3: TcxGridDBColumn;
    TableView2Column2: TcxGridDBColumn;
    TableView3: TcxGridDBTableView;
    TableView3Column1: TcxGridDBColumn;
    TableView3Column2: TcxGridDBColumn;
    TableView3Column3: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    Lv2: TcxGridLevel;
    Lv3: TcxGridLevel;
    ChartData: TdxMemData;
    ChartProjectName: TStringField;
    ChartSumMoney: TCurrencyField;
    RzToolbar4: TRzToolbar;
    RzSpacer17: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer12: TRzSpacer;
    pm: TPopupMenu;
    N1: TMenuItem;
    procedure TableView1Column1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzPageControl2TabClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSumDetailed: TfrmSumDetailed;

implementation

{$R *.dfm}

procedure TfrmSumDetailed.N1Click(Sender: TObject);
begin
  CxGridToExcel(Self.Grid,'������ϸ��');
end;

procedure TfrmSumDetailed.RzPageControl2TabClick(Sender: TObject);
begin
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
      Width := 390;
      Height:= 500;
    end;
    1:
    begin
      Width := 900;
      Height:= 700;
    end;
  end;
//  Self.Position := poScreenCenter;
end;

procedure TfrmSumDetailed.TableView1Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

end.
