object frmCustomerPlate: TfrmCustomerPlate
  Left = 0
  Top = 0
  Caption = #25253#20215
  ClientHeight = 654
  ClientWidth = 1676
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pnl: TPanel
    Left = 0
    Top = 0
    Width = 1676
    Height = 654
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnl'
    TabOrder = 0
    object pnl2: TPanel
      Left = 0
      Top = 0
      Width = 280
      Height = 654
      Align = alLeft
      BevelInner = bvSpace
      BevelOuter = bvLowered
      TabOrder = 0
      object Bevel1: TBevel
        Left = 2
        Top = 192
        Width = 276
        Height = 10
        Align = alBottom
        Shape = bsSpacer
        ExplicitLeft = 1
        ExplicitTop = 533
        ExplicitWidth = 350
      end
      object RzToolbar6: TRzToolbar
        Left = 2
        Top = 2
        Width = 276
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdLeft, sdRight]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 0
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer16
          RzToolButton18
          RzSpacer26
          RzToolButton19
          RzSpacer17
          RzToolButton20
          RzSpacer21
          RzToolButton2)
        object RzSpacer16: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton18: TRzToolButton
          Left = 12
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #26032#22686
          OnClick = RzToolButton18Click
        end
        object RzSpacer17: TRzSpacer
          Left = 124
          Top = 4
        end
        object RzToolButton20: TRzToolButton
          Left = 132
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #21024#38500
          OnClick = RzToolButton18Click
        end
        object RzSpacer21: TRzSpacer
          Left = 184
          Top = 4
        end
        object RzSpacer26: TRzSpacer
          Left = 64
          Top = 4
        end
        object RzToolButton19: TRzToolButton
          Left = 72
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #32534#36753
          OnClick = RzToolButton18Click
        end
        object RzToolButton2: TRzToolButton
          Left = 192
          Top = 4
          Width = 76
          ImageIndex = 16
          Images = DM.cxImageList1
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #25253#20215#21015#34920
          OnClick = RzToolButton2Click
        end
      end
      object tv: TTreeView
        Left = 2
        Top = 34
        Width = 276
        Height = 158
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        DragMode = dmAutomatic
        HideSelection = False
        Images = DM.TreeImageList
        Indent = 19
        PopupMenu = pm3
        RowSelect = True
        SortType = stBoth
        TabOrder = 1
        OnDblClick = tvDblClick
        OnEdited = tvEdited
        OnKeyDown = tvKeyDown
        Items.NodeData = {
          0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
          00000000000104805F656755534D4F}
      end
      object pnl3: TPanel
        Left = 2
        Top = 202
        Width = 276
        Height = 450
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'pnl3'
        TabOrder = 2
        object Label1: TLabel
          Left = 80
          Top = 152
          Width = 31
          Height = 13
          Caption = 'Label1'
        end
        object Grid1: TcxGrid
          Left = 0
          Top = 0
          Width = 276
          Height = 450
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = ''
          object tvView1: TcxGridDBTableView
            OnDblClick = tvView1DblClick
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = True
            Navigator.Buttons.PriorPage.Visible = True
            Navigator.Buttons.Prior.Visible = True
            Navigator.Buttons.Next.Visible = True
            Navigator.Buttons.NextPage.Visible = True
            Navigator.Buttons.Last.Visible = True
            Navigator.Buttons.Insert.Visible = True
            Navigator.Buttons.Append.Visible = False
            Navigator.Buttons.Delete.Visible = True
            Navigator.Buttons.Edit.Visible = True
            Navigator.Buttons.Post.Visible = True
            Navigator.Buttons.Cancel.Visible = True
            Navigator.Buttons.Refresh.Visible = True
            Navigator.Buttons.SaveBookmark.Visible = True
            Navigator.Buttons.GotoBookmark.Visible = True
            Navigator.Buttons.Filter.Visible = True
            DataController.DataSource = dsGroupNameSource
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.DragDropText = True
            OptionsBehavior.DragFocusing = dfDragDrop
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.NavigatorHints = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsBehavior.PullFocusing = True
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.CellAutoHeight = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 16
            object tvView1Col1: TcxGridDBColumn
              Caption = #21333#39033#30446#21517#31216
              DataBinding.FieldName = 'GroupName'
              Width = 276
            end
          end
          object Lv1: TcxGridLevel
            GridView = tvView1
          end
        end
        object btn3: TButton
          Left = 28
          Top = 48
          Width = 67
          Height = 37
          Caption = 'btn3'
          TabOrder = 1
          Visible = False
          OnClick = btn3Click
        end
      end
    end
    object pnl1: TPanel
      Left = 288
      Top = 0
      Width = 1388
      Height = 654
      Align = alClient
      BevelOuter = bvNone
      Caption = 'pnl1'
      TabOrder = 1
      object RzClient: TRzPageControl
        Left = 0
        Top = 0
        Width = 1088
        Height = 654
        Hint = ''
        ActivePage = RzCustomer
        Align = alClient
        UseColoredTabs = True
        HotTrack = False
        HotTrackStyle = htsTabBar
        ShowShadow = False
        TabOverlap = -6
        TabHeight = 22
        TabHints = True
        TabOrder = 0
        TabStyle = tsSquareCorners
        TabWidth = 70
        FixedDimension = 22
        object RzInfo: TRzTabSheet
          Color = clWhite
          TabVisible = False
          Caption = #24037#31243#20449#24687
          OnResize = RzInfoResize
          object cxButton1: TcxButton
            Left = 448
            Top = 562
            Width = 169
            Height = 49
            Caption = #25253#12288#20215
            Enabled = False
            LookAndFeel.Kind = lfFlat
            LookAndFeel.NativeStyle = False
            LookAndFeel.SkinName = 'Office2013LightGray'
            TabOrder = 2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = RzToolButton2Click
          end
          object cxGroupBox2: TcxGroupBox
            Left = 551
            Top = 42
            Caption = #21512#21516#20449#24687
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clBlack
            Style.IsFontAssigned = True
            TabOrder = 1
            Height = 495
            Width = 538
            object cxLabel13: TcxLabel
              Left = 43
              Top = 205
              Caption = #39318#27454#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel14: TcxLabel
              Left = 43
              Top = 172
              Caption = #39564#25910#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel15: TcxLabel
              Left = 270
              Top = 141
              Caption = #31459#24037#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel16: TcxLabel
              Left = 43
              Top = 139
              Caption = #24320#24037#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel17: TcxLabel
              Left = 270
              Top = 108
              Caption = #30005#12288#12288#35805#65306
              Style.TextColor = clBlack
            end
            object cxLabel18: TcxLabel
              Left = 43
              Top = 106
              Caption = #24037#31243#30417#29702#65306
              Style.TextColor = clBlack
            end
            object cxLabel19: TcxLabel
              Left = 270
              Top = 75
              Caption = #30005#12288#12288#35805#65306
              Style.TextColor = clBlack
            end
            object cxLabel20: TcxLabel
              Left = 43
              Top = 73
              Caption = #39033#30446#32463#29702#65306
              Style.TextColor = clBlack
            end
            object cxLabel3: TcxLabel
              Left = 43
              Top = 40
              Caption = #31614#21333#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel11: TcxLabel
              Left = 270
              Top = 42
              Caption = #21512#21516#37329#39069#65306
              Style.TextColor = clBlack
            end
            object cxLabel12: TcxLabel
              Left = 43
              Top = 238
              Caption = #20013#27454#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel21: TcxLabel
              Left = 43
              Top = 271
              Caption = #23614#27454#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel22: TcxLabel
              Left = 270
              Top = 207
              Caption = #37329#12288#12288#39069#65306
              Style.TextColor = clBlack
            end
            object cxLabel23: TcxLabel
              Left = 270
              Top = 240
              Caption = #37329#12288#12288#39069#65306
              Style.TextColor = clBlack
            end
            object cxLabel24: TcxLabel
              Left = 270
              Top = 273
              Caption = #37329#12288#12288#39069#65306
              Style.TextColor = clBlack
            end
            object cxLabel25: TcxLabel
              Left = 43
              Top = 333
              Caption = #21512#21516#22791#27880#65306
              Style.TextColor = clBlack
            end
            object cxLabel26: TcxLabel
              Left = 270
              Top = 174
              Caption = #25972#21333#25240#25187#65306
              Style.TextColor = clBlack
            end
            object cxDBDateEdit1: TcxDBDateEdit
              Left = 125
              Top = 40
              DataBinding.DataField = 'SignBillDate'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 0
              Width = 129
            end
            object cxDBDateEdit2: TcxDBDateEdit
              Left = 125
              Top = 139
              DataBinding.DataField = 'StartDate'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 4
              Width = 129
            end
            object cxDBDateEdit3: TcxDBDateEdit
              Left = 125
              Top = 172
              DataBinding.DataField = 'CheckDate'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 6
              Width = 129
            end
            object cxDBDateEdit4: TcxDBDateEdit
              Left = 125
              Top = 205
              DataBinding.DataField = 'firstdate'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 8
              Width = 129
            end
            object cxDBDateEdit5: TcxDBDateEdit
              Left = 125
              Top = 238
              DataBinding.DataField = 'MiddleData'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 10
              Width = 129
            end
            object cxDBDateEdit6: TcxDBDateEdit
              Left = 125
              Top = 271
              DataBinding.DataField = 'LastDate'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 12
              Width = 129
            end
            object cxDBSpinEdit5: TcxDBSpinEdit
              Left = 352
              Top = 75
              DataBinding.DataField = 'ProjectManagerPhone'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtFloat
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 2
              Width = 164
            end
            object cxDBSpinEdit6: TcxDBSpinEdit
              Left = 352
              Top = 108
              DataBinding.DataField = 'ProjectSupervisionPhone'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtFloat
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 3
              Width = 164
            end
            object cxDBDateEdit8: TcxDBDateEdit
              Left = 352
              Top = 141
              DataBinding.DataField = 'EndDate'
              DataBinding.DataSource = dsSounceInfo
              Properties.OnEditValueChanged = cxDBDateEdit8PropertiesEditValueChanged
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 5
              Width = 164
            end
            object cxDBSpinEdit7: TcxDBSpinEdit
              Left = 352
              Top = 172
              DataBinding.DataField = 'Signlediscount'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtFloat
              Properties.OnValidate = cxDBSpinEdit7PropertiesValidate
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 7
              Width = 164
            end
            object cxDBCurrencyEdit2: TcxDBCurrencyEdit
              Left = 352
              Top = 42
              DataBinding.DataField = 'ContractMoney'
              DataBinding.DataSource = dsSounceInfo
              Properties.OnValidate = cxDBCurrencyEdit2PropertiesValidate
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 1
              Width = 164
            end
            object cxDBCurrencyEdit3: TcxDBCurrencyEdit
              Left = 352
              Top = 205
              DataBinding.DataField = 'FirstMoney'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 9
              Width = 164
            end
            object cxDBCurrencyEdit4: TcxDBCurrencyEdit
              Left = 352
              Top = 238
              DataBinding.DataField = 'MiddleMoney'
              DataBinding.DataSource = dsSounceInfo
              Properties.OnValidate = cxDBCurrencyEdit2PropertiesValidate
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 11
              Width = 164
            end
            object cxDBCurrencyEdit5: TcxDBCurrencyEdit
              Left = 352
              Top = 271
              DataBinding.DataField = 'LastMoney'
              DataBinding.DataSource = dsSounceInfo
              Properties.OnValidate = cxDBCurrencyEdit2PropertiesValidate
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 13
              Width = 164
            end
            object cxDBMemo2: TcxDBMemo
              Left = 125
              Top = 337
              DataBinding.DataField = 'ContractRemarks'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 14
              Height = 113
              Width = 390
            end
            object cxDBButtonEdit3: TcxDBButtonEdit
              Left = 125
              Top = 73
              DataBinding.DataField = 'ProjectManager'
              DataBinding.DataSource = dsSounceInfo
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 32
              Width = 129
            end
            object cxDBButtonEdit4: TcxDBButtonEdit
              Left = 125
              Top = 106
              DataBinding.DataField = 'ProjectSupervision'
              DataBinding.DataSource = dsSounceInfo
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 33
              Width = 129
            end
            object cxLabel35: TcxLabel
              Left = 40
              Top = 304
              Caption = #24037#26399#22825#25968#65306
              Style.TextColor = clBlack
            end
            object cxLabel36: TcxLabel
              Left = 270
              Top = 304
              Caption = #25171#21360#31867#22411#65306
              Style.TextColor = clBlack
            end
            object cxDBSpinEdit9: TcxDBSpinEdit
              Left = 125
              Top = 304
              DataBinding.DataField = 'NumDays'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtFloat
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 36
              Width = 129
            end
            object cxDBComboBox4: TcxDBComboBox
              Left = 352
              Top = 304
              DataBinding.DataField = 'PrintType'
              DataBinding.DataSource = dsSounceInfo
              Properties.Items.Strings = (
                #25253#20215#39044#31639#34920
                #24037#31243#20915#31639#34920
                #39033#30446#28165#21333
                #20154#24037#25253#20215#39044#31639#34920
                #20027#26448#28165#21333#25253#20215
                #21830#21697#39033#30446#28165#21333)
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 37
              Width = 164
            end
          end
          object cxGroupBox1: TcxGroupBox
            Left = 40
            Top = 42
            Caption = #24037#31243#20449#24687
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clBlack
            Style.IsFontAssigned = True
            TabOrder = 0
            Height = 495
            Width = 505
            object cxLabel4: TcxLabel
              Left = 22
              Top = 271
              Caption = #22320#12288#12288#22336#65306
              Style.TextColor = clBlack
            end
            object cxLabel5: TcxLabel
              Left = 22
              Top = 139
              Caption = #26469#35775#26085#26399#65306
              Style.TextColor = clBlack
            end
            object cxLabel6: TcxLabel
              Left = 230
              Top = 73
              Caption = #30005#12288#12288#35805#65306
            end
            object cxLabel7: TcxLabel
              Left = 22
              Top = 73
              Caption = #20027#26696#35774#35745#65306
              Style.TextColor = clBlack
            end
            object cxLabel8: TcxLabel
              Left = 230
              Top = 40
              Caption = #30005#12288#12288#35805#65306
            end
            object cxLabel9: TcxLabel
              Left = 230
              Top = 205
              Caption = #24037#31243#31867#22411#65306
            end
            object cxLabel10: TcxLabel
              Left = 22
              Top = 40
              Caption = #23458#25143#21517#31216#65306
              Style.TextColor = clBlack
            end
            object cxLabel1: TcxLabel
              Left = 230
              Top = 139
              Caption = #37327#25151#26085#26399#65306
            end
            object cxLabel2: TcxLabel
              Left = 22
              Top = 343
              Caption = #23458#25143#35201#27714#65306
              Style.TextColor = clBlack
            end
            object cxLabel27: TcxLabel
              Left = 22
              Top = 172
              Caption = #24037#31243#38754#31215#65306
              Style.TextColor = clBlack
            end
            object cxLabel28: TcxLabel
              Left = 230
              Top = 172
              Caption = #25143#12288#12288#22411#65306
            end
            object cxLabel29: TcxLabel
              Left = 22
              Top = 205
              Caption = #26469#28304#28192#36947#65306
              Style.TextColor = clBlack
            end
            object cxLabel30: TcxLabel
              Left = 22
              Top = 106
              Caption = #20027#26696#33829#38144#65306
              Style.TextColor = clBlack
            end
            object cxLabel31: TcxLabel
              Left = 230
              Top = 106
              Caption = #30005#12288#12288#35805#65306
            end
            object cxLabel32: TcxLabel
              Left = 22
              Top = 238
              Caption = #23458#25143#31867#22411#65306
              Style.TextColor = clBlack
            end
            object cxLabel33: TcxLabel
              Left = 230
              Top = 238
              Caption = #35746#12288#12288#37329#65306
            end
            object cxDBTextEdit1: TcxDBTextEdit
              Left = 103
              Top = 38
              DataBinding.DataField = 'CustomerName'
              DataBinding.DataSource = dsSounceInfo
              ParentColor = True
              Style.BorderColor = clSilver
              Style.BorderStyle = ebsUltraFlat
              Style.HotTrack = True
              Style.LookAndFeel.Kind = lfStandard
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.Shadow = False
              Style.TextColor = clBlack
              Style.TextStyle = []
              StyleDisabled.LookAndFeel.Kind = lfStandard
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.Kind = lfStandard
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.Kind = lfStandard
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 0
              Width = 121
            end
            object cxDBSpinEdit1: TcxDBSpinEdit
              Left = 103
              Top = 170
              DataBinding.DataField = 'BuildAcreage'
              DataBinding.DataSource = dsSounceInfo
              Properties.Alignment.Horz = taLeftJustify
              Properties.DisplayFormat = '0.## ('#13217')'
              Properties.EditFormat = '0.##'
              Properties.ValueType = vtFloat
              Properties.OnValidate = cxDBSpinEdit1PropertiesValidate
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 7
              Width = 121
            end
            object cxDBTextEdit4: TcxDBTextEdit
              Left = 312
              Top = 172
              DataBinding.DataField = 'housetype'
              DataBinding.DataSource = dsSounceInfo
              ParentColor = True
              Style.BorderColor = clSilver
              Style.BorderStyle = ebsUltraFlat
              Style.HotTrack = True
              Style.LookAndFeel.Kind = lfStandard
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.Shadow = False
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.Kind = lfStandard
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.Kind = lfStandard
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.Kind = lfStandard
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 8
              Width = 170
            end
            object cxDBSpinEdit2: TcxDBSpinEdit
              Left = 312
              Top = 40
              DataBinding.DataField = 'Customerphone'
              DataBinding.DataSource = dsSounceInfo
              Properties.LargeIncrement = 11.000000000000000000
              Properties.ValueType = vtInt
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 2
              Width = 170
            end
            object cxDBSpinEdit3: TcxDBSpinEdit
              Left = 312
              Top = 73
              DataBinding.DataField = 'MainCaseDesignphone'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtInt
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 3
              Width = 170
            end
            object cxDBSpinEdit4: TcxDBSpinEdit
              Left = 312
              Top = 106
              DataBinding.DataField = 'MainCaseMarktingPhone'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtInt
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 4
              Width = 170
            end
            object cxDBComboBox1: TcxDBComboBox
              Left = 312
              Top = 205
              DataBinding.DataField = 'ProjectType'
              DataBinding.DataSource = dsSounceInfo
              Properties.Items.Strings = (
                #26631#20934#23478#35013
                #24037#31243#35013#39280
                #21830#19994#27004
                #21150#20844#27004
                #37202#24215
                #38376#24215#25110#27249#31383
                #39184#39278#31354#38388
                #21830#19994#31354#38388
                #23486#39302#31354#38388
                #27665#23487
                #33590#31038#31354#38388)
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 11
              Width = 170
            end
            object cxDBComboBox2: TcxDBComboBox
              Left = 103
              Top = 203
              DataBinding.DataField = 'CanalSounce'
              DataBinding.DataSource = dsSounceInfo
              Properties.Items.Strings = (
                #19978#38376#23458#25143
                #32593#38144#23458#25143
                #30005#38144#23458#25143
                #25307#26631#24037#31243
                #32769#23458#25143#20171#32461
                #21150#27963#21160#23458#25143)
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 10
              Width = 121
            end
            object cxDBTextEdit6: TcxDBTextEdit
              Left = 103
              Top = 271
              DataBinding.DataField = 'CustomerAddress'
              DataBinding.DataSource = dsSounceInfo
              ParentColor = True
              Style.BorderColor = clSilver
              Style.BorderStyle = ebsUltraFlat
              Style.HotTrack = True
              Style.LookAndFeel.Kind = lfStandard
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.Shadow = False
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.Kind = lfStandard
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.Kind = lfStandard
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.Kind = lfStandard
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 13
              Width = 378
            end
            object cxDBCurrencyEdit1: TcxDBCurrencyEdit
              Left = 312
              Top = 238
              DataBinding.DataField = 'Depostit'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 12
              Width = 170
            end
            object cxDBMemo1: TcxDBMemo
              Left = 103
              Top = 343
              DataBinding.DataField = 'CustomerRequire'
              DataBinding.DataSource = dsSounceInfo
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 14
              Height = 113
              Width = 378
            end
            object cxDBDateEdit9: TcxDBDateEdit
              Left = 103
              Top = 137
              DataBinding.DataField = 'VisitDate'
              DataBinding.DataSource = dsSounceInfo
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 5
              Width = 121
            end
            object cxDBDateEdit10: TcxDBDateEdit
              Left = 312
              Top = 139
              DataBinding.DataField = 'MeasureDate'
              DataBinding.DataSource = dsSounceInfo
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 6
              Width = 170
            end
            object cxDBButtonEdit1: TcxDBButtonEdit
              Left = 103
              Top = 71
              DataBinding.DataField = 'MainCaseDesign'
              DataBinding.DataSource = dsSounceInfo
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 29
              Width = 121
            end
            object cxDBButtonEdit2: TcxDBButtonEdit
              Left = 103
              Top = 104
              DataBinding.DataField = 'MainCaseMarkting'
              DataBinding.DataSource = dsSounceInfo
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 30
              Width = 121
            end
            object cxDBComboBox3: TcxDBComboBox
              Left = 103
              Top = 236
              DataBinding.DataField = 'ServiceType'
              DataBinding.DataSource = dsSounceInfo
              Properties.Items.Strings = (
                #28508#22312#23458#25143
                #21672#35810#23458#25143
                #24847#21521#23458#25143
                #23450#37329#23458#25143
                #27969#22833#23458#25143
                #35774#35745#31867#23458#25143
                #31614#21333#23458#25143
                ' '#25240#25187#23458#25143
                #20813#21333#23458#25143)
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 31
              Width = 121
            end
            object cxLabel37: TcxLabel
              Left = 22
              Top = 304
              Caption = #20844#21496#21517#31216#65306
              Style.TextColor = clBlack
            end
            object cxLabel38: TcxLabel
              Left = 230
              Top = 304
              Caption = #20844#21496#30005#35805#65306
              Style.TextColor = clBlack
            end
            object cxDBButtonEdit5: TcxDBButtonEdit
              Left = 103
              Top = 304
              DataBinding.DataField = 'CompanyName'
              DataBinding.DataSource = dsSounceInfo
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 34
              Width = 121
            end
            object cxDBSpinEdit8: TcxDBSpinEdit
              Left = 312
              Top = 304
              DataBinding.DataField = 'CompanyPhone'
              DataBinding.DataSource = dsSounceInfo
              Properties.ValueType = vtInt
              Style.BorderColor = clSilver
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = 'Office2013White'
              Style.TextColor = clBlack
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = 'Office2013White'
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = 'Office2013White'
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = 'Office2013White'
              TabOrder = 35
              Width = 170
            end
          end
        end
        object RzCustomer: TRzTabSheet
          TabVisible = False
          Caption = #25253#20215#21015#34920
          object pnl5: TPanel
            Left = 0
            Top = 0
            Width = 1086
            Height = 652
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object RzToolbar10: TRzToolbar
              Left = 0
              Top = 0
              Width = 1086
              Height = 34
              AutoStyle = False
              Images = DM.cxImageList1
              RowHeight = 30
              BorderInner = fsFlat
              BorderOuter = fsNone
              BorderSides = [sdBottom]
              BorderWidth = 0
              GradientColorStyle = gcsCustom
              TabOrder = 0
              VisualStyle = vsGradient
              ToolbarControls = (
                RzSpacer77
                RzBut2
                RzSpacer5
                RzBut3
                RzSpacer78
                RzBut5
                RzSpacer79
                RzBut4
                RzSpacer7
                RzBut6
                RzSpacer6
                RzToolButton1
                RzSpacer1
                RzToolButton3
                RzSpacer2
                RzBut
                RzSpacer3
                cxLabel40
                cxLookupComboBox2
                RzSpacer4
                RzToolButton5
                RzSpacer9
                RzBut8)
              object RzSpacer77: TRzSpacer
                Left = 4
                Top = 5
                Width = 10
              end
              object RzBut2: TRzToolButton
                Left = 14
                Top = 5
                Width = 72
                Hint = #26032#28155#21152#19968#26465#24037#31243#39033#30446
                ImageIndex = 37
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #26032#22686#39033#30446
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut2Click
              end
              object RzSpacer78: TRzSpacer
                Left = 156
                Top = 5
                Width = 10
              end
              object RzBut4: TRzToolButton
                Left = 234
                Top = 5
                Width = 60
                ImageIndex = 3
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #20445#23384
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut4Click
              end
              object RzSpacer79: TRzSpacer
                Left = 226
                Top = 5
              end
              object RzBut5: TRzToolButton
                Left = 166
                Top = 5
                Width = 60
                Hint = #21024#38500#25152#26377#19982#26412#24037#31243#30456#20851#32852#30340#25968#25454#19982#23376#24037#31243
                ImageIndex = 51
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #21024#38500
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut5Click
              end
              object RzBut8: TRzToolButton
                Left = 920
                Top = 5
                Width = 60
                ImageIndex = 8
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #36864#20986
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut8Click
              end
              object RzSpacer5: TRzSpacer
                Left = 86
                Top = 5
                Width = 10
              end
              object RzBut3: TRzToolButton
                Left = 96
                Top = 5
                Width = 60
                Hint = #32534#36753#24037#31243#39033#30446
                ImageIndex = 0
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #32534#36753
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut3Click
              end
              object RzSpacer6: TRzSpacer
                Left = 362
                Top = 5
              end
              object RzSpacer7: TRzSpacer
                Left = 294
                Top = 5
              end
              object RzBut6: TRzToolButton
                Left = 302
                Top = 5
                Width = 60
                Hint = #35835#21462#25152#26377#26410#21024#38500#30340#24037#31243
                ImageIndex = 11
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                Caption = #21047#26032
                ParentShowHint = False
                ShowHint = True
                OnClick = RzBut6Click
              end
              object RzSpacer1: TRzSpacer
                Left = 444
                Top = 5
              end
              object RzToolButton1: TRzToolButton
                Left = 370
                Top = 5
                Width = 74
                DropDownMenu = pmPrint
                ImageIndex = 5
                ShowCaption = True
                UseToolbarButtonSize = False
                UseToolbarShowCaption = False
                ToolStyle = tsDropDown
                Caption = #25253#34920
              end
              object RzSpacer2: TRzSpacer
                Left = 542
                Top = 5
              end
              object RzToolButton3: TRzToolButton
                Left = 452
                Top = 5
                Width = 90
                DropDownMenu = pm1
                ImageIndex = 79
                ShowCaption = True
                UseToolbarShowCaption = False
                ToolStyle = tsDropDown
                Caption = #25253#20215#21015#34920
              end
              object RzSpacer3: TRzSpacer
                Left = 626
                Top = 5
              end
              object RzSpacer4: TRzSpacer
                Left = 879
                Top = 5
              end
              object RzBut: TRzToolButton
                Left = 550
                Top = 5
                Width = 76
                ImageIndex = 16
                Images = DM.cxImageList1
                ShowCaption = True
                UseToolbarShowCaption = False
                Caption = #25253#20215#27719#24635
                Visible = False
                OnClick = RzButClick
              end
              object RzSpacer9: TRzSpacer
                Left = 912
                Top = 5
              end
              object RzToolButton5: TRzToolButton
                Left = 887
                Top = 5
                ImageIndex = 59
                OnClick = RzToolButton5Click
              end
              object cxLabel40: TcxLabel
                Left = 634
                Top = 9
                Caption = #20379#24212#21830#65306
                Transparent = True
              end
              object cxLookupComboBox2: TcxLookupComboBox
                Left = 686
                Top = 7
                Properties.DropDownListStyle = lsEditList
                Properties.ImmediateDropDownWhenActivated = True
                Properties.KeyFieldNames = 'Supplier'
                Properties.ListColumns = <
                  item
                    Caption = #20379#24212#21830
                    FieldName = 'Supplier'
                  end>
                Properties.ListSource = dsDataSearch
                Properties.OnChange = cxLookupComboBox2PropertiesChange
                Properties.OnEditValueChanged = cxLookupComboBox2PropertiesEditValueChanged
                TabOrder = 1
                Width = 193
              end
            end
            object Grid2: TcxGrid
              Left = 0
              Top = 34
              Width = 1086
              Height = 618
              Align = alClient
              TabOrder = 1
              LockedStateImageOptions.ShowText = True
              LookAndFeel.Kind = lfUltraFlat
              LookAndFeel.NativeStyle = True
              LookAndFeel.SkinName = ''
              ExplicitLeft = 3
              ExplicitTop = 36
              object tvView2: TcxGridDBBandedTableView
                PopupMenu = pm2
                OnDblClick = tvView2DblClick
                OnKeyDown = tvView2KeyDown
                OnMouseDown = tvView2MouseDown
                OnMouseEnter = tvView2MouseEnter
                Navigator.Buttons.CustomButtons = <>
                OnCustomDrawCell = tvView2CustomDrawCell
                OnEditing = tvView2Editing
                OnEditKeyDown = tvView2EditKeyDown
                OnGetCellHeight = tvView2GetCellHeight
                DataController.DataSource = dsSource
                DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText, dcoInsertOnNewItemRowFocusing]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Column2
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col27
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col24
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col25
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col26
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col28
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col29
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col30
                  end
                  item
                    Format = #165',0.00;'#165'-,0.00'
                    Kind = skSum
                    Column = tvView2Col31
                  end>
                DataController.Summary.SummaryGroups = <
                  item
                    Links = <
                      item
                        Column = tvGroupName
                      end>
                    SummaryItems = <
                      item
                        Format = '0.00'
                        Kind = skSum
                        Column = tvView2Col6
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col11
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col24
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col26
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Column2
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col29
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col30
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col31
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col25
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col27
                      end
                      item
                        Format = #165',0.00;'#165'-,0.00'
                        Kind = skSum
                        Column = tvView2Col28
                      end>
                  end>
                DataController.Summary.OnAfterSummary = tvView2DataControllerSummaryAfterSummary
                Images = DM.cxImageList1
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.FocusFirstCellOnNewRecord = True
                OptionsBehavior.FocusCellOnCycle = True
                OptionsBehavior.BandHeaderHints = False
                OptionsCustomize.ColumnMoving = False
                OptionsCustomize.BandMoving = False
                OptionsCustomize.BandsQuickCustomization = True
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.CellAutoHeight = True
                OptionsView.Footer = True
                OptionsView.GroupRowHeight = 26
                OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
                OptionsView.HeaderHeight = 26
                OptionsView.Indicator = True
                OptionsView.BandHeaderHeight = 30
                OptionsView.BandHeaderLineCount = 2
                OptionsView.FixedBandSeparatorWidth = 20
                Styles.OnGetContentStyle = tvView2StylesGetContentStyle
                Styles.Group = DM.cxStyle103
                Styles.GroupByBox = DM.cxStyle10
                Bands = <
                  item
                    Caption = #24037#31243#21517#31216
                    Styles.Header = DM.cxStyle364
                    Width = 242
                  end
                  item
                    Caption = #39033#30446#21517#31216
                    Position.BandIndex = 0
                    Position.ColIndex = 0
                    Styles.Header = DM.cxStyle214
                    Width = 220
                  end
                  item
                    Caption = #30452#25509#36153':0.00 '
                    HeaderAlignmentHorz = taLeftJustify
                    Styles.Header = DM.cxStyle364
                  end
                  item
                    Caption = #20379#24212#21830
                    Position.BandIndex = 2
                    Position.ColIndex = 0
                    Styles.Header = DM.cxStyle214
                    Width = 120
                  end
                  item
                    Caption = #21697#29260
                    Position.BandIndex = 2
                    Position.ColIndex = 1
                    Styles.Header = DM.cxStyle214
                    Width = 60
                  end
                  item
                    Caption = #35268#26684
                    Position.BandIndex = 2
                    Position.ColIndex = 2
                    Styles.Header = DM.cxStyle214
                    Width = 60
                  end
                  item
                    Caption = #24037#31181
                    Position.BandIndex = 2
                    Position.ColIndex = 3
                    Styles.Header = DM.cxStyle214
                    Width = 60
                  end
                  item
                    Caption = #24037#38271
                    Position.BandIndex = 2
                    Position.ColIndex = 4
                    Styles.Header = DM.cxStyle214
                    Width = 60
                  end
                  item
                    Caption = #21333#20301
                    Position.BandIndex = 2
                    Position.ColIndex = 5
                    Styles.Header = DM.cxStyle214
                    Width = 40
                  end
                  item
                    Caption = #24037#31243#37327
                    Position.BandIndex = 2
                    Position.ColIndex = 6
                    Styles.Header = DM.cxStyle214
                    Width = 100
                  end
                  item
                    Caption = #25104#26412#25253#20215
                    Position.BandIndex = 2
                    Position.ColIndex = 9
                    Styles.Header = DM.cxStyle214
                  end
                  item
                    Caption = #20027#26448#25253#20215
                    Position.BandIndex = 2
                    Position.ColIndex = 7
                    Styles.Header = DM.cxStyle214
                    Width = 80
                  end
                  item
                    Caption = #20154#24037#21333#20215
                    Position.BandIndex = 10
                    Position.ColIndex = 0
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #26448#26009#25104#26412
                    Position.BandIndex = 10
                    Position.ColIndex = 1
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #32508#21512#25253#20215
                    Position.BandIndex = 2
                    Position.ColIndex = 10
                    Styles.Header = DM.cxStyle214
                  end
                  item
                    Caption = #25104#26412#21333#20215
                    Position.BandIndex = 10
                    Position.ColIndex = 2
                    Styles.Header = DM.cxStyle214
                    Width = 110
                  end
                  item
                    Caption = #20154#24037
                    Position.BandIndex = 14
                    Position.ColIndex = 0
                    Styles.Header = DM.cxStyle214
                    Visible = False
                    Width = 60
                  end
                  item
                    Caption = #20027#26009
                    Position.BandIndex = 14
                    Position.ColIndex = 1
                    Styles.Header = DM.cxStyle214
                    Visible = False
                    Width = 60
                  end
                  item
                    Caption = #36741#26009
                    Position.BandIndex = 14
                    Position.ColIndex = 2
                    Styles.Header = DM.cxStyle214
                    Visible = False
                    Width = 60
                  end
                  item
                    Caption = #25439#32791
                    Position.BandIndex = 14
                    Position.ColIndex = 3
                    Styles.Header = DM.cxStyle214
                    Visible = False
                    Width = 60
                  end
                  item
                    Caption = #32508#21512#21333#20215
                    Position.BandIndex = 14
                    Position.ColIndex = 4
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #25240#25187#29575
                    Position.BandIndex = 2
                    Position.ColIndex = 11
                    Styles.Header = DM.cxStyle214
                    Width = 58
                  end
                  item
                    Caption = #25240#21518#21333#20215
                    Position.BandIndex = 2
                    Position.ColIndex = 12
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #21512#35745#37329#39069
                    Position.BandIndex = 2
                    Position.ColIndex = 13
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #25240#21518#37329#39069
                    Position.BandIndex = 2
                    Position.ColIndex = 14
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #20027#26448#21512#35745
                    Position.BandIndex = 2
                    Position.ColIndex = 15
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #26448#26009#21512#35745
                    Position.BandIndex = 2
                    Position.ColIndex = 17
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #20154#24037#25104#26412
                    Position.BandIndex = 2
                    Position.ColIndex = 18
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #25104#26412#21512#35745
                    Position.BandIndex = 2
                    Position.ColIndex = 19
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #21033#28070
                    Position.BandIndex = 2
                    Position.ColIndex = 20
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #25240#21518#21033#28070
                    Position.BandIndex = 2
                    Position.ColIndex = 21
                    Styles.Header = DM.cxStyle214
                    Width = 78
                  end
                  item
                    Caption = #24037#33402
                    Position.BandIndex = 2
                    Position.ColIndex = 22
                    Styles.Header = DM.cxStyle214
                    Width = 356
                  end
                  item
                    Caption = #20154#24037#25253#20215
                    Position.BandIndex = 2
                    Position.ColIndex = 8
                    Styles.Header = DM.cxStyle214
                    Width = 90
                  end
                  item
                    Caption = #20154#24037#21512#35745
                    Position.BandIndex = 2
                    Position.ColIndex = 16
                    Styles.Header = DM.cxStyle214
                    Width = 83
                  end>
                object tvGroupName: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'GroupName'
                  Visible = False
                  OnCustomDrawCell = tvGroupNameCustomDrawCell
                  OnGetDataText = tvGroupNameGetDataText
                  OnGetDisplayText = tvGroupNameGetDisplayText
                  GroupIndex = 0
                  Options.Filtering = False
                  Options.FilteringFilteredItemsList = False
                  Options.FilteringMRUItemsList = False
                  Options.FilteringPopup = False
                  Options.FilteringPopupMultiSelect = False
                  Options.FilteringWithFindPanel = False
                  Options.Sorting = False
                  Options.VertSizing = False
                  Width = 79
                  OnCompareRowValuesForCellMerging = tvGroupNameCompareRowValuesForCellMerging
                  Position.BandIndex = 1
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                  IsCaptionAssigned = True
                end
                object tvSpaceName: TcxGridDBBandedColumn
                  Caption = #20998#31867#21517#31216
                  DataBinding.FieldName = 'SpaceName'
                  Options.Editing = False
                  Options.CellMerging = True
                  SortIndex = 0
                  SortOrder = soAscending
                  Width = 62
                  OnCompareRowValuesForCellMerging = tvSpaceNameCompareRowValuesForCellMerging
                  Position.BandIndex = 1
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvProjectName: TcxGridDBBandedColumn
                  Caption = #39033#30446#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 136
                  Position.BandIndex = 1
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object tvGroupIndex: TcxGridDBBandedColumn
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.Alignment.Horz = taCenter
                  OnGetDisplayText = tvGroupIndexGetDisplayText
                  Options.Editing = False
                  SortIndex = 1
                  SortOrder = soAscending
                  Width = 30
                  Position.BandIndex = 1
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                  IsCaptionAssigned = True
                end
                object tvView2Col1: TcxGridDBBandedColumn
                  Caption = #20379#24212#21830
                  DataBinding.FieldName = 'Supplier'
                  PropertiesClassName = 'TcxButtonEditProperties'
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.OnButtonClick = tvView2Col1PropertiesButtonClick
                  Position.BandIndex = 3
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col2: TcxGridDBBandedColumn
                  Caption = #35268#26684
                  DataBinding.FieldName = 'Spec'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Position.BandIndex = 5
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col3: TcxGridDBBandedColumn
                  Caption = #24037#31181
                  DataBinding.FieldName = 'Profession'
                  PropertiesClassName = 'TcxButtonEditProperties'
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.OnButtonClick = tvView2Col3PropertiesButtonClick
                  Position.BandIndex = 6
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col4: TcxGridDBBandedColumn
                  Caption = #24037#38271
                  DataBinding.FieldName = 'HeadOfWork'
                  PropertiesClassName = 'TcxButtonEditProperties'
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.OnButtonClick = tvView2Col4PropertiesButtonClick
                  Position.BandIndex = 7
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col5: TcxGridDBBandedColumn
                  Caption = #21333#20301
                  DataBinding.FieldName = 'Company'
                  Position.BandIndex = 8
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col6: TcxGridDBBandedColumn
                  Caption = #24037#31243#37327
                  DataBinding.FieldName = 'Quantities'
                  PropertiesClassName = 'TcxButtonEditProperties'
                  Properties.Buttons = <
                    item
                      Default = True
                      Kind = bkEllipsis
                    end>
                  Properties.OnButtonClick = tvView2Col6PropertiesButtonClick
                  Properties.OnValidate = tvView2Col6PropertiesValidate
                  Position.BandIndex = 9
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col7: TcxGridDBBandedColumn
                  Caption = #21697#29260
                  DataBinding.FieldName = 'brand'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 150
                  Position.BandIndex = 4
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col8: TcxGridDBBandedColumn
                  Caption = #20027#26448#25253#20215
                  DataBinding.FieldName = 'MainMaterialPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.DisplayFormat = #165',0.00;'#165'-,0.00'
                  Properties.OnValidate = tvView2Col8PropertiesValidate
                  Position.BandIndex = 11
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col9: TcxGridDBBandedColumn
                  Caption = #20154#24037#21333#20215
                  DataBinding.FieldName = 'ArtificialPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 12
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col10: TcxGridDBBandedColumn
                  Caption = #26448#26009#25104#26412
                  DataBinding.FieldName = 'MaterialPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 13
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col11: TcxGridDBBandedColumn
                  Caption = #25104#26412#21333#20215
                  DataBinding.FieldName = 'CostUnitPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 15
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col12: TcxGridDBBandedColumn
                  Caption = #20154#24037
                  DataBinding.FieldName = 'Artifi'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 16
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col13: TcxGridDBBandedColumn
                  Caption = #20027#26009
                  DataBinding.FieldName = 'MainMaterial'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 17
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col14: TcxGridDBBandedColumn
                  Caption = #36741#26009
                  DataBinding.FieldName = 'Auxil'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 18
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col15: TcxGridDBBandedColumn
                  Caption = #25439#32791
                  DataBinding.FieldName = 'loss'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 19
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col16: TcxGridDBBandedColumn
                  Caption = #32508#21512#21333#20215
                  DataBinding.FieldName = 'SyntheticalPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.OnValidate = tvView2Col16PropertiesValidate
                  Position.BandIndex = 20
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col17: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'Itemtext'
                  Visible = False
                  Width = 64
                  Position.BandIndex = 1
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
                object tvView2Col18: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'ItemCurrent'
                  Visible = False
                  Position.BandIndex = 1
                  Position.ColIndex = 8
                  Position.RowIndex = 0
                end
                object tvView2Col19: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'NoId'
                  Visible = False
                  Width = 60
                  Position.BandIndex = 1
                  Position.ColIndex = 9
                  Position.RowIndex = 0
                end
                object tvView2Col20: TcxGridDBBandedColumn
                  Caption = #29366#24577
                  DataBinding.FieldName = 'status'
                  Position.BandIndex = -1
                  Position.ColIndex = -1
                  Position.RowIndex = -1
                end
                object tvView2Col21: TcxGridDBBandedColumn
                  Caption = #32508#21512#21333#20215
                  Position.BandIndex = -1
                  Position.ColIndex = -1
                  Position.RowIndex = -1
                end
                object tvView2Col22: TcxGridDBBandedColumn
                  Caption = #25240#25187#29575
                  DataBinding.FieldName = 'DismantRate'
                  PropertiesClassName = 'TcxSpinEditProperties'
                  Properties.DisplayFormat = '0%'
                  Properties.ValueType = vtInt
                  Properties.OnValidate = tvView2Col22PropertiesValidate
                  Styles.OnGetContentStyle = tvView2Col22StylesGetContentStyle
                  Position.BandIndex = 21
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col23: TcxGridDBBandedColumn
                  Caption = #25240#21518#21333#20215
                  DataBinding.FieldName = 'AfterDismantRatePrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 22
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col24: TcxGridDBBandedColumn
                  Caption = #21512#35745#37329#39069
                  DataBinding.FieldName = 'SumMoney'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.ValidationOptions = [evoRaiseException, evoShowErrorIcon, evoAllowLoseFocus]
                  Properties.OnValidate = tvView2Col24PropertiesValidate
                  OnValidateDrawValue = tvView2Col24ValidateDrawValue
                  Options.Editing = False
                  Position.BandIndex = 23
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col25: TcxGridDBBandedColumn
                  Caption = #25240#21518#37329#39069
                  DataBinding.FieldName = 'AfterDismantSummary'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.ValidationOptions = [evoRaiseException, evoShowErrorIcon, evoAllowLoseFocus]
                  Options.Editing = False
                  Position.BandIndex = 24
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col26: TcxGridDBBandedColumn
                  Caption = #20027#26448#21512#35745
                  DataBinding.FieldName = 'MainMaterialTotal'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 25
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col27: TcxGridDBBandedColumn
                  Caption = #26448#26009#21512#35745
                  DataBinding.FieldName = 'MaterialSummary'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 26
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col28: TcxGridDBBandedColumn
                  Caption = #20154#24037#25104#26412
                  DataBinding.FieldName = 'LaborCost'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 27
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col29: TcxGridDBBandedColumn
                  Caption = #25104#26412#21512#35745
                  DataBinding.FieldName = 'CostSummary'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 28
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col30: TcxGridDBBandedColumn
                  Caption = #21033#28070
                  DataBinding.FieldName = 'Profit'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 29
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col31: TcxGridDBBandedColumn
                  Caption = #25240#21518#21033#28070
                  DataBinding.FieldName = 'loseAfterProfit'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Options.Editing = False
                  Position.BandIndex = 30
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Col32: TcxGridDBBandedColumn
                  Caption = #24037#33402
                  DataBinding.FieldName = 'Technics'
                  Position.BandIndex = 31
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Column1: TcxGridDBBandedColumn
                  Caption = #20154#24037#25253#20215
                  DataBinding.FieldName = 'ManpowerPrice'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Position.BandIndex = 32
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Column2: TcxGridDBBandedColumn
                  Caption = #20154#24037#21512#35745
                  DataBinding.FieldName = 'ManpowerSummary'
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Position.BandIndex = 33
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object tvView2Column3: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'OfferType'
                  Visible = False
                  Position.BandIndex = 1
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object tvAutoCalc: TcxGridDBBandedColumn
                  Caption = #33258#21160#35745#31639
                  DataBinding.FieldName = 'AutoCalc'
                  Visible = False
                  Position.BandIndex = 1
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object tvView2Column4: TcxGridDBBandedColumn
                  DataBinding.FieldName = 'iRec'
                  Visible = False
                  Position.BandIndex = 1
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
              end
              object Lv2: TcxGridLevel
                GridView = tvView2
              end
            end
          end
        end
      end
      object cxSplitter2: TcxSplitter
        Left = 1088
        Top = 0
        Width = 8
        Height = 654
        HotZoneClassName = 'TcxXPTaskBarStyle'
        HotZone.SizePercent = 60
        AlignSplitter = salRight
        AutoSnap = True
        ResizeUpdate = True
      end
      object Panel1: TPanel
        Left = 1096
        Top = 0
        Width = 292
        Height = 654
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object cxPageControl1: TcxPageControl
          Left = 0
          Top = 0
          Width = 292
          Height = 654
          Align = alClient
          TabOrder = 0
          Properties.ActivePage = cxTabSheet1
          Properties.CustomButtons.Buttons = <>
          Properties.Options = [pcoAlwaysShowGoDialogButton, pcoFixedTabWidthWhenRotated, pcoGradient, pcoGradientClientArea, pcoRedrawOnResize]
          Properties.TabHeight = 25
          Properties.TabWidth = 65
          ClientRectBottom = 650
          ClientRectLeft = 4
          ClientRectRight = 288
          ClientRectTop = 31
          object cxTabSheet1: TcxTabSheet
            Caption = #25253#20215#24211
            ImageIndex = 0
            object Bevel2: TBevel
              Left = 0
              Top = 172
              Width = 284
              Height = 10
              Align = alBottom
              Shape = bsSpacer
              ExplicitLeft = 2
              ExplicitTop = 168
              ExplicitWidth = 240
            end
            object tv1: TTreeView
              Left = 0
              Top = 0
              Width = 284
              Height = 172
              Align = alClient
              BevelInner = bvNone
              BevelOuter = bvNone
              DragMode = dmAutomatic
              HideSelection = False
              Images = DM.TreeImageList
              Indent = 19
              RowSelect = True
              SortType = stBoth
              TabOrder = 0
              OnClick = tv1Click
              Items.NodeData = {
                0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
                00000000000104A562F74E06527B7C}
            end
            object Grid3: TcxGrid
              Left = 0
              Top = 211
              Width = 284
              Height = 408
              Align = alBottom
              TabOrder = 1
              LookAndFeel.Kind = lfUltraFlat
              LookAndFeel.NativeStyle = True
              LookAndFeel.SkinName = ''
              object tvView3: TcxGridDBTableView
                OnDblClick = tvView3DblClick
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                DataController.DataSource = dslibSource
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.DragDropText = True
                OptionsBehavior.DragFocusing = dfDragDrop
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.FocusFirstCellOnNewRecord = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsBehavior.NavigatorHints = True
                OptionsBehavior.FocusCellOnCycle = True
                OptionsBehavior.PullFocusing = True
                OptionsCustomize.ColumnMoving = False
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.DataRowSizing = True
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsSelection.CellMultiSelect = True
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.DataRowHeight = 23
                OptionsView.GroupByBox = False
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.HeaderHeight = 23
                OptionsView.Indicator = True
                OptionsView.IndicatorWidth = 16
                object tvView3Column1: TcxGridDBColumn
                  Caption = #39033#30446#21517#31216
                  DataBinding.FieldName = 'ProjectName'
                  Width = 153
                end
                object tvView3Column2: TcxGridDBColumn
                  Caption = #25253#20215
                  PropertiesClassName = 'TcxCurrencyEditProperties'
                  Properties.Alignment.Horz = taLeftJustify
                  Width = 69
                end
                object tvView3Column3: TcxGridDBColumn
                  Caption = #35268#26684
                  DataBinding.FieldName = 'Spec'
                  Width = 65
                end
              end
              object Lv3: TcxGridLevel
                GridView = tvView3
              end
            end
            object RzToolbar1: TRzToolbar
              Left = 0
              Top = 182
              Width = 284
              Height = 29
              Align = alBottom
              AutoStyle = False
              Images = DM.cxImageList1
              BorderInner = fsNone
              BorderOuter = fsNone
              BorderSides = []
              BorderWidth = 0
              GradientColorStyle = gcsCustom
              TabOrder = 2
              VisualStyle = vsGradient
              ToolbarControls = (
                cxLabel34
                cxLookupComboBox1
                RzSpacer8
                cxLabel39
                cxLookupComboBox3
                RzToolButton4)
              object RzToolButton4: TRzToolButton
                Left = 249
                Top = 2
                ImageIndex = 59
                OnClick = RzToolButton4Click
              end
              object RzSpacer8: TRzSpacer
                Left = 145
                Top = 2
                Width = 5
              end
              object cxLookupComboBox1: TcxLookupComboBox
                Left = 36
                Top = 4
                Properties.ImmediateDropDownWhenActivated = True
                Properties.KeyFieldNames = 'ProjectName'
                Properties.ListColumns = <
                  item
                    Caption = #39033#30446#21517#31216
                    FieldName = 'ProjectName'
                  end>
                Properties.ListSource = dslibSource
                Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
                TabOrder = 0
                Width = 109
              end
              object cxLabel34: TcxLabel
                Left = 4
                Top = 6
                Caption = #21517#31216':'
              end
              object cxLookupComboBox3: TcxLookupComboBox
                Left = 182
                Top = 4
                Properties.ImmediateDropDownWhenActivated = True
                Properties.KeyFieldNames = 'Spec'
                Properties.ListColumns = <
                  item
                    Caption = #35268#26684
                    FieldName = 'Spec'
                  end>
                Properties.ListSource = dslibSource
                Properties.OnCloseUp = cxLookupComboBox3PropertiesCloseUp
                TabOrder = 2
                Width = 67
              end
              object cxLabel39: TcxLabel
                Left = 150
                Top = 6
                Caption = #35268#26684':'
              end
            end
          end
        end
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 280
      Top = 0
      Width = 8
      Height = 654
      HotZoneClassName = 'TcxXPTaskBarStyle'
      HotZone.SizePercent = 60
    end
  end
  object pmPrint: TPopupMenu
    Left = 432
    Top = 584
    object N34: TMenuItem
      Caption = #23548#20986'Excel&'
      OnClick = N34Click
    end
    object N39: TMenuItem
      Caption = #32508#21512#25253#20215#65288#25240#25187#25253#20215#65289'&'
      OnClick = N1Click
    end
    object N38: TMenuItem
      Caption = #32508#21512#25253#20215#65288#26080#20154#24037#32452#65289'&'
      OnClick = N1Click
    end
    object N1: TMenuItem
      Caption = #32508#21512#25253#20215#65288#24102#20154#24037#32452#65289'&'
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #22235#39033#25253#20215'&'
      OnClick = N1Click
    end
    object N3: TMenuItem
      Caption = #29289#26009#39033#30446#28165#21333'&'
      OnClick = N1Click
    end
    object N32: TMenuItem
      Caption = #20154#24037#25253#20215'&'
      OnClick = N1Click
    end
    object N49: TMenuItem
      Caption = #20027#26448#25253#20215'('#24102#25240#25187')&'
      OnClick = N1Click
    end
    object N33: TMenuItem
      Caption = #20027#26448#25253#20215'('#26080#25240#25187')&'
      OnClick = N1Click
    end
    object N40: TMenuItem
      Caption = #25104#26412#26680#31639#20154#24037#26680#31639'('#26597#35810')&'
      Visible = False
      OnClick = N1Click
    end
    object N35: TMenuItem
      Caption = #25104#26412#26680#31639#20154#24037#26680#31639'&'
      OnClick = N1Click
    end
    object N36: TMenuItem
      Caption = #20027#26448#26680#31639#28165#21333'&'
      OnClick = N1Click
    end
    object N37: TMenuItem
      Caption = #20154#24037#26680#31639#28165#21333'&'
      OnClick = N1Click
    end
  end
  object PopupMenu: TcxGridPopupMenu
    Grid = Grid2
    PopupMenus = <>
    AlwaysFireOnPopup = True
    Left = 376
    Top = 584
  end
  object dsMaster: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'SpaceName'
    Params = <>
    AfterClose = dsMasterAfterClose
    AfterInsert = dsMasterAfterInsert
    AfterDelete = dsMasterAfterDelete
    Left = 512
    Top = 584
  end
  object dsSource: TDataSource
    DataSet = dsMaster
    Left = 560
    Top = 584
  end
  object pm1: TPopupMenu
    Left = 474
    Top = 584
    object N24: TMenuItem
      Caption = #20840#37096#26174#31034
      Visible = False
      OnClick = N24Click
    end
    object N5: TMenuItem
      Caption = #23458#25143#25253#20215
      OnClick = N5Click
    end
    object N4: TMenuItem
      Caption = #20027#26448#25253#20215
      OnClick = N4Click
    end
    object N25: TMenuItem
      Caption = #20154#24037#25253#20215
      OnClick = N25Click
    end
    object N6: TMenuItem
      Caption = #25104#26412#26680#31639
      Visible = False
      OnClick = N6Click
    end
    object N8: TMenuItem
      Caption = #20027#26448#26680#31639
      Visible = False
      OnClick = N8Click
    end
    object N7: TMenuItem
      Caption = #20154#24037#26680#31639
      Visible = False
      OnClick = N7Click
    end
  end
  object dxGroupName: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 40
    Top = 376
    object dxGroupNameGroupName: TStringField
      FieldName = 'GroupName'
      Size = 255
    end
  end
  object dsGroupNameSource: TDataSource
    DataSet = dxGroupName
    Left = 40
    Top = 448
  end
  object pm2: TPopupMenu
    Left = 328
    Top = 584
    object N9: TMenuItem
      Caption = #26032#22686#39033#30446
      OnClick = RzBut2Click
    end
    object N10: TMenuItem
      Caption = #32534#36753#39033#30446
      OnClick = RzBut3Click
    end
    object N11: TMenuItem
      Caption = #20445#12288#12288#23384
      OnClick = RzBut4Click
    end
    object N12: TMenuItem
      Caption = #21024#12288#12288#38500
      OnClick = RzBut5Click
    end
    object N13: TMenuItem
      Caption = #21047#12288#12288#26032
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object N20: TMenuItem
      Caption = #21024#38500#31354#34892
      OnClick = N20Click
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object N15: TMenuItem
      Caption = #23637#24320#32452
      OnClick = N15Click
    end
    object N16: TMenuItem
      Caption = #25910#32553#32452
      OnClick = N16Click
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Caption = #28155#21152#21040#20854#20182#25151#38388
      OnClick = N18Click
    end
    object N28: TMenuItem
      Caption = '-'
    end
    object N27: TMenuItem
      Caption = #33258#21160#35745#31639
      Enabled = False
      OnClick = N27Click
    end
    object N29: TMenuItem
      Caption = #21462#28040#33258#21160#35745#31639
      OnClick = N29Click
    end
    object N30: TMenuItem
      Caption = '-'
    end
    object N31: TMenuItem
      Caption = #27719#24635#32479#35745
      OnClick = N31Click
    end
    object N42: TMenuItem
      Caption = '-'
    end
    object N43: TMenuItem
      Caption = #20923#32467
      OnClick = N43Click
    end
    object N48: TMenuItem
      Caption = #35299#20923
      OnClick = N43Click
    end
  end
  object dslib: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 616
    Top = 584
  end
  object dslibSource: TDataSource
    DataSet = dslib
    Left = 432
    Top = 632
  end
  object tmrClock: TTimer
    Enabled = False
    Interval = 100
    OnTimer = tmrClockTimer
    Left = 544
    Top = 208
  end
  object pm3: TPopupMenu
    Left = 128
    Top = 64
    object N21: TMenuItem
      Caption = #19978#31227
      OnClick = N21Click
    end
    object N22: TMenuItem
      Caption = #19979#31227
      OnClick = N22Click
    end
    object N23: TMenuItem
      Caption = #22797#21046#21040#20854#20182#24037#31243
      Visible = False
      OnClick = N23Click
    end
    object N26: TMenuItem
      Caption = #24037#31243#20449#24687
      OnClick = N26Click
    end
    object N41: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N45: TMenuItem
      Caption = #20923#32467#35774#35745#24072
      Visible = False
    end
    object N44: TMenuItem
      Caption = #35299#20923#35774#35745#24072
      Visible = False
    end
    object N46: TMenuItem
      Caption = #20923#32467#33829#38144#21592
      Visible = False
    end
    object N47: TMenuItem
      Caption = #35299#20923#33829#38144#21592
      Visible = False
    end
  end
  object dsSounceInfo: TDataSource
    DataSet = dsClientInfo
    Left = 776
    Top = 456
  end
  object dsDetailed: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 416
    Top = 216
  end
  object dsDetailed1: TDataSource
    DataSet = dsDetailed
    Left = 368
    Top = 216
  end
  object icCustomIconList: TcxImageCollection
    Left = 496
    Top = 216
    object icCustomIcon1: TcxImageCollectionItem
      Picture.Data = {
        07544269746D617076020000424D760200000000000036000000280000000C00
        00000C0000000100200000000000400200000000000000000000000000000000
        00002DABC7FE1195B3FF1095B4FF0F94B4FF0F94B4FF0F94B4FF0F94B4FF0F94
        B4FF1095B4FF1095B4FF1195B3FF36B3CEFF37BDD9FE1DC1E5FF18C5F0FF14C2
        F0FF14C2F0FF0A6F8BFF0B6F8AFF16C3F0FF18C5F0FF1AC6F1FF1BBFE4FF44BD
        D8FF2771819A29BCDAFF24CDEFFF1ECAF1FF1CC8F1FF042128FF052127FF21CC
        F2FF24CFF2FF26CFF1FF29BCDAFF1539414D234249533DADC4E92ECFEAFF31DA
        F5FF2ED8F4FF29C6E0FF2BC7E1FF31DAF5FF32DBF5FF2DCFE9FF3DACC3EA0F1C
        1E22000000002C616C7E29B9D5FF43E8F6FF43E9F8FF165158FF155057FF40E6
        F8FF3EE4F5FF29BAD6FF2D626D7F00000000000000000A1112142999B3DB32CE
        E3FF4DF1F9FF0C2829FF0B2728FF49EEF9FF32D0E5FF2A9AB3DC0A1112140000
        0000000000000000000060969FB020B3D1FF4AEAF4FF0D282AFF0C2729FF49EA
        F5FF20B3D2FF6097A2B1000000000000000000000000000000000000000059C6
        DBFF36D0E3FF2F8E92FF2F8E92FF37D3E5FF5AC6DCFF00000000000000000000
        00000000000000000000000000001C3B414C2BB9D4FF4BECF5FF4CEEF6FF2FBF
        D8FF1C3B424C000000000000000000000000000000000000000000000000161F
        202251B4C8E937D4E6FF3DDBEAFF51B4C9E91720212300000000000000000000
        000000000000000000000000000000000000335F68762DB5D1FC31BAD3FC3360
        6977000000000000000000000000000000000000000000000000000000000000
        0000242F313312424D5D1E454D5A151C1D1F0000000000000000000000000000
        0000}
    end
    object icCustomIconListItem1: TcxImageCollectionItem
      Picture.Data = {
        07544269746D6170E6010000424DE60100000000000036000000280000000C00
        00000C0000000100180000000000B0010000C30E0000C30E0000000000000000
        0000AD8477AC8173B08578AE8476A97E6FAE8477AD8375A97C6DAC8373AC8474
        A97D6EA97F71C6A092F8F0ECDBC1B6E1CBC1F5ECE7DCC4B9DDC4B9F6ECE6E0CA
        C0DABFB3F4EAE5C29989C9A598DFC8C0C69F90C19887E3CEC5C19887C19686E3
        CEC4C09584C49B8DDBC1B6C49E90C8A396EDDFD8D8B9ABD8BBACEDDDD6D7B8A9
        D7B8A9ECDED6DDBBA9DEBAA9ECD9CFC19B8DC6A194F9F3F0E5D0C7E9D9D1F4EB
        E6E6D3CAE6D3CAF3EBE6D3D5DCCCCED7ECEBEBC39A89CBAA9EE2CFC8C29888C2
        9888E6D3CABF9383BF9282E2D2CD3E83C3277CC3ACC1D9D0A28ECBA89DEADBD4
        D7B7A8D4B3A5EADAD3D4B2A4D4B3A4E7D9D461A4D850A2DCBACFE2CEA08DC9A5
        99FBF8F5EBDCD2EFE1DAF5EBE4EEDED5EEDED4F4EAE3E0E0E2DADCDEF1EFEDC4
        9C8DCEADA5EDD4BCD69F61E4BC88E2B984E3BB85E4BA85E2B883E7BB83D89C5B
        E8C9AEC6A49ACDABA4F3E0C5E0B06EE9C58FEBC793EBC792EBC792EAC791E9C4
        8CDFAC6BEED5B9C7A49ACBA79EFFFDF6F7E9D1F7E7D1F7E7D0F7E6D0F6E6CFF6
        E7CFF7E7D0F7E7CFFEF9F2C6A094D6B9ACDABEB2DBC2B9DBC1B9DBC1B9DBC1B9
        DBC1B8DBC1B8DBC1B8DBC2B9DABEB3D5B7AA}
    end
  end
  object dsClientInfo: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = dsClientInfoAfterInsert
    Left = 776
    Top = 392
  end
  object frxDataset1: TfrxDBDataset
    UserName = #20154#24037#25253#20215
    CloseDataSource = False
    DataSet = dsReportArtert
    BCDToCurrency = False
    Left = 130
    Top = 250
  end
  object frxDataset2: TfrxDBDataset
    UserName = #30452#25509#36153
    CloseDataSource = False
    DataSet = qry1
    BCDToCurrency = False
    Left = 130
    Top = 314
  end
  object frxDataset3: TfrxDBDataset
    UserName = #36153#29992#32452
    CloseDataSource = False
    DataSet = dsReportCost
    BCDToCurrency = False
    Left = 130
    Top = 386
  end
  object qry1: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 216
    Top = 312
  end
  object dsReportArtert: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 216
    Top = 248
  end
  object dsReportCost: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 216
    Top = 392
  end
  object dsSearch: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 344
    Top = 368
  end
  object dsDataSearch: TDataSource
    DataSet = dsSearch
    Left = 344
    Top = 424
  end
end
