object frmEnclosure: TfrmEnclosure
  Left = 0
  Top = 0
  Caption = #38468#20214
  ClientHeight = 492
  ClientWidth = 855
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter4: TSplitter
    Left = 249
    Top = 29
    Width = 10
    Height = 444
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 331
    ExplicitTop = 8
    ExplicitHeight = 713
  end
  object cxShellListView1: TcxShellListView
    Left = 259
    Top = 29
    Width = 596
    Height = 444
    Align = alClient
    Root.BrowseFolder = bfCustomPath
    TabOrder = 0
    OnDblClick = cxShellListView1DblClick
    ExplicitLeft = 0
    ExplicitWidth = 686
    ExplicitHeight = 271
  end
  object RzToolbar13: TRzToolbar
    Left = 0
    Top = 0
    Width = 855
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ExplicitWidth = 686
    ToolbarControls = (
      RzSpacer30
      RzToolButton1
      RzSpacer57
      RzToolButton88
      RzSpacer1)
    object RzSpacer30: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzSpacer57: TRzSpacer
      Left = 74
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      Width = 62
      ImageIndex = 57
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21521#19978
      OnClick = RzToolButton1Click
    end
    object RzToolButton88: TRzToolButton
      Left = 82
      Top = 2
      Width = 62
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
    end
    object RzSpacer1: TRzSpacer
      Left = 144
      Top = 2
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 473
    Width = 855
    Height = 19
    Panels = <
      item
        Text = #24403#21069#36335#24452':'
        Width = 60
      end
      item
        Width = 300
      end>
    ExplicitTop = 300
    ExplicitWidth = 686
  end
  object cxShellTreeView1: TcxShellTreeView
    Left = 0
    Top = 29
    Width = 249
    Height = 444
    Align = alLeft
    Indent = 19
    RightClickSelect = True
    Root.BrowseFolder = bfCustomPath
    ShellListView = cxShellListView1
    TabOrder = 3
    ExplicitLeft = 4
    ExplicitTop = 23
  end
end
