unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RzButton, RzPanel, RzBmpBtn, OleCtrls, SHDocVw, jpeg,
  RzBckgnd, ComCtrls, StdCtrls, XPMan;

type
  TMain = class(TForm)
    RzPanel3: TRzPanel;
    Image2: TImage;
    RzBmpButton2: TRzBmpButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Main: TMain;

implementation

uses
  ufrmMain,global;

{$R *.dfm}

procedure TMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Action := caMinimize;
end;

procedure TMain.FormCreate(Sender: TObject);
var
  s : string;

begin
  Main := Self;
  s := GetFilePath + 'Main.jpg';
  if FileExists(s) then
  begin
    Self.Image2.Picture.LoadFromFile(s);
  end;

end;

procedure TMain.FormResize(Sender: TObject);
begin
  Self.RzBmpButton2.Left:= Round(Self.Width / 2 - Self.RzBmpButton2.Width / 2 - 10);
  Self.RzBmpButton2.Top := Round(Self.Image2.Height - 110);
end;

procedure TMain.FormShow(Sender: TObject);
var
  HtmlPath : string;
begin
  HtmlPath := GetFilePath;
//  show;
end;

end.
