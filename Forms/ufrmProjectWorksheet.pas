unit ufrmProjectWorksheet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RzEdit, Vcl.Buttons,
  Vcl.ComCtrls, RzButton, Vcl.Mask, RzTabs,Data.Win.ADODB,Data.DB, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxButtons,ufrmBaseController,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxDBEdit, cxMaskEdit, cxSpinEdit,
  cxCurrencyEdit, cxDropDownEdit, cxCalendar, cxTimeEdit, cxMemo;

type
  TfrmProjectWorksheet = class(TfrmBaseController)
    GroupBox2: TGroupBox;
    Label17: TLabel;
    Label8: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    Label25: TLabel;
    Label34: TLabel;
    RzBitBtn1: TRzBitBtn;
    BitBtn3: TBitBtn;
    RzEdit15: TRzEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label19: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label23: TLabel;
    Label29: TLabel;
    Label36: TLabel;
    BitBtn1: TBitBtn;
    RzEdit14: TRzEdit;
    RzEdit20: TRzEdit;
    GroupBox3: TGroupBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    Label2: TLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    cxDBMemo1: TcxDBMemo;
    cxDBDateEdit3: TcxDBDateEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxButton2Click(Sender: TObject);
    procedure cxDBCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    tmpAdo : TADOQuery;
  end;

var
  frmProjectWorksheet: TfrmProjectWorksheet;

implementation

uses
   DateUtils , Math , global,ufrmConstructManage;


var
  tvfrmManage : TfrmConstructManage;

{$R *.dfm}

procedure TfrmProjectWorksheet.BitBtn1Click(Sender: TObject);
var
  szStartTime : TDateTime;
  szEndTime   : TDateTime;
  szValue : Integer;
  m : Currency;
  Minute : Double;
begin //添加统计
  m := Self.cxCurrencyEdit1.Value;
  szStartTime := Self.cxDBDateEdit1.Date;
  szEndTime   := Self.cxDBDateEdit2.Date;
//  szValue := StrToIntDef(Self.RzEdit4.Text,0);
  {
  Self.DateTimePicker1.Time := Self.DateTimePicker2.Time ;
  Self.DateTimePicker3.Time := Self.DateTimePicker4.Time ;

  szStartTime := Self.DateTimePicker1.DateTime;
  szEndTime   := Self.DateTimePicker3.DateTime;
  }
  if CompareDateTime(szEndTime,szStartTime) <> -1 then
  begin

    if m <> 0 then
    begin
      Minute := MinuteSpan(szEndTime , szStartTime); //得到总分钟数
      m := m / 60;
      Self.RzEdit14.Text := FloatToStr(Math.RoundTo( m * Minute , -2)) + ' 元';
    end;
    Self.RzEdit20.Text:= getTaskTimeStr(szEndTime,szStartTime,0);
    //Self.cxDBTextEdit3.Text := getDateTimeStr(szEndTime,szStartTime);
    tmpAdo.FieldByName(Self.cxDBTextEdit3.DataBinding.DataField).Value := getDateTimeStr(szEndTime,szStartTime);
  //  Self.RzEdit13.Text:= getDateTimeStr(szEndTime,szStartTime);
  end
  else
  begin
    Application.MessageBox('开始时间不能大于结束时间,无法计算正确计算加班时间',m_title,MB_OK + MB_ICONHAND);
  end;
end;


procedure TfrmProjectWorksheet.cxButton1Click(Sender: TObject);
begin
  inherited;
  case tmpAdo.State of
    dsInactive: ;
    dsBrowse: ;
    dsEdit:
      begin
        tmpAdo.UpdateRecord;
      end;
    dsInsert:
      begin
        tmpAdo.Post;
      end ;
    dsSetKey: ;
    dsCalcFields: ;
    dsFilter: ;
    dsNewValue: ;
    dsOldValue: ;
    dsCurValue: ;
    dsBlockRead: ;
    dsInternalCalc: ;
    dsOpening: ;
  end;
end;

procedure TfrmProjectWorksheet.cxButton2Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmProjectWorksheet.cxDBCurrencyEdit1PropertiesChange(
  Sender: TObject);
var
  szMoney : Currency;
begin
  inherited;
  szMoney := Self.cxDBCurrencyEdit1.Value;
  Self.Label34.Caption := MoneyConvert(szMoney);
end;

procedure TfrmProjectWorksheet.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.cxButton1.Click;
  end;
  inherited;
end;

end.
