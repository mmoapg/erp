object frmCustomerAmount: TfrmCustomerAmount
  Left = 0
  Top = 0
  Caption = #25253#20215#24037#31243#37327
  ClientHeight = 411
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 34
    Width = 775
    Height = 377
    Align = alClient
    TabOrder = 0
    object tvView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnGetCellHeight = tvViewGetCellHeight
      DataController.DataSource = dsMasterSounce
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.CellAutoHeight = True
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      object tvViewColumn1: TcxGridDBColumn
        Caption = #24207#21495
        Width = 28
      end
      object tvViewColumn8: TcxGridDBColumn
        Caption = #32452#21517
        Visible = False
        GroupIndex = 0
      end
      object tvViewColumn2: TcxGridDBColumn
        Caption = #39033#30446#21517#31216
        Options.CellMerging = True
        Width = 110
      end
      object tvViewColumn3: TcxGridDBColumn
        Caption = #37096#20301
        Width = 96
      end
      object tvViewColumn4: TcxGridDBColumn
        Caption = #20844#24335
        Width = 185
      end
      object tvViewColumn5: TcxGridDBColumn
        Caption = #21333#20301
        Width = 48
      end
      object tvViewColumn6: TcxGridDBColumn
        Caption = #25968#37327
        Width = 102
      end
      object tvViewColumn7: TcxGridDBColumn
        Caption = #22791#27880
        Width = 167
      end
    end
    object Lv: TcxGridLevel
      GridView = tvView
    end
  end
  object RzToolbar10: TRzToolbar
    Left = 0
    Top = 0
    Width = 775
    Height = 34
    AutoStyle = False
    Images = DM.cxImageList1
    RowHeight = 30
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer77
      RzBut2
      RzSpacer5
      RzBut3
      RzSpacer78
      RzBut5
      RzSpacer79
      RzBut4
      RzSpacer7
      RzBut6
      RzSpacer6
      RzToolButton1
      RzSpacer1
      RzBut8
      RzSpacer3)
    object RzSpacer77: TRzSpacer
      Left = 4
      Top = 5
      Width = 10
    end
    object RzBut2: TRzToolButton
      Left = 14
      Top = 5
      Width = 72
      Hint = #26032#28155#21152#19968#26465#24037#31243#39033#30446
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686#20998#32452
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut2Click
    end
    object RzSpacer78: TRzSpacer
      Left = 156
      Top = 5
      Width = 10
    end
    object RzBut4: TRzToolButton
      Left = 234
      Top = 5
      Width = 60
      ImageIndex = 17
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #30830#23450
      ParentShowHint = False
      ShowHint = True
    end
    object RzSpacer79: TRzSpacer
      Left = 226
      Top = 5
    end
    object RzBut5: TRzToolButton
      Left = 166
      Top = 5
      Width = 60
      Hint = #21024#38500#25152#26377#19982#26412#24037#31243#30456#20851#32852#30340#25968#25454#19982#23376#24037#31243
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      ParentShowHint = False
      ShowHint = True
    end
    object RzBut8: TRzToolButton
      Left = 452
      Top = 5
      Width = 60
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      ParentShowHint = False
      ShowHint = True
    end
    object RzSpacer5: TRzSpacer
      Left = 86
      Top = 5
      Width = 10
    end
    object RzBut3: TRzToolButton
      Left = 96
      Top = 5
      Width = 60
      Hint = #32534#36753#24037#31243#39033#30446
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      ParentShowHint = False
      ShowHint = True
    end
    object RzSpacer6: TRzSpacer
      Left = 362
      Top = 5
    end
    object RzSpacer7: TRzSpacer
      Left = 294
      Top = 5
    end
    object RzBut6: TRzToolButton
      Left = 302
      Top = 5
      Width = 60
      Hint = #35835#21462#25152#26377#26410#21024#38500#30340#24037#31243
      ImageIndex = 11
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21047#26032
      ParentShowHint = False
      ShowHint = True
    end
    object RzSpacer1: TRzSpacer
      Left = 444
      Top = 5
    end
    object RzToolButton1: TRzToolButton
      Left = 370
      Top = 5
      Width = 74
      ImageIndex = 5
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      ToolStyle = tsDropDown
      Caption = #25253#34920
    end
    object RzSpacer3: TRzSpacer
      Left = 512
      Top = 5
    end
  end
  object dsMaster: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 384
    Top = 208
  end
  object dsMasterSounce: TDataSource
    DataSet = dsMaster
    Left = 384
    Top = 264
  end
end
