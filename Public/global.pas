unit global;

interface

uses
   Windows,Classes,SysUtils,ComObj,Dialogs, Grids, DBGrids,DBGridEh,Forms,StdCtrls,
   TreeFillThrd,TreeUtils,ComCtrls,DB,ADODB;

const
   WM_LisView     = 100001;
   WM_FrameView   = 100002;
   WM_FrameDele   = 100003;
   WM_SetGridFocus= 100004;
   WM_FrameClose  = 100005;
   WM_Refresh     = 100006;
   WM_ViewNum     = 100007;
const
   Connect = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%s;;Extended Properties="Excel 8.0;HDR=YES;IMEX=0";Persist Security Info=False';

type
   TStaff = record
    dwCompanyName : string; //公司名称
    dwParent   : String;
    dwFullname : String;
    dwUnitPrice: Currency; //单价
    dwOverPrice: Currency; //加班单价
    dwwagestype: String;   //工资类型
    dwLeastWork: Integer;  //最少工作时间
    dwsection  : String;   //部门
    dwNumbers  : String;   //编号
    dwSex : string;

  end;
  pStaff  = ^TStaff;

type
   TProject = record
     dwName : string;
     dwCode : string;
     dwCodeList : string;
   end;
   PProject = ^TProject;

type
   TRebar = record
     Model : string;
     Weight: Single;
     Level : Integer;
   end;

type
  TUser = record
     dwUserName : AnsiString;
     dwPassWord : AnsiString;
     dwType     : Integer;
     dwAuth     : AnsiString;
     dwIsLogin  : Boolean;
   end;
   user = ^TUser;


type
   TPayType = record
     ID      : Integer;
     Nmae    : string;
     Remarks : string;
   end;
   PPaytype = ^TPayType;
type
  TMakings = record
    dwGoodsNo : Variant;
    dwMakingCaption : Variant;
    dwModelIndex : Variant;
    dwSpec  : Variant;
    dwBrand : Variant;
    dwManufactor : Variant;
    dwNoColour : Variant;
    dwTexture  : Variant;
    dwNoType   : Variant;
    dwNoNumber : Variant;
    dwBuyingPrice : Variant;
    dwUnitPrice: Variant;
  end;
  PMakings = ^TMakings;

type
  TCalcTool=record
    dwCalculating   : Variant;
    dwVariableCount : Variant;
    dwNumericalValue: Variant;
    dwCountTotal    : Variant;
  end;
var
  m_user : TUser ;
  m_title: PChar = '提示';
  m_OutText  : string = '请输入名称';
  m_Sign_Id  : string = 'Sign_Id';
  m_Config:string = 'Config.ini';
  m_Node  :string = 'Config';
  //****************************************************************************
  //支款表
  g_Table_Expenditure : string = 'expenditure'; //支款材料表
  g_Table_Concrete    : string = 'Concrete';    //支款混凝土

  g_Table_Branch_Mechanics   : string = 'sk_Branch_Mechanics'; //支款机械
  g_dir_BranchMechanics: string= 'BranchMechanics'; //机械目录

  g_Table_BranchMoney : string = 'sk_BranchMoney';
  g_dir_BranchMoney   : string = 'BranchMoney';

  g_Table_DetailTable : string = 'sk_BranchDetail';
  g_dir_DetailTable   : string = 'BranchDetail';

  g_Table_Branch_RebarManage : string = 'sk_RebarManage';  //钢筋
  g_Table_PactTree    : string = 'sk_BorrowAccount';  //合同树
  g_Table_Branch_EnterStorage: string = 'sk_EnterStorage';//架体入库单
  g_Table_Qualifications : string = 'sk_Qualifications';//资质

  //************************************************************
  g_Table_Maintain_Job : string = 'sk_Maintain_Job'; //职务信息
  g_Table_Maintain_Cost: string = 'sk_Maintain_Cost';//费用科目
  g_Table_Maintain_Tab : string = 'sk_Maintain_Tab'; //制表类型

  g_Table_Maintain_CompanyInfoTree : string = 'sk_Maintain_CompanyInfoTree';
  g_Table_Maintain_CompanyInfoList : string = 'sk_Maintain_CompanyInfoList';

  g_Table_Maintain_section  : string = 'sk_Maintain_section'; //部门管理
  g_Table_Maintain_WorkType : string = 'sk_Maintain_WorkType'; //工种维护
  g_Table_Maintain_Concrete : string = 'sk_Maintain_Concrete'; //混凝土维护表
  g_Table_Project_Status: string = 'sk_Project_Status';//工程状态
  g_Table_PayType       : string = 'pay_type';//交易类型
  g_Table_DetList       : string = 'sk_detList';    //债务列表
  g_Table_Maintain_Project : string     = 'sk_Maintain_Project'; //工程维护表
  g_Table_maintain_ProjectTree : string = 'sk_Maintain_ProjectTree';
  g_Table_Maintain_QuotationTree: string = 'sk_Maintain_QuotationTree';//报价库树
  g_Table_Maintain_QuotationList: string = 'sk_Maintain_QuotationList';//报价库列表
  g_Table_MakingsTree   : string = 'sk_MakingsTree';//材料树表
  g_Table_MakingsList   : string = 'sk_MakingsList';//材料管理表
  g_Table_Maintain_Work : string = 'sk_Maintain_Work';//考勤维护表
  g_Table_Maintain_Science : string = 'sk_Maintain_Science';//材料维护表
  g_Table_Maintain_SubContract:string='sk_Maintain_SubContract';//分包维护表

  //***********************收款*************************************************
  g_Table_pactDebtmoney : string = 'sk_pactDebtmoney';  //收款债务表
  g_Table_Collect_Receivables : string = 'sk_Collect_Receivables';
  g_CollectReceivables  : string = 'CollectReceivables';

  g_Table_Income  : string = 'sk_income';//表名
  g_income        : string = 'income';   //目录名

  //g_Table_Collect_DetailTable   : string = 'sk_DetailTable';
  g_Dir_Collect_DetailTable     : string = 'DetailTable';

  //****************************************************************************
  g_Table_RuningAccount : string = 'sk_RuningAccount'; //流水帐
  g_Table_RuningDetail  : string = 'sk_RuningDetail';  //流水帐明细
  g_Dir_RuningAccount   : string = 'RuningAccount';    //流水帐目录

  g_Table_CompanyTree   : string = 'sk_CompanyTree';//往来单位分类表
  g_Table_CompanyManage : string = 'sk_CompanyManage';//往来单位明细表

  //************************************************************
  g_Table_Branch_EngineeringQuantity : string = 'sk_Branch_EngineeringQuantity';
  g_Table_Branch_EngineeringDetailed : string = 'sk_Branch_EngineeringDetailed';
  g_dir_EngineeringQuantity : string = 'BranchQuantity'; //工程量目录
  //****************************************************************************
  //施工管理
  g_Table_Project_Quality  : string = 'sk_Project_Quality';
  g_Dir_Project_Quality    : string = 'ProjectQuality';

  g_Table_Project_Complete : string = 'sk_Project_Complete';   //竣工管理
  g_Dir_Project_Complete   : string = 'ProjectComplete';

  g_Table_Project_Tender   : string = 'sk_Project_Tender';  //招标管理
  g_Dir_Project_Tender     : string = 'ProjectTender';

  g_Table_Project_Concrete : string = 'sk_Project_Concrete';//工程管理混凝土
  g_Dir_Project_Concrete   : string = 'ProjectConcrete';

  g_Table_Project_Sanction : string = 'sk_Project_Sanction';  //增奖扣罚表
  g_dir_Sanction           : string = 'Sanction';  //增奖扣罚目录

  g_Table_Project_Lease  : string = 'sk_Project_Lease';  //租赁管理表
  g_Dir_Project_Lease    : string = 'ProjectLease'; //租赁管理目录

  g_Table_Project_PactInfo  : string = 'sk_Project_PactInfo';  //合同信息表
  g_dir_PactInfo  : string = 'PactInfo'; //合同信息目录

  g_Table_Project_AllocateBankroll : string = 'sk_Project_AllocateBankroll'; //拨款申请单表
  g_dir_AllocateBankroll : string = 'AllocateBankroll';  //拨款申请单目录

  g_Table_Project_Survey : string = 'sk_Project_Survey'; //工程概况表
  g_dir_Survey : string = 'Survey';  //工程概况目录

  g_Table_Projects_Tree : string = 'sk_Projects_Tree';//工程项目管理树
  g_Table_Project_EngineeringDetailed : string = 'sk_Project_EngineeringDetailed';
  g_Table_Project_EngineeringQuantity : string = 'sk_Project_EngineeringQuantity';
  g_Dir_Project_Quantity : string = 'ProjectQuantity';

  g_Table_Project_BuildFlowingAccount : string = 'sk_Project_BuildFlowingAccount';
  g_Dir_Project_Finance : string = 'ProjectFinance';

  g_Table_Project_Worksheet           : string = 'sk_Project_Worksheet';
  g_DirWork : string = 'ProjectWorkSeeht';

  g_Table_Project_Dossier_tree        : string = 'sk_Project_Dossier_tree';
  g_Table_Project_Procedures_Tree     : string = 'sk_Project_Procedures_Tree';
  g_Table_Project_SceneManage_Tree    : string = 'sk_Project_SceneManage_Tree';
  g_Table_Project_System_Tree         : string = 'sk_Project_System_Tree';

  g_Table_Project_Mechanics           : string = 'sk_Project_Mechanics';
  g_Dir_Project_Mechanics : string = 'ProjectMechanics';

  g_Table_Project_BuildStorage        : string = 'sk_Project_BuildStorage';
  g_DirBuildStorage : string = 'ProjectStorage';

  g_Table_Project_Concrete_GroupMoney : string = 'sk_Project_Concrete_GroupMoney';

  g_Table_Finance_FinalAccount : string = 'sk_Finance_FinalAccount'; //成本树
  g_Table_Finance_ProjectFinal : string = 'sk_Finance_ProjectFinal'; //明细
  g_Dir_ProjectFinal : string = 'FinanceProjectFinal'; //成本明细目录

  g_Table_Company_Staff : string = 'sk_Company_Staff';
  g_Table_Company_Staff_CheckWork : string = 'sk_Company_Staff_CheckWork';
  g_Table_Company_Staff_Over      : string = 'sk_Company_Staff_Over';

  g_Table_Company_Prize   : string = 'sk_Company_Prize';
  g_Dir_Company_Prize     : string = 'CompanyPrize';

  g_Table_Company_buckle  : string = 'sk_Company_buckle';
  g_Dir_Company_buckle    : string = 'CompanyBuckle';

  g_Table_Company_BlendStaff  : string = 'sk_Company_BlendStaff';
  g_Table_Company_BlendProjectRecord : string = 'sk_Company_BlendProjectRecord';
  //公司仓库
  g_Table_Company_StorageTree     : string = 'sk_Company_StorageTree';
  g_Table_Company_StorageBillList : string = 'sk_Company_StorageBillList';
  g_Table_Company_StorageDetailed : string = 'sk_Company_StorageDetailed';
  g_Table_Company_Storage_ContractList : string = 'sk_Company_Storage_ContractList';
  g_DirCompanyStorageContractList : string = 'CompanyStorageContractList';
  g_Table_Company_Storage_ContractDetailed : string = 'sk_Company_Storage_ContractDetailed';
  g_Table_Company_Storage_GroupUnitPrice   : string = 'sk_Company_Storage_GroupUnitPrice';//组合单价
  g_Table_Decorate_OfferTree   : string = 'sk_Decorate_OfferTree';  //报价往来单分类
  g_Table_Decorate_OfferBranchTree : string = 'sk_Decorate_OfferBranchTree';//报价分支往来单位
  g_Table_Decorate_OfferList    : string = 'sk_Decorate_OfferList'; //报价明细列表
  g_Table_Decorate_OfferInfo    : string = 'sk_Decorate_OfferInfo'; //报价信息表
  g_Table_Decorate_OfferSumCheck: string = 'sk_Decorate_OfferSumCheck';//报价汇总
  //数据库表名
  m_dwPhoto : string = 'photo';
  m_Numer   : string = '编号';
  m_Code    : string = 'Code';
  m_Caption : string = 'Caption';
  m_fixed   : string = 'fixed';
  m_phone   : string = 'phone';
  m_money   : string = 'sum_money';
  m_Address : string = 'Address';
  m_Remarks : string = 'Remarks';
  m_pay_contact : string = 'pay_record';
  m_out_contact : string = 'out_record';

  m_Photo   : string = 'Photo\';  //图片目录
  m_Payment : string = 'Payment\';//付款图片目录
  m_Payout  : string = 'Payout\'; //支款图片目录
  m_Pact    : string = 'Pact\';   //合同图片目录
  m_AppName : string;

  g_ParentIndex: Integer; //全局父ID
  g_Resources  : string; //存放附件的位置
  g_Parent     : Integer;
  g_IsSelect   : Boolean;
  g_SelectName : string;
  g_ModuleIndex: Byte = 0;  //模块序号
  g_RunningIndex : Integer; //流水帐ID
  g_pacttype : Byte = 0; //帐目
  g_tabIndex : Integer;
  g_code    : Integer;
  g_global_code : string;
  g_numbers : string;
  g_caption : string;
  g_content  : string;
  g_PurchaseCompany : string;
  g_pay_time : TDateTime;
  g_truck    : Integer;
  g_Price    : Currency;
  g_num      : Single;
  g_Units    : string;
  g_payee    : string;
  g_pay_type : string;
  g_remarks  : string;
  g_end_time : TDateTime;

  g_Number :  Integer;
  g_paytime : TDateTime;
  g_paytype : Integer;
  g_small_money : Currency;
  g_large_money : string;
  g_ImgPath : string;
  g_TreeType: Byte;   //树类型,也就是表的名称
  g_SelectProjectName : string;
  dwSelectTypes : Integer;
  CS:TRTLCriticalSection;
  m_PactIndex:Byte;
  m_IsModify:Boolean = False;
  g_RebarList : array[0..15] of TRebar;
  RebarList   : array[0..15] of TRebar;
//****************************************
  function MoneyConvert(mmje:real): WideString;
  function DBGridExport(lpGrid:TDBGridEh) : Boolean;

  function GetFilePath:string;stdcall;
  function SaveFile(AFilePath , ASavePathName: string):string;
  function GetSystem32():string;
  function IsPhoto(AFile:string):Boolean;
  function getEditData(DBGridEh : TDBGrideh):Boolean;stdcall;
  
  function getDateTimeStr(BigTime, SmallTime : TDateTime):string; //取加班小时分钟
  function getTaskTimeStr(BigTime, SmallTime : TDateTime;AValue : Integer):string; //取工时
  function GetOverTime(t0 : Double):string;
  function GetOverHour(t0 : Double ;AValue : Integer):string;

  function GetUnitList(List : TComboBox ; chType : Integer):Boolean;stdcall;
  function DeleteBorrowData(szCodeList : string):Boolean;stdcall;
  function ConcatEnclosure(dir, Numbers : string):string;
  function DeleteDirectory(NowPath: string): Boolean; // 删除整个目录
  function IsDigit(ch: char): boolean;
  function GetRowCode(lpTableName , lpFieldName : string; lpCode : string = '0000' ; lpIndex : Integer = 10000):string;
  function GetUserCode(lpTableName : string; lpIndex : Integer = 10000):string;

  function RegisterServer(const aDllFileName: string; aRegister: Boolean): Boolean;
  function treeCldnode(Anode: TTreeNode;var s : string):string;stdcall;
  function GetPyCode(AHz : string):string;
  function SearchDateRange(StartDate , EndDate , Sqltext : string; AQry : TADOQuery):Boolean;

  procedure getPactMoney(Anode: TTreeNode;var AMoney : Currency);
  procedure OutputLog(s:string);
  procedure DoEnumAllFiles(strPath: string;FileList: TStrings);
  procedure CreateOpenDir(h : THandle; dir : string  ; IsOpen : Boolean = False);
  function EAN13BarCode(xCode: string; lineWidth: integer = 2) : string;
  function DeleteMoney(s : string):Currency;
  function SplitClipboardStr(lpClipboard : string):string;
  function SplitString(const Source,ch:String):TStringList;

implementation

uses
   MD5,DateUtils,Math,uDataModule,ShellAPI,FileTree,DBGridEhImpExp;

function SplitString(const Source,ch:String):TStringList;
var
  temp:String;
  i:Integer;
begin
  Result:=TStringList.Create;
  //如果是空自符串则返回空列表
  if Source='' then exit;
  temp:=Source;
  i := pos(ch,Source);
  while i<>0 do
  begin
    Result.add(copy(temp,0,i-1));
    Delete(temp,1,i);
    i := pos(ch,temp);
  end;
  Result.add(temp);
end;

function DeleteMoney(s : string):Currency;
var
  DD1 : string;
begin
  DD1 := StringReplace(s ,'￥','',[rfReplaceAll]); //处理美元符号
  Result := StrToCurrDef(  StringReplace( DD1 ,',','',[rfReplaceAll]),0 );
end;

function SplitClipboardStr( lpClipboard : string ):string;
var
  daorustring,daorustring1,daorustring2:string;
begin
  daorustring:= ( lpClipboard  ); //处理美元符号
  daorustring1 := StringReplace(daorustring,#$D#$A,'@',[rfReplaceAll]); //换行
  Result := StringReplace(daorustring1,#9,'|',[rfReplaceAll]); //单元格分割
end;


function SearchDateRange(StartDate , EndDate , Sqltext : string; AQry : TADOQuery):Boolean;
begin
  if (StartDate = '') and (EndDate = '') then
  begin
    Application.MessageBox( '起始日期或结束日期不是有效的查询日期，查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)
  end else
  begin
    if CompareDateTime(StrToDate(EndDate),StrToDate(StartDate)) <> -1 then
    begin
      with AQry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqltext;
        Parameters.ParamByName('t1').Value := StartDate;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := EndDate;
        Open;
      end;

    end else
    begin
      Application.MessageBox( '起始日期不能大于结束日期，范围查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)

    end;

  end;

end;

function IsRowIndex(lpTableName,lpFieldName : string; lpCode , lpNumber : string ; lpIndex , lpRowCount : Integer ) : string;
var
  Id : ShortString;

begin
  Id := Format('%s',[ FormatdateTime('yymmdd',now) ]) ;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + lpTableName + ' Where ' + lpFieldName + '="' + lpNumber + '"';
    Open;
    if RecordCount <> 0 then
    begin
      lpNumber := id + lpCode +  IntToStr(lpIndex + lpRowCount + 1);
      IsRowIndex(lpTableName,lpFieldName,lpCode,lpNumber,lpIndex,lpRowCount);
    end;

  end;
  Result := lpNumber;
end;

function GetRowCode(lpTableName,lpFieldName : string; lpCode : string = '0000' ; lpIndex : Integer = 10000):string;
var
  Id : ShortString;
  i  :integer;
  Count:Integer;

begin
  Randomize;
  Count := Random(lpIndex);

  Id := Format('%s',[ FormatdateTime('yymmdd',now) ]) ;
  i  := DM.GetRowCount(lpTableName); //取出行数
  Result := id + lpCode +  IntToStr(lpIndex +  Count + i);
  IsRowIndex(lpTableName,lpFieldName,lpCode,Result,lpIndex +  Count,i);
end;

function GetUserCode(lpTableName : string; lpIndex : Integer = 10000):string;
var
  i:integer;
begin
  i  := DM.GetRowCount(lpTableName); //取出行数
  Result := IntToStr(lpIndex + i);
end;

function HZtoPY(HZStr: WideString): String;
Var
  s, c: AnsiString;
  i: Integer;
begin
  Result := '';
  for i := 1 To Length(HZStr) do
  begin
    s := HZStr[I];
    if ByteType(s, 1) = mbSingleByte Then
      c := S
    else
      case word(s[1]) shl 8 + word(s[2]) of
        $B0A1 .. $B0C4: C := 'A';    $B0C5 .. $B2C0: C := 'B';
        $B2C1 .. $B4ED: C := 'C';    $B4EE .. $B6E9: C := 'D';
        $B6EA .. $B7A1: C := 'E';    $B7A2 .. $B8C0: C := 'F';
        $B8C1 .. $B9FD: C := 'G';    $B9FE .. $BBF6: C := 'H';
        $BBF7 .. $BFA5: C := 'J';    $BFA6 .. $C0AB: C := 'K';
        $C0AC .. $C2E7: C := 'L';    $C2E8 .. $C4C2: C := 'M';
        $C4C3 .. $C5B5: C := 'N';    $C5B6 .. $C5BD: C := 'O';
        $C5BE .. $C6D9: C := 'P';    $C6DA .. $C8BA: C := 'Q';
        $C8BB .. $C8F5: C := 'R';    $C8F6 .. $CBF9: C := 'S';
        $CBFA .. $CDD9: C := 'T';    $CDDA .. $CEF3: C := 'W';
        $CEF4 .. $D1B8: C := 'X';    $D1B9 .. $D4D0: C := 'Y';
        $D4D1 .. $D7F9: C := 'Z';
      else
        c := s;
      end;
    Result := Result + C;
  end;
end;

function GetPyCode(AHz : string):string;
var
  i:integer;
  hz: string;
  py: string;

begin
  Result := '';
  for i:=1 to length(Ahz) div 2 do
  begin
    hz := copy(AHz,i*2-1,2);
    py := py + HZtoPY(hz);
  end;
  Result := py;
end;

function RegisterServer(const aDllFileName: string; aRegister: Boolean): Boolean;
type
  TRegProc = function: HResult;
  stdcall;
const
  cRegFuncNameArr: array [Boolean] of PChar =
    ('DllUnregisterServer', 'DllRegisterServer');
var
  vLibHandle: THandle;
  vRegProc: TRegProc;
begin
  Result := False;
  vLibHandle := LoadLibrary(PChar(aDllFileName));
  if vLibHandle = 0 then Exit;
    @vRegProc := GetProcAddress(vLibHandle, cRegFuncNameArr[aRegister]);
  if @vRegProc <> nil then
    Result := vRegProc = S_OK;
  FreeLibrary(vLibHandle);
end;


function IsDigit(ch: char): boolean;
begin
  Result := ch in ['0'..'9'];
end;

function GetSystem32():string;
var
  adr: array[0..MAX_PATH] of Char;
begin
    GetSystemDirectory(adr,MAX_PATH);   //获取SYSTEM路径
//GetWindowsDirectory(adr,MAX_PATH);   //获取WINDOWSM路径
   Result := string(adr);
end;


function GetFilePath:string;stdcall;
var
 ModName: array[0..MAX_PATH] of Char;
begin
 GetModuleFileName(HInstance, ModName, SizeOf(ModName));   // 取得当前项目的路径
 GetFilePath := ExtractFilePath(ModName);
end;

procedure CreateOpenDir(h : THandle; dir : string ; IsOpen : Boolean = False);
var
  s : Cardinal;

begin
  if not DirectoryExists( dir ) then //判断目录是否存在
  begin
    ForceDirectories(dir);
  //  fileSetAttr(dir,faDirectory);
  //  CreateDirectory(PChar( dir ), nil);
  end;

  if IsOpen then
  begin
    ShellExecute(0,'open',nil,nil,PWideChar( dir ),SW_SHOW);
  end;

end;

function ConcatEnclosure(dir, Numbers : string):string;
begin
  //拼接路径
  Result := Concat(Dir,'\' + Numbers);
end;

function DBGridExport(lpGrid:TDBGridEh) : Boolean;
var
  ExpClass: TDBGridEhExportClass;
  Ext: string;
  SaveDlg: TSaveDialog;
  ExportAll: Boolean;
begin
  ExportAll := True; //缺省情况下导出所有数据
  SaveDlg := TSaveDialog.Create(Application);
  SaveDlg.Filter := 'Excel表格(*.XLS)|*.XLS|HTML文档(*.HTM)|*.HTM|RTF格式(*.RTF)|*.RTF|纯文本文件(*.TXT)|*.TXT|逗号分隔文本格式(*.CSV)|*.CSV';
  SaveDlg.FileName := '明细表' + '_' + FormatDateTime('YYMMDDHHmmSS', now);
  SaveDlg.Options := [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing];
  if SaveDlg.Execute then
    begin
      case SaveDlg.FilterIndex of
        1:
          begin
            ExpClass := TDBGridEhExportAsXLS;
            Ext := 'xls';
          end;
        2:
          begin
            ExpClass := TDBGridEhExportAsHTML;
            Ext := 'htm';
          end;
        3:
          begin
            ExpClass := TDBGridEhExportAsRTF;
            Ext := 'rtf';
          end;
        4:
          begin
            ExpClass := TDBGridEhExportAsText;
            Ext := 'txt';
          end;
        5:
          begin
            ExpClass := TDBGridEhExportAsCSV;
            Ext := 'csv';
          end;
      else
        ExpClass := nil;
        Ext := '';
      end;

      if ExpClass <> nil then
      begin
        if UpperCase(Copy(SaveDlg.FileName, Length(SaveDlg.FileName) - 2, 3)) <> UpperCase(Ext) then
          SaveDlg.FileName := SaveDlg.FileName + '.' + Ext;
                //如果grid中有选择的区域，则只导出所选择的区域
        if lpGrid.Selection.SelectionType <> gstNon then ExportAll := False;
        try
          SaveDBGridEhToExportFile(ExpClass, lpGrid, SaveDlg.FileName, ExportAll);
          ShellExecute(0,'open',PWideChar( SaveDlg.FileName ),nil,nil,SW_SHOW);
        except

          on E: Exception do
          begin
            MessageBox(0, LPCTSTR('保存文件发生异常,请检查是否有重名文件并且该文件没有被打开!'), '注意', MB_OK or MB_ICONWARNING);
            Exit;
          end;

        end;

      end;

    end;

end;

function getEditData(DBGridEh : TDBGridEh):Boolean;stdcall; //支款信息编辑
var
   i : Integer;

begin
  Result := False;

  if not DBGridEh.DataSource.DataSet.IsEmpty then
  begin
    g_code     := DBGridEh.DataSource.DataSet.FieldByName(m_Numer).AsInteger;
    g_numbers  := DBGridEh.DataSource.DataSet.FieldByName('numbers').AsString;
    g_global_code := DBGridEh.DataSource.DataSet.FieldByName('Code').AsString;
    g_caption  := DBGridEh.DataSource.DataSet.FieldByName('caption').AsString;
    g_content  := DBGridEh.DataSource.DataSet.FieldByName('content').AsString;
    g_PurchaseCompany := DBGridEh.DataSource.DataSet.FieldByName('PurchaseCompany').AsString;
    g_pay_time := DBGridEh.DataSource.DataSet.FieldByName('pay_time').AsDateTime;
    g_truck    := DBGridEh.DataSource.DataSet.FieldByName('truck').AsInteger;
    g_Price    := DBGridEh.DataSource.DataSet.FieldByName('Price').AsCurrency;
    g_num      := DBGridEh.DataSource.DataSet.FieldByName('num').AsFloat;
    g_Units    := DBGridEh.DataSource.DataSet.FieldByName('Units').AsString;

    g_remarks  := DBGridEh.DataSource.DataSet.FieldByName('remarks').AsString;
    g_End_time := DBGridEh.DataSource.DataSet.FieldByName('End_time').AsDateTime;
    Result := True;

  end;

end;

procedure  OutputLog(s:string);
var
  s0 : array[1..5] of Char; // string;
begin
  try
    s0[1]:=Char(84);
    s0[2]:=Char(97);
    s0[3]:=Char(108);
    s0[4]:=Char(101);
    s0[5]:=Char(58);

    OutputDebugString(PWideChar( s0 + s )); //PAnsiChar('Tale: '+ str)
  except
    OutputDebugString(PWideChar(s));
  end;
end;

function MoneyConvert(mmje:real): WideString;
const
  cNum: WideString = '零壹贰叁肆伍陆柒捌玖-万仟佰拾亿仟佰拾万仟佰拾元角分';
        cCha: array[0..1, 0..11] of string =
        (('零仟','零佰','零拾','零零零','零零',
           '零亿','零万','零元','亿万','零角','零分','零整'),
         ( '零','零','零','零','零','亿','万','元','亿','零','整','整'));
var
  i: Integer;
  sNum :WideString;
begin
  Result := '';
  if mmje < 0 then
  begin
    Result := '负';
    mmje := -mmje;
  end;
  sNum := FormatFloat('0',mmje*100);
  for i := 1 to Length(sNum) do
  begin
    Result := Result + cNum[ord(sNum[i])-47] + cNum[26-Length(sNum)+i];
  end;

  for i:= 0 to 11 do // 去掉多余的零
  begin
    Result := StringReplace(result, cCha[0,i], cCha[1,i], [rfReplaceAll]);
  end;
end;

function SaveFile(AFilePath , ASavePathName: string):string;
var
  FileName : string;
  NewFile  : string;
  NewName  : string;
  szFileExt: string;
  Num : Integer;
  
begin
  Result := '';
  
  if FileExists( AFilePath ) then
  begin
    FileName := ExtractFileName( AFilePath );
    szFileExt:= ExtractFileExt( AFilePath ) ;
    randomize;
    Num := Random(9999999);
  //  NewName  := GetMD5Text(IntToStr(Num));
    NewFile  := GetFilePath + m_Photo + ASavePathName + NewName+ szFileExt;
    OutputLog(NewFile);
    
    if not FileExists(NewFile) then
    begin
    //  if CopyFile(PAnsiChar( AFilePath ),PAnsiChar(NewFile) ,False) then
    //  begin
    //    Result := NewName+ szFileExt;
    //  end;
    end
    else
    begin
      SaveFile(AFilePath,ASavePathName);
    end;

  end;
end;

function getTaskTimeStr(BigTime, SmallTime : TDateTime;AValue : Integer):string;
var
  t0 : Double;
  t1 : Integer;
begin
  t0 := MinuteSpan(BigTime , SmallTime); //加班工时
  t1 := Trunc( t0 / 60 / 8 );
  Result := IntToStr(t1 + AValue);
end;

function getDateTimeStr(BigTime, SmallTime : TDateTime):string;
var
  t0 : Double;
  t1  : Integer;
  Time: TDateTime;
  Hour: Word;
  Minute : Word;
  Second : Word;
  s : string;

begin
  t0 := MinuteSpan(BigTime , SmallTime); //加班总分钟
  t1 := Trunc( t0 / 60 / 8 );
  t0   := t0 - (t1 * 480);
  Time := StrToTime(TimeToStr(Round( t0 * 60 ) /86400 ));
  Hour := HourOf(Time);
  Minute:=MinuteOf(Time);
  Second:=SecondOf(Time);
  if Hour <> 0 then
  begin
    s := IntToStr(Hour) + ' 小时 ';
  end;

  if Minute <> 0 then
  begin
    s := s + IntToStr(Minute) + ' 分钟';
  end;

  Result := s;
//  Result := FormatDateTime('h 小时 nn 分钟 ss 秒',Time);
end;

function GetOverHour(t0 : Double ;AValue : Integer):string;
var
  t1 : Integer;  //获取加班工时
begin
  t1 := Trunc( t0 / 60 / 8 );
  Result := IntToStr(t1 + AValue);
end;

function GetOverTime(t0 : Double):string;
var
  t1  : Integer;
  Time: TDateTime;
  Hour: Word;
  Minute : Word;
  Second : Word;
  s : string;

begin     //获取加班小时分钟
  t1 := Trunc( t0 / 60 / 8 );
  t0   := t0 - (t1 * 480);
  Time := StrToTime(TimeToStr(Round( t0 * 60 ) /86400 ));
  Hour := HourOf(Time);
  Minute:=MinuteOf(Time);
  Second:=SecondOf(Time);
  if Hour <> 0 then
  begin
    s := IntToStr(Hour) + ' 小时 ';
  end;

  if Minute <> 0 then
  begin
    s := s + IntToStr(Minute) + ' 分钟';
  end;

  Result := s;
end;


function IsPhoto(AFile:string):Boolean;
var   //声明变量
   MyImage:TMemoryStream;   //内存流对象
   Buffer:Word;
   i:integer;

begin
  Result := False;
  MyImage:=TMemoryStream.Create; //建立内存流对象
  try
        MyImage.LoadFromFile( AFile ); //把刚刚用户选择的文件载入到内存流中
         MyImage.Position := 0;   //移动指针到最开头的位置
        if MyImage.Size = 0 then   //如果文件大小等于0，那么
        begin
           //ShowMessage('错误');
           Exit;
        end;
        MyImage.ReadBuffer(Buffer,2); //读取文件的前２个字节，放到Buffer里面
        if Buffer=$4D42 then //如果前两个字节是以4D42[低位到高位]
        begin
          Result := True;
          //  ShowMessage('BMP'); //那么这个是BMP格式的文件
        end
        else if Buffer=$D8FF then //如果前两个字节是以D8FF[低位到高位]
       begin
         Result := True;
         // ShowMessage('JPEG'); //........一样　下面不注释了
       end
       else if Buffer=$4947 then
       begin
       //  Result := True;
        //  ShowMessage('GIF');
       end
       else if Buffer=$050A then
       begin
        //  ShowMessage('PCX');
       end
       else if Buffer=$5089 then
       begin
         
        // ShowMessage('PNG');
      end
       else if Buffer=$4238 then
       begin
        //  ShowMessage('PSD');
       end
       else if Buffer=$A659 then
       begin
         //  ShowMessage('RAS');
     end
       else if Buffer=$DA01 then
      begin
       //  ShowMessage('SGI');
      end
    else if Buffer=$4949 then
      begin
        //  ShowMessage('TIFF');
      end
      else   //如是其他类型的文件的话，直接显示错误
      begin
      //   ShowMessage('ERR');
     end;

  finally
    MyImage.Free;   //释放内存流对象
  end;
end;

function GetUnitList(List : TComboBox ; chType : Integer):Boolean;stdcall;
var
  name , Remarks , id : string;
  i : Integer;
  s : string;

begin
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from unit where chType=' + IntToStr(chType);
  DM.Qry.Open;
  i:=0;

  if not DM.Qry.IsEmpty then
  begin
    List.Clear;
    while not DM.Qry.Eof do
    begin
      name := DM.Qry.FieldByName('name').AsString;
      Inc(i);
      //有所有权限
      List.Items.Add(name);
      DM.Qry.Next;
    end;
  end;
end;

function DeleteBorrowData(szCodeList : string):Boolean;stdcall;
var
  szPact   : string;
  s : string;
  szFileName : string;

begin
  szPact        := 'sk_BorrowMoney_Tree';
  s := 'select * from ' + szPact +' where code in('''+ szCodeList  +''')';
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;

    if 0 <> RecordCount then
    begin
      while not Eof do
      begin
        szFileName := FieldByName('photo').AsString;
        s := GetFilePath + m_Photo + m_Pact + szFileName;
        if FileExists(s) then DeleteFile(s);
        Next;
      end;
    end;
  end;

  DM.InDeleteData('sk_BorrowMoney',szCodeList);
  DM.InDeleteData('sk_GiveBorrowMoney',szCodeList);
  //DM.InDeleteData(szPactmoney,szCodeList);
  DM.InDeleteData(szPact,szCodeList);
end;

procedure DoEnumAllFiles(strPath: string;FileList: TStrings);{功能:枚举指定目录及子目录下的所有文件}
var
  sr: TSearchRec;

begin
  if strPath = '' then Exit;
  
  strPath := IncludeTrailingPathDelimiter(strPath);

  if not DirectoryExists(strPath) then Exit;

  if FindFirst(strPath + '*.*', SysUtils.faAnyFile, sr) = 0 then
  begin
    try
      repeat
        //非目录的，就是文件
        if (sr.Attr and SysUtils.faDirectory = 0 ) then
        begin
          FileList.Add(strPath + sr.Name);
        end;
      until FindNext(sr) <> 0;
    finally
      SysUtils.FindClose(sr);
    end;
  end;

  //查找子目录。
  if FindFirst(strPath + '*', SysUtils.faDirectory, sr) = 0 then
  begin
    try
      repeat
        if (sr.Attr and SysUtils.faDirectory<>0) and (sr.Name<>'.') and (sr.Name<>'..') then
        begin
          DoEnumAllFiles(strPath + sr.Name, FileList);
        end;
      until FindNext(sr) <> 0;
    finally
      FindClose(sr);
    end;
  end;

end;

function DeleteDirectory(NowPath: string): Boolean; // 删除整个目录
var
  f : TSearchRec;
  ret: integer;
  key: string;

begin
  if not DeleteTree(NowPath) then
  begin
      if NowPath[Length(NowPath)] <> '\' then
        NowPath := NowPath + '\';
      key := NowPath + '*.*';
      
      ret := FindFirst(key, faAnyFile, f);
      while ret = 0 do
      begin
        if ((f.Attr and fadirectory) = fadirectory) then
        begin
          if (f.Name <> '.') and (f.name <> '..') then
            DeleteDirectory(NowPath + f.name);
        end
        else
        begin
          if ((f.Attr and fadirectory) <> fadirectory) then
          begin
            deletefile(NowPath + f.name);
          end;
        end;
        ret := FindNext(f);
      end;
      findClose(f);

      removedir(NowPath);// 如果需要删除文件夹则添加
      result := True;
  end;
end;

procedure getSontreeCode(Anode: TTreeNode;var s : string);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
begin
    for i := 0 to Anode.Count - 1 do
    begin
      Node := ANode.Item[i];
      if Assigned(node) then
      begin
        szData := PNodeData(Node.Data);
        if Length(s) = 0 then
          s := szData.Code// + ''',''' + PNodeData(ANode.Data).Code
        else
          s := s + ''','''+ szData.Code;
      end;

      if (node.Count > 0)   then
      begin
        getSontreeCode(Node , s);
      end;

    end;
end;

function treeCldnode(Anode: TTreeNode;var s : string):string;stdcall;
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szCodeList : string;
  
begin
  if Assigned(Anode) then
  begin
    szData := PNodeData(ANode.Data);
    s := szData.Code;
    if Anode.Count <> 0 then
    begin
      getSontreeCode(Anode,szCodeList);
      Result := szCodeList;
    end
    else
    begin
      Result := s;
    end;

    s := Result;
  end;
  
end;

procedure getPactMoney(Anode: TTreeNode;var AMoney : Currency);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
begin
  if Anode.Count = 0 then
  begin
    szData := PNodeData(ANode.Data);
    AMoney := szData.sum_Money;
  end
  else
  begin
    for i := 0 to Anode.Count - 1 do
    begin
      Node := ANode.Item[i];
      if Assigned(node) then
      begin
        szData := PNodeData(Node.Data);
        AMoney := AMoney + szData.sum_Money;
      end;

      if (node.Count > 0)   then
      begin
        getPactMoney(Node , AMoney );
      end;
    end;

  end;

end;

function EAN13BarCode(xCode: string; lineWidth: integer = 2) : string;
const
  CABcode: array[1..9] of array[0..5] of integer = ( //由前置码决定编码方案（0 为 A类，1 为 B类）：
   (0, 0, 0, 0, 0, 0),                               //1
   (0, 0, 1, 0, 1, 1),                               //2
   (0, 0, 1, 1, 0, 1),                               //3
   (0, 1, 0, 0, 1, 1),                               //4
   (0, 1, 1, 0, 1, 1),                               //5
   (0, 1, 1, 1, 0, 0),                               //6 （中国）
   (0, 1, 0, 1, 0, 1),                               //7
   (0, 1, 0, 1, 1, 0),                               //8
   (0, 1, 1, 0, 1, 0)                                //9
  );

  Lz: array[0..1] of array [0..9] of integer = (         //左数据编码
    ($1A, $32, $26, $7A, $46, $62, $5E, $76, $6E, $16),  //A 类编码
    ($4E, $66, $36, $42, $3A, $72, $0A, $22, $12, $2E)   //B 类编码
  );

  Rz: array [0..9] of integer =                          //右数据编码
    ($E4, $CC, $D8, $84, $B8, $9C, $A0, $88, $90, $E8);  //C 类编码

  SymbolX: array[0..5] of integer = (11, 13, 57, 59, 103, 105); //符号线横坐标
  LX: array[0..5] of integer = (14, 21, 28, 35, 42, 49); //左数据首条线横坐标
  RX: array[0..5] of integer = (61, 68, 75, 82, 89, 96); //右数据首条线横坐标

var
  statecode, i, v, c1, c2, ab: integer;
  Ls, Rs: string;
  szCode : string;

begin
  if (Length(xCode) <> 12) then
    exit;
  Ls := copy(xCode, 2, 6);//左数据
  Rs := copy(xCode, 8, 5);//右数据
  try
    c1 := 0;
    c2 := 0;
    for i := 0 to 5 do begin
      c1 := c1 + strtoint(xCode[i * 2 + 1]);
      c2 := c2 + strtoint(xCode[i * 2 + 2]);
    end;
    c2 := (10 - (c2 * 3 + c1) mod 10) mod 10;//校验码
    Rs := Rs + inttostr(c2);
    statecode := strtoint(xCode[1]);//前置码
  except
    exit;
  end;
  Result := Concat(xCode,Rs[5 + 1] ) ;
end;


initialization
  begin
  //  GetMem(m_user,SizeOf(m_user));
  end;

{
finalization  begin    freemem(m_user);  end;}

end.
