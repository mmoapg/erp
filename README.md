# Delphi企业管理ERP

现在主要用Java+vue开发，这份代码留着也没什么用,所以开源出来给有需要的人学习用,也没什么技术含量，好代码都是慢慢熬出来的。
如果你有定制产品的需求可以加我微信QQ:30940976

主要功能有
公司管理
- 公司档案
- 员工考勤
- 增奖扣罚
进销管理
- 合同管理
- 交易快捷
- 进销管理
报价管理
- 快捷报价
- 销售报价
- 人工报价
- 报价管理
- 结算收量
- 项目合同
等等。。。。有需要自己下载源码体验

DevExpressVCL_v15.2.2

[码云git](https://gitee.com/sqymail/erp.git)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0305/145545_a941eb19_543700.png "屏幕截图.png")

 **打赏** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0305/145737_3c92fac3_543700.png "wechatpaycode.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0305/150121_6351436f_543700.jpeg "alipaypaycode.jpg")