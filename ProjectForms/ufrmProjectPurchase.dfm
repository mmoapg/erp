inherited frmProjectPurchase: TfrmProjectPurchase
  Caption = #37319#36141#21333
  ClientWidth = 1136
  OldCreateOrder = True
  ExplicitWidth = 1142
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    ExplicitHeight = 551
  end
  inherited RzPanel1: TRzPanel
    Width = 852
    ExplicitWidth = 852
    inherited RzToolbar13: TRzToolbar
      Width = 850
      ExplicitWidth = 850
      ToolbarControls = (
        RzSpacer30
        RzToolButton1
        RzSpacer2
        RzToolButton2
        RzSpacer4
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer10
        btnGridSet
        RzSpacer1
        RzToolButton87
        RzSpacer3
        RzToolButton88
        RzSpacer113)
      inherited RzToolButton85: TRzToolButton
        Left = 212
        ExplicitLeft = 212
      end
      inherited RzSpacer113: TRzSpacer
        Left = 498
        ExplicitLeft = 498
      end
      inherited RzToolButton87: TRzToolButton
        Left = 366
        ExplicitLeft = 366
      end
      inherited RzToolButton88: TRzToolButton
        Left = 436
        ExplicitLeft = 436
      end
      inherited RzToolButton90: TRzToolButton
        Left = 142
        ExplicitLeft = 142
      end
      inherited RzSpacer120: TRzSpacer
        Left = 204
        ExplicitLeft = 204
      end
      inherited RzSpacer1: TRzSpacer
        Left = 358
        ExplicitLeft = 358
      end
      object RzSpacer2: TRzSpacer [8]
        Left = 64
        Top = 2
      end
      object RzToolButton1: TRzToolButton [9]
        Left = 12
        Top = 2
        Width = 52
        SelectionColorStop = 16119543
        ImageIndex = 37
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #26032#22686
        OnClick = RzToolButton1Click
      end
      object RzSpacer3: TRzSpacer [10]
        Left = 428
        Top = 2
      end
      inherited RzSpacer10: TRzSpacer
        Left = 274
        ExplicitLeft = 274
      end
      inherited btnGridSet: TRzToolButton
        Left = 282
        Width = 76
        SelectionColorStop = 16119543
        ExplicitLeft = 282
        ExplicitWidth = 76
      end
      object RzSpacer4: TRzSpacer
        Left = 134
        Top = 2
      end
      object RzToolButton2: TRzToolButton
        Left = 72
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton2Click
      end
    end
    inherited Grid: TcxGrid
      Width = 850
      ExplicitLeft = 6
      ExplicitWidth = 850
      inherited tvView: TcxGridDBTableView
        inherited tvViewColumn26: TcxGridDBColumn
          Caption = #37319#36141#31867#21035
          Properties.Items.Strings = (
            #36134#30446#20837#24211)
        end
        inherited tvViewColumn16: TcxGridDBColumn
          Properties.Items.Strings = (
            #20837#24211)
        end
      end
    end
  end
  inherited ds: TDataSource
    Top = 216
  end
end
